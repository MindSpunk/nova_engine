/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::pretty_printing;
use crate::traits::Process;
use clap::{App, Arg, ArgMatches, SubCommand};
use console::style;
use indicatif::{ProgressBar, ProgressStyle};
use serde::{Deserialize, Serialize};
use std::fs;
use std::io::Read;
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};
use std::time::Duration;
use std::{io, process, thread};

#[derive(Copy, Clone, Debug, Serialize, Deserialize, Eq, PartialEq)]
pub enum ShaderType {
    Vertex,
    Fragment,
    Geometry,
    Compute,
    None,
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize, Eq, PartialEq)]
pub enum ShaderLanguage {
    HLSL,
    GLSL,
}

pub const COMPILE_SHADERS_NAME: &str = "compile-shaders";

pub const COMPILE_SHADERS_OUTPUT_NAME: &str = "out-dir";
pub const COMPILE_SHADERS_OUTPUT_SHORT: &str = "o";

pub const COMPILE_SHADERS_INCLUDE_NAME: &str = "include-dir";
pub const COMPILE_SHADERS_INCLUDE_SHORT: &str = "I";

pub const COMPILE_SHADERS_RESOURCES_NAME: &str = "resources";
pub const COMPILE_SHADERS_RESOURCES_SHORT: &str = "r";

pub struct ShaderCompilation {}

impl ShaderCompilation {
    pub const COMMAND_NAME: &'static str = COMPILE_SHADERS_NAME;
    pub fn new() -> Self {
        Self {}
    }
}

impl Process for ShaderCompilation {
    fn args<'a, 'b>(&mut self, app: App<'a, 'b>) -> App<'a, 'b> {
        let output_arg = Arg::with_name(COMPILE_SHADERS_OUTPUT_NAME)
            .long(COMPILE_SHADERS_OUTPUT_NAME)
            .short(COMPILE_SHADERS_OUTPUT_SHORT)
            .takes_value(true)
            .value_name("OUTPUT DIR")
            .help("Set the output directory of the \"compile all shaders\" command");

        let resource_arg = Arg::with_name(COMPILE_SHADERS_RESOURCES_NAME)
            .long(COMPILE_SHADERS_RESOURCES_NAME)
            .short(COMPILE_SHADERS_RESOURCES_SHORT)
            .takes_value(true)
            .value_name("RESOURCES DIR")
            .help("Set the resources directory");

        let include_arg = Arg::with_name(COMPILE_SHADERS_INCLUDE_NAME)
            .long(COMPILE_SHADERS_INCLUDE_NAME)
            .short(COMPILE_SHADERS_INCLUDE_SHORT)
            .takes_value(true)
            .multiple(true)
            .value_name("INCLUDES")
            .help("List of include directories to pass to dxc");

        let sub_command = SubCommand::with_name(COMPILE_SHADERS_NAME)
            .author("MindSpunk (Nathan Voglsam)")
            .about("Compile all the game's shaders to SPIR-V")
            .arg(output_arg)
            .arg(resource_arg)
            .arg(include_arg);

        app.subcommand(sub_command)
    }

    fn exec(&mut self, matches: &ArgMatches) {
        // Get resources dir
        let resources = {
            if let Some(val) = matches.value_of(COMPILE_SHADERS_RESOURCES_NAME) {
                val
            } else {
                "resources"
            }
        };

        // Get the output dir
        let out = {
            if let Some(val) = matches.value_of(COMPILE_SHADERS_OUTPUT_NAME) {
                val
            } else {
                "resources/spirv"
            }
        };

        let includes = {
            if let Some(val) = matches.values_of(COMPILE_SHADERS_INCLUDE_NAME) {
                val.collect()
            } else {
                Vec::new()
            }
        };

        let walker = ShaderWalker::builder()
            .resources(resources)
            .out(out)
            .dxc("dxc")
            .include(resources)
            .includes(&includes)
            .build();

        walker.walk();
    }
}

fn print_dir_err(path: &Path, desc: &str, extra: &str) {
    let head_err = style("ERROR: ").magenta();
    let head_desc = style("DESCRIPTION: ").magenta();
    let head_extra = style("EXTRA: ").magenta();

    println!(
        "{}Error occured in \"{}\"\n\
         {}{}\n\
         {}{}",
        &head_err,
        path.to_str().unwrap(),
        &head_desc,
        desc,
        &head_extra,
        extra
    );
}

pub struct ShaderWalkerBuilder<'a, 'b, 'c> {
    resources: &'a str,
    out: &'b str,
    dxc: &'c str,
    includes: Vec<PathBuf>,
}

impl<'a, 'b, 'c> ShaderWalkerBuilder<'a, 'b, 'c> {
    pub fn new() -> Self {
        ShaderWalkerBuilder {
            resources: "invalid",
            out: "invalid",
            dxc: "invalid",
            includes: Vec::new(),
        }
    }

    pub fn resources(mut self, resources: &'a str) -> Self {
        self.resources = resources;
        self
    }

    pub fn out(mut self, out: &'b str) -> Self {
        self.out = out;
        self
    }

    pub fn dxc(mut self, dxc: &'c str) -> Self {
        self.dxc = dxc;
        self
    }

    pub fn include(mut self, include: &str) -> Self {
        self.includes.push(Path::new(include).to_path_buf());
        self
    }

    pub fn includes(mut self, includes: &[&str]) -> Self {
        for inc in includes.iter() {
            self.includes.push(Path::new(inc).to_path_buf());
        }
        self
    }

    pub fn build(self) -> ShaderWalker {
        // Gets a completely normalized path
        let resources = match Path::new(self.resources).to_path_buf().canonicalize() {
            Ok(path) => path,
            Err(err) => {
                print_dir_err(
                    Path::new(self.resources),
                    "Failed to canonicalize",
                    &err.to_string(),
                );
                process::exit(1)
            }
        };

        // Gets a completely normalized path
        let out = match Path::new(self.out).to_path_buf().canonicalize() {
            Ok(path) => path,
            Err(err) => {
                if err.kind() == io::ErrorKind::NotFound {
                    if fs::create_dir(self.out).is_err() {
                        print_dir_err(&resources, "Failed to create output dir", "");
                        process::exit(7)
                    } else {
                        Path::new(self.out).to_path_buf().canonicalize().unwrap()
                    }
                } else {
                    print_dir_err(
                        Path::new(self.out),
                        "Failed to canonicalize",
                        &err.to_string(),
                    );
                    process::exit(2)
                }
            }
        };

        let dxc = if self.dxc != "dxc" {
            // Gets a completely normalized path
            match Path::new(self.dxc).to_path_buf().canonicalize() {
                Ok(path) => path,
                Err(err) => {
                    print_dir_err(
                        Path::new(self.dxc),
                        "Failed to canonicalize",
                        &err.to_string(),
                    );
                    process::exit(1)
                }
            }
        } else {
            Path::new(self.dxc).to_path_buf()
        };

        //let includes : Vec<PathBuf> = self.includes
        //    .iter()
        //    .map(|item| item.canonicalize().unwrap())
        //    .collect();

        ShaderWalker {
            resources,
            out,
            dxc,
            to_compile: Vec::new(),
            includes: self.includes,
        }
    }
}

impl<'a, 'b, 'c> Default for ShaderWalkerBuilder<'a, 'b, 'c> {
    fn default() -> Self {
        Self::new()
    }
}

pub struct ShaderWalker {
    resources: PathBuf,
    out: PathBuf,
    dxc: PathBuf,
    to_compile: Vec<(PathBuf, PathBuf)>,
    includes: Vec<PathBuf>,
}

impl ShaderWalker {
    pub fn builder<'a, 'b, 'c>() -> ShaderWalkerBuilder<'a, 'b, 'c> {
        ShaderWalkerBuilder::new()
    }

    pub fn walk(mut self) {
        pretty_printing::print_title("Compiling all Shaders");

        let head_resources = style("Resources  : ").cyan();
        println!("{}{}", &head_resources, &self.resources.to_str().unwrap());

        let head_output = style("Output     : ").cyan();
        println!("{}{}", &head_output, &self.out.to_str().unwrap());

        let head_dxc = style("DXC        : ").cyan();
        println!("{}{}", &head_dxc, &self.dxc.to_str().unwrap());

        println!();

        // Clear directory
        fs::remove_dir_all(&self.out).ok();
        thread::sleep(Duration::from_millis(200));
        fs::create_dir(&self.out).ok();
        thread::sleep(Duration::from_millis(200));

        let path = self.resources.clone();

        let style = ProgressStyle::default_spinner()
            .template("{spinner:.red} Walking : {msg}")
            .progress_chars("█▇▆▅▄▃▂▁  ");
        let walk_progress = ProgressBar::new(0);
        walk_progress.set_style(style);
        walk_progress.disable_steady_tick();

        self.handle_directory(&path, &walk_progress);

        walk_progress.finish_with_message("Done!");

        let style = ProgressStyle::default_bar()
            .template("File: {msg} : {wide_bar:.cyan}")
            .progress_chars("█▇▆▅▄▃▂▁  ");
        let compile_progress = ProgressBar::new(self.to_compile.len() as u64);
        compile_progress.set_style(style);

        for (i, (file, out)) in self.to_compile.iter().enumerate() {
            self.compile_shader(file, out, &compile_progress, i);
        }

        compile_progress.finish_with_message("Done!");

        println!("Successfully compiled {} shaders!", self.to_compile.len());
    }

    fn handle_file(&mut self, path: &Path) {
        if let Some(ext) = path.extension() {
            if ext.to_str().unwrap() == "hlsl" {
                let mut out_file = self.out.clone();
                let diff =
                    pathdiff::diff_paths(path, &self.resources).expect("Failed to diff paths");

                out_file.push(diff);

                let out_file = out_file.with_extension("spv");

                self.to_compile.push((path.to_path_buf(), out_file));
            }
        }
    }

    fn compile_shader(&self, path: &Path, out: &Path, progress: &ProgressBar, i: usize) {
        progress.inc(1);
        progress.set_message(&format!("{}/{}", i, self.to_compile.len()));

        let p = path.to_str().unwrap();

        let shader_type = if p.ends_with(".frag.hlsl") {
            ShaderType::Fragment
        } else if p.ends_with(".vert.hlsl") {
            ShaderType::Vertex
        } else if p.ends_with(".geom.hlsl") {
            ShaderType::Geometry
        } else if p.ends_with(".comp.hlsl") {
            ShaderType::Compute
        } else {
            ShaderType::None
        };

        if shader_type == ShaderType::None {
            return;
        }

        let mut command = Command::new("dxc");
        command.arg("-spirv");

        match shader_type {
            ShaderType::Vertex => {
                command.arg("/T");
                command.arg("vs_6_0");
            }
            ShaderType::Fragment => {
                command.arg("/T");
                command.arg("ps_6_0");
            }
            ShaderType::Geometry => {
                command.arg("/T");
                command.arg("gs_6_0");
            }
            ShaderType::Compute => {
                command.arg("/T");
                command.arg("cs_6_0");
            }
            _ => {}
        }

        for include in &self.includes {
            command.arg("-I");
            command.arg(include);
        }

        command.arg("-Fo");
        command.arg(out);

        command.arg(path);

        command.stdout(Stdio::piped());
        command.stderr(Stdio::piped());

        let command = command.spawn();

        match command {
            Ok(mut child) => match child.wait() {
                Ok(status) => {
                    if !status.success() {
                        let mut stdout = child.stdout.expect("Failed to acquire stdout");
                        let mut stdout_str = String::new();
                        stdout
                            .read_to_string(&mut stdout_str)
                            .expect("Failed to read stdout to string");

                        let mut stderr = child.stderr.expect("Failed to acquire stderr");
                        let mut stderr_str = String::new();
                        stderr
                            .read_to_string(&mut stderr_str)
                            .expect("Failed to read stderr to string");

                        let extra = format!(
                            "\n\
                             --\n\
                             status: {}\n\
                             --\n\
                             stdout:\n\
                             {}\n\
                             --\
                             stderr:\n\
                             {}\
                             --",
                            &status.to_string(),
                            &stdout_str,
                            &stderr_str
                        );

                        print_dir_err(path, "Failed to compile shader", &extra);
                        process::exit(9);
                    }
                }
                Err(err) => {
                    print_dir_err(path, "Failed to wait on child process", &err.to_string());
                    process::exit(10);
                }
            },
            Err(err) => {
                print_dir_err(path, "Failed to invoke dxc", &err.to_string());
                process::exit(8);
            }
        }
    }

    fn handle_directory(&mut self, path: &Path, progress: &ProgressBar) {
        progress.set_message(path.to_str().expect("Failed to build string from path"));
        progress.tick();

        let path_iter = match path.read_dir() {
            Ok(iter) => iter,
            Err(err) => {
                print_dir_err(path, "Is not a directory", &err.to_string());
                process::exit(3)
            }
        };

        for child in path_iter {
            let entry = match child {
                Ok(entry) => entry,
                Err(err) => {
                    print_dir_err(path, "Error processing child element", &err.to_string());
                    process::exit(4)
                }
            };

            let file_type = entry.file_type().unwrap();

            if file_type.is_dir() {
                if self.out != entry.path() {
                    let mut rel_path = self.out.clone();
                    let diffed = match pathdiff::diff_paths(&entry.path(), &self.resources) {
                        None => {
                            print_dir_err(&entry.path(), "Failed to diff path", "");
                            process::exit(5)
                        }
                        Some(rel_path) => rel_path,
                    };

                    rel_path.push(&diffed);

                    match fs::create_dir(&rel_path) {
                        Ok(_) => {}
                        Err(err) => {
                            let desc =
                                format!("Failed to create \"{}\".", &rel_path.to_str().unwrap());
                            print_dir_err(path, &desc, &err.to_string())
                        }
                    }

                    self.handle_directory(&entry.path(), progress);
                }
            } else if file_type.is_file() {
                self.handle_file(&entry.path());
            } else {
                print_dir_err(
                    path,
                    "Currently don't support symlinks",
                    entry.path().to_str().unwrap(),
                );
                process::exit(6)
            }
        }
    }
}
