/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#![allow(clippy::suspicious_else_formatting)]
#![deny(bare_trait_objects)]

//!
//! # nova-tools
//!
//! This crate is the nova "uber tool" that serves as the one stop executable for all of the tools
//! and utilities provided by NovaEngine. Examples of what this tool include
//!
//! - Compiling shaders
//! - Generating android projects
//! - Exporting a project
//! - Packaging assets into an nfs file
//!

// Includes a common set of checks to ensure compiling for the right platform
include!("../../platform_check.rs");

extern crate clap;
extern crate console;
extern crate dunce;
extern crate handlebars;
extern crate indicatif;
extern crate pathdiff;
extern crate serde;
extern crate serde_json;

extern crate nova_archive as archive;
extern crate nova_target_crate as target;

pub mod android_gen;
pub mod export;
pub mod make_bundle;
pub mod pretty_printing;
pub mod shaders;
pub mod traits;

use crate::android_gen::AndroidProjectGenerator;
use crate::export::ExportGame;
use crate::make_bundle::MakeBundle;
use crate::shaders::ShaderCompilation;
use crate::traits::Process;
use clap::App;

fn main() {
    let mut export_game = ExportGame::new();
    let mut android_generator = AndroidProjectGenerator::new();
    let mut shader_compilation = ShaderCompilation::new();
    let mut bundle_maker = MakeBundle::new();

    let app = App::new("nova-tools")
        .version("0.4.0")
        .about("The primary \"meta tool\" that manages all nova_engine related tasks (shader compilation, asset importing, project export, etc")
        .author("MindSpunk (Nathan Voglsam)");

    let app = export_game.args(app);
    let app = android_generator.args(app);
    let app = shader_compilation.args(app);
    let app = bundle_maker.args(app);

    let matches = app.get_matches();

    match matches.subcommand() {
        (ExportGame::COMMAND_NAME, Some(sub_matches)) => {
            export_game.exec(sub_matches);
        }
        (ShaderCompilation::COMMAND_NAME, Some(sub_matches)) => {
            shader_compilation.exec(sub_matches);
        }
        (AndroidProjectGenerator::COMMAND_NAME, Some(sub_matches)) => {
            android_generator.exec(sub_matches);
        }
        (MakeBundle::COMMAND_NAME, Some(sub_matches)) => {
            bundle_maker.exec(sub_matches);
        }
        _ => {}
    }
}
