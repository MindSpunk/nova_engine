/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::traits::Process;
use clap::{App, Arg, ArgMatches, SubCommand};
use dunce;
use std::path::{Path, PathBuf};
use std::process::Stdio;
use std::thread;
use std::time::Duration;
use std::{env, fs};
use target::build::target_architecture;
use target::build::target_platform;
use target::BuildType;
use target::Platform;

pub const EXPORT_NAME: &str = "export-project";
pub const EXPORT_SKIP_BUILD_NAME: &str = "skip-build";
pub const EXPORT_SKIP_BUILD_SHORT: &str = "s";
pub const EXPORT_OUTPUT_NAME: &str = "out-dir";
pub const EXPORT_OUTPUT_NAME_SHORT: &str = "o";
pub const EXPORT_TARGET_TRIPLE_NAME: &str = "target";
pub const EXPORT_TARGET_TRIPLE_NAME_SHORT: &str = "t";
pub const EXPORT_DEBUG_BUILD_NAME: &str = "debug";
pub const EXPORT_DEBUG_BUILD_NAME_SHORT: &str = "d";

pub struct ExportGame {}

impl ExportGame {
    pub const COMMAND_NAME: &'static str = EXPORT_NAME;
    pub fn new() -> Self {
        Self {}
    }
}

impl Process for ExportGame {
    fn args<'a, 'b>(&mut self, app: App<'a, 'b>) -> App<'a, 'b> {
        let skip_build_arg = Arg::with_name(EXPORT_SKIP_BUILD_NAME)
            .long(EXPORT_SKIP_BUILD_NAME)
            .short(EXPORT_SKIP_BUILD_SHORT)
            .help("Will skip building the game through cargo before exporting");

        let output_arg = Arg::with_name(EXPORT_OUTPUT_NAME)
            .long(EXPORT_OUTPUT_NAME)
            .short(EXPORT_OUTPUT_NAME_SHORT)
            .help(
                "Sets the output directory where the results of the export command will be placed",
            )
            .value_name("OUTPUT")
            .default_value("EXPORT")
            .takes_value(true);

        let default_triple =
            target::recreate_triple(target_platform(), target_architecture()).unwrap();
        let target_arg = Arg::with_name(EXPORT_TARGET_TRIPLE_NAME)
            .long(EXPORT_TARGET_TRIPLE_NAME)
            .short(EXPORT_TARGET_TRIPLE_NAME_SHORT)
            .help("Provide a rust target triple to choose which platform to export for")
            .value_name("TRIPLE")
            .default_value(default_triple)
            .takes_value(true);

        let build_type_arg = Arg::with_name(EXPORT_DEBUG_BUILD_NAME)
            .long(EXPORT_DEBUG_BUILD_NAME)
            .short(EXPORT_DEBUG_BUILD_NAME_SHORT)
            .help("Specify a debug build export");

        let sub_command = SubCommand::with_name(EXPORT_NAME)
            .about("Exports the binary and it's dependencies for distribution into the output directory")
            .arg(skip_build_arg)
            .arg(output_arg)
            .arg(target_arg)
            .arg(build_type_arg);

        app.subcommand(sub_command)
    }

    fn exec(&mut self, matches: &ArgMatches) {
        use crate::pretty_printing::print_dir_create;
        use crate::pretty_printing::print_file_copy;
        use crate::pretty_printing::print_title;
        use console::style;

        // -----------------------------------------------------------------------------------------
        //  Get platform info
        // -----------------------------------------------------------------------------------------
        let platform;
        let architecture;
        let build_type;

        if let Some(triple) = matches.value_of(EXPORT_TARGET_TRIPLE_NAME) {
            platform = target::get_platform_from(triple);
            architecture = target::get_architecture_from(triple);
        } else {
            platform = target_platform();
            architecture = target_architecture();
        };

        if matches.is_present(EXPORT_DEBUG_BUILD_NAME) {
            build_type = BuildType::Debug
        } else {
            build_type = BuildType::Release
        }

        // -----------------------------------------------------------------------------------------
        //  Print sub-command
        // -----------------------------------------------------------------------------------------
        print_title("Exporting Project");

        // -----------------------------------------------------------------------------------------
        //  Print current platform
        // -----------------------------------------------------------------------------------------
        let platform_header = style("Platform").bold().blue();
        let platform_text = platform.pretty_name();
        let platform_text = style(platform_text).italic();
        println!("{}: {}", platform_header, platform_text);

        // -----------------------------------------------------------------------------------------
        //  Print build type
        // -----------------------------------------------------------------------------------------
        let build_header = style("Build Type").bold().blue();
        let build_text = build_type.pretty_name();
        let build_text = style(build_text).italic();
        println!("{}: {}", build_header, build_text);

        // -----------------------------------------------------------------------------------------
        //  Print architecture
        // -----------------------------------------------------------------------------------------
        let arch_header = style("Arch").bold().blue();
        let arch_text = architecture.name();
        let arch_text = style(arch_text).italic();
        println!("{}: {}", arch_header, arch_text);

        // -----------------------------------------------------------------------------------------
        //  Get export output directory
        // -----------------------------------------------------------------------------------------
        let mut output = {
            if let Some(val) = matches.value_of(EXPORT_OUTPUT_NAME) {
                Path::new(val).to_path_buf()
            } else {
                Path::new("EXPORT").to_path_buf()
            }
        };

        // -----------------------------------------------------------------------------------------
        //  Print output directory
        // -----------------------------------------------------------------------------------------
        let output_dir_header = style("Output Directory").bold().blue();
        let output_dir_text = style(output.to_path_buf()).italic();
        println!("{}: {:?}", output_dir_header, output_dir_text);

        // -----------------------------------------------------------------------------------------
        //  Build with cargo
        // -----------------------------------------------------------------------------------------
        if !matches.is_present(EXPORT_SKIP_BUILD_NAME) {
            let text = style("Building Project").bold().green();
            println!("{}\n", text);

            let mut command = std::process::Command::new("cargo");

            let mut toolchain_arg = String::new();
            toolchain_arg.push_str("+stable-");
            toolchain_arg.push_str(architecture.name());

            match platform {
                Platform::WindowsGNU => {
                    toolchain_arg.push_str("-pc-windows-gnu");
                }
                Platform::WindowsMSVC => {
                    toolchain_arg.push_str("-pc-windows-msvc");
                }
                Platform::Linux => {
                    toolchain_arg.push_str("-unknown-linux-gnu");
                }
                Platform::Android => {
                    toolchain_arg.push_str("-linux-android");
                }
            }

            command.arg(&toolchain_arg);

            command.arg("build");

            match build_type {
                BuildType::Release => {
                    command.arg("--release");
                }
                BuildType::Debug => {}
            }

            command.stdout(Stdio::inherit());
            command.stderr(Stdio::inherit());

            if platform.is_android() {
                command.arg("-p");
                command.arg("nova-test-android"); // TODO: This needs to be configurable
            } else {
                command.arg("-p");
                command.arg("nova-test-desktop"); // TODO: This needs to be configurable
            }

            let mut child = command.spawn().expect("Failed to spawn cargo process");

            child.wait().expect("Failed to build project");
        }

        // -----------------------------------------------------------------------------------------
        //  Create EXPORT directory if it doesn't exist
        // -----------------------------------------------------------------------------------------
        if !output.exists() {
            fs::create_dir(&output).expect("Failed to create EXPORT directory");
            print_dir_create(&output);
        }

        // -----------------------------------------------------------------------------------------
        //  Create platform directory if it doesn't exist
        // -----------------------------------------------------------------------------------------
        output.push(platform.name());
        if !output.exists() {
            fs::create_dir(&output).expect("Failed to create platform directory");
            print_dir_create(&output);
        }

        // -----------------------------------------------------------------------------------------
        //  Wipe the leaf directory that will contain this build's export
        // -----------------------------------------------------------------------------------------
        output.push(build_type.name());
        if output.exists() {
            fs::remove_dir_all(&output).expect("Failed to erase build type directory");
        }
        thread::sleep(Duration::from_millis(50));
        fs::create_dir(&output).expect("Failed to create build type directory");
        print_dir_create(&output);

        // -----------------------------------------------------------------------------------------
        //  Copy GNU DLLs to export directory
        // -----------------------------------------------------------------------------------------
        if platform.is_windows() && platform.is_gnu() {
            let name = Path::new("libwinpthread-1.dll");
            find_and_copy_dll(name, &output);
            print_file_copy(name);

            let name = Path::new("libgcc_s_seh-1.dll");
            find_and_copy_dll(name, &output);
            print_file_copy(name);

            let name = Path::new("libstdc++-6.dll");
            find_and_copy_dll(name, &output);
            print_file_copy(name);
        }

        // -----------------------------------------------------------------------------------------
        //  Copy the executable and library
        // -----------------------------------------------------------------------------------------
        let exe_output = output.join(get_exe_name(platform));

        let exe_source = Path::new("target").join(build_type.name());
        let exe_source = exe_source.join(get_exe_name(platform));
        let exe_source = dunce::canonicalize(exe_source).unwrap();

        if fs::copy(&exe_source, &exe_output).is_err() {
            panic!("Failed to copy {}", get_exe_name(platform));
        }
        print_file_copy(&exe_source);

        // -----------------------------------------------------------------------------------------
        //  Copy the SDL2 dynamic library
        // -----------------------------------------------------------------------------------------
        let sdl_name = {
            if platform.is_msvc() {
                "SDL2.dll"
            } else if platform.is_windows() {
                "libSDL2.dll"
            } else {
                "libSDL2.so"
            }
        };
        if platform.is_windows() {
            let sdl2_output = output.join(sdl_name);

            let sdl2_source = Path::new(get_artifacts_dir(platform)).join(sdl_name);
            let sdl2_source = dunce::canonicalize(sdl2_source).unwrap();

            fs::copy(&sdl2_source, &sdl2_output).expect("Failed to copy SDL2 lib");
            print_file_copy(&sdl2_source);
        }

        // -----------------------------------------------------------------------------------------
        //  Copy the VMA dynamic library
        // -----------------------------------------------------------------------------------------
        let vma_name = {
            if platform.is_msvc() {
                "vma.dll"
            } else if platform.is_windows() {
                "libvma.dll"
            } else {
                "libvma.so"
            }
        };
        let vma_output = output.join(vma_name);

        let vma_source = Path::new(get_artifacts_dir(platform)).join(vma_name);
        let vma_source = dunce::canonicalize(vma_source).unwrap();

        fs::copy(&vma_source, &vma_output).expect("Failed to copy VMA lib");
        print_file_copy(&vma_source);

        // -----------------------------------------------------------------------------------------
        //  Copy the FMOD shared libraries
        // -----------------------------------------------------------------------------------------
        let (studio_name, core_name) = {
            if platform.is_windows() {
                ("fmodstudio.dll", "fmod.dll")
            } else {
                ("libfmodstudio.so", "libfmod.so")
            }
        };

        let studio_output = output.join(studio_name);
        let core_output = output.join(core_name);

        let studio_source = Path::new(get_artifacts_dir(platform)).join(studio_name);
        let studio_source = dunce::canonicalize(studio_source).unwrap();
        let core_source = Path::new(get_artifacts_dir(platform)).join(core_name);
        let core_source = dunce::canonicalize(core_source).unwrap();

        fs::copy(&studio_source, &studio_output).expect("Failed to copy FMOD Studio lib");
        print_file_copy(&studio_source);

        fs::copy(&core_source, &core_output).expect("Failed to copy FMOD lib");
        print_file_copy(&core_source);
    }
}

pub fn find_and_copy_dll(name: &Path, out_dir: &Path) {
    let mut dllpath: Option<PathBuf> = None;

    let path = env::var_os("PATH").expect("Failed to get path env variable");

    for p in env::split_paths(&path) {
        let mut maybe_lib = p.clone();
        maybe_lib.push(name);

        if maybe_lib.exists() && maybe_lib.is_file() {
            dllpath = Some(maybe_lib);
            break;
        }
    }

    let dllpath = match dllpath {
        None => panic!("Failed to find {:?}", name),
        Some(dllpath) => dllpath,
    };

    let mut output_name = out_dir.to_path_buf();
    output_name.push(name);

    let result = fs::copy(&dllpath, &output_name);
    if result.is_err() {
        panic!("Failed to copy {:?} to {:?}", &dllpath, &output_name);
    }
}

fn get_exe_name(platform: Platform) -> &'static str {
    match platform {
        Platform::WindowsGNU | Platform::WindowsMSVC => "nova-test-desktop.exe",
        Platform::Linux => "nova-test-desktop",
        Platform::Android => panic!("Android doesn't produce an executable"),
    }
}

fn get_artifacts_dir(platform: Platform) -> &'static str {
    match platform {
        Platform::WindowsMSVC | Platform::WindowsGNU | Platform::Linux => {
            match target::build::target_build_type() {
                BuildType::Release => "target/release/artifacts",
                BuildType::Debug => "target/debug/artifacts",
            }
        }
        Platform::Android => {
            "" // TODO: SDL2
        }
    }
}
