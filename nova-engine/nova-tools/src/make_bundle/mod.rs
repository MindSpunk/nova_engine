/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::traits::Process;
use clap::{App, Arg, ArgMatches, SubCommand};
use std::path::Path;

const MAKE_BUNDLE_NAME: &'static str = "make-bundle";
const MAKE_BUNDLE_INPUT: &'static str = "input-dir";
const MAKE_BUNDLE_INPUT_SHORT: &'static str = "i";
const MAKE_BUNDLE_OUTPUT: &'static str = "output-file";
const MAKE_BUNDLE_OUTPUT_SHORT: &'static str = "o";

pub struct MakeBundle {}

impl MakeBundle {
    pub const COMMAND_NAME: &'static str = MAKE_BUNDLE_NAME;
    pub fn new() -> Self {
        Self {}
    }
}

impl Process for MakeBundle {
    fn args<'a, 'b>(&mut self, app: App<'a, 'b>) -> App<'a, 'b> {
        let input_arg = Arg::with_name(MAKE_BUNDLE_INPUT)
            .long(MAKE_BUNDLE_INPUT)
            .short(MAKE_BUNDLE_INPUT_SHORT)
            .takes_value(true)
            .value_name("INPUT")
            .required(true)
            .help("The input directory to bundle");

        let output_arg = Arg::with_name(MAKE_BUNDLE_OUTPUT)
            .long(MAKE_BUNDLE_OUTPUT)
            .short(MAKE_BUNDLE_OUTPUT_SHORT)
            .value_name("OUTPUT")
            .takes_value(true)
            .required(true)
            .help("The file to output");

        let sub_command = SubCommand::with_name(MAKE_BUNDLE_NAME)
            .about("Bundles the given directory into a nova asset bundle")
            .arg(input_arg)
            .arg(output_arg);

        app.subcommand(sub_command)
    }

    fn exec(&mut self, matches: &ArgMatches) {
        let input = matches.value_of(MAKE_BUNDLE_INPUT).unwrap();
        let output = matches.value_of(MAKE_BUNDLE_OUTPUT).unwrap();

        let input = Path::new(input);
        let output = Path::new(output);

        let output = std::fs::OpenOptions::new()
            .read(true)
            .write(true)
            .create_new(true)
            .open(output)
            .unwrap();

        archive::ArchiveBuilder::from_directory(input)
            .unwrap()
            .build(output)
            .unwrap();
    }
}
