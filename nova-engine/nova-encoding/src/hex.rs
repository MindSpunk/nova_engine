/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use std::mem::size_of;

//==================================================================================================
///
/// Decodes an integer from the given string. May return None if the string contains invalid hex
/// characters.
///
pub fn decode(text: &str) -> Option<usize> {
    let mut num: usize = 0;

    for c in text.chars() {
        if !c.is_ascii() {
            return None;
        }
        let cc = c.to_ascii_uppercase();
        let cc = cc as usize;

        num <<= 4;

        if cc >= 48 && cc <= 57 {
            num += cc - 48;
        } else if cc >= 65 && cc <= 70 {
            num += cc - 55;
        } else {
            return None;
        }
    }
    Some(num)
}

//==================================================================================================
///
/// Encodes the integer into a string and returns it without any leading zeroes.
///
pub fn encode(num: usize) -> String {
    const HEX_CHARS: &str = "0123456789ABCDEF";
    const BIT_COUNT: usize = size_of::<usize>() * 8;
    const GROUP_COUNT: usize = BIT_COUNT / 4;
    const LEFTOVER_BITS: usize = BIT_COUNT - (GROUP_COUNT * 4);
    const HEX_BITS: usize = BIT_COUNT - LEFTOVER_BITS;

    let mut trailing_zeroes = true;

    let mut string = String::new();

    let mut i: isize = (HEX_BITS - 4) as isize;
    loop {
        if i < 0 {
            break;
        }

        let num_shift: usize = (num >> i) & 0xF;

        if num_shift != 0 && trailing_zeroes {
            trailing_zeroes = false;
        }
        if !trailing_zeroes {
            let character = HEX_CHARS.as_bytes()[num_shift];
            let character = character as char;
            string.push(character);
        }
        i -= 4;
    }
    string
}

//==================================================================================================
///
/// Will encode the integer given into a string and return it. This version of the function will
/// return a string with leading zeroes.
///
pub fn encode_leading(num: usize) -> String {
    const HEX_CHARS: &str = "0123456789ABCDEF";
    const BIT_COUNT: usize = size_of::<usize>() * 8;
    const GROUP_COUNT: usize = BIT_COUNT / 4;
    const LEFTOVER_BITS: usize = BIT_COUNT - (GROUP_COUNT * 4);
    const HEX_BITS: usize = BIT_COUNT - LEFTOVER_BITS;

    let mut string = String::new();

    let mut i: isize = (HEX_BITS - 4) as isize;
    loop {
        if i < 0 {
            break;
        }

        let num_shift: usize = (num >> i) & 0xF;

        let character = HEX_CHARS.as_bytes()[num_shift];
        let character = character as char;
        string.push(character);

        i -= 4;
    }
    string
}

#[cfg(test)]
pub mod tests {

    use crate::hex;

    #[test]
    pub fn hex_round_trip_1() {
        let initial_num: usize = 56;

        let string = hex::encode(initial_num);
        let num = hex::decode(&string);

        assert!(num.is_some());

        let num = num.unwrap();

        assert_eq!(num, initial_num);
    }

    #[test]
    pub fn hex_round_trip_2() {
        let initial_string = "FADFAD";

        let num = hex::decode(initial_string);

        assert!(num.is_some());

        let string = hex::encode(num.unwrap());

        assert_eq!(string, initial_string);
    }

    #[test]
    pub fn hex_leading_round_trip_1() {
        let initial_num: usize = 56;

        let string = hex::encode_leading(initial_num);
        let num = hex::decode(&string);

        assert!(num.is_some());

        let num = num.unwrap();

        assert_eq!(num, initial_num);
    }

    #[test]
    pub fn hex_leading_round_trip_2() {
        let initial_string = "FADFAD";

        let num = hex::decode(initial_string);

        assert!(num.is_some());

        let string = hex::encode_leading(num.unwrap());

        assert_eq!(string, "0000000000FADFAD");
    }

    #[test]
    pub fn hex_invalid_characters_produces_none() {
        let initial_string = "ASDASDFADZASD";

        let num = hex::decode(initial_string);

        assert!(num.is_none());
    }
}
