/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

//==================================================================================================
///
/// Encode the bytes given into a base64 string
///
pub fn encode(data: &[u8]) -> String {
    const ENCODING_TABLE: [char; 64] = [
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
        'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
        'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1',
        '2', '3', '4', '5', '6', '7', '8', '9', '+', '/',
    ];

    let len = data.len();
    let mut out = String::with_capacity((len + 2) / 3);
    let mut i = 0;

    for _ in (0..len - 2).step_by(3) {
        let idx = ((data[i] >> 2) & 0x3F) as usize;
        out.push(ENCODING_TABLE[idx]);

        let idx = (((data[i] & 0x3) << 4) | ((data[i + 1] & 0xF0) >> 4)) as usize;
        out.push(ENCODING_TABLE[idx]);

        let idx = (((data[i + 1] & 0xF) << 2) | ((data[i + 2] & 0xC0) >> 6)) as usize;
        out.push(ENCODING_TABLE[idx]);

        let idx = (data[i + 2] & 0x3F) as usize;
        out.push(ENCODING_TABLE[idx]);

        i += 3;
    }

    if i < len {
        let idx = ((data[i] >> 2) & 0x3F) as usize;
        out.push(ENCODING_TABLE[idx]);

        if i == len - 1 {
            let idx = ((data[i] & 0x3) << 4) as usize;
            out.push(ENCODING_TABLE[idx]);

            out.push('=');
        } else {
            let idx = (((data[i] & 0x3) << 4) | ((data[i + 1] & 0xF0) >> 4)) as usize;
            out.push(ENCODING_TABLE[idx]);

            let idx = ((data[i + 1] & 0xF) << 2) as usize;
            out.push(ENCODING_TABLE[idx]);
        }
        out.push('=');
    }

    out
}

//==================================================================================================
///
/// Decode the string into a Vec<u8> and return it
///
/// # Errors
///
/// Will error and return None if the input string's length is not a multiple of 4. All base64
/// strings have a length that is a multiple of 4 so we should error on invalid base64 strings
/// whenever possible.
///
#[allow(clippy::char_lit_as_u8)]
pub fn decode(input: &str) -> Option<Vec<u8>> {
    const DECODING_TABLE: [u8; 256] = [
        64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
        64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 62, 64, 64,
        64, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 64, 64, 64, 64, 64, 64, 64, 0, 1, 2, 3, 4,
        5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 64, 64, 64,
        64, 64, 64, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45,
        46, 47, 48, 49, 50, 51, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
        64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
        64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
        64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
        64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
        64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
        64,
    ];

    if !input.is_ascii() {
        return None;
    }

    // If string length is a multiple of 4
    let in_len = input.len();
    if in_len & 0b11 != 0 {
        return None;
    }

    let bytes = input.as_bytes();
    let mut out_len = input.len() / 4 * 3;
    let mut out = Vec::<u8>::new();

    if bytes[in_len - 1] == '=' as u8 {
        out_len -= 1;
    }
    if bytes[in_len - 2] == '=' as u8 {
        out_len -= 1;
    }

    out.resize(out_len, 0);

    let mut i = 0;
    let mut j = 0;

    loop {
        if i >= in_len {
            break;
        }

        let cond: bool = bytes[i] as char == '=';
        let high = u32::from(if cond {
            0
        } else {
            DECODING_TABLE[bytes[i] as usize]
        });
        i += 1;

        let cond: bool = bytes[i] as char == '=';
        let midu = u32::from(if cond {
            0
        } else {
            DECODING_TABLE[bytes[i] as usize]
        });
        i += 1;

        let cond: bool = bytes[i] as char == '=';
        let midb = u32::from(if cond {
            0
        } else {
            DECODING_TABLE[bytes[i] as usize]
        });
        i += 1;

        let cond: bool = bytes[i] as char == '=';
        let botm = u32::from(if cond {
            0
        } else {
            DECODING_TABLE[bytes[i] as usize]
        });
        i += 1;

        let triple = (high << (3 * 6)) + (midu << (2 * 6)) + (midb << 6) + botm;

        if j < out_len {
            out[j] = ((triple >> (2 * 8)) & 0xFF) as u8;
        }
        j += 1;

        if j < out_len {
            out[j] = ((triple >> 8) & 0xFF) as u8;
        }
        j += 1;

        if j < out_len {
            out[j] = (triple & 0xFF) as u8;
        }
        j += 1;
    }

    Some(out)
}

#[cfg(test)]
pub mod tests {

    use crate::base64;

    #[test]
    pub fn base64_round_trip_1() {
        let initial_data: [u8; 11] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

        let string = base64::encode(&initial_data);
        let data = base64::decode(&string);

        assert!(data.is_some());

        let data = data.unwrap();

        assert_eq!(data, initial_data);
    }

    #[test]
    pub fn base64_round_trip_2() {
        let initial_string = "1234ASDF";

        let data = base64::decode(initial_string);

        assert!(data.is_some());

        let string = base64::encode(&data.unwrap());

        assert_eq!(string, initial_string);
    }

    #[test]
    pub fn base64_invalid_length_produces_none() {
        let initial_string = "1234ASDFA";

        let data = base64::decode(initial_string);

        assert!(data.is_none());
    }
}
