/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use std::mem;
use std::num::NonZeroUsize;
use std::vec::Vec;

//==================================================================================================
//      CONSTANTS
//==================================================================================================

pub const LEB128_I8_MAX_BYTES: usize = 2;
pub const LEB128_I16_MAX_BYTES: usize = 3;
pub const LEB128_I32_MAX_BYTES: usize = 5;
pub const LEB128_I64_MAX_BYTES: usize = 10;

//==================================================================================================
//      FUNCTIONS
//==================================================================================================

//==================================================================================================
///
/// Encodes an unsigned integer, num, into an array and returns it.
///
pub fn unsigned_encode(num: u64) -> Vec<u8> {
    let mut buffer: Vec<u8> = Vec::new();
    buffer.resize(10, 0);

    let result = unsigned_encode_inplace(num, buffer.as_mut_slice());

    match result {
        Some(size) => {
            buffer.resize(size.get(), 0);
            buffer
        }
        None => {
            panic!("The maximum bytes a 64 bit can use is 10 so this should be impossible");
        }
    }
}

//==================================================================================================
///
/// Encodes an unsigned integer, num, into the buffer slice and returns a result of Ok with number
/// of bytes written to the slice or Err if there is not enough space in the slice.
///
pub fn unsigned_encode_inplace(num: u64, buffer: &mut [u8]) -> Option<NonZeroUsize> {
    let mut n: u64 = num;
    let mut shifted: usize = 0;

    loop {
        buffer[shifted] = n as u8 & 0b0111_1111;
        n >>= 7;
        shifted += 1;

        if n != 0 {
            buffer[shifted - 1] |= 0b1000_0000;

            if shifted >= buffer.len() {
                return None;
            }
        } else {
            break;
        }
    }
    Some(unsafe { NonZeroUsize::new_unchecked(shifted) })
}

//==================================================================================================
///
/// Decodes an unsigned leb128 integer from the buffer and returns it with the number of bytes read
/// on success, otherwise returns Err on failure (we reach the end of the buffer before the end of
/// the leb128 int)
///
pub fn unsigned_decode(buffer: &[u8]) -> Option<(NonZeroUsize, u64)> {
    let mut num: u64 = 0;
    let mut shifted: usize = 0;

    while shifted < buffer.len() {
        let byte: u64 = buffer[shifted].into();

        let v: u64 = (byte & 0b0111_1111) << (shifted * 7);

        num |= v;

        shifted += 1;

        if byte & 0b1000_0000 == 0 {
            break;
        } else if shifted >= buffer.len() {
            return None;
        }
    }

    Some((unsafe { NonZeroUsize::new_unchecked(shifted) }, num))
}

//==================================================================================================
///
/// Encodes a signed integer, num, into an array and returns it.
///
pub fn signed_encode(num: i64) -> Vec<u8> {
    let mut buffer: Vec<u8> = Vec::new();
    buffer.resize(10, 0);

    let result = signed_encode_inplace(num, buffer.as_mut_slice());

    match result {
        Some(size) => {
            buffer.resize(size.get(), 0);
            buffer
        }
        None => {
            panic!("The maximum bytes a 64 bit can use is 10 so this should be impossible");
        }
    }
}

//==================================================================================================
///
/// Encodes a signed integer, num, into the buffer slice and returns a result of Ok with number of
/// bytes written to the slice or Err if there is not enough space in the slice.
///
pub fn signed_encode_inplace(num: i64, buffer: &mut [u8]) -> Option<NonZeroUsize> {
    let mut n: i64 = num;
    let mut more: bool = true;
    let mut bytes_used: usize = 0;

    while more {
        if bytes_used >= buffer.len() {
            return None;
        }

        let mut byte: u8 = (n & 0b0111_1111) as u8;
        n >>= 7;

        if (n == 0 && (byte & 0b1000_0000) == 0) || (n == -1 && (byte & 0b0100_0000) != 0) {
            more = false;
        } else {
            byte |= 0b1000_0000;
        }

        buffer[bytes_used] = byte as u8;
        bytes_used += 1;
    }

    Some(unsafe { NonZeroUsize::new_unchecked(bytes_used) })
}

//==================================================================================================
///
///
///
pub fn signed_decode(buffer: &[u8]) -> Option<(NonZeroUsize, i64)> {
    let mut num: i64 = 0;
    let mut shifted: usize = 0;
    let mut byte: u8;

    loop {
        if shifted >= buffer.len() {
            return None;
        }

        byte = buffer[shifted];

        let byte_wide: i64 = i64::from(byte & 0b0111_1111);

        num |= byte_wide << (shifted * 7);
        shifted += 1;

        if (byte & 0b1000_0000) != 0 {
            continue;
        } else {
            break;
        }
    }

    if (shifted * 7) < (mem::size_of::<u64>() * 8) && (byte & 0b0100_0000) != 0 {
        let mut or_val: i64 = !0;
        or_val <<= shifted * 7;
        num |= or_val;
    }

    Some((unsafe { NonZeroUsize::new_unchecked(shifted) }, num))
}

//==================================================================================================
//      TRAITS
//==================================================================================================

//==================================================================================================
///
/// Marks a type that can be converted to the leb128 integer encoding
///
pub trait ToLeb128 {
    fn to_leb128(&self) -> Vec<u8>;
    fn to_leb128_inplace(&self, buffer: &mut [u8]) -> Option<NonZeroUsize>;
}

//==================================================================================================
///
/// Marks a type that can be created from a leb128 encoded integer
///
pub trait FromLeb128 {
    type Output;

    fn from_leb128(buffer: &[u8]) -> Self::Output;
}

//==================================================================================================
//      UNSIGNED IMPLS
//==================================================================================================

//==================================================================================================
impl ToLeb128 for u8 {
    fn to_leb128(&self) -> Vec<u8> {
        unsigned_encode(u64::from(*self))
    }

    fn to_leb128_inplace(&self, buffer: &mut [u8]) -> Option<NonZeroUsize> {
        unsigned_encode_inplace(u64::from(*self), buffer)
    }
}

impl FromLeb128 for u8 {
    type Output = Option<(NonZeroUsize, u8)>;

    fn from_leb128(buffer: &[u8]) -> Option<(NonZeroUsize, u8)> {
        let opt = unsigned_decode(buffer);
        match opt {
            Some((size, num)) => Some((size, num as u8)),
            None => None,
        }
    }
}

//==================================================================================================
impl ToLeb128 for u16 {
    fn to_leb128(&self) -> Vec<u8> {
        unsigned_encode(u64::from(*self))
    }

    fn to_leb128_inplace(&self, buffer: &mut [u8]) -> Option<NonZeroUsize> {
        unsigned_encode_inplace(u64::from(*self), buffer)
    }
}

impl FromLeb128 for u16 {
    type Output = Option<(NonZeroUsize, u16)>;

    fn from_leb128(buffer: &[u8]) -> Option<(NonZeroUsize, u16)> {
        let opt = unsigned_decode(buffer);
        match opt {
            Some((size, num)) => Some((size, num as u16)),
            None => None,
        }
    }
}

//==================================================================================================
impl ToLeb128 for u32 {
    fn to_leb128(&self) -> Vec<u8> {
        unsigned_encode(u64::from(*self))
    }

    fn to_leb128_inplace(&self, buffer: &mut [u8]) -> Option<NonZeroUsize> {
        unsigned_encode_inplace(u64::from(*self), buffer)
    }
}

impl FromLeb128 for u32 {
    type Output = Option<(NonZeroUsize, u32)>;

    fn from_leb128(buffer: &[u8]) -> Option<(NonZeroUsize, u32)> {
        let opt = unsigned_decode(buffer);
        match opt {
            Some((size, num)) => Some((size, num as u32)),
            None => None,
        }
    }
}

//==================================================================================================
impl ToLeb128 for u64 {
    fn to_leb128(&self) -> Vec<u8> {
        unsigned_encode(*self)
    }

    fn to_leb128_inplace(&self, buffer: &mut [u8]) -> Option<NonZeroUsize> {
        unsigned_encode_inplace(*self, buffer)
    }
}

impl FromLeb128 for u64 {
    type Output = Option<(NonZeroUsize, u64)>;

    fn from_leb128(buffer: &[u8]) -> Option<(NonZeroUsize, u64)> {
        unsigned_decode(buffer)
    }
}

//==================================================================================================
//      SIGNED IMPLS
//==================================================================================================

//==================================================================================================
impl ToLeb128 for i8 {
    fn to_leb128(&self) -> Vec<u8> {
        signed_encode(i64::from(*self))
    }

    fn to_leb128_inplace(&self, buffer: &mut [u8]) -> Option<NonZeroUsize> {
        signed_encode_inplace(i64::from(*self), buffer)
    }
}

impl FromLeb128 for i8 {
    type Output = Option<(NonZeroUsize, i8)>;

    fn from_leb128(buffer: &[u8]) -> Option<(NonZeroUsize, i8)> {
        let opt = signed_decode(buffer);
        match opt {
            Some((size, num)) => Some((size, num as i8)),
            None => None,
        }
    }
}

//==================================================================================================
impl ToLeb128 for i16 {
    fn to_leb128(&self) -> Vec<u8> {
        signed_encode(i64::from(*self))
    }

    fn to_leb128_inplace(&self, buffer: &mut [u8]) -> Option<NonZeroUsize> {
        signed_encode_inplace(i64::from(*self), buffer)
    }
}

impl FromLeb128 for i16 {
    type Output = Option<(NonZeroUsize, i16)>;

    fn from_leb128(buffer: &[u8]) -> Option<(NonZeroUsize, i16)> {
        let opt = signed_decode(buffer);
        match opt {
            Some((size, num)) => Some((size, num as i16)),
            None => None,
        }
    }
}

//==================================================================================================
impl ToLeb128 for i32 {
    fn to_leb128(&self) -> Vec<u8> {
        signed_encode(i64::from(*self))
    }

    fn to_leb128_inplace(&self, buffer: &mut [u8]) -> Option<NonZeroUsize> {
        signed_encode_inplace(i64::from(*self), buffer)
    }
}

impl FromLeb128 for i32 {
    type Output = Option<(NonZeroUsize, i32)>;

    fn from_leb128(buffer: &[u8]) -> Option<(NonZeroUsize, i32)> {
        let opt = signed_decode(buffer);
        match opt {
            Some((size, num)) => Some((size, num as i32)),
            None => None,
        }
    }
}

//==================================================================================================
impl ToLeb128 for i64 {
    fn to_leb128(&self) -> Vec<u8> {
        signed_encode(*self)
    }

    fn to_leb128_inplace(&self, buffer: &mut [u8]) -> Option<NonZeroUsize> {
        signed_encode_inplace(*self, buffer)
    }
}

impl FromLeb128 for i64 {
    type Output = Option<(NonZeroUsize, i64)>;

    fn from_leb128(buffer: &[u8]) -> Option<(NonZeroUsize, i64)> {
        signed_decode(buffer)
    }
}

#[cfg(test)]
pub mod tests {

    use crate::leb128::FromLeb128;
    use crate::leb128::ToLeb128;

    //==============================================================================================
    //      Unsigned Tests
    //==============================================================================================

    //==============================================================================================
    #[test]
    pub fn leb128_round_trip_unsigned_u8() {
        let val1: u8 = 56;
        let vec = val1.to_leb128();
        let (_bytes, val2) = u8::from_leb128(vec.as_slice()).unwrap();
        assert_eq!(val1, val2);
    }

    //==============================================================================================
    #[test]
    pub fn leb128_round_trip_unsigned_u16() {
        let val1: u16 = 512;
        let vec = val1.to_leb128();
        let (_bytes, val2) = u16::from_leb128(vec.as_slice()).unwrap();
        assert_eq!(val1, val2);
    }

    //==============================================================================================
    #[test]
    pub fn leb128_round_trip_unsigned_u32() {
        let val1: u32 = 131_071;
        let vec = val1.to_leb128();
        let (_bytes, val2) = u32::from_leb128(vec.as_slice()).unwrap();
        assert_eq!(val1, val2);
    }

    //==============================================================================================
    #[test]
    pub fn leb128_round_trip_unsigned_u64() {
        let val1: u64 = 68_719_476_735;
        let vec = val1.to_leb128();
        let (_bytes, val2) = u64::from_leb128(vec.as_slice()).unwrap();
        assert_eq!(val1, val2);
    }

    //==============================================================================================
    //      Unsigned Inplace Tests
    //==============================================================================================

    //==============================================================================================
    #[test]
    pub fn leb128_round_trip_unsigned_u8_inplace() {
        let val1: u8 = 56;

        let mut vec: Vec<u8> = Vec::new();
        vec.resize(2, 0);

        val1.to_leb128_inplace(vec.as_mut_slice());

        let (_, val2) = u8::from_leb128(vec.as_slice()).unwrap();
        assert_eq!(val1, val2);
    }

    //==============================================================================================
    #[test]
    pub fn leb128_round_trip_unsigned_u16_inplace() {
        let val1: u16 = 512;

        let mut vec: Vec<u8> = Vec::new();
        vec.resize(3, 0);

        val1.to_leb128_inplace(vec.as_mut_slice());

        let (_, val2) = u16::from_leb128(vec.as_slice()).unwrap();
        assert_eq!(val1, val2);
    }

    //==============================================================================================
    #[test]
    pub fn leb128_round_trip_unsigned_u32_inplace() {
        let val1: u32 = 131_071;

        let mut vec: Vec<u8> = Vec::new();
        vec.resize(5, 0);

        val1.to_leb128_inplace(vec.as_mut_slice());

        let (_, val2) = u32::from_leb128(vec.as_slice()).unwrap();
        assert_eq!(val1, val2);
    }

    //==============================================================================================
    #[test]
    pub fn leb128_round_trip_unsigned_u64_inplace() {
        let val1: u64 = 68_719_476_735;

        let mut vec: Vec<u8> = Vec::new();
        vec.resize(10, 0);

        val1.to_leb128_inplace(vec.as_mut_slice());

        let (_, val2) = u64::from_leb128(vec.as_slice()).unwrap();
        assert_eq!(val1, val2);
    }

    //==============================================================================================
    //      Signed Tests
    //==============================================================================================

    //==============================================================================================
    #[test]
    pub fn leb128_round_trip_signed_i8() {
        let val1: i8 = -56;
        let vec = val1.to_leb128();
        let (_bytes, val2) = i8::from_leb128(vec.as_slice()).unwrap();
        assert_eq!(val1, val2);
    }

    //==============================================================================================
    #[test]
    pub fn leb128_round_trip_signed_i16() {
        let val1: i16 = -512;
        let vec = val1.to_leb128();
        let (_bytes, val2) = i16::from_leb128(vec.as_slice()).unwrap();
        assert_eq!(val1, val2);
    }

    //==============================================================================================
    #[test]
    pub fn leb128_round_trip_signed_i32() {
        let val1: i32 = -131_071;
        let vec = val1.to_leb128();
        let (_bytes, val2) = i32::from_leb128(vec.as_slice()).unwrap();
        assert_eq!(val1, val2);
    }

    //==============================================================================================
    #[test]
    pub fn leb128_round_trip_signed_i64() {
        let val1: i64 = -68_719_476_735;
        let vec = val1.to_leb128();
        let (_bytes, val2) = i64::from_leb128(vec.as_slice()).unwrap();
        assert_eq!(val1, val2);
    }

    //==============================================================================================
    //      Signed Inplace Tests
    //==============================================================================================

    //==============================================================================================
    #[test]
    pub fn leb128_round_trip_unsigned_i8_inplace() {
        let val1: i8 = -56;

        let mut vec: Vec<u8> = Vec::new();
        vec.resize(2, 0);

        val1.to_leb128_inplace(vec.as_mut_slice());

        let (_, val2) = i8::from_leb128(vec.as_slice()).unwrap();
        assert_eq!(val1, val2);
    }

    //==============================================================================================
    #[test]
    pub fn leb128_round_trip_unsigned_i16_inplace() {
        let val1: i16 = -512;

        let mut vec: Vec<u8> = Vec::new();
        vec.resize(3, 0);

        val1.to_leb128_inplace(vec.as_mut_slice());

        let (_, val2) = i16::from_leb128(vec.as_slice()).unwrap();
        assert_eq!(val1, val2);
    }

    //==============================================================================================
    #[test]
    pub fn leb128_round_trip_unsigned_i32_inplace() {
        let val1: i32 = -131_071;

        let mut vec: Vec<u8> = Vec::new();
        vec.resize(5, 0);

        val1.to_leb128_inplace(vec.as_mut_slice());

        let (_, val2) = i32::from_leb128(vec.as_slice()).unwrap();
        assert_eq!(val1, val2);
    }

    //==============================================================================================
    #[test]
    pub fn leb128_round_trip_unsigned_i64_inplace() {
        let val1: i64 = -68_719_476_735;

        let mut vec: Vec<u8> = Vec::new();
        vec.resize(10, 0);

        val1.to_leb128_inplace(vec.as_mut_slice());

        let (_, val2) = i64::from_leb128(vec.as_slice()).unwrap();
        assert_eq!(val1, val2);
    }
}
