bindgen .\studio\inc\fmod_studio.h ^
-o src/lib.rs ^
--default-enum-style rust ^
--bitfield-enum FMOD_CHANNELMASK.* ^
-- ^
-I.\studio\inc ^
-I.\core\inc