/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::enums::MemoryUsage;
use crate::flags::AllocationCreateFlag;

use vma_sys::raw;

use ash::vk;

use core::ptr;

//==================================================================================================
///
/// Wrapper around a VmaAllocation
///
#[derive(Copy, Clone)]
pub struct Allocation {
    allocation: raw::VmaAllocation,
}

//==================================================================================================
impl Allocation {
    ///
    /// Create from a raw handle.
    ///
    /// Must have a lifetime shorter than it's parent allocator
    ///
    pub fn from_raw(allocation: raw::VmaAllocation) -> Self {
        Allocation { allocation }
    }

    ///
    /// Returns the underlying raw::VmaAllocation for use with raw function calls
    ///
    pub fn into_raw(self) -> raw::VmaAllocation {
        self.allocation
    }

    ///
    /// Returns if this value can not be holding a valid allocation
    ///
    pub fn is_invalid(&self) -> bool {
        self.allocation.is_null()
    }
}

//
// Implementing these is safe because this is simply a handle and doesn't own any data
//
unsafe impl Send for Allocation {}
unsafe impl Sync for Allocation {}

impl Default for Allocation {
    fn default() -> Self {
        Self {
            allocation: ptr::null_mut(),
        }
    }
}

//==================================================================================================
///
/// VmaAllocationCreateInfo
///
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct AllocationCreateInfo {
    flags: AllocationCreateFlag,
    usage: MemoryUsage,
    required_flags: vk::MemoryPropertyFlags,
    preferred_flags: vk::MemoryPropertyFlags,
    memory_type_bits: u32,
    pool: raw::VmaPool,
    p_user_data: *mut ::std::os::raw::c_void,
}

//==================================================================================================
impl AllocationCreateInfo {
    pub fn builder() -> AllocationCreateInfoBuilder {
        AllocationCreateInfoBuilder::new()
    }
}

//==================================================================================================
///
///
///
pub struct AllocationCreateInfoBuilder {
    info: AllocationCreateInfo,
}

//==================================================================================================
impl AllocationCreateInfoBuilder {
    ///
    ///
    ///
    pub fn new() -> Self {
        let info = AllocationCreateInfo {
            flags: AllocationCreateFlag::from(0u32),
            usage: MemoryUsage::Unknown,
            required_flags: vk::MemoryPropertyFlags::from_raw(0),
            preferred_flags: vk::MemoryPropertyFlags::from_raw(0),
            memory_type_bits: 0,
            pool: ptr::null_mut(),
            p_user_data: ptr::null_mut(),
        };
        AllocationCreateInfoBuilder { info }
    }

    ///
    ///
    ///
    pub fn flags(mut self, flags: AllocationCreateFlag) -> Self {
        self.info.flags = flags;
        self
    }

    ///
    ///
    ///
    pub fn required_flags(mut self, flags: vk::MemoryPropertyFlags) -> Self {
        self.info.required_flags = flags;
        self
    }

    ///
    ///
    ///
    pub fn preferred_flags(mut self, flags: vk::MemoryPropertyFlags) -> Self {
        self.info.preferred_flags = flags;
        self
    }

    ///
    ///
    ///
    pub fn usage(mut self, usage: MemoryUsage) -> Self {
        self.info.usage = usage;
        self
    }

    ///
    ///
    ///
    pub fn build(self) -> AllocationCreateInfo {
        self.info
    }
}

//==================================================================================================
impl Default for AllocationCreateInfoBuilder {
    fn default() -> Self {
        Self::new()
    }
}

//==================================================================================================
///
/// VmaAllocationInfo
///
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct AllocationInfo {
    pub memory_type: u32,
    pub device_memory: vk::DeviceMemory,
    pub offset: vk::DeviceSize,
    pub size: vk::DeviceSize,
    pub p_mapped_data: *mut ::std::os::raw::c_void,
    pub p_user_data: *mut ::std::os::raw::c_void,
}
