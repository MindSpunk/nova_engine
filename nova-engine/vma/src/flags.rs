/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use vma_sys::raw;

//==================================================================================================
///
/// A rusty wrapper around the raw VmaAllocatorCreateFlag constants
///
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Default)]
#[repr(transparent)]
pub struct AllocatorCreateFlag(u32);

//==================================================================================================
impl AllocatorCreateFlag {
    ///
    /// VMA_ALLOCATOR_CREATE_EXTERNALLY_SYNCHRONIZED_BIT
    ///
    pub const EXTERNALLY_SYNCHRONIZED_BIT: AllocatorCreateFlag = AllocatorCreateFlag(
        raw::VmaAllocatorCreateFlagBits_VMA_ALLOCATOR_CREATE_EXTERNALLY_SYNCHRONIZED_BIT as u32,
    );

    ///
    /// VMA_ALLOCATOR_CREATE_KHR_DEDICATED_ALLOCATION_BIT
    ///
    pub const KHR_DEDICATED_ALLOCATION_BIT: AllocatorCreateFlag = AllocatorCreateFlag(
        raw::VmaAllocatorCreateFlagBits_VMA_ALLOCATOR_CREATE_KHR_DEDICATED_ALLOCATION_BIT as u32,
    );
}

//==================================================================================================
impl From<u32> for AllocatorCreateFlag {
    fn from(input: u32) -> Self {
        AllocatorCreateFlag(input)
    }
}

//==================================================================================================
impl Into<u32> for AllocatorCreateFlag {
    fn into(self) -> u32 {
        self.0
    }
}

//==================================================================================================
///
/// A rusty wrapper around the raw VmaAllocationCreateFlag constants
///
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Default)]
#[repr(transparent)]
pub struct AllocationCreateFlag(u32);

//==================================================================================================
impl AllocationCreateFlag {
    ///
    /// VMA_ALLOCATION_CREATE_DEDICATED_MEMORY_BIT
    ///
    pub const DEDICATED_MEMORY_BIT: AllocationCreateFlag = AllocationCreateFlag(
        raw::VmaAllocationCreateFlagBits_VMA_ALLOCATION_CREATE_DEDICATED_MEMORY_BIT,
    );

    ///
    /// VMA_ALLOCATION_CREATE_NEVER_ALLOCATE_BIT
    ///
    pub const NEVER_ALLOCATE_BIT: AllocationCreateFlag = AllocationCreateFlag(
        raw::VmaAllocationCreateFlagBits_VMA_ALLOCATION_CREATE_NEVER_ALLOCATE_BIT,
    );

    ///
    /// VMA_ALLOCATION_CREATE_MAPPED_BIT
    ///
    pub const MAPPED_BIT: AllocationCreateFlag =
        AllocationCreateFlag(raw::VmaAllocationCreateFlagBits_VMA_ALLOCATION_CREATE_MAPPED_BIT);

    ///
    /// VMA_ALLOCATION_CREATE_CAN_BECOME_LOST_BIT
    ///
    pub const CAN_BECOME_LOST_BIT: AllocationCreateFlag = AllocationCreateFlag(
        raw::VmaAllocationCreateFlagBits_VMA_ALLOCATION_CREATE_CAN_BECOME_LOST_BIT,
    );

    ///
    /// VMA_ALLOCATION_CREATE_CAN_MAKE_OTHER_LOST_BIT
    ///
    pub const CAN_MAKE_OTHER_LOST_BIT: AllocationCreateFlag = AllocationCreateFlag(
        raw::VmaAllocationCreateFlagBits_VMA_ALLOCATION_CREATE_CAN_MAKE_OTHER_LOST_BIT,
    );

    ///
    /// VMA_ALLOCATION_CREATE_USER_DATA_COPY_STRING_BIT
    ///
    pub const USER_DATA_COPY_STRING_BIT: AllocationCreateFlag = AllocationCreateFlag(
        raw::VmaAllocationCreateFlagBits_VMA_ALLOCATION_CREATE_USER_DATA_COPY_STRING_BIT,
    );

    ///
    /// VMA_ALLOCATION_CREATE_UPPER_ADDRESS_BIT
    ///
    pub const UPPER_ADDRESS_BIT: AllocationCreateFlag = AllocationCreateFlag(
        raw::VmaAllocationCreateFlagBits_VMA_ALLOCATION_CREATE_UPPER_ADDRESS_BIT,
    );

    ///
    /// VMA_ALLOCATION_CREATE_STRATEGY_BEST_FIT_BIT
    ///
    pub const STRATEGY_BEST_FIT_BIT: AllocationCreateFlag = AllocationCreateFlag(
        raw::VmaAllocationCreateFlagBits_VMA_ALLOCATION_CREATE_STRATEGY_BEST_FIT_BIT,
    );

    ///
    /// VMA_ALLOCATION_CREATE_STRATEGY_WORST_FIT_BIT
    ///
    pub const STRATEGY_WORST_FIT_BIT: AllocationCreateFlag = AllocationCreateFlag(
        raw::VmaAllocationCreateFlagBits_VMA_ALLOCATION_CREATE_STRATEGY_WORST_FIT_BIT,
    );

    ///
    /// VMA_ALLOCATION_CREATE_STRATEGY_FIRST_FIT_BIT
    ///
    pub const STRATEGY_FIRST_FIT_BIT: AllocationCreateFlag = AllocationCreateFlag(
        raw::VmaAllocationCreateFlagBits_VMA_ALLOCATION_CREATE_STRATEGY_FIRST_FIT_BIT,
    );

    ///
    /// VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT
    ///
    pub const STRATEGY_MIN_MEMORY_BIT: AllocationCreateFlag = AllocationCreateFlag(
        raw::VmaAllocationCreateFlagBits_VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT,
    );

    ///
    /// VMA_ALLOCATION_CREATE_STRATEGY_MIN_TIME_BIT
    ///
    pub const STRATEGY_MIN_TIME_BIT: AllocationCreateFlag = AllocationCreateFlag(
        raw::VmaAllocationCreateFlagBits_VMA_ALLOCATION_CREATE_STRATEGY_MIN_TIME_BIT,
    );

    ///
    /// VMA_ALLOCATION_CREATE_STRATEGY_MIN_FRAGMENTATION_BIT
    ///
    pub const STRATEGY_MIN_FRAGMENTATION_BIT: AllocationCreateFlag = AllocationCreateFlag(
        raw::VmaAllocationCreateFlagBits_VMA_ALLOCATION_CREATE_STRATEGY_MIN_FRAGMENTATION_BIT,
    );

    ///
    /// VMA_ALLOCATION_CREATE_STRATEGY_MASK
    ///
    pub const STRATEGY_MASK: AllocationCreateFlag =
        AllocationCreateFlag(raw::VmaAllocationCreateFlagBits_VMA_ALLOCATION_CREATE_STRATEGY_MASK);
}

//==================================================================================================
impl From<u32> for AllocationCreateFlag {
    fn from(input: u32) -> Self {
        AllocationCreateFlag(input)
    }
}

//==================================================================================================
impl Into<u32> for AllocationCreateFlag {
    fn into(self) -> u32 {
        self.0
    }
}

//==================================================================================================
///
/// A rusty wrapper around the raw VmaPoolCreateFlag constants
///
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Default)]
#[repr(transparent)]
pub struct PoolCreateFlag(u32);

//==================================================================================================
impl PoolCreateFlag {
    ///
    /// VMA_POOL_CREATE_IGNORE_BUFFER_IMAGE_GRANULARITY_BIT
    ///
    pub const IGNORE_BUFFER_IMAGE_GRANULARITY_BIT: PoolCreateFlag = PoolCreateFlag(
        raw::VmaPoolCreateFlagBits_VMA_POOL_CREATE_IGNORE_BUFFER_IMAGE_GRANULARITY_BIT as u32,
    );

    ///
    /// VMA_POOL_CREATE_LINEAR_ALGORITHM_BIT
    ///
    pub const LINEAR_ALGORITHM_BIT: PoolCreateFlag =
        PoolCreateFlag(raw::VmaPoolCreateFlagBits_VMA_POOL_CREATE_LINEAR_ALGORITHM_BIT as u32);

    ///
    /// VMA_POOL_CREATE_BUDDY_ALGORITHM_BIT
    ///
    pub const BUDDY_ALGORITHM_BIT: PoolCreateFlag =
        PoolCreateFlag(raw::VmaPoolCreateFlagBits_VMA_POOL_CREATE_BUDDY_ALGORITHM_BIT as u32);

    ///
    /// VMA_POOL_CREATE_ALGORITHM_MASK
    ///
    pub const ALGORITHM_MASK: PoolCreateFlag =
        PoolCreateFlag(raw::VmaPoolCreateFlagBits_VMA_POOL_CREATE_ALGORITHM_MASK as u32);
}

//==================================================================================================
impl From<u32> for PoolCreateFlag {
    fn from(input: u32) -> Self {
        PoolCreateFlag(input)
    }
}

//==================================================================================================
impl Into<u32> for PoolCreateFlag {
    fn into(self) -> u32 {
        self.0
    }
}
