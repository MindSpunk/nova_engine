/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::matrix::TMat4x4;
use crate::vector::{TVec2, TVec3, TVec4};

use crate::quaternion::TQuat;

///
/// A 2 component f32 vector
///
pub type Vec2 = TVec2<f32>;

///
/// A 3 component f32 vector
///
pub type Vec3 = TVec3<f32>;

///
/// A 4 component f32 vector
///
pub type Vec4 = TVec4<f32>;

///
/// A 2 component f64 vector
///
pub type DVec2 = TVec2<f64>;

///
/// A 3 component f64 vector
///
pub type DVec3 = TVec3<f64>;

///
/// A 4 component f64 vector
///
pub type DVec4 = TVec4<f64>;

///
/// A f32 quaternion
///
pub type Quat = TQuat<f32>;

///
/// A f64 quaternion
///
pub type DQuat = TQuat<f64>;

///
/// A 4x4 f32 matrix
///
pub type Mat4x4 = TMat4x4<f32>;

///
/// A 4x4 f64 matrix
///
pub type DMat4x4 = TMat4x4<f64>;

pub mod std140 {
    use crate::matrix::std140::TMat4x4P;
    use crate::vector::std140::TVec2P;
    use crate::vector::std140::TVec3P;
    use crate::vector::std140::TVec4P;

    ///
    /// A packed 2 component f32 vector
    ///
    pub type Vec2P = TVec2P<f32>;

    ///
    /// A packed 3 component f32 vector
    ///
    pub type Vec3P = TVec3P<f32>;

    ///
    /// A packed 4 component f32 vector
    ///
    pub type Vec4P = TVec4P<f32>;

    ///
    /// A packed 2 component f64 vector
    ///
    pub type DVec2P = TVec2P<f64>;

    ///
    /// A packed 3 component f64 vector
    ///
    pub type DVec3P = TVec3P<f64>;

    ///
    /// A packed 4 component f64 vector
    ///
    pub type DVec4P = TVec4P<f64>;

    ///
    /// A packed 4x4 f32 matrix
    ///
    pub type Mat4x4P = TMat4x4P<f32>;
    ///
    /// A packed 4x4 f64 matrix
    ///
    pub type DMat4x4P = TMat4x4P<f64>;
}
