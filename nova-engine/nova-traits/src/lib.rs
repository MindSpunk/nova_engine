/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#![no_std]

use core::mem;
use core::slice;

///
/// Trait for getting a value of type `T` as a slice `[T]` of length 1.
///
pub trait AsSlice: Sized {
    fn as_slice(&self) -> &[Self];
}

impl<T> AsSlice for T
where
    T: Sized,
{
    fn as_slice(&self) -> &[Self] {
        unsafe { slice::from_raw_parts(self as *const Self, 1) }
    }
}

///
/// Trait for getting a slice of the bytes representing a `T`
///
pub trait AsDataSlice: Sized {
    fn as_data_slice(&self) -> &[u8];
}

impl<T> AsDataSlice for T
where
    T: Sized,
{
    fn as_data_slice(&self) -> &[u8] {
        let data = self as *const Self as *const u8;
        let size = mem::size_of::<Self>();
        unsafe { slice::from_raw_parts(data, size) }
    }
}
