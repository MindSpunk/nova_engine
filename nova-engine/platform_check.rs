/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


// -------------------------------------------------------------------------------------------------
//
// Standard NovaEngine Support Guards
//
// -------------------------------------------------------------------------------------------------

#[
    cfg(
        not(
            any(
                target_os="windows",
                target_os="linux",
                target_os="android"
            )
        )
    )
]
compile_error!("NovaEngine only supports the Windows, Linux or Android operating systems");

#[
    cfg(
        not(
            any(
                target_arch="x86_64",
                target_arch="aarch64",
            )
        )
    )
]
compile_error!("NovaEngine only supports x86, x86_64, aarch64, and arm");

//
// Building on arm doesn't seem to like these for some reason
//
//#[
//    cfg(
//        all(
//            any(
//                target_arch="x86_64"
//            ),
//            not(
//                any(
//                    target_arch="aarch64",
//                )
//            ),
//            not(
//                target_os="android"
//            ),
//            not(target_feature="sse4.1")
//        )
//    )
//]
//compile_error!("NovaEngine requires sse4.1 instructions on x86 platforms");
//
//#[
//    cfg(
//        all(
//            any(
//                target_arch="x86_64"
//            ),
//            not(
//                any(
//                    target_arch="aarch64",
//                )
//            ),
//            not(
//                target_os="android"
//            ),
//            not(target_feature="avx")
//        )
//    )
//]
//compile_error!("NovaEngine requires avx instructions on x86 platforms");
