/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::AtomicBumpAllocator;
use std::alloc::Layout;
use std::marker::PhantomData;
use std::mem::{align_of, size_of};
use std::sync::atomic::Ordering;

///
/// An atomic object pool allocator that is safe to share between threads
///
pub struct AtomicPoolAllocator<'memory, T> {
    inner: AtomicBumpAllocator<'memory>,
    length: usize,
    phantom: PhantomData<T>,
}

impl<'memory, T> AtomicPoolAllocator<'memory, T> {
    const ITEM_ALIGN: usize = align_of::<T>();
    const ALIGN_MASK: usize = Self::ITEM_ALIGN - 1;

    ///
    /// Creates a new PoolAllocator from the given memory buffer
    ///
    pub fn from_memory(memory: &'memory mut [u8]) -> Self {
        //
        // Get an aligned sub slice of the given memory
        //
        let unaligned = memory.as_ptr() as usize;
        let offset = unaligned & Self::ALIGN_MASK;
        let offset = Self::ITEM_ALIGN - offset;
        let aligned = memory.split_at_mut(offset).1;
        let length = aligned.len();
        Self {
            inner: AtomicBumpAllocator::from_memory(aligned),
            length,
            phantom: Default::default(),
        }
    }

    ///
    ///
    ///
    #[inline]
    unsafe fn store_object(&self, val: T) -> *mut T {
        let layout = Layout::for_value(&val);
        let obj_mem = self.inner.allocate_layout(layout);
        if obj_mem.is_null() {
            std::ptr::null_mut()
        } else {
            let obj_mem = obj_mem as *mut T;
            obj_mem.write(val);
            std::sync::atomic::fence(Ordering::Release);
            obj_mem
        }
    }

    ///
    ///
    ///
    #[inline]
    pub fn store_object_ref(&self, t: T) -> Option<&T> {
        unsafe {
            let ptr = self.store_object(t);
            if ptr.is_null() {
                None
            } else {
                Some(&*ptr)
            }
        }
    }

    ///
    ///
    ///
    #[inline]
    pub fn store_object_mut(&self, t: T) -> Option<&mut T> {
        unsafe {
            let ptr = self.store_object(t);
            if ptr.is_null() {
                None
            } else {
                Some(&mut *ptr)
            }
        }
    }
}

unsafe impl<'memory, T: Sync> Sync for AtomicPoolAllocator<'memory, T> {}

impl<'memory, T> Drop for AtomicPoolAllocator<'memory, T> {
    fn drop(&mut self) {
        unsafe {
            let mut head = self.inner.head.load(Ordering::Relaxed) as *mut T;
            let end = self.inner.end;

            let remaining = head as usize - end as usize;
            let allocated_bytes = self.length - remaining;

            let base = head.sub(allocated_bytes);

            while head > base {
                let ptr = head as *mut T;
                std::ptr::drop_in_place(ptr);
                head = head.add(size_of::<T>());
            }
        }
    }
}
