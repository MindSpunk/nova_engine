/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use std::alloc::Layout;
use std::marker::PhantomData;
use std::sync::atomic::{AtomicUsize, Ordering};

///
/// An atomic bump allocator that is safe to share across threads.
///
pub struct AtomicBumpAllocator<'memory> {
    pub(crate) head: AtomicUsize,
    pub(crate) end: *mut u8,
    pub(crate) phantom: PhantomData<&'memory mut u8>,
}

impl<'memory> AtomicBumpAllocator<'memory> {
    ///
    /// Use the given slice as the backing storage for the allocator
    ///
    #[inline]
    pub fn from_memory(memory: &'memory mut [u8]) -> Self {
        unsafe {
            let head = memory.as_mut_ptr().add(memory.len());
            let head = AtomicUsize::new(head as usize);
            let end = memory.as_mut_ptr();

            Self {
                head,
                end,
                phantom: Default::default(),
            }
        }
    }

    ///
    /// Allocate a block of memory that meets the given layout
    ///
    /// # Warning
    ///
    /// Returns null on failure to allocate
    ///
    #[inline]
    pub fn allocate_layout(&self, layout: Layout) -> *mut u8 {
        loop {
            let old = self.head.load(Ordering::Relaxed);
            let new = old - layout.size();
            if new < self.end as usize {
                return std::ptr::null_mut();
            } else {
                let new = new & !(layout.align() - 1);
                if self.head.compare_and_swap(old, new, Ordering::Relaxed) == old {
                    return new as *mut u8;
                }
            }
        }
    }

    ///
    /// Allocate space in the bump allocator and make a copy of the given string with said space
    ///
    /// Returns `None` on failure to allocate
    ///
    #[inline]
    pub fn allocate_str(&self, source: &str) -> &mut str {
        let buffer = self.allocate_slice_copy(source.as_bytes());
        unsafe { std::str::from_utf8_unchecked_mut(buffer) }
    }

    ///
    /// Allocate a slice of objects, copying from the source array
    ///
    /// Returns `None` on failure to allocate
    ///
    #[inline]
    pub fn allocate_slice_copy<T: Copy>(&self, source: &[T]) -> &mut [T] {
        unsafe {
            let layout = Layout::for_value(source);
            let ptr = self.allocate_layout(layout) as *mut T;
            if ptr == std::ptr::null_mut() {
                std::slice::from_raw_parts_mut(std::ptr::null_mut(), 0)
            } else {
                std::ptr::copy_nonoverlapping(source.as_ptr(), ptr, source.len());
                std::sync::atomic::fence(Ordering::Release);
                std::slice::from_raw_parts_mut(ptr as *mut T, source.len())
            }
        }
    }

    ///
    /// Allocates a slice of objects, cloning from the source array
    ///
    /// Returns `None` on failure to allocate
    ///
    #[inline]
    pub fn allocate_slice_clone<T: Clone>(&self, source: &[T]) -> &mut [T] {
        unsafe {
            let layout = Layout::for_value(source);
            let ptr = self.allocate_layout(layout) as *mut T;
            if ptr == std::ptr::null_mut() {
                std::slice::from_raw_parts_mut(std::ptr::null_mut(), 0)
            } else {
                for (i, val) in source.iter().enumerate() {
                    ptr.add(i).write(val.clone());
                }
                std::sync::atomic::fence(Ordering::Release);
                std::slice::from_raw_parts_mut(ptr as *mut T, source.len())
            }
        }
    }
}

unsafe impl<'memory> Sync for AtomicBumpAllocator<'memory> {}
