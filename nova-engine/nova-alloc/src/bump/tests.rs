/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::BumpAllocator;
use std::sync::atomic::{AtomicUsize, Ordering};

struct DropTest<'a> {
    val: &'a AtomicUsize,
}

impl<'a> DropTest<'a> {
    fn new(val: &'a AtomicUsize) -> Self {
        val.fetch_add(1, Ordering::Relaxed);
        Self { val }
    }
}

impl<'a> Clone for DropTest<'a> {
    fn clone(&self) -> Self {
        Self::new(self.val)
    }
}

impl<'a> Drop for DropTest<'a> {
    fn drop(&mut self) {
        self.val.fetch_sub(1, Ordering::Relaxed);
    }
}

#[test]
pub fn test_drop_slice() {
    let atomic = AtomicUsize::new(0);
    let mut buffer = [0u8; 256];
    let allocator = BumpAllocator::from_memory(&mut buffer);
    {
        let objects = [
            DropTest::new(&atomic),
            DropTest::new(&atomic),
            DropTest::new(&atomic),
            DropTest::new(&atomic),
        ];
        assert_eq!(atomic.load(Ordering::Relaxed), 4);
        {
            let cloned_objects = allocator.allocate_slice_clone(&objects).unwrap();
            assert_eq!(atomic.load(Ordering::Relaxed), 8);
            drop(cloned_objects);
        }
        assert_eq!(atomic.load(Ordering::Relaxed), 8);
    }
    assert_eq!(atomic.load(Ordering::Relaxed), 4);
}
