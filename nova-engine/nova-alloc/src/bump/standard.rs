/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use std::alloc::Layout;
use std::cell::Cell;
use std::marker::PhantomData;
use std::mem::{align_of, size_of};

///
/// The core for a non thread safe bump allocator that can only be used within a thread
///
/// This should be used wrapped by `BumpAllocator`
///
#[derive(Copy, Clone)]
pub struct BumpAllocatorCore<'memory> {
    pub(crate) head: *mut u8,
    pub(crate) base: *mut u8,
    pub(crate) end: *mut u8,
    pub(crate) phantom: PhantomData<&'memory mut u8>,
}

impl<'memory> BumpAllocatorCore<'memory> {
    ///
    /// Create a bump allocator that allocates over the given region of memory
    ///
    #[inline]
    pub fn from_memory(memory: &'memory mut [u8]) -> Self {
        unsafe {
            let head = memory.as_mut_ptr().add(memory.len());
            let base = head;
            let end = memory.as_mut_ptr();

            Self {
                head,
                base,
                end,
                phantom: Default::default(),
            }
        }
    }

    ///
    /// Resets the allocator, effectively "de-allocating" the memory.
    ///
    #[inline]
    pub fn reset(self) -> Self {
        Self {
            head: self.base,
            base: self.base,
            end: self.end,
            phantom: Default::default(),
        }
    }

    ///
    /// Allocate a block of memory that meets the given layout
    ///
    /// # Warning
    ///
    /// Returns null on failure to allocate
    ///
    #[inline]
    pub fn allocate_layout(&mut self, layout: Layout) -> *mut u8 {
        unsafe {
            //
            // Bump the pointer down
            //
            let ptr = self.head.sub(layout.size());

            //
            // If we've passed the end of the buffer then we should return a null pointer, otherwise
            // return our newly allocated pointer
            //
            if ptr < self.end {
                std::ptr::null_mut()
            } else {
                //
                // Align the pointer and return
                //
                let ptr = ptr as usize;
                let ptr = ptr & !(layout.align() - 1);
                self.head = ptr as *mut u8;
                self.head
            }
        }
    }

    ///
    /// Allocate space in the bump allocator and make a copy of the given string with said space
    ///
    /// Returns `None` on failure to allocate
    ///
    #[inline]
    pub fn allocate_str(&mut self, source: &str) -> Option<&mut str> {
        unsafe {
            self.allocate_slice_copy(source.as_bytes())
                .map(|buffer| std::str::from_utf8_unchecked_mut(buffer))
        }
    }

    ///
    /// Attempts to reallocate the string, shrinking or growing it if required.
    ///
    /// This function only works if the passed string was the most recently allocated object. This
    /// is we may need space to expand the string which we can't do if there's an object allocated
    /// after the string as that would need to be moved too.
    ///
    /// This function will either shrink or grow the string, depending on whether new_size is larger
    /// or smaller than the current length of the string. If the string is shrunk, it will truncate
    /// the length. If the string grows it will fill the empty space with null bytes
    ///
    /// Returns null on failure to allocate
    ///
    /// # Warning
    ///
    /// If a bad new_size value is passed in it is possible for this function to truncate the string
    /// inside a utf8 character producing an invalid utf8 sequence. Always ensure that the new size
    /// will yield a valid utf8 string. This is why the function is marked unsafe, we leave the utf8
    /// validation to you.
    ///
    #[inline]
    pub unsafe fn try_realloc_str(
        &mut self,
        string: &mut str,
        new_size: usize,
    ) -> Option<&mut str> {
        if string.as_mut_ptr() == self.head {
            //
            // Calculate the new pointer
            //
            let old_size = string.len();
            let diff = old_size as isize - new_size as isize;
            let ptr = self.head.offset(diff);

            //
            // Ensure that we don't allocate past the edge of the buffer
            //
            if ptr < self.end || ptr > self.base {
                return None;
            }

            //
            // Align the pointer (probably will always be a noop but must do it for safety)
            //
            let ptr = (ptr as usize) & !(align_of::<u8>() - 1);
            let ptr = ptr as *mut u8;

            //
            // Copy the data down to the shifted address
            //
            let copy_size = usize::min(old_size, new_size);
            std::ptr::copy(string.as_ptr(), ptr, copy_size);

            //
            // Fill the empty space with null bytes
            //
            if diff.is_negative() {
                std::ptr::write_bytes(ptr.add(old_size), 0, -diff as usize);
            }

            //
            // Update the head ptr
            //
            self.head = ptr;

            //
            // Return a new str reference
            //
            let out = if new_size == 0 {
                let out = std::slice::from_raw_parts_mut(std::ptr::null_mut(), 0);
                let out = std::str::from_utf8_unchecked_mut(out);
                out
            } else {
                let out = std::slice::from_raw_parts_mut(ptr, new_size);
                let out = std::str::from_utf8_unchecked_mut(out);
                out
            };
            Some(out)
        } else {
            None
        }
    }

    ///
    /// Allocate a slice of objects filled with a default value
    ///
    /// Returns `None` on failure to allocate
    ///
    #[inline]
    pub fn allocate_slice_default<T: Default>(&mut self, len: usize) -> Option<&mut [T]> {
        unsafe {
            //
            // Allocate space for the slice
            //
            let layout = Layout::from_size_align_unchecked(len * size_of::<T>(), align_of::<T>());
            let ptr = self.allocate_layout(layout) as *mut T;

            //
            // If we failed to allocate return an empty slice, otherwise copy the data and return our
            // new slice
            //
            if ptr == std::ptr::null_mut() {
                None
            } else {
                for i in 0..len {
                    let ptr = ptr.add(i);
                    ptr.write(T::default());
                }
                Some(std::slice::from_raw_parts_mut(ptr as *mut T, len))
            }
        }
    }

    ///
    /// Allocate a slice of objects, copying from the source array
    ///
    /// Returns `None` on failure to allocate
    ///
    #[inline]
    pub fn allocate_slice_copy<T: Copy>(&mut self, source: &[T]) -> Option<&mut [T]> {
        unsafe {
            //
            // Allocate space for the slice
            //
            let layout = Layout::for_value(source);
            let ptr = self.allocate_layout(layout) as *mut T;

            //
            // If we failed to allocate return an empty slice, otherwise copy the data and return our
            // new slice
            //
            if ptr == std::ptr::null_mut() {
                None
            } else {
                std::ptr::copy_nonoverlapping(source.as_ptr(), ptr, source.len());
                Some(std::slice::from_raw_parts_mut(ptr as *mut T, source.len()))
            }
        }
    }

    ///
    /// Allocates a slice of objects, cloning from the source array
    ///
    /// Returns `None` on failure to allocate
    ///
    /// # Warning
    ///
    /// None of the values in the allocated slice will be dropped unless you manually drop them
    ///
    #[inline]
    pub fn allocate_slice_clone<T: Clone>(&mut self, source: &[T]) -> Option<&mut [T]> {
        unsafe {
            //
            // Allocate space for the slice
            //
            let layout = Layout::for_value(source);
            let ptr = self.allocate_layout(layout) as *mut T;

            //
            // If we failed to allocate return an empty slice, otherwise copy the data and return our
            // new slice
            //
            if ptr == std::ptr::null_mut() {
                None
            } else {
                for (i, val) in source.iter().enumerate() {
                    ptr.add(i).write(val.clone());
                }
                Some(std::slice::from_raw_parts_mut(ptr as *mut T, source.len()))
            }
        }
    }
}

///
/// A simple, non thread safe bump allocator.
///
pub struct BumpAllocator<'memory> {
    pub(crate) inner: Cell<BumpAllocatorCore<'memory>>,
}

impl<'memory> BumpAllocator<'memory> {
    ///
    /// Create a bump allocator that allocates over the given region of memory
    ///
    pub fn from_memory(memory: &'memory mut [u8]) -> Self {
        Self {
            inner: Cell::new(BumpAllocatorCore::from_memory(memory)),
        }
    }

    ///
    /// Resets the allocator, effectively "de-allocating" the memory.
    ///
    #[inline]
    pub fn reset(self) -> Self {
        let inner = self.inner.get();
        self.inner.set(inner.reset());
        self
    }

    ///
    /// Allocate a block of memory that meets the given layout
    ///
    /// # Warning
    ///
    /// Returns null on failure to allocate
    ///
    #[inline]
    pub fn allocate_layout(&self, layout: Layout) -> *mut u8 {
        let mut state = self.inner.get();
        let ret = state.allocate_layout(layout);
        self.inner.set(state);
        ret
    }

    ///
    /// Allocate space in the bump allocator and make a copy of the given string with said space
    ///
    /// Returns `None` on failure to allocate
    ///
    #[inline]
    pub fn allocate_str(&self, source: &str) -> Option<&mut str> {
        unsafe {
            let state = &mut *self.inner.as_ptr();
            let ret = state.allocate_str(source);
            ret
        }
    }

    ///
    /// Attempts to reallocate the string, shrinking or growing it if required.
    ///
    /// This function only works if the passed string was the most recently allocated object. This
    /// is we may need space to expand the string which we can't do if there's an object allocated
    /// after the string as that would need to be moved too.
    ///
    /// This function will either shrink or grow the string, depending on whether new_size is larger
    /// or smaller than the current length of the string. If the string is shrunk, it will truncate
    /// the length. If the string grows it will fill the empty space with null bytes
    ///
    /// Returns null on failure to allocate
    ///
    /// # Warning
    ///
    /// If a bad new_size value is passed in it is possible for this function to truncate the string
    /// inside a utf8 character producing an invalid utf8 sequence. Always ensure that the new size
    /// will yield a valid utf8 string. This is why the function is marked unsafe, we leave the utf8
    /// validation to you.
    ///
    #[inline]
    pub unsafe fn try_realloc_str(&self, string: &mut str, new_size: usize) -> Option<&mut str> {
        let state = &mut *self.inner.as_ptr();
        let ret = state.try_realloc_str(string, new_size);
        ret
    }

    ///
    /// Allocate a slice of objects filled with a default value
    ///
    /// Returns `None` on failure to allocate
    ///
    #[inline]
    pub fn allocate_slice_default<T: Default>(&self, len: usize) -> Option<&mut [T]> {
        unsafe {
            let state = &mut *self.inner.as_ptr();
            let ret = state.allocate_slice_default(len);
            ret
        }
    }

    ///
    /// Allocate a slice of objects, copying from the source array
    ///
    /// Returns `None` on failure to allocate
    ///
    #[inline]
    pub fn allocate_slice_copy<T: Copy>(&self, source: &[T]) -> Option<&mut [T]> {
        unsafe {
            let state = &mut *self.inner.as_ptr();
            let ret = state.allocate_slice_copy(source);
            ret
        }
    }

    ///
    /// Allocates a slice of objects, cloning from the source array
    ///
    /// Returns `None` on failure to allocate
    ///
    #[inline]
    pub fn allocate_slice_clone<T: Clone>(&self, source: &[T]) -> Option<&mut [T]> {
        unsafe {
            let state = &mut *self.inner.as_ptr();
            let ret = state.allocate_slice_clone(source);
            ret
        }
    }
}
