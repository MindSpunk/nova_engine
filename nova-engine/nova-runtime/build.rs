/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

extern crate nova_compile as compile;
extern crate nova_target as target;

use cmake;
use std::path::Path;
use std::path::PathBuf;
use target::{Architecture, Platform};

fn link_fmod() {
    let project_dir = compile::manifest_dir();

    let dir = match target::build::target_platform() {
        Platform::WindowsGNU | Platform::WindowsMSVC => "thirdparty/windows/x64",
        Platform::Linux => "thirdparty/linux/x86_64",
        Platform::Android => match target::build::target_architecture() {
            Architecture::X8664 => "thirdparty/android/x86_64",
            Architecture::AARCH64 => "thirdparty/android/arm64-v8a",
        },
    };

    let dir = project_dir.join(dir);

    let dir = dir.into_os_string().into_string().unwrap();

    let (studio, core) = match target::build::target_platform() {
        Platform::WindowsGNU | Platform::WindowsMSVC => ("fmodstudio_vc", "fmod_vc"),
        Platform::Linux | Platform::Android => ("fmodstudio", "fmod"),
    };

    println!("cargo:rustc-link-search={}", &dir);
    println!("cargo:rustc-link-lib={}", studio);
    println!("cargo:rustc-link-lib={}", core);

    let mut studio = PathBuf::from(dir.clone());
    let mut core = PathBuf::from(dir.clone());
    match target::build::target_platform() {
        Platform::WindowsGNU | Platform::WindowsMSVC => {
            studio.push("fmodstudio.dll");
            core.push("fmod.dll");
        }
        Platform::Linux | Platform::Android => {
            studio.push("libfmodstudio.so");
            core.push("libfmod.so");
        }
    }

    copy_to_artifacts(&studio);
    copy_to_artifacts(&core);
}

fn compile_and_link_sdl2() {
    match target::build::target_platform() {
        Platform::WindowsGNU | Platform::WindowsMSVC => {
            let mut build = cmake::Config::new("thirdparty/sdl2");
            build.generator("Ninja");
            let out_dir = build.build();

            println!("cargo:rustc-link-search=all={}", &out_dir.display());

            let source = out_dir.join(dll_name());

            copy_to_artifacts(&source);
        }
        Platform::Linux => {}
        Platform::Android => {
            compile_sdl2_for_arch(target::build::target_architecture());
            let mut lib_dir = compile::artifacts_dir();
            lib_dir.push("obj");
            lib_dir.push("local");
            lib_dir.push(target::build::target_architecture().ndk_name());
            //let lib_dir = dunce::canonicalize(&lib_dir).unwrap();
            println!("cargo:rustc-link-search=all={}", lib_dir.display());
        }
    }
}

fn get_ndk_build_file() -> String {
    let ndk_build = std::env::var("ANDROID_HOME").unwrap();
    match target::build::host_platform() {
        Platform::WindowsGNU | Platform::WindowsMSVC => {
            format!("{}\\ndk-bundle\\ndk-build.cmd", &ndk_build)
        }
        Platform::Linux => format!("{}/ndk-bundle/ndk-build", &ndk_build),
        Platform::Android => panic!("Unsupported host"),
    }
}

fn compile_sdl2_for_arch(arch: Architecture) {
    let ndk_build_dir = get_ndk_build_file();
    let mut ndk_build = std::process::Command::new(&ndk_build_dir);

    let out_dir = compile::artifacts_dir();

    let mut obj_dir = out_dir.clone();
    obj_dir.push("obj");
    let obj_dir = format!("NDK_OUT={}", obj_dir.display());

    let mut lib_dir = out_dir.clone();
    lib_dir.push("lib");
    let lib_dir = format!("NDK_LIBS_OUT={}", lib_dir.display());

    ndk_build.arg("NDK_PROJECT_PATH=null");
    ndk_build.arg("APP_BUILD_SCRIPT=Android.mk");
    ndk_build.arg("APP_PLATFORM=android-24");
    ndk_build.arg("APP_STL=c++_shared");
    ndk_build.arg("APP_MODULES=SDL2 SDL2_main");
    ndk_build.arg(&obj_dir);
    ndk_build.arg(&lib_dir);

    let abi = format!("APP_ABI={}", arch.ndk_name());
    ndk_build.arg(&abi);

    ndk_build.current_dir("./thirdparty/sdl2");

    ndk_build.stdout(std::process::Stdio::inherit());
    ndk_build.stderr(std::process::Stdio::inherit());

    println!("ndk-build: {}", &ndk_build_dir);
    let exit_status = ndk_build
        .spawn()
        .expect("Failed to start ndk-build")
        .wait()
        .expect("ndk-build failed unexpectedly");

    if !exit_status.success() {
        panic!("ndk-build failed");
    }
}

fn dll_name() -> &'static str {
    match target::build::target_platform() {
        Platform::WindowsGNU => "libSDL2.dll",
        Platform::WindowsMSVC => "SDL2.dll",
        Platform::Linux | Platform::Android => "libSDL2.so",
    }
}

fn copy_to_artifacts(source: &Path) {
    let mut out_artifact = compile::artifacts_dir();

    if !out_artifact.exists() {
        std::fs::create_dir_all(&out_artifact).unwrap();
    }

    out_artifact.push(source.file_name().unwrap());

    std::fs::copy(source, &out_artifact).unwrap();
}

fn main() {
    compile_and_link_sdl2();
    link_fmod();
}
