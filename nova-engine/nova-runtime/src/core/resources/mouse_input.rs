/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::core::application::Mouse;
use sdl2::event::Event;
use sdl2::mouse::MouseButton;
use sdl2::mouse::MouseWheelDirection;
use std::slice::Iter;

pub struct MouseInput {
    events: Vec<Event>,
    button_states: [bool; 16],
    mouse_pos: (i32, i32),
    mouse_rel: (i32, i32),
    mouse_whl: (i32, i32),
    mouse: Mouse,
}

universe::impl_resource!(MouseInput);

impl MouseInput {
    pub fn new(mouse: Mouse) -> Self {
        Self {
            events: Vec::with_capacity(256),
            button_states: [false; 16],
            mouse_pos: (0, 0),
            mouse_rel: (0, 0),
            mouse_whl: (0, 0),
            mouse,
        }
    }

    ///
    /// Set whether the mouse will be captured in relative mode or in normal detached mode
    ///
    #[inline]
    pub fn relative_mode(&self, enabled: bool) {
        self.mouse.request_relative_mode(enabled);
    }

    ///
    /// Gets the absolute position of the mouse this frame relative to the main window
    ///
    #[inline]
    pub fn mouse_position(&self) -> (i32, i32) {
        self.mouse_pos
    }

    #[inline]
    pub fn get_mouse_position_global(&self) -> (i32, i32) {
        let mut pos: (i32, i32) = (0, 0);

        unsafe {
            let xptr = &mut pos.0 as *mut i32;
            let yptr = &mut pos.1 as *mut i32;

            sdl2::sys::SDL_GetGlobalMouseState(xptr, yptr);
        }

        pos
    }

    #[inline]
    pub fn set_mouse_position_global(&self, pos: (i32, i32)) {
        unsafe {
            sdl2::sys::SDL_WarpMouseGlobal(pos.0, pos.1);
        }
    }

    ///
    /// Gets the cumulative mouse motion since last frame
    ///
    #[inline]
    pub fn mouse_motion(&self) -> (i32, i32) {
        self.mouse_rel
    }

    ///
    /// Gets the cumulative mouse wheel motion since last frame
    ///
    #[inline]
    pub fn mouse_wheel(&self) -> (i32, i32) {
        self.mouse_whl
    }

    ///
    /// Get an iterator over all mouse events
    ///
    #[inline]
    pub fn event_iter(&self) -> Iter<Event> {
        self.events.iter()
    }

    ///
    /// Checks whether the given mouse button is down
    ///
    #[inline]
    pub fn is_mouse_button_down(&self, button: MouseButton) -> bool {
        self.button_states[button as u8 as usize]
    }

    ///
    /// Consumes events and maintains per frame internal state. Should be called once per frame, and
    /// only once per frame.
    ///
    pub fn handle_events(&mut self, events: &[Event]) {
        self.events.clear();
        self.mouse_rel = (0, 0);
        self.mouse_whl = (0, 0);

        for event in events.iter() {
            match event {
                Event::MouseMotion {
                    x, y, xrel, yrel, ..
                } => {
                    self.mouse_pos = (*x, *y);
                    self.mouse_rel.0 += *xrel;
                    self.mouse_rel.1 += *yrel;

                    self.events.push(event.clone());
                }
                Event::MouseButtonDown { mouse_btn, .. } => {
                    let mouse_btn = *mouse_btn;
                    let mouse_btn = mouse_btn as u8 as usize;
                    self.button_states[mouse_btn] = true;

                    self.events.push(event.clone());
                }
                Event::MouseButtonUp { mouse_btn, .. } => {
                    let mouse_btn = *mouse_btn;
                    let mouse_btn = mouse_btn as u8 as usize;
                    self.button_states[mouse_btn] = false;

                    self.events.push(event.clone());
                }
                Event::MouseWheel {
                    x, y, direction, ..
                } => {
                    match direction {
                        MouseWheelDirection::Normal => {
                            self.mouse_whl.0 += *x;
                            self.mouse_whl.1 += *y;
                        }
                        MouseWheelDirection::Flipped => {
                            self.mouse_whl.0 -= *x;
                            self.mouse_whl.1 -= *y;
                        }
                        _ => {}
                    }

                    self.events.push(event.clone());
                }
                _ => {}
            }
        }
    }
}
