/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::core::application::Timer;

pub struct DeltaTimer {
    timer: Timer,
    first: u64,
    current: u64,
    last: u64,
    freq: u64,
}

universe::impl_resource!(DeltaTimer);

impl DeltaTimer {
    pub fn new(timer: Timer) -> Self {
        let freq = timer.performance_frequency();

        // We get these twice + an offset to make sure we NEVER have a 0 delta time
        let first = timer.performance_counter();
        let last = timer.performance_counter();
        let current = timer.performance_counter() + 1;

        Self {
            timer,
            first,
            current,
            last,
            freq,
        }
    }

    pub fn frame(&mut self) {
        self.last = self.current;
        self.current = self.timer.performance_counter();
    }

    pub fn delta_time(&self) -> f64 {
        let current = self.current as f64;
        let last = self.last as f64;
        let freq = self.freq as f64;

        let delta = current - last;
        let delta = delta / freq;

        delta
    }

    pub fn elapsed_time(&self) -> f64 {
        let first = self.first as f64;
        let current = self.current as f64;
        let freq = self.freq as f64;

        let elapsed = current - first;
        let elapsed = elapsed / freq;

        elapsed
    }
}
