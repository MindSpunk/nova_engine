/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

///
/// A wrapper around an Arc<AtomicBool>
///
#[derive(Clone)]
pub struct QuitHandle {
    should_quit: Arc<AtomicBool>,
}

universe::impl_resource!(QuitHandle);

impl QuitHandle {
    ///
    /// New, unique quit handle
    ///
    pub fn new() -> QuitHandle {
        Self {
            should_quit: Arc::new(AtomicBool::new(false)),
        }
    }

    ///
    /// Mark this handle that it should quit
    ///
    pub fn quit(&self) {
        self.should_quit.store(true, Ordering::Relaxed);
    }

    ///
    /// Is this handle set so that it should quit
    ///
    pub fn should_quit(&self) -> bool {
        self.should_quit.load(Ordering::Relaxed)
    }
}
