/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::core::application::ControllerBuffer;
use crate::core::application::ControllerFrame;
use crate::core::application::ControllerState;
use std::ops::Deref;

pub struct Controllers {
    controller_buffer: ControllerBuffer,
    frame: ControllerFrame,
}

universe::impl_resource!(Controllers);

impl Controllers {
    pub fn new(controller_buffer: ControllerBuffer) -> Self {
        Self {
            controller_buffer,
            frame: Default::default(),
        }
    }

    ///
    /// Update the controller state with the latest information
    ///
    pub fn frame(&mut self) {
        self.frame = self.controller_buffer.frame();
    }
}

impl Deref for Controllers {
    type Target = [Option<ControllerState>; 4];

    fn deref(&self) -> &Self::Target {
        &self.frame
    }
}
