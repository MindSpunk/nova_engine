/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::keyboard::Scancode;

use std::slice::Iter;

pub struct KeyboardInput {
    events: Vec<Event>,
    key_states: [bool; 512],
}

universe::impl_resource!(KeyboardInput);

impl KeyboardInput {
    pub fn new() -> Self {
        Self {
            events: Vec::with_capacity(256),
            key_states: [false; 512],
        }
    }

    #[inline]
    pub fn is_key_down_keycode(&self, keycode: Keycode) -> bool {
        let scancode =
            Scancode::from_keycode(keycode).expect("Failed to convert keycode to scancode");

        self.is_key_down_scancode(scancode)
    }

    #[inline]
    pub fn is_key_down_scancode(&self, scancode: Scancode) -> bool {
        let scancode = scancode as i32 as usize;

        self.key_states[scancode]
    }

    #[inline]
    pub fn event_iter(&self) -> Iter<Event> {
        self.events.iter()
    }

    pub fn handle_events(&mut self, events: &[Event]) {
        self.events.clear();

        for event in events.iter() {
            match event {
                Event::KeyDown { scancode, .. } => {
                    if let Some(code) = *scancode {
                        let code = code as i32 as usize;
                        self.key_states[code] = true;
                    }
                    self.events.push(event.clone());
                }
                Event::KeyUp { scancode, .. } => {
                    if let Some(code) = *scancode {
                        let code = code as i32 as usize;
                        self.key_states[code] = false;
                    }
                    self.events.push(event.clone());
                }
                Event::TextEditing { .. } => {
                    self.events.push(event.clone());
                }
                Event::TextInput { .. } => {
                    self.events.push(event.clone());
                }
                _ => {}
            }
        }
    }
}
