/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::math::types::Mat4x4;
use crate::universe::{
    Dependency, MatrixGroupMut, NodeHierarchy, NodeReference, TaskBox, TaskGraphBuilder,
    TransformGroup,
};
use rayon::prelude::*;

pub struct CalculateTransforms {
    pub last_pos: (i32, i32),
}

impl CalculateTransforms {
    pub fn schedule_task(task_graph: &mut TaskGraphBuilder) {
        let task = TaskBox::new(|access| {
            let mut matrices = access.get_matrices_mut().unwrap();
            let transforms = access.get_transforms_ref().unwrap();
            let hierarchy = access.get_node_hierarchy_ref().unwrap();

            let mut roots = Vec::new();

            for (node, item) in hierarchy.iter_everything() {
                if item.parent.is_null() {
                    roots.push(*node);
                }
            }

            #[derive(Copy, Clone)]
            struct PtrGroup<'a> {
                matrices: *mut MatrixGroupMut<'a>,
                transforms: *const TransformGroup<'a>,
                hierarchy: *const NodeHierarchy<'a>,
            }

            unsafe impl<'a> Send for PtrGroup<'a> {}
            unsafe impl<'a> Sync for PtrGroup<'a> {}

            fn handle_child(node: NodeReference, pointers: PtrGroup) {
                let matrices = unsafe { &mut *pointers.matrices };
                let transforms = unsafe { &*pointers.transforms };
                let hierarchy = unsafe { &*pointers.hierarchy };

                let parent = hierarchy.get_parent(node);

                let mut matrix = {
                    if !parent.is_null() {
                        matrices[parent].clone()
                    } else {
                        Mat4x4::identity()
                    }
                };

                let transform = &transforms[node];

                let translation = Mat4x4::translation(transform.pos);
                let rotation: Mat4x4 = transform.rot.into();
                let scaling = Mat4x4::scaling(transform.scl);

                matrix = matrix * translation * rotation * scaling;

                matrices[node] = matrix;

                hierarchy
                    .get_children(node)
                    .par_iter()
                    .for_each(move |node| {
                        handle_child(*node, pointers);
                    });
            }

            let pointers = PtrGroup {
                matrices: &mut matrices as *mut _,
                transforms: &transforms as *const _,
                hierarchy: &hierarchy as *const _,
            };

            roots.par_iter().for_each(|root| {
                handle_child(*root, pointers);
            });
        });

        task_graph.add_task(task, |deps| {
            deps.write(Dependency::matrices());
            deps.read(Dependency::node_hierarchy());
            deps.read(Dependency::transforms());
        });
    }
}
