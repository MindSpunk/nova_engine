/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::math::traits::IntoSTD140;
use crate::math::traits::Pack;
use crate::math::types::Mat4x4;

#[repr(C)]
#[derive(Clone, Debug)]
pub struct ModelLayout {
    pub model_matrix: Mat4x4,
    pub normal_matrix: Mat4x4,
}

impl Pack for ModelLayout {
    type GLSLOutput = ModelLayoutPacked;
    type HLSLOutput = ModelLayoutPacked;
    type GLSLOutputArray = ModelLayoutPacked;
    type HLSLOutputArray = ModelLayoutPacked;
    type CPUOutput = ModelLayoutPacked;

    fn into_packed_glsl(self) -> Self::GLSLOutput {
        let mpack = self.model_matrix.into_std140().into_packed_glsl();
        let npack = self.normal_matrix.into_std140().into_packed_glsl();
        ModelLayoutPacked { a: mpack, b: npack }
    }

    fn into_packed_hlsl(self) -> Self::HLSLOutput {
        let mpack = self.model_matrix.into_std140().into_packed_hlsl();
        let npack = self.normal_matrix.into_std140().into_packed_hlsl();
        ModelLayoutPacked { a: mpack, b: npack }
    }

    fn into_packed_glsl_array(self) -> Self::GLSLOutputArray {
        let mpack = self.model_matrix.into_std140().into_packed_glsl_array();
        let npack = self.normal_matrix.into_std140().into_packed_glsl_array();
        ModelLayoutPacked { a: mpack, b: npack }
    }

    fn into_packed_hlsl_array(self) -> Self::HLSLOutputArray {
        let mpack = self.model_matrix.into_std140().into_packed_hlsl_array();
        let npack = self.normal_matrix.into_std140().into_packed_hlsl_array();
        ModelLayoutPacked { a: mpack, b: npack }
    }

    fn into_packed_cpu(self) -> Self::CPUOutput {
        let mpack = self.model_matrix.into_std140().into_packed_cpu();
        let npack = self.normal_matrix.into_std140().into_packed_cpu();
        ModelLayoutPacked { a: mpack, b: npack }
    }
}

#[repr(C)]
#[derive(Clone, Debug)]
pub struct ModelLayoutPacked {
    a: [f32; 16],
    b: [f32; 16],
}
