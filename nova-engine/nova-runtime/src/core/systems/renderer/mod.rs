/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

mod data;

use crate::ash;
use crate::ash::version::DeviceV1_0;
use crate::ash::version::InstanceV1_0;
use crate::ash::vk;
use crate::core::application::Window;
use crate::core::nodes::Camera;
use crate::core::nodes::StaticMesh;
use crate::core::resources::WindowResource;
use crate::cstring::cstr;
use crate::fs::filesystem::Filesystem;
use crate::fs::path::Path;
use crate::math::traits::Inverse;
use crate::math::traits::Pack;
use crate::math::traits::Transpose;
use crate::math::types::Mat4x4;
use crate::math::types::Vec3;
use crate::traits::AsDataSlice;
use crate::traits::AsSlice;
use crate::universe::{Dependency, NodeReference, TaskBox, TaskGraphBuilder};
use crate::vma;
use crate::vulkan;
use crate::vulkan::QueueFamily;
use crate::vulkan::SwapchainAquireError;
use crate::vulkan::VertexFormat;
use crate::vulkan::VulkanContext;
use crate::vulkan::VulkanDevice;
use crate::vulkan::VulkanSurface;
use crate::vulkan::VulkanSwapchain;

use std::io::Read;
use std::slice::from_raw_parts_mut;

use self::data::CameraLayout;
use self::data::ModelLayout;
use self::data::VertexData;

use std::mem;
use std::time::Duration;

pub struct Renderer {
    active_camera: NodeReference,

    vk_context: VulkanContext,
    vk_surface: VulkanSurface,
    vk_device: VulkanDevice,
    vk_allocator: vma::Allocator,
    vk_swapchain: VulkanSwapchain,

    staging_pool: vk::CommandPool,
    render_pool: vk::CommandPool,

    vert_module: vk::ShaderModule,
    frag_module: vk::ShaderModule,

    descriptor_pool: vk::DescriptorPool,
    descriptor_set: vk::DescriptorSet,

    set_layout: vk::DescriptorSetLayout,
    pipeline_layout: vk::PipelineLayout,
    pipeline: vk::Pipeline,

    unif_buffer: vk::Buffer,
    unif_alloc: vma::Allocation,

    vert_buffer: vk::Buffer,
    vert_alloc: vma::Allocation,

    indx_buffer: vk::Buffer,
    indx_alloc: vma::Allocation,
    indx_count: u32,

    queue: vk::Queue,
    queue_family: QueueFamily,
    render_pass: vk::RenderPass,
    image_views: Vec<vk::ImageView>,
    depth_images: Vec<(vk::Image, vma::Allocation)>,
    depth_image_views: Vec<vk::ImageView>,
    depth_format: vk::Format,
    framebuffers: Vec<vk::Framebuffer>,

    command_buffers: Vec<vk::CommandBuffer>,

    acquire_semaphore: vk::Semaphore,
    present_semaphore: vk::Semaphore,
}

universe::impl_resource!(Renderer);

impl Renderer {
    pub fn new(window: &Window, fs: &Filesystem) -> Self {
        // =========================================================================================
        //  Context Creation
        // =========================================================================================
        let vk_context = unsafe {
            VulkanContext::builder()
                .application_name(cstr!("NovaTest"))
                .app_version(0, 3, 0)
                .api_version(1, 0, 0)
                .debug()
                .support_surface(window)
                .build()
        };

        // =========================================================================================
        //  Surface Creation
        // =========================================================================================
        let vk_surface = unsafe { VulkanSurface::builder(&vk_context, window).build() };

        // =========================================================================================
        //  Device Creation
        // =========================================================================================
        let vk_device = unsafe {
            VulkanDevice::builder(&vk_context)
                .supports_surface(&vk_surface)
                .name(cstr!("Main Device"))
                .build()
        };

        // =========================================================================================
        //  Allocator Creation
        // =========================================================================================
        let vk_allocator = unsafe {
            vma::Allocator::builder()
                .instance(&vk_context)
                .device(&vk_device)
                .physical_device(vk_device.gpu())
                .build()
                .expect("Failed to create vma allocator")
        };

        // =========================================================================================
        //  Swapchain Creation
        // =========================================================================================
        let vk_swapchain = unsafe {
            VulkanSwapchain::builder(&vk_surface, &window, &vk_context, &vk_device)
                .mailbox()
                .triple_buffer()
                .build()
        };

        // =========================================================================================
        //  Swapchain Semaphores
        // =========================================================================================
        let acquire_semaphore = unsafe {
            vk_device
                .build_semaphore(None)
                .expect("Failed to create aquire semaphore")
        };
        let present_semaphore = unsafe {
            vk_device
                .build_semaphore(None)
                .expect("Failed to create present semaphore")
        };

        // =========================================================================================
        //  Queue Creation
        // =========================================================================================
        let queue_family = vk_device
            .get_general_queue_family()
            .expect("No valid queue family available");
        let queue = unsafe { vk_device.get_device_queue(queue_family.i, 0) };

        // =========================================================================================
        //  Swap Framebuffer Creation
        // =========================================================================================
        let format = *vk_swapchain.format();
        let depth_format = select_depth_format(&vk_context, &vk_device)
            .expect("Failed to find a supported depth buffer format");
        let depth_images =
            create_depth_images(&vk_allocator, &vk_swapchain, depth_format, &queue_family);
        let depth_image_views = create_depth_image_views(&vk_device, &depth_images, depth_format);
        let image_views = create_image_views(&vk_device, &vk_swapchain);
        let render_pass = create_render_pass(&vk_device, format, depth_format);
        let framebuffers = create_framebuffers(
            &vk_device,
            &vk_swapchain,
            render_pass,
            &image_views,
            &depth_image_views,
        );

        // =========================================================================================
        //  Command Pool Creation
        // =========================================================================================
        let staging_pool = vk::CommandPoolCreateInfo::builder()
            .queue_family_index(queue_family.i)
            .flags(vk::CommandPoolCreateFlags::TRANSIENT)
            .build();
        let staging_pool = unsafe {
            vk_device
                .create_command_pool(&staging_pool, None)
                .expect("Failed to create staging command pool")
        };
        vk_device.name_command_pool(staging_pool, cstr!("Command Pool: Staging"));

        let render_pool = vk::CommandPoolCreateInfo::builder()
            .queue_family_index(queue_family.i)
            .flags(vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER)
            .build();
        let render_pool = unsafe {
            vk_device
                .create_command_pool(&render_pool, None)
                .expect("Failed to create render command pool")
        };
        vk_device.name_command_pool(render_pool, cstr!("Command Pool: Rendering"));

        let command_buffers = vk::CommandBufferAllocateInfo::builder()
            .command_buffer_count(vk_swapchain.images().len() as u32)
            .command_pool(render_pool)
            .level(vk::CommandBufferLevel::PRIMARY)
            .build();
        let command_buffers = unsafe {
            vk_device
                .allocate_command_buffers(&command_buffers)
                .expect("Failed to allocate command buffers")
        };

        // =========================================================================================
        //  Shader Module Creation
        // =========================================================================================
        let path = Path::new("assets/spirv/shaders/basic3d/vert_3d_phong.vert.spv");
        let vert_module = load_spriv_module_from_file(&vk_device, fs, path);
        vk_device.name_shader_module(vert_module, cstr!("Shader: <Phong 3D> Vert"));

        let path = Path::new("assets/spirv/shaders/basic3d/frag_3d_phong.frag.spv");
        let frag_module = load_spriv_module_from_file(&vk_device, fs, path);
        vk_device.name_shader_module(frag_module, cstr!("Shader: <Phong 3D> Frag"));

        // =========================================================================================
        //  Descriptor Set Layout Creation
        // =========================================================================================
        let descriptor_layout_binding = vk::DescriptorSetLayoutBinding::builder()
            .binding(0)
            .descriptor_count(1)
            .descriptor_type(vk::DescriptorType::UNIFORM_BUFFER)
            .stage_flags(vk::ShaderStageFlags::VERTEX | vk::ShaderStageFlags::FRAGMENT)
            .build();
        let bindings = [descriptor_layout_binding];
        let set_layout = vk::DescriptorSetLayoutCreateInfo::builder()
            .bindings(&bindings)
            .build();
        let set_layout = unsafe {
            vk_device
                .create_descriptor_set_layout(&set_layout, None)
                .expect("Failed to create descriptor set layout")
        };
        vk_device.name_descriptor_set_layout(set_layout, cstr!("Descriptor Set Layout: Camera"));

        // =========================================================================================
        //  Descriptor Pool Creation
        // =========================================================================================
        let pool_sizes = vk::DescriptorPoolSize::builder()
            .ty(vk::DescriptorType::UNIFORM_BUFFER)
            .descriptor_count(256)
            .build();
        let pool_sizes = [pool_sizes];
        let descriptor_pool = vk::DescriptorPoolCreateInfo::builder()
            .pool_sizes(&pool_sizes)
            .max_sets(2)
            .build();
        let descriptor_pool = unsafe {
            vk_device
                .create_descriptor_pool(&descriptor_pool, None)
                .expect("Failed to create descriptor pool")
        };
        vk_device.name_descriptor_pool(descriptor_pool, cstr!("Descriptor Pool: Main"));

        // =========================================================================================
        //  Descriptor Set Creation
        // =========================================================================================
        let set_layouts = [set_layout];
        let descriptor_set = vk::DescriptorSetAllocateInfo::builder()
            .descriptor_pool(descriptor_pool)
            .set_layouts(&set_layouts)
            .build();
        let descriptor_set = unsafe {
            vk_device
                .allocate_descriptor_sets(&descriptor_set)
                .expect("Failed to allocate descriptor set")[0]
        };
        vk_device.name_descriptor_set(descriptor_set, cstr!("Descriptor Set: Camera"));

        // =========================================================================================
        //  Pipeline Layout Creation
        // =========================================================================================
        let push_constant_ranges = vk::PushConstantRange::builder()
            .stage_flags(vk::ShaderStageFlags::VERTEX)
            .size(128)
            .offset(0)
            .build();
        let push_constant_ranges = [push_constant_ranges];
        let set_layouts = [set_layout];
        let pipeline_layout = vk::PipelineLayoutCreateInfo::builder()
            .push_constant_ranges(&push_constant_ranges)
            .set_layouts(&set_layouts)
            .build();
        let pipeline_layout = unsafe {
            vk_device
                .create_pipeline_layout(&pipeline_layout, None)
                .expect("Failed to create pipeline layout")
        };
        vk_device.name_pipeline_layout(pipeline_layout, cstr!("Pipeline Layout: Main"));

        // =========================================================================================
        //  Pipeline Creation
        // =========================================================================================
        let swap_extent = *vk_swapchain.extent();
        let pipeline = create_pipeline(
            &vk_device,
            render_pass,
            pipeline_layout,
            vert_module,
            frag_module,
            swap_extent,
        );

        let active_camera = NodeReference::null();
        let mut renderer = Self {
            active_camera,
            vk_context,
            vk_surface,
            vk_device,
            vk_allocator,
            vk_swapchain,
            staging_pool,
            render_pool,
            vert_module,
            frag_module,
            descriptor_pool,
            descriptor_set,
            set_layout,
            pipeline_layout,
            pipeline,
            unif_buffer: Default::default(),
            unif_alloc: Default::default(),
            vert_buffer: Default::default(),
            vert_alloc: Default::default(),
            indx_buffer: Default::default(),
            indx_alloc: Default::default(),
            indx_count: 0,
            queue,
            queue_family,
            render_pass,
            image_views,
            depth_images,
            depth_format,
            depth_image_views,
            framebuffers,
            command_buffers,
            acquire_semaphore,
            present_semaphore,
        };

        // =========================================================================================
        //  Vertex/Index/Uniform Buffer Creation
        // =========================================================================================
        let mesh = Self::create_mesh();

        let staging_cmd = vk::CommandBufferAllocateInfo::builder()
            .command_buffer_count(1)
            .command_pool(staging_pool)
            .level(vk::CommandBufferLevel::PRIMARY)
            .build();
        let staging_cmd = unsafe {
            *renderer
                .vk_device
                .allocate_command_buffers(&staging_cmd)
                .expect("Failed to allocate staging command buffer")
                .get(0)
                .unwrap()
        };

        renderer.begin_one_time_submit(staging_cmd);
        renderer.vk_device.begin_command_section(
            staging_cmd,
            cstr!("Upload Vertex Buffer"),
            [1.0, 1.0, 1.0, 1.0],
        );

        // UPLOAD VERTEX BUFFER
        let vert_buffer = renderer.create_vertex_buffer(staging_cmd, &mesh.0);
        renderer.vert_buffer = vert_buffer.0;
        renderer.vert_alloc = vert_buffer.1;
        renderer
            .vk_device
            .name_buffer(vert_buffer.0, cstr!("Mesh: <Sqaure> V"));
        renderer.vk_device.end_command_section(staging_cmd);

        // UPLOAD INDEX BUFFER
        renderer.vk_device.begin_command_section(
            staging_cmd,
            cstr!("Upload Index Buffer"),
            [1.0, 1.0, 1.0, 1.0],
        );
        let indx_buffer = renderer.create_index_buffer(staging_cmd, &mesh.1);
        renderer.indx_buffer = indx_buffer.0;
        renderer.indx_alloc = indx_buffer.1;
        renderer.indx_count = mesh.1.len() as u32;
        renderer
            .vk_device
            .name_buffer(indx_buffer.0, cstr!("Mesh: <Square> I"));
        renderer.vk_device.end_command_section(staging_cmd);

        // CREATE UNIFORM BUFFER
        let unif_buffer = renderer.create_uniform_buffer(144);
        renderer.unif_buffer = unif_buffer.0;
        renderer.unif_alloc = unif_buffer.1;
        renderer
            .vk_device
            .name_buffer(unif_buffer.0, cstr!("Buffer: <Camera> U"));

        let descriptor_buffers = vk::DescriptorBufferInfo::builder()
            .buffer(unif_buffer.0)
            .range(144)
            .offset(0)
            .build();
        let descriptor_buffers = [descriptor_buffers];
        let descriptor_write = vk::WriteDescriptorSet::builder()
            .descriptor_type(vk::DescriptorType::UNIFORM_BUFFER)
            .buffer_info(&descriptor_buffers)
            .dst_set(descriptor_set)
            .dst_binding(0)
            .dst_array_element(0)
            .build();
        unsafe {
            renderer
                .vk_device
                .update_descriptor_sets(&[descriptor_write], &[]);
        }

        unsafe {
            renderer
                .vk_device
                .end_command_buffer(staging_cmd)
                .expect("Failed to end command buffer");

            let buffers = [staging_cmd];
            let submit_info = vk::SubmitInfo::builder().command_buffers(&buffers).build();
            let submit_info = [submit_info];

            renderer.vk_device.begin_queue_section(
                renderer.queue,
                cstr!("Static Resource Load"),
                [1.0, 1.0, 1.0, 1.0],
            );
            renderer
                .vk_device
                .queue_submit(renderer.queue, &submit_info, vk::Fence::default())
                .expect("Failed to submit command buffer");
            renderer.vk_device.end_queue_section(renderer.queue);

            renderer
                .vk_device
                .device_wait_idle()
                .expect("Failed to wait on device idle");

            renderer
                .vk_allocator
                .destroy_buffer(vert_buffer.2, vert_buffer.3);
            renderer
                .vk_allocator
                .destroy_buffer(indx_buffer.2, indx_buffer.3);
        }

        renderer
    }

    ///
    ///
    ///
    pub fn schedule_task(task_graph: &mut TaskGraphBuilder) {
        let task = TaskBox::new(|access| {
            // =========================================================================================
            //  Get Node and Resource Access
            // =========================================================================================
            let cameras = access.get_nodes_ref::<Camera>().unwrap();
            let meshes = access.get_nodes_ref::<StaticMesh>().unwrap();
            let window = access.get_resource_ref::<WindowResource>().unwrap();
            let renderer = access.get_resource_mut::<Renderer>().unwrap();
            let transforms = access.get_transforms_ref().unwrap();
            let matrices = access.get_matrices_ref().unwrap();

            // =========================================================================================
            //  Select an Active Camera if One Doesn't Exist
            // =========================================================================================
            if renderer.active_camera.is_null() {
                cameras.for_each(|node, _| {
                    renderer.active_camera = node.into();
                });
            } else if let Some(camera) = renderer.active_camera.to_typed::<Camera>() {
                if !cameras.is_valid(camera) {
                    cameras.for_each(|node, _| {
                        renderer.active_camera = node.into();
                    });
                }
            }

            // =========================================================================================
            //  Wait Until the previous frame has finished rendering
            // =========================================================================================
            unsafe {
                renderer
                    .vk_device
                    .queue_wait_idle(renderer.queue)
                    .expect("Failed to wait on queue");
            }

            // =========================================================================================
            //  Grab a few resources
            // =========================================================================================
            let (image_index, _) = {
                if let Some(swap) = renderer.swap_acquire(window) {
                    swap
                } else {
                    return;
                }
            };

            let command_buffer = renderer.command_buffers[image_index];
            let framebuffer = renderer.framebuffers[image_index];

            // =========================================================================================
            //  Update Camera Uniform Buffer
            // =========================================================================================
            unsafe {
                let camera_node = renderer.active_camera.to_typed().unwrap();
                let camera = &cameras[camera_node];

                let view_matrix = matrices[camera_node.into()].clone().inverse();

                let proj_matrix = Mat4x4::perspective(
                    renderer.vk_swapchain.aspect(),
                    camera.fov,
                    camera.near,
                    camera.far,
                );

                let cam_pos = transforms[camera_node.into()].pos;

                let data = CameraLayout {
                    view_matrix,
                    proj_matrix,
                    cam_pos,
                };

                let staging = vk::CommandBufferAllocateInfo::builder()
                    .command_buffer_count(1)
                    .command_pool(renderer.staging_pool)
                    .level(vk::CommandBufferLevel::PRIMARY)
                    .build();

                let staging = renderer
                    .vk_device
                    .allocate_command_buffers(&staging)
                    .expect("Failed to allocate staging command buffer")[0];

                renderer.begin_one_time_submit(staging);
                renderer.vk_device.begin_command_section(
                    staging,
                    cstr!("Upload Camera Buffer"),
                    [1.0, 1.0, 1.0, 1.0],
                );
                let data = data.into_packed_hlsl();
                let data_ptr = data.as_data_slice();
                let buf = renderer.update_uniform_buffer(staging, renderer.unif_buffer, data_ptr);
                renderer.vk_device.end_command_section(staging);
                renderer
                    .vk_device
                    .end_command_buffer(staging)
                    .expect("Failed to end command buffer");

                renderer.vk_device.begin_queue_section(
                    renderer.queue,
                    cstr!("Upload"),
                    [1.0, 1.0, 1.0, 1.0],
                );
                let buffers = [staging];
                let submit_info = vk::SubmitInfo::builder().command_buffers(&buffers).build();
                let submit_info = [submit_info];
                renderer
                    .vk_device
                    .queue_submit(renderer.queue, &submit_info, vk::Fence::default())
                    .expect("Failed to submit command buffer");
                renderer.vk_device.end_queue_section(renderer.queue);

                renderer
                    .vk_device
                    .queue_wait_idle(renderer.queue)
                    .expect("Failed to wait on queue");

                renderer.vk_allocator.destroy_buffer(buf.0, buf.1);
                renderer
                    .vk_device
                    .free_command_buffers(renderer.staging_pool, &[staging]);
            }

            // =========================================================================================
            //  Record Rendering Command Buffer
            // =========================================================================================
            renderer.begin_one_time_submit(command_buffer);
            renderer.vk_device.begin_command_section(
                command_buffer,
                cstr!("Initial Binding"),
                [1.0, 0.5, 0.0, 1.0],
            );
            renderer.record_draw_setup(command_buffer, framebuffer);
            renderer.vk_device.end_command_section(command_buffer);

            renderer.vk_device.begin_command_section(
                command_buffer,
                cstr!("Rendering Meshes"),
                [1.0, 0.5, 0.0, 1.0],
            );

            meshes.for_each(|node, _| {
                renderer.vk_device.begin_command_section(
                    command_buffer,
                    cstr!("Draw Mesh"),
                    [1.0, 1.0, 1.0, 1.0],
                );
                let matrix = &matrices[node.into()];
                renderer.record_draw(command_buffer, matrix);
                renderer.vk_device.end_command_section(command_buffer);
            });
            renderer.vk_device.end_command_section(command_buffer);

            renderer.vk_device.begin_command_section(
                command_buffer,
                cstr!("Rendering Finalization"),
                [1.0, 0.5, 0.0, 1.0],
            );
            renderer.record_draw_end(command_buffer);
            renderer.vk_device.end_command_section(command_buffer);

            renderer.vk_device.begin_queue_section(
                renderer.queue,
                cstr!("Rendering"),
                [1.0, 1.0, 1.0, 1.0],
            );
            let buffers = [command_buffer];
            let waits = [renderer.acquire_semaphore];
            let signals = [renderer.present_semaphore];
            let wait_dst = [vk::PipelineStageFlags::TOP_OF_PIPE];
            let submits = vk::SubmitInfo::builder()
                .command_buffers(&buffers)
                .wait_semaphores(&waits)
                .signal_semaphores(&signals)
                .wait_dst_stage_mask(&wait_dst)
                .build();
            let submits = [submits];
            unsafe {
                renderer
                    .vk_device
                    .queue_submit(renderer.queue, &submits, vk::Fence::default())
                    .expect("Failed to submit queue");
            }
            renderer.vk_device.end_queue_section(renderer.queue);

            unsafe {
                let wait = [renderer.present_semaphore];
                renderer
                    .vk_swapchain
                    .present(renderer.queue, image_index, &wait);
            }
        });
        task_graph.add_task(task, |deps| {
            deps.read(Dependency::node::<StaticMesh>());
            deps.read(Dependency::node::<Camera>());
            deps.read(Dependency::resource::<WindowResource>());
            deps.read(Dependency::matrices());
            deps.read(Dependency::transforms());
            deps.write(Dependency::resource::<Renderer>());
        });
    }
}

impl Drop for Renderer {
    fn drop(&mut self) {
        unsafe {
            self.vk_device
                .device_wait_idle()
                .expect("Failed to wait on device");

            for fb in self.framebuffers.iter() {
                self.vk_device.destroy_framebuffer(*fb, None);
            }

            for iv in self.image_views.iter() {
                self.vk_device.destroy_image_view(*iv, None);
            }

            for iv in self.depth_image_views.iter() {
                self.vk_device.destroy_image_view(*iv, None);
            }

            for i in self.depth_images.iter() {
                self.vk_device.destroy_image(i.0, None);
                self.vk_allocator.free_memory(i.1);
            }

            self.vk_device
                .destroy_descriptor_pool(self.descriptor_pool, None);
            self.vk_allocator
                .destroy_buffer(self.unif_buffer, self.unif_alloc);
            self.vk_allocator
                .destroy_buffer(self.vert_buffer, self.vert_alloc);
            self.vk_allocator
                .destroy_buffer(self.indx_buffer, self.indx_alloc);
            self.vk_device.destroy_pipeline(self.pipeline, None);
            self.vk_device
                .destroy_pipeline_layout(self.pipeline_layout, None);
            self.vk_device
                .destroy_descriptor_set_layout(self.set_layout, None);
            self.vk_device.destroy_shader_module(self.frag_module, None);
            self.vk_device.destroy_shader_module(self.vert_module, None);
            self.vk_device.destroy_render_pass(self.render_pass, None);
            self.vk_device.destroy_command_pool(self.staging_pool, None);
            self.vk_device.destroy_command_pool(self.render_pool, None);
            self.vk_device
                .destroy_semaphore(self.present_semaphore, None);
            self.vk_device
                .destroy_semaphore(self.acquire_semaphore, None);
            self.vk_swapchain.destroy();
            self.vk_allocator.destroy();
            self.vk_device.destroy();
            self.vk_surface.destroy();
            self.vk_context.destroy();
        }
    }
}

// =================================================================================================
//  Renderer utility functions
// =================================================================================================

impl Renderer {
    #[inline]
    fn record_draw_setup(&self, command_buffer: vk::CommandBuffer, framebuffer: vk::Framebuffer) {
        let render_area = vk::Rect2D::builder()
            .offset(vk::Offset2D { x: 0, y: 0 })
            .extent(*self.vk_swapchain.extent())
            .build();

        let mut clear_color = vk::ClearColorValue::default();
        clear_color.float32 = [0.0f32, 0.0f32, 0.0f32, 1.0f32];
        let mut clear_value = vk::ClearValue::default();
        clear_value.color = clear_color;

        let clear_depth = vk::ClearDepthStencilValue::builder()
            .depth(1.0)
            .stencil(0)
            .build();
        let mut clear_depth_value = vk::ClearValue::default();
        clear_depth_value.depth_stencil = clear_depth;

        let clear_values = [clear_value, clear_depth_value];

        let begin_info = vk::RenderPassBeginInfo::builder()
            .render_pass(self.render_pass)
            .framebuffer(framebuffer)
            .render_area(render_area)
            .clear_values(&clear_values)
            .build();

        unsafe {
            self.vk_device.cmd_begin_render_pass(
                command_buffer,
                &begin_info,
                vk::SubpassContents::INLINE,
            );

            self.vk_device.cmd_bind_pipeline(
                command_buffer,
                vk::PipelineBindPoint::GRAPHICS,
                self.pipeline,
            );
            self.vk_device.cmd_bind_descriptor_sets(
                command_buffer,
                vk::PipelineBindPoint::GRAPHICS,
                self.pipeline_layout,
                0,
                &[self.descriptor_set],
                &[],
            );
        }
    }

    #[inline]
    fn record_draw(&self, command_buffer: vk::CommandBuffer, matrix: &Mat4x4) {
        unsafe {
            let buffers = [self.vert_buffer];
            let offsets = [0];
            self.vk_device
                .cmd_bind_vertex_buffers(command_buffer, 0, &buffers, &offsets);

            self.vk_device.cmd_bind_index_buffer(
                command_buffer,
                self.indx_buffer,
                0,
                vk::IndexType::UINT16,
            );

            let model_matrix = matrix.clone();
            let normal_matrix = model_matrix.clone().inverse().transpose();

            let model_data = ModelLayout {
                model_matrix,
                normal_matrix,
            };

            let data = model_data.into_packed_hlsl();
            let data_ptr = data.as_data_slice();

            self.vk_device.cmd_push_constants(
                command_buffer,
                self.pipeline_layout,
                vk::ShaderStageFlags::VERTEX,
                0,
                data_ptr,
            );

            self.vk_device
                .cmd_draw_indexed(command_buffer, self.indx_count, 1, 0, 0, 0);
        }
    }

    #[inline]
    fn record_draw_end(&self, command_buffer: vk::CommandBuffer) {
        unsafe {
            self.vk_device.cmd_end_render_pass(command_buffer);
            self.vk_device
                .end_command_buffer(command_buffer)
                .expect("Failed to end command buffer");
        }
    }

    #[allow(unused_assignments)]
    #[inline]
    fn swap_acquire(&mut self, window: &Window) -> Option<(usize, vk::Image)> {
        let mut swapchain = (0, vk::Image::default());

        // We loop until we have acquired a swapchain image, rebuilding it if we need to.
        // If we time out then we've probably hit an error so we should just crash.
        loop {
            let timeout = Duration::from_secs(5);
            let fence = vk::Fence::default();
            let result = self
                .vk_swapchain
                .acquire_next(timeout, self.acquire_semaphore, fence);
            match result {
                Ok(out) => {
                    swapchain = (out.0 as usize, out.1);
                    break;
                }
                Err(SwapchainAquireError::OutOfDate) => {
                    if !self.swap_recreate(window) {
                        return None;
                    }
                }
                Err(SwapchainAquireError::Timeout) => {
                    panic!("Waited for over 5 seconds for swapchain image, something is wrong");
                }
            };
        }
        Some(swapchain)
    }

    #[inline]
    fn swap_recreate(&mut self, window: &Window) -> bool {
        unsafe {
            self.vk_device
                .device_wait_idle()
                .expect("Failed to wait on device");

            if let Err(_e) = self.vk_swapchain.rebuild(window, &self.vk_context) {
                return false;
            }

            for fb in self.framebuffers.iter() {
                self.vk_device.destroy_framebuffer(*fb, None);
            }

            for iv in self.image_views.iter() {
                self.vk_device.destroy_image_view(*iv, None);
            }

            for iv in self.depth_image_views.iter() {
                self.vk_device.destroy_image_view(*iv, None);
            }

            for i in self.depth_images.iter() {
                self.vk_device.destroy_image(i.0, None);
                self.vk_allocator.free_memory(i.1);
            }

            self.image_views = create_image_views(&self.vk_device, &self.vk_swapchain);

            self.depth_format = select_depth_format(&self.vk_context, &self.vk_device)
                .expect("Failed to find a supported depth buffer format");
            self.depth_images = create_depth_images(
                &self.vk_allocator,
                &self.vk_swapchain,
                self.depth_format,
                &self.queue_family,
            );

            self.depth_image_views =
                create_depth_image_views(&self.vk_device, &self.depth_images, self.depth_format);

            self.framebuffers = create_framebuffers(
                &self.vk_device,
                &self.vk_swapchain,
                self.render_pass,
                &self.image_views,
                &self.depth_image_views,
            );

            self.vk_device.destroy_pipeline(self.pipeline, None);

            let swap_extent = *self.vk_swapchain.extent();
            self.pipeline = create_pipeline(
                &self.vk_device,
                self.render_pass,
                self.pipeline_layout,
                self.vert_module,
                self.frag_module,
                swap_extent,
            );
        }
        true
    }

    #[inline]
    fn begin_one_time_submit(&self, command_buffer: vk::CommandBuffer) {
        unsafe {
            let begin_info = vk::CommandBufferBeginInfo::builder()
                .flags(vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT)
                .build();
            self.vk_device
                .begin_command_buffer(command_buffer, &begin_info)
                .expect("Failed to begin command buffer");
        }
    }

    #[inline]
    fn create_vertex_buffer(
        &self,
        command_buffer: vk::CommandBuffer,
        data: &[[f32; 11]],
    ) -> (vk::Buffer, vma::Allocation, vk::Buffer, vma::Allocation) {
        let size = (data.len() * (mem::size_of::<f32>() * 11)) as u64;

        let vertex = self.create_staged_buffer(size, vk::BufferUsageFlags::VERTEX_BUFFER);
        let (buffer, allocation) = vertex;

        let staging = self.create_staging_buffer(size);
        let (staging_buffer, staging_allocation) = staging;

        self.map_and_copy(&staging_allocation, data.as_ptr() as *const u8, size);
        self.stage_buffer(command_buffer, staging_buffer, buffer, size);

        (buffer, allocation, staging_buffer, staging_allocation)
    }

    #[inline]
    fn create_index_buffer(
        &self,
        command_buffer: vk::CommandBuffer,
        data: &[u16],
    ) -> (vk::Buffer, vma::Allocation, vk::Buffer, vma::Allocation) {
        let size = (data.len() * (mem::size_of::<u16>())) as u64;

        let vertex = self.create_staged_buffer(size, vk::BufferUsageFlags::INDEX_BUFFER);
        let (buffer, allocation) = vertex;

        let staging = self.create_staging_buffer(size);
        let (staging_buffer, staging_allocation) = staging;

        self.map_and_copy(&staging_allocation, data.as_ptr() as *const u8, size);
        self.stage_buffer(command_buffer, staging_buffer, buffer, size);

        (buffer, allocation, staging_buffer, staging_allocation)
    }

    fn create_uniform_buffer(&self, size: usize) -> (vk::Buffer, vma::Allocation) {
        let size = size as u64;

        let uniform = self.create_staged_buffer(size, vk::BufferUsageFlags::UNIFORM_BUFFER);
        let (buffer, allocation) = uniform;

        (buffer, allocation)
    }

    fn update_uniform_buffer(
        &self,
        command_buffer: vk::CommandBuffer,
        buffer: vk::Buffer,
        data: &[u8],
    ) -> (vk::Buffer, vma::Allocation) {
        let size = data.len() as u64;
        let staging = self.create_staging_buffer(size);
        let (staging_buffer, staging_allocation) = staging;

        self.map_and_copy(&staging_allocation, data.as_ptr(), size);
        self.stage_buffer(command_buffer, staging_buffer, buffer, size);

        (staging_buffer, staging_allocation)
    }

    #[inline]
    fn map_and_copy(&self, allocation: &vma::Allocation, src: *const u8, size: u64) {
        unsafe {
            let dst = self
                .vk_allocator
                .map_memory(allocation)
                .expect("Failed to map staging memory");

            std::ptr::copy_nonoverlapping(src, dst, size as usize);

            self.vk_allocator.unmap_memory(allocation);
        }
    }

    #[inline]
    fn stage_buffer(
        &self,
        command_buffer: vk::CommandBuffer,
        src: vk::Buffer,
        dst: vk::Buffer,
        size: u64,
    ) {
        unsafe {
            let region = vk::BufferCopy::builder()
                .size(size)
                .src_offset(0)
                .dst_offset(0)
                .build();
            self.vk_device
                .cmd_copy_buffer(command_buffer, src, dst, region.as_slice());
        }
    }

    #[inline]
    fn create_staged_buffer(
        &self,
        size: u64,
        usage: vk::BufferUsageFlags,
    ) -> (vk::Buffer, vma::Allocation) {
        let vertex_buffer_info = vk::BufferCreateInfo::builder()
            .size(size)
            .usage(usage | vk::BufferUsageFlags::TRANSFER_DST)
            .sharing_mode(vk::SharingMode::EXCLUSIVE)
            .build();
        let vertex_alloc_info = vma::AllocationCreateInfo::builder()
            .usage(vma::MemoryUsage::GPUOnly)
            .build();

        unsafe {
            let result = self
                .vk_allocator
                .create_buffer(&vertex_buffer_info, &vertex_alloc_info);
            result.expect("Failed to create vertex buffer")
        }
    }

    #[inline]
    fn create_staging_buffer(&self, size: u64) -> (vk::Buffer, vma::Allocation) {
        let staging_buffer_info = vk::BufferCreateInfo::builder()
            .size(size)
            .usage(vk::BufferUsageFlags::TRANSFER_SRC)
            .sharing_mode(vk::SharingMode::EXCLUSIVE)
            .build();
        let staging_alloc_info = vma::AllocationCreateInfo::builder()
            .usage(vma::MemoryUsage::CPUOnly)
            .build();
        unsafe {
            let result = self
                .vk_allocator
                .create_buffer(&staging_buffer_info, &staging_alloc_info);
            result.expect("Failed to create vertex staging buffer")
        }
    }

    #[inline]
    fn create_mesh() -> (Vec<[f32; 11]>, Vec<u16>) {
        let mut vertices = Vec::with_capacity(16);
        let mut indices = Vec::with_capacity(64);

        fn vert(pos: Vec3, normal: Vec3) -> [f32; 11] {
            let mut vert = [0f32; 11];
            vert[0] = pos[0];
            vert[1] = pos[1];
            vert[2] = pos[2];

            vert[3] = normal[0];
            vert[4] = normal[1];
            vert[5] = normal[2];
            vert
        }

        fn push_vert(vec: &mut Vec<[f32; 11]>, pos: Vec3, normal: Vec3) {
            vec.push(vert(pos, normal));
        }

        // FRONT FACE
        push_vert(
            &mut vertices,
            [-0.5, 0.5, -0.5].into(),
            [0.0, 0.0, -1.0].into(),
        );
        push_vert(
            &mut vertices,
            [0.5, 0.5, -0.5].into(),
            [0.0, 0.0, -1.0].into(),
        );
        push_vert(
            &mut vertices,
            [0.5, -0.5, -0.5].into(),
            [0.0, 0.0, -1.0].into(),
        );
        push_vert(
            &mut vertices,
            [-0.5, -0.5, -0.5].into(),
            [0.0, 0.0, -1.0].into(),
        );
        // TRI 1
        indices.push(0);
        indices.push(1);
        indices.push(3);
        // TRI 2
        indices.push(3);
        indices.push(1);
        indices.push(2);

        // BACK FACE
        push_vert(
            &mut vertices,
            [-0.5, 0.5, 0.5].into(),
            [0.0, 0.0, 1.0].into(),
        );
        push_vert(
            &mut vertices,
            [0.5, 0.5, 0.5].into(),
            [0.0, 0.0, 1.0].into(),
        );
        push_vert(
            &mut vertices,
            [0.5, -0.5, 0.5].into(),
            [0.0, 0.0, 1.0].into(),
        );
        push_vert(
            &mut vertices,
            [-0.5, -0.5, 0.5].into(),
            [0.0, 0.0, 1.0].into(),
        );
        // TRI 1
        indices.push(3 + 4);
        indices.push(1 + 4);
        indices.push(0 + 4);
        // TRI 2
        indices.push(2 + 4);
        indices.push(1 + 4);
        indices.push(3 + 4);

        // LEFT FACE
        push_vert(
            &mut vertices,
            [-0.5, -0.5, 0.5].into(),
            [-1.0, 0.0, 0.0].into(),
        );
        push_vert(
            &mut vertices,
            [-0.5, 0.5, 0.5].into(),
            [-1.0, 0.0, 0.0].into(),
        );
        push_vert(
            &mut vertices,
            [-0.5, 0.5, -0.5].into(),
            [-1.0, 0.0, 0.0].into(),
        );
        push_vert(
            &mut vertices,
            [-0.5, -0.5, -0.5].into(),
            [-1.0, 0.0, 0.0].into(),
        );
        // TRI 1
        indices.push(0 + 8);
        indices.push(1 + 8);
        indices.push(3 + 8);
        // TRI 2
        indices.push(3 + 8);
        indices.push(1 + 8);
        indices.push(2 + 8);

        // RIGHT FACE
        push_vert(
            &mut vertices,
            [0.5, -0.5, 0.5].into(),
            [1.0, 0.0, 0.0].into(),
        );
        push_vert(
            &mut vertices,
            [0.5, 0.5, 0.5].into(),
            [1.0, 0.0, 0.0].into(),
        );
        push_vert(
            &mut vertices,
            [0.5, 0.5, -0.5].into(),
            [1.0, 0.0, 0.0].into(),
        );
        push_vert(
            &mut vertices,
            [0.5, -0.5, -0.5].into(),
            [1.0, 0.0, 0.0].into(),
        );
        // TRI 1
        indices.push(3 + 12);
        indices.push(1 + 12);
        indices.push(0 + 12);
        // TRI 2
        indices.push(2 + 12);
        indices.push(1 + 12);
        indices.push(3 + 12);

        // TOP FACE
        push_vert(
            &mut vertices,
            [-0.5, 0.5, 0.5].into(),
            [0.0, 1.0, 0.0].into(),
        );
        push_vert(
            &mut vertices,
            [0.5, 0.5, 0.5].into(),
            [0.0, 1.0, 0.0].into(),
        );
        push_vert(
            &mut vertices,
            [0.5, 0.5, -0.5].into(),
            [0.0, 1.0, 0.0].into(),
        );
        push_vert(
            &mut vertices,
            [-0.5, 0.5, -0.5].into(),
            [0.0, 1.0, 0.0].into(),
        );
        // TRI 1
        indices.push(0 + 16);
        indices.push(1 + 16);
        indices.push(3 + 16);
        // TRI 2
        indices.push(3 + 16);
        indices.push(1 + 16);
        indices.push(2 + 16);

        // BOTTOM FACE
        push_vert(
            &mut vertices,
            [-0.5, -0.5, 0.5].into(),
            [0.0, -1.0, 0.0].into(),
        );
        push_vert(
            &mut vertices,
            [0.5, -0.5, 0.5].into(),
            [0.0, -1.0, 0.0].into(),
        );
        push_vert(
            &mut vertices,
            [0.5, -0.5, -0.5].into(),
            [0.0, -1.0, 0.0].into(),
        );
        push_vert(
            &mut vertices,
            [-0.5, -0.5, -0.5].into(),
            [0.0, -1.0, 0.0].into(),
        );
        // TRI 1
        indices.push(3 + 20);
        indices.push(1 + 20);
        indices.push(0 + 20);
        // TRI 2
        indices.push(2 + 20);
        indices.push(1 + 20);
        indices.push(3 + 20);

        (vertices, indices)
    }
}

pub fn create_render_pass(
    device: &ash::Device,
    format: vk::SurfaceFormatKHR,
    depth_format: vk::Format,
) -> vk::RenderPass {
    let attachment_description = vk::AttachmentDescription::builder()
        .format(format.format)
        .samples(vk::SampleCountFlags::TYPE_1)
        .load_op(vk::AttachmentLoadOp::CLEAR)
        .store_op(vk::AttachmentStoreOp::STORE)
        .stencil_load_op(vk::AttachmentLoadOp::DONT_CARE)
        .stencil_store_op(vk::AttachmentStoreOp::DONT_CARE)
        .initial_layout(vk::ImageLayout::UNDEFINED)
        .final_layout(vk::ImageLayout::PRESENT_SRC_KHR)
        .build();

    let attachment_reference = vk::AttachmentReference::builder()
        .attachment(0)
        .layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
        .build();

    let depth_attachment_description = vk::AttachmentDescription::builder()
        .format(depth_format)
        .samples(vk::SampleCountFlags::TYPE_1)
        .load_op(vk::AttachmentLoadOp::CLEAR)
        .store_op(vk::AttachmentStoreOp::DONT_CARE)
        .stencil_load_op(vk::AttachmentLoadOp::DONT_CARE)
        .stencil_store_op(vk::AttachmentStoreOp::DONT_CARE)
        .initial_layout(vk::ImageLayout::UNDEFINED)
        .final_layout(vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
        .build();

    let depth_attachment_reference = vk::AttachmentReference::builder()
        .attachment(1)
        .layout(vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
        .build();

    let subpass = vk::SubpassDescription::builder()
        .pipeline_bind_point(vk::PipelineBindPoint::GRAPHICS)
        .color_attachments(attachment_reference.as_slice())
        .depth_stencil_attachment(&depth_attachment_reference)
        .build();

    let subpass_dependency = vk::SubpassDependency::builder()
        .src_subpass(vk::SUBPASS_EXTERNAL)
        .dst_subpass(0)
        .src_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
        .dst_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
        .dst_access_mask(
            vk::AccessFlags::COLOR_ATTACHMENT_READ | vk::AccessFlags::COLOR_ATTACHMENT_WRITE,
        )
        .build();

    let attachments = [attachment_description, depth_attachment_description];
    let render_pass_info = vk::RenderPassCreateInfo::builder()
        .attachments(&attachments)
        .subpasses(subpass.as_slice())
        .dependencies(subpass_dependency.as_slice())
        .build();

    unsafe {
        device
            .create_render_pass(&render_pass_info, None)
            .expect("Failed to create render pass")
    }
}

pub fn create_pipeline(
    device: &ash::Device,
    render_pass: vk::RenderPass,
    layout: vk::PipelineLayout,
    vert_module: vk::ShaderModule,
    frag_module: vk::ShaderModule,
    swap_extent: vk::Extent2D,
) -> vk::Pipeline {
    // Shaders
    let frag = vulkan::utils::frag_shader_stage(frag_module);
    let vert = vulkan::utils::vert_shader_stage(vert_module);
    let stages = [frag, vert];

    // Input Assembly
    let input_assembly = vulkan::utils::input_assembly_tri_list();

    // Viewport and Scissor
    let width = swap_extent.width as f32;
    let height = swap_extent.height as f32;
    let viewport = vulkan::utils::viewport(width, height);
    let scissor = vulkan::utils::scissor_none(swap_extent);
    let viewport_state = vulkan::utils::viewport_state(viewport.as_slice(), scissor.as_slice());

    // Rasterizer
    let rasterizer = vulkan::utils::rasterizer_back_clockwise();

    // Multisampling
    let multisampling = vulkan::utils::multisampling_none();

    // Frambuffer Attachments
    let mask = vk::ColorComponentFlags::R
        | vk::ColorComponentFlags::G
        | vk::ColorComponentFlags::B
        | vk::ColorComponentFlags::A;
    let attachments = vulkan::utils::blend_attachment_none(mask);
    let color_blending = vulkan::utils::blend_state_none(attachments.as_slice());

    // Vertex Format
    let vertex_input = VertexData::input_description();

    // Depth Stencil
    let depth_stencil = vk::PipelineDepthStencilStateCreateInfo::builder()
        .min_depth_bounds(0.0)
        .max_depth_bounds(1.0)
        .depth_test_enable(true)
        .depth_write_enable(true)
        .depth_bounds_test_enable(false)
        .depth_compare_op(vk::CompareOp::LESS)
        .build();

    // Pipeline
    let pipeline_info = vk::GraphicsPipelineCreateInfo::builder()
        .stages(&stages)
        .depth_stencil_state(&depth_stencil)
        .vertex_input_state(&vertex_input)
        .input_assembly_state(&input_assembly)
        .viewport_state(&viewport_state)
        .rasterization_state(&rasterizer)
        .multisample_state(&multisampling)
        .color_blend_state(&color_blending)
        .render_pass(render_pass)
        .layout(layout)
        .subpass(0)
        .build();

    let pipeline = unsafe {
        device
            .create_graphics_pipelines(vk::PipelineCache::default(), &[pipeline_info], None)
            .expect("Failed to create graphics pipeline")
    };

    pipeline[0]
}

// =================================================================================================
pub fn load_spriv_module_from_file(
    device: &ash::Device,
    fs: &Filesystem,
    path: &Path,
) -> vk::ShaderModule {
    const CHUNK_SIZE: usize = 4096;

    let file = { fs.lookup(path).unwrap() };
    let mut file = file.file_reader().unwrap();

    // We want the backing data and chunk to be u32 aligned
    let mut data = vec![0; CHUNK_SIZE * 32];
    let mut words_total: usize = 0;

    'outer: loop {
        // Take a slice from the backing data to write into
        let write_buf = &mut data[words_total..words_total + CHUNK_SIZE];

        // Reinterpret slice because function signatures.png
        let write_buf =
            unsafe { from_raw_parts_mut(write_buf.as_mut_ptr() as *mut u8, CHUNK_SIZE * 4) };

        // Read into the slice
        if let Ok(bytes) = file.read(write_buf) {
            // We're given number of bytes read, we need number of words (u32) read
            let words = bytes / 4;
            words_total += words;

            // If we have less than the requested number of bytes then we've read the whole file
            // and can exit
            if words < CHUNK_SIZE {
                break 'outer;
            }

            // If we've run out of space in our buffer we need to realloc
            if data.len() <= words_total {
                data.resize(data.len() * 2, 0);
            }
        }
    }

    let create_info = vk::ShaderModuleCreateInfo::builder()
        .code(&data[0..words_total])
        .build();

    unsafe {
        device
            .create_shader_module(&create_info, None)
            .expect("Failed to create shader module")
    }
}

pub fn create_image_views(
    device: &ash::Device,
    vk_swapchain: &VulkanSwapchain,
) -> Vec<vk::ImageView> {
    let mut out = Vec::new();

    for i in vk_swapchain.images().iter() {
        let subresource_range = vk::ImageSubresourceRange::builder()
            .layer_count(1)
            .base_array_layer(0)
            .level_count(1)
            .base_mip_level(0)
            .aspect_mask(vk::ImageAspectFlags::COLOR)
            .build();
        let component_mapping = vk::ComponentMapping::builder()
            .a(vk::ComponentSwizzle::IDENTITY)
            .r(vk::ComponentSwizzle::IDENTITY)
            .g(vk::ComponentSwizzle::IDENTITY)
            .b(vk::ComponentSwizzle::IDENTITY)
            .build();
        let info = vk::ImageViewCreateInfo::builder()
            .format(vk_swapchain.format().format)
            .image(*i)
            .subresource_range(subresource_range)
            .view_type(vk::ImageViewType::TYPE_2D)
            .components(component_mapping)
            .build();

        let image_view = unsafe {
            device
                .create_image_view(&info, None)
                .expect("Failed to create ImageView")
        };

        out.push(image_view);
    }

    out
}

fn select_depth_format(vk_context: &VulkanContext, vk_device: &VulkanDevice) -> Option<vk::Format> {
    let formats = [
        vk::Format::D24_UNORM_S8_UINT,
        vk::Format::D32_SFLOAT_S8_UINT,
        vk::Format::D32_SFLOAT,
    ];

    for format in formats.iter() {
        let props =
            unsafe { vk_context.get_physical_device_format_properties(vk_device.gpu(), *format) };
        if props
            .optimal_tiling_features
            .contains(vk::FormatFeatureFlags::DEPTH_STENCIL_ATTACHMENT)
        {
            return Some(*format);
        }
    }
    None
}

fn create_depth_image_views(
    vk_device: &VulkanDevice,
    depth_images: &Vec<(vk::Image, vma::Allocation)>,
    format: vk::Format,
) -> Vec<vk::ImageView> {
    let mut depth_image_views = Vec::new();
    for image in depth_images.iter() {
        let components = vk::ComponentMapping::builder()
            .a(vk::ComponentSwizzle::IDENTITY)
            .r(vk::ComponentSwizzle::IDENTITY)
            .g(vk::ComponentSwizzle::IDENTITY)
            .b(vk::ComponentSwizzle::IDENTITY)
            .build();
        let subresource_range = vk::ImageSubresourceRange::builder()
            .aspect_mask(vk::ImageAspectFlags::DEPTH | vk::ImageAspectFlags::STENCIL)
            .base_mip_level(0)
            .level_count(1)
            .base_array_layer(0)
            .layer_count(1)
            .build();
        let create_info = vk::ImageViewCreateInfo::builder()
            .image(image.0)
            .view_type(vk::ImageViewType::TYPE_2D)
            .format(format)
            .components(components)
            .subresource_range(subresource_range)
            .build();
        let view = unsafe {
            vk_device
                .create_image_view(&create_info, None)
                .expect("Failed to create depth image view")
        };
        depth_image_views.push(view);
    }
    depth_image_views
}

fn create_depth_images(
    vk_allocator: &vma::Allocator,
    vk_swapchain: &VulkanSwapchain,
    format: vk::Format,
    queue_family: &QueueFamily,
) -> Vec<(vk::Image, vma::Allocation)> {
    let mut depth_images = Vec::new();

    for _ in 0..vk_swapchain.images().len() {
        let extent = *vk_swapchain.extent();
        let extent = vk::Extent3D::builder()
            .width(extent.width)
            .height(extent.height)
            .depth(1)
            .build();
        let image_create_info = vk::ImageCreateInfo::builder()
            .image_type(vk::ImageType::TYPE_2D)
            .format(format)
            .extent(extent)
            .mip_levels(1)
            .array_layers(1)
            .samples(vk::SampleCountFlags::TYPE_1)
            .tiling(vk::ImageTiling::OPTIMAL)
            .usage(vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT)
            .sharing_mode(vk::SharingMode::EXCLUSIVE)
            .queue_family_indices(queue_family.i.as_slice())
            .initial_layout(vk::ImageLayout::UNDEFINED)
            .build();

        let alloc_create_info = vma::AllocationCreateInfo::builder()
            .flags(vma::AllocationCreateFlag::DEDICATED_MEMORY_BIT)
            .usage(vma::MemoryUsage::GPUOnly)
            .build();
        let image = unsafe {
            vk_allocator
                .create_image(&image_create_info, &alloc_create_info)
                .expect("Failed to allocate depth buffer image")
        };
        depth_images.push(image);
    }
    depth_images
}

pub fn create_framebuffers(
    device: &ash::Device,
    vk_swapchain: &VulkanSwapchain,
    render_pass: vk::RenderPass,
    image_views: &[vk::ImageView],
    depth_image_views: &[vk::ImageView],
) -> Vec<vk::Framebuffer> {
    let mut out = Vec::new();

    for iv in image_views.iter().zip(depth_image_views.iter()) {
        let attachments = [*iv.0, *iv.1];
        let info = vk::FramebufferCreateInfo::builder()
            .render_pass(render_pass)
            .attachments(&attachments)
            .width(vk_swapchain.extent().width)
            .height(vk_swapchain.extent().height)
            .layers(1)
            .build();

        let fb = unsafe {
            device
                .create_framebuffer(&info, None)
                .expect("Failed to create framebuffer")
        };

        out.push(fb);
    }

    out
}
