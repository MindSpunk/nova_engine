/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::core::input::Axis;
use crate::core::nodes::{DebugCamera, StaticMesh};
use crate::core::resources::{Controllers, DeltaTimer, KeyboardInput, MouseInput};
use crate::math::types::{Quat, Vec2, Vec3};
use crate::universe::{Dependency, TaskBox, TaskGraphBuilder, TaskUniverseBuilder, Transform3D};
use sdl2::event::Event;
use sdl2::keyboard::Scancode;
use sdl2::mouse::MouseButton;

pub struct DebugCameraSystem {
    pub last_pos: (i32, i32),
}

universe::impl_resource!(DebugCameraSystem);

impl DebugCameraSystem {
    pub fn new() -> Self {
        Self { last_pos: (0, 0) }
    }
}

impl DebugCameraSystem {
    pub fn schedule_task(task_graph: &mut TaskGraphBuilder) {
        let task = TaskBox::new(|access| {
            let system = access.get_resource_mut::<DebugCameraSystem>().unwrap();
            let timer = access.get_resource_ref::<DeltaTimer>().unwrap();
            let keyboard = access.get_resource_ref::<KeyboardInput>().unwrap();
            let mouse = access.get_resource_ref::<MouseInput>().unwrap();
            let controllers = access.get_resource_ref::<Controllers>().unwrap();
            let mut nodes = access.get_nodes_mut::<DebugCamera>().unwrap();
            let mesh_nodes = access.get_nodes_mut::<StaticMesh>().unwrap();
            let mut transforms = access.get_transforms_mut().unwrap();
            let matrices = access.get_matrices_mut().unwrap();
            let hierarchy = access.get_node_hierarchy_mut().unwrap();
            let delta = timer.delta_time() as f32;

            let mut movement: Vec3 = Vec3::zero();
            let mut rotation: Vec2 = Vec2::zero();

            for event in mouse.event_iter() {
                match event {
                    Event::MouseButtonDown { mouse_btn, .. } => {
                        if *mouse_btn == MouseButton::Right {
                            system.last_pos = mouse.get_mouse_position_global();
                            mouse.relative_mode(true);
                        }
                    }
                    Event::MouseButtonUp { mouse_btn, .. } => {
                        if *mouse_btn == MouseButton::Right {
                            mouse.relative_mode(false);
                            mouse.set_mouse_position_global(system.last_pos);
                        }
                    }
                    _ => {}
                }
            }

            let mouse_delta = {
                if mouse.is_mouse_button_down(MouseButton::Right) {
                    if keyboard.is_key_down_scancode(Scancode::W) {
                        movement[2] = 1.0;
                    }
                    if keyboard.is_key_down_scancode(Scancode::S) {
                        movement[2] = -1.0;
                    }
                    if keyboard.is_key_down_scancode(Scancode::A) {
                        movement[0] = -1.0;
                    }
                    if keyboard.is_key_down_scancode(Scancode::D) {
                        movement[0] = 1.0;
                    }
                    if keyboard.is_key_down_scancode(Scancode::Q) {
                        movement[1] = -1.0;
                    }
                    if keyboard.is_key_down_scancode(Scancode::E) {
                        movement[1] = 1.0;
                    }

                    rotation[0] = mouse.mouse_motion().0 as f32;
                    rotation[1] = mouse.mouse_motion().1 as f32;

                    1.0
                } else {
                    delta * 1000.0
                }
            };

            if let Some(controller) = &controllers[0] {
                let lx = controller.get_axis(Axis::LeftStickX);
                let ly = controller.get_axis(Axis::LeftStickY);
                let lt = controller.get_axis(Axis::LeftTrigger);
                let rx = controller.get_axis(Axis::RightStickX);
                let ry = controller.get_axis(Axis::RightStickY);
                let rt = controller.get_axis(Axis::RightTrigger);

                use crate::math::in_range;

                if !in_range(lx, -0.15, 0.15) {
                    movement[0] = lx;
                }

                let lz = rt + (-lt);
                if !in_range(lz, -0.15, 0.15) {
                    movement[1] = lz;
                }

                if !in_range(ly, -0.15, 0.15) {
                    movement[2] = ly;
                }

                if !in_range(rx, -0.15, 0.15) {
                    rotation[0] = rx;
                }

                if !in_range(ry, -0.15, 0.15) {
                    rotation[1] = -ry;
                }
            }

            nodes.for_each_mut(|node, nodes| {
                let tform = &mut transforms[node];
                let cam = &mut nodes[node];

                cam.cam_yaw += rotation[0] * 0.01 * 0.2 * mouse_delta;
                cam.cam_pitch += rotation[1] * 0.01 * 0.2 * mouse_delta;

                fn clamp(val: f32, min: f32, max: f32) -> f32 {
                    if val < min {
                        min
                    } else if val > max {
                        max
                    } else {
                        val
                    }
                }
                cam.cam_pitch = clamp(cam.cam_pitch, -90f32.to_radians(), 90f32.to_radians());

                tform.rot = Quat::from_euler_angles([cam.cam_pitch, cam.cam_yaw, 0.0].into());

                let forward = tform.get_forward();
                let right = tform.get_right();

                tform.pos += right * movement[0] * 4.0 * delta;
                tform.pos += forward * movement[2] * 4.0 * delta;
                tform.pos += Vec3::up() * movement[1] * 4.0 * delta;
            });

            let mut transform = Transform3D::default();
            nodes.for_each(|node, _| {
                transform = transforms[node].clone();
            });

            let mut universe = TaskUniverseBuilder::new(transforms, matrices, hierarchy)
                .add_node_type(mesh_nodes)
                .build();

            for event in keyboard.event_iter() {
                match event {
                    Event::KeyDown { scancode, .. } => {
                        if let Some(scancode) = scancode {
                            if *scancode == Scancode::Space {
                                universe.create_node(StaticMesh::new(), transform);
                                break;
                            }
                        }
                    }
                    _ => {}
                }
            }
        });

        task_graph.add_task(task, |deps| {
            deps.write(Dependency::resource::<DebugCameraSystem>());
            deps.write(Dependency::node::<DebugCamera>());
            deps.write(Dependency::node::<StaticMesh>());
            deps.write(Dependency::transforms());
            deps.write(Dependency::matrices());
            deps.write(Dependency::node_hierarchy());
            deps.read(Dependency::resource::<DeltaTimer>());
            deps.read(Dependency::resource::<KeyboardInput>());
            deps.read(Dependency::resource::<MouseInput>());
            deps.read(Dependency::resource::<Controllers>());
        });
    }
}
