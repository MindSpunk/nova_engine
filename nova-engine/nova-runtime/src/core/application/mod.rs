/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

mod close_handle;
mod controller_buffer;
mod mouse;
mod sdl2_thread;
mod timer;
mod window;

pub use close_handle::CloseHandle;
pub use controller_buffer::ControllerBuffer;
pub use controller_buffer::ControllerFrame;
pub use controller_buffer::ControllerState;
pub use mouse::Mouse;
pub use timer::Timer;
pub use window::Window;

use crate::owned_arc::Weak;
use error_message::err_msg;
use log::info;
use parking_lot::{Condvar, Mutex};
use sdl2::TimerSubsystem;
use std::sync::Arc;

type InitTimer = Arc<(Mutex<Option<Weak<TimerSubsystem>>>, Condvar)>;

pub struct Application {
    pub window: Window,
    pub mouse: Mouse,
    pub controller_buffer: ControllerBuffer,
    pub timer: Timer,
}

impl Application {
    pub fn run(your_main: impl FnOnce(Application)) {
        //
        // Init logging and add our message box panic hook
        //
        logger::init();
        crate::panic::add_hook();
        info!("Initialized logging");
        info!("Added panic hook");

        //
        // Init the rayon thread pool
        //
        let builder = rayon::ThreadPoolBuilder::new()
            .num_threads(num_cpus::get())
            .thread_name(|v| format!("Rayon Worker {}", v));

        //
        // Need to init COM for windows when the thread starts
        //
        #[cfg(target_os = "windows")]
        let builder = builder.start_handler(|v| unsafe {
            use winapi::shared::winerror::S_FALSE;
            use winapi::shared::winerror::S_OK;
            use winapi::um::combaseapi::CoInitializeEx;
            use winapi::um::objbase::COINIT_APARTMENTTHREADED;

            #[allow(non_snake_case)]
            let pvReserved = std::ptr::null_mut();

            #[allow(non_snake_case)]
            let dwCoInit = COINIT_APARTMENTTHREADED;

            let err = CoInitializeEx(pvReserved, dwCoInit);

            if err != S_OK && err != S_FALSE {
                panic!("Failed to init COM on Rayon Thread {}. HRESULT: {}", v, err);
            };

            info!("Initialized COM for Rayon Thread {}", v);
        });

        //
        // Need to un-init COM for windows when the thread closes
        //
        #[cfg(target_os = "windows")]
        let builder = builder.exit_handler(|_| unsafe {
            use winapi::um::combaseapi::CoUninitialize;

            CoUninitialize();
        });

        builder
            .build_global()
            .expect(err_msg!("Failed to init rayon thread pool"));

        let thread = sdl2_thread::SDL2Thread::new();
        let (application, close_handle) = thread.spawn();

        //
        // User's main function get's called once everything has been setup for them
        //
        info!("Moving into user main function");
        your_main(application);
        info!("Exited user main function");

        //
        // Tell the SDL2 thread that it's time to close up shop
        //
        close_handle.enqueue_close();
        info!("SDL2 thread instructed to close, waiting for it to close");

        //
        // Wait for the SDL2 thread to finish
        //
        close_handle.wait_close();
        info!("SDL2 thread closed");
        info!("NovaEngine closing");
    }
}
