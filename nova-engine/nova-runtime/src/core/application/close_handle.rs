/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::{Arc, Barrier};

///
/// This struct is used to handle closing the window
///
#[derive(Clone)]
pub struct CloseHandle {
    condition: Arc<(AtomicBool, Barrier)>,
}

impl CloseHandle {
    pub fn new() -> Self {
        Self {
            condition: Arc::new((AtomicBool::new(false), Barrier::new(2))),
        }
    }

    pub fn should_close(&self) -> bool {
        self.condition.0.load(Ordering::Relaxed)
    }

    pub fn enqueue_close(&self) {
        self.condition.0.store(true, Ordering::Relaxed);
    }

    pub fn wait_close(&self) {
        self.condition.1.wait();
    }
}
