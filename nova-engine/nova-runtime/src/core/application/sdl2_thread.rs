/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::core::application::mouse::MouseRequest;
use crate::core::application::window::{WindowRequest, WindowState};
use crate::core::application::{
    Application, CloseHandle, ControllerBuffer, ControllerState, InitTimer, Mouse, Timer,
};
use crate::log::info;
use crate::owned_arc::OwnedArc;
use ash::vk;
use ash::vk::Handle;
use crossbeam::queue::ArrayQueue;
use error_message::err_msg;
use parking_lot::{Condvar, Mutex};
use sdl2::controller::GameController;
use sdl2::event::{Event, WindowEvent};
use sdl2::mouse::MouseUtil;
use sdl2::video::{FullscreenType, Window};
use sdl2::GameControllerSubsystem;
use std::sync::{Arc, Barrier};

#[derive(Clone)]
pub struct SDL2Thread {
    close_handle: CloseHandle,
    init_barrier: Arc<Barrier>,
    init_timer: InitTimer,
    window_state: Arc<Mutex<WindowState>>,
    window_events: Arc<ArrayQueue<Event>>,
    window_requests: Arc<ArrayQueue<WindowRequest>>,
    mouse_requests: Arc<ArrayQueue<MouseRequest>>,
    controller_states: Arc<Mutex<[Option<ControllerState>; 4]>>,
}

impl SDL2Thread {
    pub fn new() -> Self {
        Self {
            close_handle: CloseHandle::new(),
            init_barrier: Arc::new(Barrier::new(2)),
            init_timer: Arc::new((Mutex::new(None), Condvar::new())),
            window_state: Arc::new(Mutex::new(Default::default())),
            window_events: Arc::new(ArrayQueue::new(65536)),
            window_requests: Arc::new(ArrayQueue::new(256)),
            mouse_requests: Arc::new(ArrayQueue::new(256)),
            controller_states: Arc::new(Mutex::new(Default::default())),
        }
    }

    pub fn spawn(self) -> (Application, CloseHandle) {
        self.clone().run_thread();

        //
        // Wait for the timer to be set by the init thread
        //
        {
            let mut lock = self.init_timer.0.lock();
            if lock.is_none() {
                self.init_timer.1.wait(&mut lock);
            }
        }

        //
        // We need to extract to an owned value
        //
        let weak_timer;
        let mut arc = self.init_timer;
        loop {
            match Arc::try_unwrap(arc) {
                Ok(weak) => {
                    weak_timer = weak;
                    break;
                }
                Err(err) => {
                    arc = err;
                }
            }
        }
        let (weak_timer, _) = weak_timer;
        let weak_timer = weak_timer.into_inner().unwrap();
        let timer = Timer { timer: weak_timer };
        info!("TimerSubsystem pointer received on main thread");

        self.init_barrier.wait();
        info!("Main Thread: Init barrier passed");

        let window = crate::core::application::Window {
            state: self.window_state,
            events: self.window_events,
            requests: self.window_requests,
        };

        let mouse = Mouse {
            requests: self.mouse_requests,
        };

        let controller_buffer = ControllerBuffer {
            state: self.controller_states,
        };

        let app = Application {
            window,
            mouse,
            controller_buffer,
            timer,
        };

        (app, self.close_handle)
    }

    ///
    /// The actual thread that will be spun up
    ///
    fn run_thread(mut self) {
        info!("Preparing to spin up SDL2 thread");
        std::thread::Builder::new()
            .name("SDL2 Thread".to_string())
            .spawn(move || {
                //
                // Init SDL2 Context
                //
                let context = sdl2::init().expect(err_msg!("Failed to init SDL2"));
                info!("Initialized SDL2");

                //
                // Init SDL2 Video subsystem
                //
                let video = context
                    .video()
                    .expect(err_msg!("Failed to init SDL2 Video subsystem"));
                info!("Initialized SDL2 Video subsystem");

                //
                // Init SDL2 Controller subsystem
                //
                let controller_system = context
                    .game_controller()
                    .expect(err_msg!("Failed to init SDL2 Game Controller subsystem"));
                info!("Initialized SDL2 Game Controller subsystem");

                //
                // Init SDL2 Mouse subsystem
                //
                let mouse = context.mouse();
                info!("Initialized SDL2 Mouse subsystem");

                //
                // Init SDL2 Event Pump
                //
                let mut event_pump = context
                    .event_pump()
                    .expect(err_msg!("Failed to init SDL2 Event Pump"));
                info!("Initialized SDL2 Event Pump");

                //
                // Init SDL2 Timer subsystem
                //
                let timer = context
                    .timer()
                    .expect(err_msg!("Failed to init SDL2 Timer subsystem"));
                let timer = OwnedArc::new(timer);
                info!("Initialized SDL2 Timer subsystem");

                //
                // Send a weak reference of the SDL2 Timer to the main thread's handle and replace
                // the old arc so it will be dropped and ownership can be taken by the main thread
                //
                {
                    *self.init_timer.0.lock() = Some(timer.weak());
                    self.init_timer.1.notify_all();
                    self.init_timer = Arc::new((Mutex::new(None), Condvar::new()))
                }
                info!("TimerSubsystem pointer passed to main thread");

                //
                // Create the window we're going to draw to
                //
                let mut window = video
                    .window("NovaEngine", 1280, 720)
                    .resizable()
                    .vulkan()
                    .allow_highdpi()
                    .build()
                    .expect(err_msg!("Failed to create an SDL2 window"));
                info!("Initialized SDL2 Window");

                //
                // Initialize the window state
                //
                let mode = window
                    .display_mode()
                    .expect(err_msg!("Failed to get window display mode"));

                let (current_width, current_height) = (mode.w as u32, mode.h as u32);
                let (drawable_width, drawable_height) = window.drawable_size();

                {
                    let mut lock = self.window_state.lock();
                    *lock = WindowState {
                        title: "NovaEngine",
                        current_width,
                        current_height,
                        windowed_width: 1280,
                        windowed_height: 720,
                        drawable_width,
                        drawable_height,
                        fullscreen: false,
                        resizeable: true,
                    };
                }
                info!("Initialized WindowState");

                //
                // Sync point for the main thread once initialization has been completed
                //
                self.init_barrier.wait();
                info!("SDL2 Thread: Init barrier passed. Moving to SDL2 event loop");

                //
                // The core loop for this thread is to handle and forward events and to handle requests
                // from the rest of the threads
                //
                let mut controllers = [None, None, None, None];
                let mut events = Vec::new();
                while !self.close_handle.should_close() {
                    //
                    // Pop all the queued events off the SDL2 event pump and push them to our own array
                    //
                    while let Some(event) = event_pump.poll_event() {
                        events.push(event);
                    }

                    self.handle_window(&mut window, &events);
                    self.handle_controllers(&controller_system, &mut controllers, &events);
                    self.handle_mouse(&mouse);
                    self.flush_events(&mut events);

                    std::thread::sleep(std::time::Duration::from_millis(1));
                }

                info!("Exited SDL2 event loop, preparing to close SDL2 thread");
                self.close_handle.wait_close();
            })
            .expect(err_msg!("Failed to start SDL2 thread"));
        info!("SDL2 thread scheduled");
    }

    ///
    /// Flush the events we've operated on to the queue for the rest of the threads to
    /// consume
    ///
    fn flush_events(&self, events: &mut Vec<Event>) {
        events.drain(..).for_each(|v| {
            self.window_events
                .push(v)
                .expect(err_msg!("Event queue full"));
        });
    }

    ///
    /// Handle the game controller system's events and update the state
    ///
    fn handle_controllers(
        &self,
        system: &GameControllerSubsystem,
        controllers: &mut [Option<GameController>; 4],
        events: &[Event],
    ) {
        let mut states = self.controller_states.lock();
        for event in events.iter() {
            match event {
                Event::ControllerDeviceAdded { which, .. } => {
                    // Log the new connection
                    info!("Controller Connected: Joystick Index = {}", *which);

                    // Open the controller for use
                    let controller = system.open(*which).unwrap();

                    // Get the instance id
                    let id = controller.instance_id() as usize;

                    // Log the instance id
                    info!("Controller Added: Joystick ID {}", id);

                    // Store the handle to the SDL2 game controller
                    controllers[id] = Some(controller);

                    // Initialize the controller state
                    states[id] = Some(Default::default());
                }
                Event::ControllerDeviceRemoved { which, .. } => {
                    // Remove the SDL2 game controller handle
                    controllers[*which as usize] = None;

                    // Remove the controller state
                    states[*which as usize] = None;

                    // Log the instance id
                    info!("Controller Removed: Joystick ID {}", which);
                }
                Event::ControllerButtonDown { which, button, .. } => {
                    let state = states[*which as usize].as_mut().unwrap();
                    match button {
                        sdl2::controller::Button::A => {
                            state.button_mask.set(super::controller_buffer::A_IDX, true)
                        }
                        sdl2::controller::Button::B => {
                            state.button_mask.set(super::controller_buffer::B_IDX, true)
                        }
                        sdl2::controller::Button::X => {
                            state.button_mask.set(super::controller_buffer::X_IDX, true)
                        }
                        sdl2::controller::Button::Y => {
                            state.button_mask.set(super::controller_buffer::Y_IDX, true)
                        }
                        sdl2::controller::Button::Back => state
                            .button_mask
                            .set(super::controller_buffer::BACK_IDX, true),
                        sdl2::controller::Button::Guide => state
                            .button_mask
                            .set(super::controller_buffer::GUIDE_IDX, true),
                        sdl2::controller::Button::Start => state
                            .button_mask
                            .set(super::controller_buffer::START_IDX, true),
                        sdl2::controller::Button::LeftStick => state
                            .button_mask
                            .set(super::controller_buffer::LSTICK_IDX, true),
                        sdl2::controller::Button::RightStick => state
                            .button_mask
                            .set(super::controller_buffer::RSTICK_IDX, true),
                        sdl2::controller::Button::LeftShoulder => state
                            .button_mask
                            .set(super::controller_buffer::LB_IDX, true),
                        sdl2::controller::Button::RightShoulder => state
                            .button_mask
                            .set(super::controller_buffer::RB_IDX, true),
                        sdl2::controller::Button::DPadUp => state
                            .button_mask
                            .set(super::controller_buffer::UP_IDX, true),
                        sdl2::controller::Button::DPadDown => state
                            .button_mask
                            .set(super::controller_buffer::DOWN_IDX, true),
                        sdl2::controller::Button::DPadLeft => state
                            .button_mask
                            .set(super::controller_buffer::LEFT_IDX, true),
                        sdl2::controller::Button::DPadRight => state
                            .button_mask
                            .set(super::controller_buffer::RIGHT_IDX, true),
                    }
                }
                Event::ControllerButtonUp { which, button, .. } => {
                    let state = states[*which as usize].as_mut().unwrap();
                    match button {
                        sdl2::controller::Button::A => state
                            .button_mask
                            .set(super::controller_buffer::A_IDX, false),
                        sdl2::controller::Button::B => state
                            .button_mask
                            .set(super::controller_buffer::B_IDX, false),
                        sdl2::controller::Button::X => state
                            .button_mask
                            .set(super::controller_buffer::X_IDX, false),
                        sdl2::controller::Button::Y => state
                            .button_mask
                            .set(super::controller_buffer::Y_IDX, false),
                        sdl2::controller::Button::Back => state
                            .button_mask
                            .set(super::controller_buffer::BACK_IDX, false),
                        sdl2::controller::Button::Guide => state
                            .button_mask
                            .set(super::controller_buffer::GUIDE_IDX, false),
                        sdl2::controller::Button::Start => state
                            .button_mask
                            .set(super::controller_buffer::START_IDX, false),
                        sdl2::controller::Button::LeftStick => state
                            .button_mask
                            .set(super::controller_buffer::LSTICK_IDX, false),
                        sdl2::controller::Button::RightStick => state
                            .button_mask
                            .set(super::controller_buffer::RSTICK_IDX, false),
                        sdl2::controller::Button::LeftShoulder => state
                            .button_mask
                            .set(super::controller_buffer::LB_IDX, false),
                        sdl2::controller::Button::RightShoulder => state
                            .button_mask
                            .set(super::controller_buffer::RB_IDX, false),
                        sdl2::controller::Button::DPadUp => state
                            .button_mask
                            .set(super::controller_buffer::UP_IDX, false),
                        sdl2::controller::Button::DPadDown => state
                            .button_mask
                            .set(super::controller_buffer::DOWN_IDX, false),
                        sdl2::controller::Button::DPadLeft => state
                            .button_mask
                            .set(super::controller_buffer::LEFT_IDX, false),
                        sdl2::controller::Button::DPadRight => state
                            .button_mask
                            .set(super::controller_buffer::RIGHT_IDX, false),
                    }
                }
                _ => {}
            }
        }

        for i in 0..4 {
            if let Some(controller) = &mut controllers[i] {
                if let Some(state) = &mut states[i] {
                    let l_trigger = controller.axis(sdl2::controller::Axis::TriggerLeft);
                    let r_trigger = controller.axis(sdl2::controller::Axis::TriggerRight);
                    let l_stick_x = controller.axis(sdl2::controller::Axis::LeftX);
                    let l_stick_y = controller.axis(sdl2::controller::Axis::LeftY);
                    let r_stick_x = controller.axis(sdl2::controller::Axis::RightX);
                    let r_stick_y = controller.axis(sdl2::controller::Axis::RightY);
                    state.l_trigger = math::quantize::snorm_to_float::<i16, f32>(l_trigger);
                    state.r_trigger = math::quantize::snorm_to_float::<i16, f32>(r_trigger);
                    state.l_stick_x = math::quantize::snorm_to_float::<i16, f32>(l_stick_x);
                    state.l_stick_y = -math::quantize::snorm_to_float::<i16, f32>(l_stick_y);
                    state.r_stick_x = math::quantize::snorm_to_float::<i16, f32>(r_stick_x);
                    state.r_stick_y = -math::quantize::snorm_to_float::<i16, f32>(r_stick_y);
                }
            }
        }
    }

    ///
    /// Handle mouse requests
    ///
    fn handle_mouse(&self, mouse: &MouseUtil) {
        while let Ok(request) = self.mouse_requests.pop() {
            match request {
                MouseRequest::SetRelativeMode(value) => {
                    mouse.set_relative_mouse_mode(value);
                }
            }
        }
    }

    ///
    /// Handle the window events and requests
    ///
    fn handle_window(&self, window: &mut Window, events: &[Event]) {
        let mut window_state = self.window_state.lock();
        for event in events.iter() {
            match event {
                Event::Window { win_event, .. } => {
                    if let WindowEvent::Resized(x, y) = win_event {
                        if !window_state.fullscreen {
                            window_state.windowed_width = *x as u32;
                            window_state.windowed_height = *y as u32;
                        }
                        window_state.current_width = *x as u32;
                        window_state.current_height = *y as u32;
                    }
                }
                _ => {}
            }
        }

        while let Ok(request) = self.window_requests.pop() {
            match request {
                WindowRequest::Resize((width, height)) => {
                    if !window_state.fullscreen {
                        window
                            .set_size(width, height)
                            .expect(err_msg!("Failed to resize window"));
                        window_state.current_width = width;
                        window_state.current_height = height;
                        window_state.windowed_width = width;
                        window_state.windowed_height = height;
                    }
                }
                WindowRequest::ChangeTitle(new_title) => {
                    window
                        .set_title(new_title)
                        .expect(err_msg!("Failed to change window title"));
                    window_state.title = new_title;
                }
                WindowRequest::GoFullscreen => {
                    if !window_state.fullscreen {
                        let video = window.subsystem();

                        let display = window
                            .display_index()
                            .expect(err_msg!("Failed to get window display index"));

                        let mode = video
                            .desktop_display_mode(display)
                            .expect(err_msg!("Failed to get desktop display mode"));

                        window
                            .set_size(mode.w as u32, mode.h as u32)
                            .expect(err_msg!("Failed to resize window while going fullscreen"));

                        window
                            .set_fullscreen(FullscreenType::True)
                            .expect(err_msg!("Failed to set window to fullscreen"));

                        window_state.fullscreen = true;
                        window_state.current_width = mode.w as u32;
                        window_state.current_height = mode.h as u32;
                    }
                }
                WindowRequest::GoWindowed => {
                    if window_state.fullscreen {
                        let width = window_state.windowed_width;
                        let height = window_state.windowed_height;

                        window
                            .set_fullscreen(FullscreenType::Off)
                            .expect(err_msg!("Failed to set window to windowed mode"));

                        window
                            .set_size(width, height)
                            .expect(err_msg!("Failed to resize window while going windowed"));

                        window_state.fullscreen = false;
                        window_state.current_width = width;
                        window_state.current_height = height;
                    }
                }
                WindowRequest::GetInsanceExtensions(request) => {
                    let mut list = request.inner.0.lock();
                    *list = window
                        .vulkan_instance_extensions()
                        .expect(err_msg!("Failed to get window instance extensions"));
                    request.inner.1.notify_one();
                }
                WindowRequest::GetSurface(request) => {
                    let mut handle = request.inner.0.lock();
                    let surface = window
                        .vulkan_create_surface(request.instance.as_raw() as usize)
                        .expect(err_msg!("Failed to create window surface"));
                    *handle = vk::SurfaceKHR::from_raw(surface);
                    request.inner.1.notify_one();
                }
            }
        }
    }
}
