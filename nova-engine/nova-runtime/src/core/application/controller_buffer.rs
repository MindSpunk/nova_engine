/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::core::input::{Axis, Button};
use parking_lot::Mutex;
use std::ops::Deref;
use std::sync::Arc;

pub const A_IDX: usize = 0;
pub const B_IDX: usize = 1;
pub const X_IDX: usize = 2;
pub const Y_IDX: usize = 3;
pub const UP_IDX: usize = 4;
pub const DOWN_IDX: usize = 5;
pub const LEFT_IDX: usize = 6;
pub const RIGHT_IDX: usize = 7;
pub const LB_IDX: usize = 8;
pub const RB_IDX: usize = 9;
pub const LSTICK_IDX: usize = 10;
pub const RSTICK_IDX: usize = 11;
pub const START_IDX: usize = 12;
pub const BACK_IDX: usize = 13;
pub const GUIDE_IDX: usize = 14;

///
/// A button mask for a controller
///
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Debug, Hash, Default)]
pub struct ButtonMask {
    pub(crate) mask: u16,
}

impl ButtonMask {
    pub fn get(&self, idx: usize) -> bool {
        (self.mask & (0b1 << idx)) != 0
    }

    pub fn set(&mut self, idx: usize, on: bool) {
        let mask = self.mask;

        let mask = if on {
            mask & !(0b1 << idx)
        } else {
            mask | (0b1 << idx)
        };

        self.mask = mask;
    }
}

#[derive(Clone, Default)]
pub struct ControllerState {
    pub(crate) l_stick_x: f32,
    pub(crate) l_stick_y: f32,
    pub(crate) r_stick_x: f32,
    pub(crate) r_stick_y: f32,
    pub(crate) l_trigger: f32,
    pub(crate) r_trigger: f32,
    pub(crate) button_mask: ButtonMask,
}

impl ControllerState {
    pub fn get_axis(&self, axis: Axis) -> f32 {
        match axis {
            Axis::LeftStickX => self.l_stick_x,
            Axis::LeftStickY => self.l_stick_y,
            Axis::RightStickX => self.r_stick_x,
            Axis::RightStickY => self.r_stick_y,
            Axis::LeftTrigger => self.l_trigger,
            Axis::RightTrigger => self.r_trigger,
        }
    }

    pub fn get_button_down(&self, button: Button) -> bool {
        match button {
            Button::A => self.button_mask.get(A_IDX),
            Button::B => self.button_mask.get(B_IDX),
            Button::X => self.button_mask.get(X_IDX),
            Button::Y => self.button_mask.get(Y_IDX),
            Button::Up => self.button_mask.get(UP_IDX),
            Button::Down => self.button_mask.get(DOWN_IDX),
            Button::Left => self.button_mask.get(LEFT_IDX),
            Button::Right => self.button_mask.get(RIGHT_IDX),
            Button::LeftBumper => self.button_mask.get(LB_IDX),
            Button::RightBumper => self.button_mask.get(RB_IDX),
            Button::LeftStick => self.button_mask.get(LSTICK_IDX),
            Button::RightStick => self.button_mask.get(RSTICK_IDX),
            Button::Start => self.button_mask.get(START_IDX),
            Button::Back => self.button_mask.get(BACK_IDX),
            Button::Guide => self.button_mask.get(GUIDE_IDX),
        }
    }
}

///
/// A single frame of controller state
///
#[derive(Clone)]
pub struct ControllerFrame {
    state: [Option<ControllerState>; 4],
}

impl Deref for ControllerFrame {
    type Target = [Option<ControllerState>; 4];

    fn deref(&self) -> &Self::Target {
        &self.state
    }
}

impl Default for ControllerFrame {
    fn default() -> Self {
        Self {
            state: [None, None, None, None],
        }
    }
}

///
/// Access to the controller state
///
pub struct ControllerBuffer {
    pub(crate) state: Arc<Mutex<[Option<ControllerState>; 4]>>,
}

impl ControllerBuffer {
    pub fn frame(&self) -> ControllerFrame {
        let state = self.state.lock().clone();
        ControllerFrame { state }
    }
}
