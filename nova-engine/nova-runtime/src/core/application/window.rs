/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use ash::vk;
use crossbeam::queue::ArrayQueue;
use error_message::err_msg;
use parking_lot::{Condvar, Mutex};
use sdl2::event::Event;
use std::sync::Arc;

///
/// Struct that holds the current state of the Window, as was last updated by the SDL thread
///
pub struct WindowState {
    pub(crate) title: &'static str,
    pub(crate) current_width: u32,
    pub(crate) current_height: u32,
    pub(crate) windowed_width: u32,
    pub(crate) windowed_height: u32,
    pub(crate) drawable_width: u32,
    pub(crate) drawable_height: u32,
    pub(crate) fullscreen: bool,
    pub(crate) resizeable: bool,
}

impl WindowState {
    pub fn new() -> Self {
        Self {
            title: "",
            current_width: 0,
            current_height: 0,
            windowed_width: 0,
            windowed_height: 0,
            drawable_width: 0,
            drawable_height: 0,
            fullscreen: false,
            resizeable: false,
        }
    }
}

impl Default for WindowState {
    fn default() -> Self {
        Self::new()
    }
}

///
/// Wrapper for getting a value from the SDL2 thread
///
#[derive(Clone)]
pub struct InstanceExtensionsRequest {
    pub(crate) inner: Arc<(Mutex<Vec<&'static str>>, Condvar)>,
}

///
/// Wrapper for getting a value from the SDL2 thread
///
#[derive(Clone)]
pub struct SurfaceRequest {
    pub(crate) instance: vk::Instance,
    pub(crate) inner: Arc<(Mutex<vk::SurfaceKHR>, Condvar)>,
}

///
/// This is the enum used to request updates to the window.
///
/// We have to do this as these events must be handled on the window thread which we can't guarantee
/// that someone will always be on when calling this function.
///
pub enum WindowRequest {
    Resize((u32, u32)),
    ChangeTitle(&'static str),
    GoFullscreen,
    GoWindowed,
    GetInsanceExtensions(InstanceExtensionsRequest),
    GetSurface(SurfaceRequest),
}

///
/// Represents an OS Window. Wraps most of the SDL2 functionality into a simple interface
///
/// Lifetime of the window is tied to the lifetime of this struct
///
pub struct Window {
    pub(crate) state: Arc<Mutex<WindowState>>,
    pub(crate) events: Arc<ArrayQueue<Event>>,
    pub(crate) requests: Arc<ArrayQueue<WindowRequest>>,
}

impl Window {
    ///
    /// Get the title of the window
    ///
    pub fn title(&self) -> &'static str {
        let state = self.state.lock();
        state.title
    }

    ///
    /// Get the width of the window
    ///
    /// This returns the virtual "size" of the window on the desktop, and is not necessarily the
    /// resolution of the drawable object that the window houses. When creating backing graphics
    /// resources (like the Vulkan swapchain) it is recommended to use one of the
    /// Window::`the drawing_*` functions which will account for this and provide a correct, pixel
    /// accurate size without any scaling that could be applied by the desktop environment.
    ///
    pub fn width(&self) -> u32 {
        let state = self.state.lock();
        state.current_width
    }

    ///
    /// Get the height of the window
    ///
    /// This returns the virtual "size" of the window on the desktop, and is not necessarily the
    /// resolution of the drawable object that the window houses. When creating backing graphics
    /// resources (like the Vulkan swapchain) it is recommended to use one of the
    /// Window::`the drawing_*` functions which will account for this and provide a correct, pixel
    /// accurate size without any scaling that could be applied by the desktop environment.
    ///
    pub fn height(&self) -> u32 {
        let state = self.state.lock();
        state.current_height
    }

    ///
    /// Get the dimensions of the window as a tuple (width,height)
    ///
    /// This returns the virtual "size" of the window on the desktop, and is not necessarily the
    /// resolution of the drawable object that the window houses. When creating backing graphics
    /// resources (like the Vulkan swapchain) it is recommended to use one of the
    /// Window::`the drawing_*` functions which will account for this and provide a correct, pixel
    /// accurate size without any scaling that could be applied by the desktop environment.
    ///
    pub fn dimensions(&self) -> (u32, u32) {
        let state = self.state.lock();
        (state.current_width, state.current_height)
    }

    ///
    /// Get the width of the drawable housed by the window.
    ///
    /// Use this when creating graphics resources (like framebuffers) that depend on the window
    /// resolution
    ///
    pub fn drawable_width(&self) -> u32 {
        let state = self.state.lock();
        state.drawable_width
    }

    ///
    /// Get the height of the drawable housed by the window.
    ///
    /// Use this when creating graphics resources (like framebuffers) that depend on the window
    /// resolution
    ///
    pub fn drawable_height(&self) -> u32 {
        let state = self.state.lock();
        state.drawable_height
    }

    ///
    /// Get the dimensions of the drawable housed by the window.
    ///
    /// Use this when creating graphics resources (like framebuffers) that depend on the window
    /// resolution
    ///
    pub fn drawable_dimensions(&self) -> (u32, u32) {
        let state = self.state.lock();
        (state.drawable_width, state.drawable_height)
    }

    ///
    /// Is the window currently fullscreen
    ///
    pub fn is_fullscreen(&self) -> bool {
        let state = self.state.lock();
        state.fullscreen
    }

    ///
    /// Does the window support resizing
    ///
    pub fn is_resizeable(&self) -> bool {
        let state = self.state.lock();
        state.resizeable
    }

    ///
    /// Return the required list of Vulkan extensions
    ///
    pub fn vulkan_instance_extensions(&self) -> Vec<&'static str> {
        let mutex = Mutex::new(Vec::new());
        let condvar = Condvar::new();
        let request = InstanceExtensionsRequest {
            inner: Arc::new((mutex, condvar)),
        };

        let sdl_request = request.clone();
        let sdl_request = WindowRequest::GetInsanceExtensions(sdl_request);
        self.requests
            .push(sdl_request)
            .expect(err_msg!("Window message queue full"));

        let mut list = request.inner.0.lock();
        if list.is_empty() {
            request.inner.1.wait(&mut list);
        }

        let mut out = Vec::new();
        core::mem::swap(&mut out, &mut list);
        out
    }

    ///
    /// Create a Vulkan surface
    ///
    pub fn vulkan_create_surface(&self, instance: vk::Instance) -> vk::SurfaceKHR {
        let mutex = Mutex::new(vk::SurfaceKHR::null());
        let condvar = Condvar::new();
        let request = SurfaceRequest {
            instance,
            inner: Arc::new((mutex, condvar)),
        };

        let sdl_request = request.clone();
        let sdl_request = WindowRequest::GetSurface(sdl_request);
        self.requests
            .push(sdl_request)
            .expect(err_msg!("Window message queue full"));

        let mut handle = request.inner.0.lock();
        if *handle == vk::SurfaceKHR::null() {
            request.inner.1.wait(&mut handle);
        }

        let mut out = vk::SurfaceKHR::null();
        core::mem::swap(&mut out, &mut handle);
        out
    }

    ///
    /// Resize the window with the given dimension
    ///
    pub fn resize(&self, width: u32, height: u32) {
        let request = WindowRequest::Resize((width, height));
        self.requests
            .push(request)
            .expect(err_msg!("Window message queue full"));
    }

    ///
    /// Set the window's title to the given string
    ///
    /// Must be a static string
    ///
    pub fn set_title(&self, title: &'static str) {
        let request = WindowRequest::ChangeTitle(title);
        self.requests
            .push(request)
            .expect(err_msg!("Window message queue full"));
    }

    ///
    /// Set the window to be fullscreen
    ///
    pub fn set_fullscreen(&self) {
        let request = WindowRequest::GoFullscreen;
        self.requests
            .push(request)
            .expect(err_msg!("Window message queue full"));
    }

    ///
    /// Set the window to not be fullscreen
    ///
    pub fn set_windowed(&self) {
        let request = WindowRequest::GoWindowed;
        self.requests
            .push(request)
            .expect(err_msg!("Window message queue full"));
    }

    ///
    /// Drain the current events into a vector and return it
    ///
    pub fn drain_events(&self) -> Vec<Event> {
        let mut events = Vec::new();
        while let Ok(event) = self.events.pop() {
            events.push(event);
        }
        events
    }
}
