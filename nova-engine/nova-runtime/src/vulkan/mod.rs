/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

mod context;
mod debug;
mod device;
mod surface;
mod swapchain;
mod traits;
pub mod utils;
mod wrappers;

pub use self::traits::VertexFormat;

pub use debug::vulkan_debug_messenger;

pub use context::VulkanContext;
pub use context::VulkanContextBuilder;

pub use device::FamilyType;
pub use device::QueueFamily;
pub use device::VulkanDevice;
pub use device::VulkanDeviceBuilder;

pub use surface::VulkanSurface;
pub use surface::VulkanSurfaceBuilder;

pub use swapchain::SwapchainAquireError;
pub use swapchain::VulkanSwapchain;
pub use swapchain::VulkanSwapchainBuilder;
pub use swapchain::VulkanSwapchainReport;
