/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use ash;
use ash::extensions::ext::DebugUtils;
use ash::extensions::khr::Swapchain;
use ash::version::DeviceV1_0;
use ash::version::InstanceV1_0;
use ash::vk;
use ash::vk::DeviceCreateInfo;
use ash::vk::Handle;

use crate::cstring::cstr;
use crate::vulkan::utils;
use crate::vulkan::utils::vk_bool;
use crate::vulkan::VulkanContext;
use crate::vulkan::VulkanSurface;

use std::ffi::CStr;
use std::fmt::{Debug, Error, Formatter};
use std::mem;
use std::ops::Deref;
use std::os::raw::c_char;

use crate::log::trace;

#[repr(transparent)]
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
///
/// Simple wrapper that handles the bit flags for what types of queue a family is compatible with
///
pub struct FamilyType(u8);

//==================================================================================================
impl FamilyType {
    pub const GRAPHICS: Self = FamilyType(0b00001);
    pub const COMPUTE: Self = FamilyType(0b00010);
    pub const TRANSFER: Self = FamilyType(0b00100);
    pub const SPARSE_BINDING: Self = FamilyType(0b01000);
    pub const PRESENT: Self = FamilyType(0b10000);

    pub const GENERAL: Self = FamilyType(0b10111);

    ///
    /// Constructs a FamilyType with a default internal value of 0
    ///
    pub fn new() -> FamilyType {
        FamilyType(0)
    }

    ///
    /// Whether any of the flags of other can be found in self
    ///
    pub fn intersect(&self, other: &Self) -> bool {
        self.0 & other.0 != 0
    }

    ///
    /// Whether all of the flags of other can be found in self
    ///
    pub fn contains(&self, other: &Self) -> bool {
        self.0 & other.0 == other.0
    }

    ///
    /// Set the flags in the other mask onto self
    ///
    pub fn set(&mut self, other: &Self) {
        self.0 |= other.0;
    }

    ///
    /// Unset all flags in the other mask that are also in self
    ///
    pub fn unset(&mut self, other: &Self) {
        self.0 &= !other.0;
    }
}

//==================================================================================================
impl Into<u8> for FamilyType {
    fn into(self) -> u8 {
        self.0
    }
}

//==================================================================================================
impl Default for FamilyType {
    fn default() -> Self {
        Self::new()
    }
}

impl Debug for FamilyType {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        if f.alternate() {
            let mut flags = String::new();

            if self.intersect(&FamilyType::GRAPHICS) {
                flags.push_str("\n    GRAPHICS,");
            }

            if self.intersect(&FamilyType::COMPUTE) {
                flags.push_str("\n    COMPUTE,");
            }

            if self.intersect(&FamilyType::TRANSFER) {
                flags.push_str("\n    TRANSFER,");
            }

            if self.intersect(&FamilyType::SPARSE_BINDING) {
                flags.push_str("\n    SPARSE_TRANSFER,");
            }

            if self.intersect(&FamilyType::PRESENT) {
                flags.push_str("\n    PRESENT,");
            }

            if flags.is_empty() {
                write!(f, "FamilyType {{ }}")
            } else {
                write!(f, "FamilyType {{{}\n}}", flags)
            }
        } else {
            let mut flags = String::new();

            if self.intersect(&FamilyType::GRAPHICS) {
                flags.push_str("GRAPHICS ");
            }

            if self.intersect(&FamilyType::COMPUTE) {
                flags.push_str("COMPUTE ");
            }

            if self.intersect(&FamilyType::TRANSFER) {
                flags.push_str("TRANSFER ");
            }

            if self.intersect(&FamilyType::SPARSE_BINDING) {
                flags.push_str("SPARSE_TRANSFER ");
            }

            if self.intersect(&FamilyType::PRESENT) {
                flags.push_str("PRESENT ");
            }

            write!(f, "FamilyType {{ {}}}", flags)
        }
    }
}

//==================================================================================================
///
/// Holds the type, index and number of queues that can be found in the queue family
///
#[derive(Clone, Debug)]
pub struct QueueFamily {
    pub i: u32,
    pub count: u32,
    pub family_type: FamilyType,
}

//==================================================================================================
impl QueueFamily {
    ///
    /// Default construction for a QueueFamily
    ///
    pub fn new() -> QueueFamily {
        QueueFamily {
            i: 0,
            count: 0,
            family_type: FamilyType::default(),
        }
    }

    ///
    /// If the queue family is compatible with any types of queue
    ///
    pub fn is_complete(&self) -> bool {
        self.family_type != FamilyType::default()
    }
}

//==================================================================================================
impl Default for QueueFamily {
    fn default() -> Self {
        Self::new()
    }
}

//==================================================================================================
///
/// A utility struct for constructing a VulkanDevice
///
pub struct VulkanDeviceBuilder<'inst, 'surf, 'name> {
    ctx: &'inst VulkanContext,
    surface: Option<&'surf VulkanSurface>,
    enabled_features: vk::PhysicalDeviceFeatures,
    queue_indices: Vec<QueueFamily>,
    extension_names: Vec<&'static CStr>,
    name: Option<&'name CStr>,
    prefer_igpu: bool,
}

//==================================================================================================
impl<'inst, 'surf, 'name> VulkanDeviceBuilder<'inst, 'surf, 'name> {
    ///
    /// Construct a new VulkanDeviceBuilder
    ///
    pub fn new(ctx: &'inst VulkanContext) -> Self {
        VulkanDeviceBuilder {
            ctx,
            surface: None,
            enabled_features: vk::PhysicalDeviceFeatures::default(),
            queue_indices: Vec::new(),
            extension_names: Vec::new(),
            name: None,
            prefer_igpu: false,
        }
    }

    ///
    /// Set if device should support tesselation shaders
    ///
    pub fn tesselation_shader(mut self, enable: bool) -> Self {
        self.enabled_features.tessellation_shader = vk_bool(enable);
        self
    }

    pub fn prefer_igpu(mut self) -> Self {
        self.prefer_igpu = true;
        self
    }

    ///
    /// Set if device should support geometry shaders
    ///
    pub fn geometry_shader(mut self, enable: bool) -> Self {
        self.enabled_features.geometry_shader = vk_bool(enable);
        self
    }

    ///
    /// Add an extension to be loaded by name
    ///
    pub fn extension(mut self, extension: &'static CStr) -> Self {
        self.extension_names.push(extension);
        self
    }

    ///
    /// Make sure device supports this surface
    ///
    pub fn supports_surface(mut self, surface: &'surf VulkanSurface) -> Self {
        self.surface = Some(surface);
        self
    }

    ///
    /// Name the device if the debug_utils extension is loaded
    ///
    pub fn name(mut self, name: &'name CStr) -> Self {
        self.name = Some(name);
        self
    }

    ///
    /// Construct the VulkanDevice
    ///
    /// # Panics
    ///
    /// - If we fail to enumerate the available devices
    /// - If we fail to enumerate extension properties
    /// - If we can't find a usable GPU
    /// - If we fail to create a vulkan device
    ///
    pub unsafe fn build(mut self) -> VulkanDevice {
        let ctx = self.ctx;

        let gpu = utils::select_device(ctx, &self.enabled_features, self.surface, self.prefer_igpu);
        if gpu.is_none() {
            panic!("Failed to find a usable GPU");
        }
        let gpu = gpu.unwrap();

        self.select_queue_indices(gpu);

        // If we end up with 128 queues in one family we're probably a very long way in the
        // future so this should be fine for now
        let priorities = [1.0f32; 128];

        let mut queues: Vec<vk::DeviceQueueCreateInfo> = Vec::new();

        for family in self.queue_indices.iter() {
            queues.push(
                vk::DeviceQueueCreateInfo::builder()
                    .queue_family_index(family.i)
                    .queue_priorities(&priorities[0..family.count as usize])
                    .build(),
            );
        }

        if ctx.surface_loader().is_some() {
            self = self.extension(cstr!("VK_KHR_swapchain"));
        }

        let supported_extensions = ctx.enumerate_device_extension_properties(gpu).unwrap();

        trace!("[Supported Extensions]");
        for ext in supported_extensions.iter() {
            let ext_ptr = ext.extension_name.as_ptr() as *const c_char;
            let ext_str = CStr::from_ptr(ext_ptr);
            trace!("| {:?}", ext_str);
        }

        trace!("[Wanted Extensions]");
        for ext in self.extension_names.iter() {
            trace!("| {:?}", ext);
        }

        let extension_names: Vec<*const c_char> = self
            .extension_names
            .iter()
            .map(|val| val.as_ptr())
            .collect();

        let create_info = DeviceCreateInfo::builder()
            .enabled_features(&self.enabled_features)
            .enabled_extension_names(&extension_names)
            .queue_create_infos(&queues)
            .build();

        let device = self
            .ctx
            .instance()
            .create_device(gpu, &create_info, None)
            .expect("Failed to create vulkan device");

        let swap_loader = if self.ctx.surface_loader().is_some() {
            Some(Swapchain::new(self.ctx.instance(), &device))
        } else {
            None
        };

        let gpu_props = ctx.instance().get_physical_device_properties(gpu);
        let device_name = CStr::from_ptr(gpu_props.device_name.as_ptr());
        let device_name = device_name
            .to_str()
            .expect("Whoever wrote this GPU driver is an idiot and used non ASCII or UTF-8");

        if let Some(name) = self.name {
            if let Some(loader) = self.ctx.debug_loader() {
                let name_info = vk::DebugUtilsObjectNameInfoEXT::builder()
                    .object_handle(device.handle().as_raw())
                    .object_type(vk::ObjectType::DEVICE)
                    .object_name(name)
                    .build();
                loader
                    .debug_utils_set_object_name(device.handle(), &name_info)
                    .expect("Failed to name object");
            }
        }

        VulkanDevice {
            queue_families: self.queue_indices,
            gpu,
            device,
            swap_loader,
            debug_loader: self.ctx.debug_loader().cloned(),
            initialized: true,
            device_name,
        }
    }

    ///
    /// Internal function for setting up the list of queue indices
    ///
    fn select_queue_indices(&mut self, gpu: vk::PhysicalDevice) {
        let instance = self.ctx.instance();

        let families = unsafe { instance.get_physical_device_queue_family_properties(gpu) };

        for (i, family) in families.iter().enumerate() {
            let mut index = QueueFamily {
                i: i as u32,
                count: family.queue_count,
                family_type: FamilyType::default(),
            };

            // If the queue count is 0 then there's no real point looking for what it supports
            if family.queue_count > 0 {
                // We only want to look for present queues if we have a surface available for us to
                // use
                match self.surface {
                    None => {}
                    Some(surface) => {
                        if surface.device_queue_supported(gpu, i as u32) {
                            index.family_type.set(&FamilyType::PRESENT);
                        }
                    }
                }

                if family.queue_flags.intersects(vk::QueueFlags::GRAPHICS) {
                    index.family_type.set(&FamilyType::GRAPHICS);
                }

                if family.queue_flags.intersects(vk::QueueFlags::COMPUTE) {
                    index.family_type.set(&FamilyType::COMPUTE);
                }

                if family.queue_flags.intersects(vk::QueueFlags::TRANSFER) {
                    index.family_type.set(&FamilyType::TRANSFER);
                }

                if family
                    .queue_flags
                    .intersects(vk::QueueFlags::SPARSE_BINDING)
                {
                    index.family_type.set(&FamilyType::SPARSE_BINDING);
                }
            } else {
            }

            self.queue_indices.push(index);
        }
    }
}

//==================================================================================================
///
/// A wrapper around a vulkan device that caches some important information, like the queue indices
/// and the physical device it is associated with
///
pub struct VulkanDevice {
    queue_families: Vec<QueueFamily>,
    gpu: vk::PhysicalDevice,
    device: ash::Device,
    device_name: &'static str,
    swap_loader: Option<Swapchain>,
    debug_loader: Option<DebugUtils>,
    initialized: bool,
}

//==================================================================================================
impl VulkanDevice {
    ///
    /// Return a builder instance for constructing VulkanDevice
    ///
    pub fn builder<'inst, 'surf, 'name>(
        ctx: &'inst VulkanContext,
    ) -> VulkanDeviceBuilder<'inst, 'surf, 'name> {
        VulkanDeviceBuilder::new(ctx)
    }

    ///
    /// Cleanup the internal handles and close the device
    ///
    pub unsafe fn destroy(&mut self) {
        self.device
            .device_wait_idle()
            .expect("Failed to wait on device");

        self.device.destroy_device(None);

        self.initialized = false;
    }

    pub unsafe fn build_semaphore(
        &self,
        allocation_callbacks: Option<&vk::AllocationCallbacks>,
    ) -> Result<vk::Semaphore, vk::Result> {
        static INFO: [i32; 5] = [9, 0, 0, 0, 0];

        self.device
            .create_semaphore(mem::transmute(&INFO), allocation_callbacks)
    }

    pub fn get_general_queue_family(&self) -> Option<QueueFamily> {
        for family in self.queue_families.iter() {
            if family.family_type.intersect(&FamilyType::GENERAL) {
                return Some(family.clone());
            }
        }
        None
    }

    ///
    /// Return the underlying physical device handle
    ///
    pub fn gpu(&self) -> vk::PhysicalDevice {
        self.gpu
    }

    ///
    /// Return a reference to the ash::Device held internally
    ///
    pub fn device(&self) -> &ash::Device {
        &self.device
    }

    ///
    /// Return an optional reference to the swapchain extension loader
    ///
    pub fn swap_loader(&self) -> Option<&Swapchain> {
        self.swap_loader.as_ref()
    }

    ///
    /// Return a reference to the list of queue families
    ///
    pub fn queue_families(&self) -> &Vec<QueueFamily> {
        &self.queue_families
    }

    ///
    /// Return a &str to the name of the selected GPU
    ///
    pub fn gpu_name(&self) -> &str {
        &self.device_name
    }

    ///
    /// Name the object if the debug_utils extension is loaded
    ///
    pub fn name_buffer(&self, buffer: vk::Buffer, name: &CStr) {
        let handle = buffer.as_raw();
        let htype = vk::ObjectType::BUFFER;
        self.name_handle(handle, htype, name);
    }

    ///
    /// Name the object if the debug_utils extension is loaded
    ///
    pub fn name_buffer_view(&self, module: vk::BufferView, name: &CStr) {
        let handle = module.as_raw();
        let htype = vk::ObjectType::BUFFER_VIEW;
        self.name_handle(handle, htype, name);
    }

    ///
    /// Name the object if the debug_utils extension is loaded
    ///
    pub fn name_command_buffer(&self, command_buffer: vk::CommandBuffer, name: &CStr) {
        let handle = command_buffer.as_raw();
        let htype = vk::ObjectType::COMMAND_BUFFER;
        self.name_handle(handle, htype, name);
    }

    ///
    /// Name the object if the debug_utils extension is loaded
    ///
    pub fn name_command_pool(&self, command_pool: vk::CommandPool, name: &CStr) {
        let handle = command_pool.as_raw();
        let htype = vk::ObjectType::COMMAND_POOL;
        self.name_handle(handle, htype, name);
    }

    ///
    /// Name the object if the debug_utils extension is loaded
    ///
    pub fn name_shader_module(&self, module: vk::ShaderModule, name: &CStr) {
        let handle = module.as_raw();
        let htype = vk::ObjectType::SHADER_MODULE;
        self.name_handle(handle, htype, name);
    }

    ///
    /// Name the object if the debug_utils extension is loaded
    ///
    pub fn name_descriptor_pool(&self, pool: vk::DescriptorPool, name: &CStr) {
        let handle = pool.as_raw();
        let htype = vk::ObjectType::DESCRIPTOR_POOL;
        self.name_handle(handle, htype, name);
    }

    ///
    /// Name the object if the debug_utils extension is loaded
    ///
    pub fn name_descriptor_set(&self, set: vk::DescriptorSet, name: &CStr) {
        let handle = set.as_raw();
        let htype = vk::ObjectType::DESCRIPTOR_SET;
        self.name_handle(handle, htype, name);
    }

    ///
    /// Name the object if the debug_utils extension is loaded
    ///
    pub fn name_descriptor_set_layout(&self, set_layout: vk::DescriptorSetLayout, name: &CStr) {
        let handle = set_layout.as_raw();
        let htype = vk::ObjectType::DESCRIPTOR_SET_LAYOUT;
        self.name_handle(handle, htype, name);
    }

    ///
    /// Name the object if the debug_utils extension is loaded
    ///
    pub fn name_pipeline(&self, pipeline: vk::Pipeline, name: &CStr) {
        let handle = pipeline.as_raw();
        let htype = vk::ObjectType::PIPELINE;
        self.name_handle(handle, htype, name);
    }

    ///
    /// Name the object if the debug_utils extension is loaded
    ///
    pub fn name_pipeline_layout(&self, layout: vk::PipelineLayout, name: &CStr) {
        let handle = layout.as_raw();
        let htype = vk::ObjectType::PIPELINE_LAYOUT;
        self.name_handle(handle, htype, name);
    }

    ///
    /// Name the object if the debug_utils extension is loaded
    ///
    pub fn name_pipeline_cache(&self, cache: vk::PipelineCache, name: &CStr) {
        let handle = cache.as_raw();
        let htype = vk::ObjectType::PIPELINE_CACHE;
        self.name_handle(handle, htype, name);
    }

    ///
    /// Name the object if the debug_utils extension is loaded
    ///
    pub fn name_queue(&self, queue: vk::Queue, name: &CStr) {
        let handle = queue.as_raw();
        let htype = vk::ObjectType::QUEUE;
        self.name_handle(handle, htype, name);
    }

    #[inline]
    fn name_handle(&self, handle: u64, htype: vk::ObjectType, name: &CStr) {
        unsafe {
            if let Some(debug_loader) = &self.debug_loader {
                let name_info = vk::DebugUtilsObjectNameInfoEXT::builder()
                    .object_handle(handle)
                    .object_type(htype)
                    .object_name(name)
                    .build();
                debug_loader
                    .debug_utils_set_object_name(self.device.handle(), &name_info)
                    .expect("Failed to name object");
            }
        }
    }

    ///
    /// Begins a new debug label section in the command buffer
    ///
    pub fn begin_command_section(
        &self,
        command_buffer: vk::CommandBuffer,
        name: &CStr,
        color: [f32; 4],
    ) {
        unsafe {
            if let Some(debug_loader) = &self.debug_loader {
                let label = vk::DebugUtilsLabelEXT::builder()
                    .color(color)
                    .label_name(name)
                    .build();
                debug_loader.cmd_begin_debug_utils_label(command_buffer, &label);
            }
        }
    }

    ///
    /// Ends a debug label section in the command buffer
    ///
    pub fn end_command_section(&self, command_buffer: vk::CommandBuffer) {
        unsafe {
            if let Some(debug_loader) = &self.debug_loader {
                debug_loader.cmd_end_debug_utils_label(command_buffer);
            }
        }
    }

    ///
    /// Begins a new debug label section in the command buffer
    ///
    pub fn insert_command_section(
        &self,
        command_buffer: vk::CommandBuffer,
        name: &CStr,
        color: [f32; 4],
    ) {
        unsafe {
            if let Some(debug_loader) = &self.debug_loader {
                let label = vk::DebugUtilsLabelEXT::builder()
                    .color(color)
                    .label_name(name)
                    .build();
                debug_loader.cmd_insert_debug_utils_label(command_buffer, &label);
            }
        }
    }

    ///
    /// Begins a new debug label section in the queue
    ///
    pub fn begin_queue_section(&self, queue: vk::Queue, name: &CStr, color: [f32; 4]) {
        unsafe {
            if let Some(debug_loader) = &self.debug_loader {
                let label = vk::DebugUtilsLabelEXT::builder()
                    .color(color)
                    .label_name(name)
                    .build();
                debug_loader.queue_begin_debug_utils_label(queue, &label);
            }
        }
    }

    ///
    /// Ends a debug label section in the queue
    ///
    pub fn end_queue_section(&self, queue: vk::Queue) {
        unsafe {
            if let Some(debug_loader) = &self.debug_loader {
                debug_loader.queue_end_debug_utils_label(queue);
            }
        }
    }

    ///
    /// Begins a new debug label section in the command buffer
    ///
    pub fn insert_queue_section(&self, queue: vk::Queue, name: &CStr, color: [f32; 4]) {
        unsafe {
            if let Some(debug_loader) = &self.debug_loader {
                let label = vk::DebugUtilsLabelEXT::builder()
                    .color(color)
                    .label_name(name)
                    .build();
                debug_loader.queue_insert_debug_utils_label(queue, &label);
            }
        }
    }
}

//==================================================================================================
impl<'inst> Drop for VulkanDevice {
    fn drop(&mut self) {
        if self.initialized {
            panic!("VulkanDevice dropped without being destroyed");
        }
    }
}

//==================================================================================================
impl<'inst> Deref for VulkanDevice {
    type Target = ash::Device;

    fn deref(&self) -> &Self::Target {
        &self.device
    }
}
