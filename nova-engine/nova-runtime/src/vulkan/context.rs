/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use ash;
use ash::extensions::ext::DebugUtils;
use ash::extensions::khr::Surface;
use ash::version::EntryV1_0;
use ash::version::InstanceV1_0;
use ash::vk;
use ash::vk_make_version;

use std::ffi::CStr;
use std::ops::Deref;
use std::os::raw::c_char;

use crate::core::application::Window;
use crate::cstring::cstr;
use crate::log::trace;

//==================================================================================================
///
/// A utility struct for constructing a VulkanContext
///
/// Manages extensions, required versions and layers, etc
///
#[derive(Default)]
pub struct VulkanContextBuilder {
    extension_names: Vec<&'static CStr>,
    layer_names: Vec<&'static CStr>,
    app_name: &'static CStr,
    app_version: u32,
    api_version: u32,
    debug: bool,
    support_surface: bool,
}

//==================================================================================================
impl VulkanContextBuilder {
    ///
    /// Construct a new VulkanContextBuilder with a default minimum viable state
    ///
    pub fn new() -> Self {
        VulkanContextBuilder {
            extension_names: Vec::new(),
            layer_names: Vec::new(),
            app_name: cstr!("Default Nova Game"),
            app_version: vk_make_version!(1, 0, 0),
            api_version: vk_make_version!(1, 0, 0),
            debug: false,
            support_surface: false,
        }
    }

    ///
    /// Set the name of the application
    ///
    /// # Default:
    ///
    /// Defaults to: "Default Nova Game"
    ///
    pub fn application_name(mut self, name: &'static CStr) -> Self {
        self.app_name = name;
        self
    }

    ///
    /// Set the version of the application
    ///
    /// # Default:
    ///
    /// Defaults to: 1.0.0
    ///
    pub fn app_version(mut self, major: u32, minor: u32, patch: u32) -> Self {
        self.app_version = vk_make_version!(major, minor, patch);
        self
    }

    ///
    /// Set which version of the vulkan API we are using
    ///
    /// # Default:
    ///
    /// Defaults to: 1.0.0
    ///
    pub fn api_version(mut self, major: u32, minor: u32, patch: u32) -> Self {
        self.api_version = vk_make_version!(major, minor, patch);
        self
    }

    ///
    /// Require and load the given extension by name
    ///
    pub fn extension(mut self, extension: &'static CStr) -> Self {
        self.extension_names.push(extension);
        self
    }

    ///
    /// Require and load the given layer by name
    ///
    pub fn layer(mut self, layer: &'static CStr) -> Self {
        self.layer_names.push(layer);
        self
    }

    ///
    /// Load the debug report extension and the standard validation layers
    ///
    pub fn debug(mut self) -> Self {
        self.debug = true;
        if target::build::target_platform().is_android() {
            self.layer(cstr!("VK_LAYER_GOOGLE_threading"))
                .layer(cstr!("VK_LAYER_LUNARG_parameter_validation"))
                .layer(cstr!("VK_LAYER_LUNARG_object_tracker"))
                .layer(cstr!("VK_LAYER_LUNARG_core_validation"))
                .layer(cstr!("VK_LAYER_GOOGLE_unique_objects"))
        //.extension(DebugUtils::name())
        } else {
            self.layer(cstr!("VK_LAYER_LUNARG_standard_validation"))
                .extension(DebugUtils::name())
        }
    }

    ///
    /// Add support to the context to build surfaces/present to a window using a surface
    ///
    /// Uses the window to decide which Vulkan surface extensions to load
    ///
    pub fn support_surface(mut self, window: &Window) -> Self {
        let extensions = window.vulkan_instance_extensions();

        for extension in extensions.iter() {
            let extension = unsafe {
                let ptr = extension.as_ptr();
                let ptr = ptr as *const c_char;
                CStr::from_ptr(ptr)
            };
            self = self.extension(extension);
        }

        self.support_surface = true;
        self
    }

    ///
    /// Construct the VulkanContext
    ///
    /// This has a number of ways to produce a panic, but all panics are triggered in cases where
    /// there are major underlying issues that will prevent the game engine from ever doing anything
    /// useful so there's no real reason to add more API complexity to deal with almost certainly
    /// fatal errors.
    ///
    /// # Panics
    ///
    /// - If we fail to initialize the vulkan library (can't find vulkan-1.dll, etc)
    /// - If we fail to create the vulkan instance (almost certainly BIG problems)
    /// - If we fail to initialize the validation layers if requested (certainly BIG problems)
    /// - If we fail to create the surface if requested (certainly BIG problems)
    ///
    pub unsafe fn build(self) -> VulkanContext {
        let entry: ash::Entry = ash::Entry::new().expect("Cannot initialize vulkan library");

        let app_info = vk::ApplicationInfo::builder()
            .application_name(&self.app_name)
            .application_version(self.app_version)
            .engine_name(cstr!("NovaEngine"))
            .engine_version(vk_make_version!(0, 4, 0))
            .api_version(self.api_version)
            .build();

        let supported_extensions = entry.enumerate_instance_extension_properties().unwrap();

        trace!("[Supported Extensions]");
        for ext in supported_extensions.iter() {
            let ext_ptr = ext.extension_name.as_ptr() as *const c_char;
            let ext_str = CStr::from_ptr(ext_ptr);
            trace!("| {:?}", ext_str);
        }

        let layer_names: Vec<*const c_char> =
            self.layer_names.iter().map(|val| val.as_ptr()).collect();
        let extension_names: Vec<*const c_char> = self
            .extension_names
            .iter()
            .map(|val| val.as_ptr())
            .collect();

        trace!("[Wanted Extensions]");
        for ext in extension_names.iter() {
            let ext_ptr = (*ext) as *const c_char;
            let ext_str = CStr::from_ptr(ext_ptr);
            trace!("| {:?}", ext_str);
        }

        let create_info = vk::InstanceCreateInfo::builder()
            .application_info(&app_info)
            .enabled_layer_names(&layer_names)
            .enabled_extension_names(&extension_names)
            .build();

        let instance = {
            match entry.create_instance(&create_info, None) {
                Ok(instance) => instance,
                Err(error) => {
                    panic!(
                        "Failed to create a vulkan instance.\nReason: {}",
                        error.to_string()
                    );
                }
            }
        };

        let mut debug_loader: Option<DebugUtils> = None;
        let mut debug_messenger = vk::DebugUtilsMessengerEXT::default();
        if self.debug && !target::build::target_platform().is_android() {
            let loader = DebugUtils::new(&entry, &instance);

            let severity = vk::DebugUtilsMessageSeverityFlagsEXT::ERROR
                | vk::DebugUtilsMessageSeverityFlagsEXT::INFO
                | vk::DebugUtilsMessageSeverityFlagsEXT::VERBOSE
                | vk::DebugUtilsMessageSeverityFlagsEXT::WARNING;
            let types = vk::DebugUtilsMessageTypeFlagsEXT::PERFORMANCE
                | vk::DebugUtilsMessageTypeFlagsEXT::VALIDATION;
            let messenger = vk::DebugUtilsMessengerCreateInfoEXT::builder()
                .message_severity(severity)
                .message_type(types)
                .pfn_user_callback(Some(crate::vulkan::vulkan_debug_messenger))
                .build();
            debug_messenger = loader
                .create_debug_utils_messenger(&messenger, None)
                .expect("Failed to create debug messenger");

            debug_loader = Some(loader);
        }

        let surface_loader = if self.support_surface {
            Some(Surface::new(&entry, &instance))
        } else {
            None
        };

        VulkanContext {
            surface_loader,
            entry,
            instance,
            debug_messenger,
            debug_loader,
            initialized: true,
        }
    }
}

//==================================================================================================
///
/// Wraps some minimal state for using vulkan. Holds the instance and entry for initial loading
/// of the function pointers and will optionally setup a surface attached to a provided window
/// as well as optionally set up the standard validation layers
///
/// If told to setup a surface then the lifetime if this struct will be enforced to not outlive
/// the window it is attached to
///
pub struct VulkanContext {
    entry: ash::Entry,
    instance: ash::Instance,
    surface_loader: Option<Surface>,
    debug_messenger: vk::DebugUtilsMessengerEXT,
    debug_loader: Option<DebugUtils>,
    initialized: bool,
}

//==================================================================================================
impl VulkanContext {
    ///
    /// Return a builder instance for constructing a VulkanContext
    ///
    pub fn builder() -> VulkanContextBuilder {
        VulkanContextBuilder::new()
    }

    ///
    /// Cleanup the internal handles and close the instance
    ///
    pub unsafe fn destroy(&mut self) {
        match &self.debug_loader {
            Some(loader) => {
                loader.destroy_debug_utils_messenger(self.debug_messenger, None);
            }
            None => {}
        }
        self.instance.destroy_instance(None);

        self.initialized = false;
    }

    ///
    /// Return a reference to the ash entry struct held internally
    ///
    pub fn entry(&self) -> &ash::Entry {
        &self.entry
    }

    ///
    /// Return a reference to the ash instance struct held internally
    ///
    pub fn instance(&self) -> &ash::Instance {
        &self.instance
    }

    ///
    /// Return an optional reference to the surface extension loader held internally
    ///
    pub fn surface_loader(&self) -> Option<&Surface> {
        self.surface_loader.as_ref()
    }

    pub fn debug_loader(&self) -> Option<&DebugUtils> {
        self.debug_loader.as_ref()
    }
}

//==================================================================================================
impl<'wind> Drop for VulkanContext {
    fn drop(&mut self) {
        if self.initialized {
            panic!("VulkanContext dropped without being destroyed");
        }
    }
}

//==================================================================================================
impl Deref for VulkanContext {
    type Target = ash::Instance;

    fn deref(&self) -> &Self::Target {
        &self.instance
    }
}
