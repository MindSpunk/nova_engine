/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use ash;
use ash::extensions;
use ash::version::InstanceV1_0;
use ash::vk;
use ash::vk_make_version;

use std::ffi::CStr;

use crate::cstring::cstr;
use crate::vulkan::VulkanContext;
use crate::vulkan::VulkanSurface;

pub const VK_VENDOR_ID_AMD: u32 = 0x1002;
pub const VK_VENDOR_ID_IMGTECH: u32 = 0x1010;
pub const VK_VENDOR_ID_NVIDIA: u32 = 0x10DE;
pub const VK_VENDOR_ID_ARM: u32 = 0x13B5;
pub const VK_VENDOR_ID_QUALCOMM: u32 = 0x5143;
pub const VK_VENDOR_ID_INTEL: u32 = 0x8086;

//==================================================================================================
///
/// Simple collection of some information for swapchain support
///
struct SwapChainSupport {
    formats: Vec<vk::SurfaceFormatKHR>,
    present_modes: Vec<vk::PresentModeKHR>,
}

//==================================================================================================
///
/// Simple function for converting a vk::Bool32 to a bool
///
#[inline]
pub fn vk_bool(b: bool) -> vk::Bool32 {
    if b {
        vk::TRUE
    } else {
        vk::FALSE
    }
}

//==================================================================================================
///
/// Returns whether the device supports the surface in use by ctx and additional information
/// about the devices swapchain abilities
///
/// Returns Some(SwapChainSupport) if can present to surface and None if the device can not present
/// to the surface
///
fn get_swapchain_support(
    device: vk::PhysicalDevice,
    ctx: &VulkanContext,
    surface: &VulkanSurface,
) -> Option<SwapChainSupport> {
    let surface_loader = ctx
        .surface_loader()
        .expect("Can't get swapchain support without the extension loaded");

    let surface = surface.handle();

    let formats;
    let present_modes;

    unsafe {
        let fmts = surface_loader.get_physical_device_surface_formats(device, surface);
        if fmts.is_err() {
            return None;
        }
        formats = fmts.unwrap();
    }

    unsafe {
        let p = surface_loader.get_physical_device_surface_present_modes(device, surface);
        if p.is_err() {
            return None;
        }
        present_modes = p.unwrap();
    }

    Some(SwapChainSupport {
        formats,
        present_modes,
    })
}

//==================================================================================================
///
/// Chose the best available physical device
///
pub fn select_device(
    ctx: &VulkanContext,
    features: &vk::PhysicalDeviceFeatures,
    surface: Option<&VulkanSurface>,
    prefer_igpu: bool,
) -> Option<vk::PhysicalDevice> {
    let instance = ctx.instance();

    let devices = unsafe { instance.enumerate_physical_devices() };
    let devices = devices.expect("Failed to list vulkan devices");
    let mut scores: Vec<(vk::PhysicalDevice, i32)> = Vec::new();

    for device in devices.iter() {
        let score = (*device, 0i32);

        scores.push(score);

        let score = scores.last_mut().unwrap();

        let (props, feats, extns) = unsafe {
            let props = instance.get_physical_device_properties(*device);
            let feats = instance.get_physical_device_features(*device);
            let extns = instance
                .enumerate_device_extension_properties(*device)
                .expect("Failed to list extension properties");
            (props, feats, extns)
        };

        let _name = unsafe { CStr::from_ptr(props.device_name.as_ptr()).to_str().unwrap() };

        if prefer_igpu {
            if props.vendor_id == VK_VENDOR_ID_AMD || props.vendor_id == VK_VENDOR_ID_INTEL {
                score.1 += 10000;
            }
        } else if props.vendor_id == VK_VENDOR_ID_AMD || props.vendor_id == VK_VENDOR_ID_NVIDIA {
            score.1 += 10000;
        }

        match surface {
            None => {}
            Some(surface) => {
                let mut supported: bool = false;
                for e in extns.iter() {
                    let current_name = unsafe { CStr::from_ptr(e.extension_name.as_ptr()) };
                    let current_name = current_name.to_str().unwrap();
                    let search_name = extensions::khr::Surface::name();
                    let search_name = search_name.to_str().unwrap();
                    if current_name == search_name {
                        supported = true;
                        break;
                    }
                }
                // We're missing required extensions so this device can not be used
                if !supported {
                    score.1 = -1_000_000;
                    continue;
                }

                let swap_support = get_swapchain_support(*device, ctx, surface);

                // We want something we can swap with
                if swap_support.is_none() {
                    score.1 = -1_000_000;
                    continue;
                }

                let swap_support = swap_support.unwrap();

                if swap_support.present_modes.is_empty() {
                    score.1 = -1_000_000;
                    continue;
                }

                if swap_support.formats.is_empty() {
                    score.1 = -1_000_000;
                    continue;
                }

                // This should never happen but always good to be safe
                if props.api_version < vk_make_version!(1, 0, 0) {
                    score.1 = -100_000;
                    continue;
                }

                // A discrete gpu will almost always be faster than any thing else in the system.
                if props.device_type == vk::PhysicalDeviceType::DISCRETE_GPU {
                    if prefer_igpu {
                        score.1 += 1000;
                    } else {
                        score.1 += 10000;
                    }
                }
                // Next best is an integrated GPU which is at the very least an actual hardware GPU
                else if props.device_type == vk::PhysicalDeviceType::INTEGRATED_GPU {
                    if prefer_igpu {
                        score.1 += 10000;
                    } else {
                        score.1 += 1000;
                    }
                }
                // Otherwise anything is probably WAAAY to slow to be useful so we REALLY don't want to
                // use it if we can avoid it
                else {
                    score.1 -= 1000;
                }

                // Tesselation shaders are very powerful so we would like to weight it pretty high
                if features.tessellation_shader == vk::TRUE && feats.tessellation_shader == vk::TRUE
                {
                    score.1 += 3000
                }

                // Geometry shaders are pretty crap but may as well check for them
                if features.geometry_shader == vk::TRUE && feats.geometry_shader == vk::TRUE {
                    score.1 += 3000
                }
            }
        }
    }

    let mut final_device = (vk::PhysicalDevice::null(), -100_000_000i32);

    for score in scores.iter() {
        if score.0 != vk::PhysicalDevice::null() && score.1 > final_device.1 {
            final_device = *score;
        }
    }

    if final_device.0 == vk::PhysicalDevice::null() && final_device.1 <= 0 {
        return None;
    }

    Some(final_device.0)
}

///
/// Returns a `vk::PipelineColorBlendAttachmentState` for a pipeline that will not perform any
/// colour blending.
///
pub fn blend_attachment_none(
    mask: vk::ColorComponentFlags,
) -> vk::PipelineColorBlendAttachmentState {
    vk::PipelineColorBlendAttachmentState::builder()
        .color_write_mask(mask)
        .blend_enable(false)
        .src_color_blend_factor(vk::BlendFactor::ONE)
        .dst_color_blend_factor(vk::BlendFactor::ZERO)
        .color_blend_op(vk::BlendOp::ADD)
        .src_alpha_blend_factor(vk::BlendFactor::ONE)
        .dst_alpha_blend_factor(vk::BlendFactor::ZERO)
        .alpha_blend_op(vk::BlendOp::ADD)
        .build()
}

///
/// Returns a `vk::PipelineColorBlendStateCreateInfo` for a pipeline that will not perform any
/// colour blending.
///
pub fn blend_state_none(
    attachments: &[vk::PipelineColorBlendAttachmentState],
) -> vk::PipelineColorBlendStateCreateInfo {
    vk::PipelineColorBlendStateCreateInfo::builder()
        .logic_op_enable(false)
        .logic_op(vk::LogicOp::COPY)
        .attachments(&attachments)
        .blend_constants([0.0f32, 0.0f32, 0.0f32, 0.0f32])
        .build()
}

///
/// Returns a `vk::PipelineInputAssemblyStateCreateInfo` for a pipeline dealing with a triangle list
///
pub fn input_assembly_tri_list() -> vk::PipelineInputAssemblyStateCreateInfo {
    vk::PipelineInputAssemblyStateCreateInfo::builder()
        .primitive_restart_enable(false)
        .topology(vk::PrimitiveTopology::TRIANGLE_LIST)
        .build()
}

///
/// Returns a standard viewport of given `width` and `height`
///
pub fn viewport(width: f32, height: f32) -> vk::Viewport {
    vk::Viewport::builder()
        .x(0f32)
        .y(0f32)
        .width(width)
        .height(height)
        .min_depth(0.0f32)
        .max_depth(1.0f32)
        .build()
}

///
/// Returns a `vk::Rect2D` that represents a scissor region that applies no scissoring
///
pub fn scissor_none(extent: vk::Extent2D) -> vk::Rect2D {
    vk::Rect2D::builder()
        .offset(vk::Offset2D { x: 0, y: 0 })
        .extent(extent)
        .build()
}

///
/// Returns a `vk::PipelineMultisampleStateCreateInfo` for no performing no multisampling
///
pub fn multisampling_none() -> vk::PipelineMultisampleStateCreateInfo {
    vk::PipelineMultisampleStateCreateInfo::builder()
        .sample_shading_enable(false)
        .rasterization_samples(vk::SampleCountFlags::TYPE_1)
        .min_sample_shading(1.0f32)
        .alpha_to_coverage_enable(false)
        .alpha_to_one_enable(false)
        .build()
}

///
/// Returns a `vk::PipelineShaderStageCreateInfo` for a fragment stage for the given module with an
/// entry point named "main"
///
pub fn frag_shader_stage(module: vk::ShaderModule) -> vk::PipelineShaderStageCreateInfo {
    vk::PipelineShaderStageCreateInfo::builder()
        .stage(vk::ShaderStageFlags::FRAGMENT)
        .module(module)
        .name(cstr!("main"))
        .build()
}

///
/// Returns a `vk::PipelineShaderStageCreateInfo` for a vertex stage for the given module with an
/// entry point named "main"
///
pub fn vert_shader_stage(module: vk::ShaderModule) -> vk::PipelineShaderStageCreateInfo {
    vk::PipelineShaderStageCreateInfo::builder()
        .stage(vk::ShaderStageFlags::VERTEX)
        .module(module)
        .name(cstr!("main"))
        .build()
}

///
/// Returns a `vk::PipelineRasterizationStateCreateInfo` for rasterizing with polygon fill, backface
/// culling and clockwise winding order for front faces
///
pub fn rasterizer_back_clockwise() -> vk::PipelineRasterizationStateCreateInfo {
    vk::PipelineRasterizationStateCreateInfo::builder()
        .rasterizer_discard_enable(false)
        .polygon_mode(vk::PolygonMode::FILL)
        .line_width(1.0f32)
        .cull_mode(vk::CullModeFlags::BACK)
        .front_face(vk::FrontFace::CLOCKWISE)
        .depth_bias_enable(false)
        .depth_bias_clamp(0.0f32)
        .depth_bias_constant_factor(0.0f32)
        .depth_bias_slope_factor(0.0f32)
        .build()
}

///
/// Wrapper for the `vk::vk::PipelineViewportStateCreateInfo` builder
///
pub fn viewport_state(
    viewports: &[vk::Viewport],
    scissors: &[vk::Rect2D],
) -> vk::PipelineViewportStateCreateInfo {
    vk::PipelineViewportStateCreateInfo::builder()
        .viewports(viewports)
        .scissors(scissors)
        .build()
}
