/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use ash::extensions::khr::Surface;
use ash::version::InstanceV1_0;
use ash::vk;

use crate::core::application::Window;
use crate::vulkan::VulkanContext;

///
/// Builder for constructor a VulkanSurface associated with a given window
///
pub struct VulkanSurfaceBuilder<'inst, 'wind> {
    ctx: &'inst VulkanContext,
    window: &'wind Window,
}

impl<'inst, 'wind> VulkanSurfaceBuilder<'inst, 'wind> {
    ///
    /// Construct a new builder
    ///
    pub fn new(ctx: &'inst VulkanContext, window: &'wind Window) -> Self {
        VulkanSurfaceBuilder { ctx, window }
    }

    /// Build the VulkanSurface and consume the builder
    pub unsafe fn build(self) -> VulkanSurface {
        let surface = self
            .window
            .vulkan_create_surface(self.ctx.instance().handle());

        let surface_loader = self
            .ctx
            .surface_loader()
            .expect("No surface extension loaded")
            .clone();

        VulkanSurface {
            surface_loader,
            surface,
        }
    }
}

///
/// A wrapper for a Vulkan surface that attaches additional lifetime information to help mitigate
/// use after free issues
///
pub struct VulkanSurface {
    surface_loader: Surface,
    surface: vk::SurfaceKHR,
}

impl VulkanSurface {
    ///
    /// Get a builder
    ///
    pub fn builder<'inst, 'wind>(
        ctx: &'inst VulkanContext,
        window: &'wind Window,
    ) -> VulkanSurfaceBuilder<'inst, 'wind> {
        VulkanSurfaceBuilder::new(ctx, window)
    }

    ///
    /// Get the underlying surface handle
    ///
    pub fn handle(&self) -> vk::SurfaceKHR {
        self.surface
    }

    ///
    /// Check if the given physical device and queue family support using this surface
    ///
    pub fn device_queue_supported(
        &self,
        physical_device: vk::PhysicalDevice,
        queue_index: u32,
    ) -> bool {
        unsafe {
            self.surface_loader.get_physical_device_surface_support(
                physical_device,
                queue_index,
                self.surface,
            )
        }
    }

    ///
    /// Destroy the underlying surface handle and consume the VulkanSurface to help mitigate use
    /// after free.
    ///
    pub unsafe fn destroy(&mut self) {
        self.surface_loader.destroy_surface(self.surface, None);
        self.surface = vk::SurfaceKHR::default();
    }
}

//==================================================================================================
impl Drop for VulkanSurface {
    fn drop(&mut self) {
        if self.surface != vk::SurfaceKHR::default() {
            panic!("VulkanContext dropped without being destroyed");
        }
    }
}
