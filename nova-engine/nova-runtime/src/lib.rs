/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#![allow(clippy::suspicious_else_formatting)]
#![deny(bare_trait_objects)]

//!
//! # nova-runtime
//!
//! This crate serves as the "uber" crate through which a game is written with NovaEngine. The crate
//! holds the implementation of all the systems that would exclusively be used by the actual game
//! itself. This crate provides
//!
//! - The Universe
//! - Renderer
//! - Windowing
//! - Input
//! - Threading
//!
//! As well as reexporting dependencies
//!
//! - nova-utils
//! - nova-macros
//! - vma
//! - sdl2
//! - ash
//! - rayon
//! - crossbeam
//! - smallbox
//! - atom
//!
//! The crate reexports are to ensure that the end user is using the same version as the runtime
//! crate.
//!

// Includes a common set of checks to ensure compiling for the right platform
include!("../../platform_check.rs");

pub extern crate ash;
pub extern crate log;
pub extern crate nova_alloc as alloc;
pub extern crate nova_cstring as cstring;
pub extern crate nova_error_message as error_message;
pub extern crate nova_fmod as fmod;
pub extern crate nova_fs as fs;
pub extern crate nova_logger as logger;
pub extern crate nova_math as math;
pub extern crate nova_owned_arc as owned_arc;
pub extern crate nova_target_crate as target;
pub extern crate nova_traits as traits;
pub extern crate nova_universe as universe;
pub extern crate rayon;
pub extern crate smallbox;
pub extern crate vma;

extern crate atom;
extern crate crossbeam;
extern crate msgbox;
extern crate num_cpus;
extern crate parking_lot;
extern crate sdl2;
#[cfg(target_os = "windows")]
extern crate winapi;

pub mod core;
pub mod panic;
pub mod vulkan;
