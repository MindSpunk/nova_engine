/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::raw;
use crate::Archive;
use crate::ArchiveBuilder;

use std::collections::HashSet;

#[test]
pub fn load_test() {
    assert_eq!(std::mem::size_of::<raw::HeaderRaw>(), 512);
    assert_eq!(std::mem::size_of::<raw::DirRaw>(), 24);
    assert_eq!(std::mem::size_of::<raw::EntryRaw>(), 32);

    let path = std::path::Path::new("./src");
    let builder = ArchiveBuilder::from_directory(path).unwrap();

    let file = std::fs::OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .truncate(true)
        .open("output.bundle")
        .unwrap();

    builder.build(file).unwrap();

    let file = std::fs::OpenOptions::new()
        .read(true)
        .write(false)
        .create(false)
        .open("output.bundle")
        .unwrap();
    let archive = Archive::open(&file).unwrap();

    let root = archive.root();

    let mut files = HashSet::new();
    files.insert("archive.rs");
    files.insert("builder.rs");
    files.insert("constants.rs");
    files.insert("iter.rs");
    files.insert("lib.rs");
    files.insert("raw.rs");
    files.insert("test.rs");

    for child in root.children().unwrap() {
        assert!(child.is_file());
        assert!(files.remove(child.name()));
    }

    assert_eq!(files.len(), 0);
}
