/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

//!
//! # Nova Archive (NovaEngine Asset Archive Format)
//!
//! This library provides the tools required to create, modify and use the package files used by
//! nova-engine. The engine uses this format to simplify cross platform resource access down to a
//! single uniform interface that exposes nothing about the underlying platform or storage medium.
//!
//! The goal is to make file access completely abstract from the underlying file access mechanism.
//!
//! # Design
//!
//! Nova-FS works with the use of a custom archive structure that packs all individual resources
//! into a single file. It aims to support file compression as well as file patching.
//!
//! The virtual filesystem is designed to be mmap'd so makes extensive use of absolute pointers.
//!
//! ## Archive Format
//!
//! A base nova archive consists of three basic parts. The header, blob and directory
//! sections.
//!
//! The blob section always starts 512 bytes from the start of the file and is simply a "soup" of
//! binary data that will be used to store any dynamically sized data. This is where the actual
//! data of the files is stored, as well as any other data used by the archive that can't be
//! stored in a statically sized area. Like how you must dynamically allocate un-sized types, the
//! blob serves as a "heap" of sorts.
//!
//! The directory section starts at an offset from the end of the header specified in the header
//! It contains a data store of all folders in the archive and their children.
//!
//! ### Header Section
//!
//! The header is used for identifying and validating the archive and will always be 512 bytes long.
//! The contents of the header include a 8-byte "magic number" identifier, an 8-byte version number,
//! an 8-byte hash of the blob section, an 8-byte hash of the directory section, an 8-byte pointer
//! to the directory section and a further 472 bytes of reserved space for expansion.
//!
//! #### Format
//!
//! - 8 byte file identifier (0x56ABACA5)
//! - 8 byte file version identifier
//! - 8 byte hash of blob section
//! - 8 byte pointer to entry section
//! - 8 byte pointer to directory section
//! - 472 bytes of reserved space
//!
//! ### Blob Section
//!
//! A completely untyped raw chunk of data. Referenced by other parts of the filesystem.
//!
//! ### Entry Section
//!
//! The entry section contains a list of all the files and folders in the archive in an undefined
//! order. This is similar to the 'blob' section but is used exclusively for file and folder
//! entires.
//!
//! This section starts directly after the blob section, forward aligned to 8-bytes.
//!
//! Array of directory entries.
//!   - 8-byte entry count
//!   - 'entry count' number of entries.
//!
//! #### Format
//!
//! - 8-byte hash of name string
//! - 8-byte pointer to a string in the blob section that stores the name of this
//!   directory entry
//! - 8-byte flags
//! - 8-byte pointer to file data in blob section (or nullptr if this is a folder)
//!
//! ### Directory Section
//!
//! This contains the actual directory structure of the archive. It starts with a root level folder
//! and then branches outwards. It is stored completely flat with no indirection to improve
//! iteration performance. It is essentially a recursive array of arrays with some extra data at the
//! front of each array to help skipping them.
//!
//! This section starts directly after the entry section, forward aligned to 8-bytes
//!
//! Starts with a root array.
//!
//! An element is formatted like:
//! - 8-byte offset to next element
//! - 8-byte index into entries section
//! - 8-byte entry count
//! - n-byte entries
//!
//! #### Format
//!
//!

#[macro_use]
extern crate bitflags;
extern crate flate2;
extern crate memmap;
extern crate nova_traits;

mod archive;
mod builder;
mod constants;
mod iter;
mod raw;

#[cfg(test)]
mod test;

pub use archive::Archive;
pub use archive::Entry;
pub use archive::File;
pub use archive::Folder;
pub use builder::ArchiveBuilder;
pub use builder::BuilderDataFile;
pub use builder::BuilderEntry;
pub use builder::BuilderFolder;
pub use builder::BuilderItem;
pub use builder::BuilderOSFile;
pub use iter::DirIter;
