/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::archive::{Archive, Entry, File, Folder};
use crate::raw;

use crate::constants::FileFlags;
use core::ptr;

pub struct DirIter<'archive, 'file> {
    pub(crate) archive: &'archive Archive<'file>,
    pub(crate) child_count: usize,
    pub(crate) next: *const raw::DirRaw,
}

impl<'archive, 'file> Iterator for DirIter<'archive, 'file> {
    type Item = Entry<'archive, 'file>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.next == ptr::null() || self.child_count == 0 {
            None
        } else {
            //
            // Get a reference to the DirRaw entry and then iterate to then setup iterator for the
            // next iteration
            //
            let ptr = self.archive.assert_dir_ptr(self.next);
            let dir = unsafe { &*ptr };
            let next = unsafe { (ptr as *const u8).add(dir.next_offset as usize) };
            self.next = next as *const raw::DirRaw;
            self.child_count -= 1;

            //
            // Get the EntryRaw from the entry array
            //
            let entries = self.archive.entries_slice();
            let entry_raw = &entries[dir.index as usize];

            //
            // Get the pointer to the name as an absolute pointer and verify it is valid
            //
            let name = self.archive.calc_slice(entry_raw.string_ptr as *const u8);
            let name = std::str::from_utf8(name).unwrap();

            //
            // Get the name hash
            //
            let name_hash = entry_raw.string_hash;

            //
            // If the data_ptr is null then this is a folder, otherwise this is a file
            //
            if entry_raw.data_ptr == 0 {
                let folder = Folder::new(self.archive, name_hash, name, ptr);
                Some(Entry::Folder(folder))
            } else {
                let flags = FileFlags::from_bits(entry_raw.flags).unwrap();

                let data = self.archive.calc_slice(entry_raw.data_ptr as *const u8);

                let file = File::new(name_hash, name, flags, data);
                Some(Entry::File(file))
            }
        }
    }
}
