/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::constants::{FileFlags, HEADER_MAGIC, VERSION_0_5_0};
use crate::raw;
use nova_traits::AsDataSlice;
use std::collections::hash_map::DefaultHasher;
use std::fs::{DirEntry, File, ReadDir};
use std::hash::{Hash, Hasher};
use std::io::{Cursor, Seek, SeekFrom, Write};
use std::path::Path;
use std::{fs, io};

///
/// Type of compression used for storing a file
///
pub enum CompressionType {
    Uncompressed,
    Zlib,
}

///
/// A folder in the (to be created) archive
///
pub struct BuilderFolder<'data> {
    entries: Vec<BuilderEntry<'data>>,
}

impl<'data> BuilderFolder<'data> {
    ///
    /// Internal function for searching for an entry by name
    ///
    fn search_entry(&self, name: &str) -> Result<usize, usize> {
        self.entries.binary_search_by(|v| v.name.as_str().cmp(name))
    }

    ///
    /// List of children
    ///
    pub fn entries(&self) -> &[BuilderEntry<'data>] {
        &self.entries
    }

    ///
    /// Get a reference to an entry if it exists
    ///
    pub fn get_entry_ref(&self, name: &str) -> Option<&BuilderEntry<'data>> {
        match self.search_entry(name) {
            Ok(index) => Some(&self.entries[index]),
            Err(_) => None,
        }
    }

    ///
    /// Get a mutable reference to an entry if it exists
    ///
    pub fn get_entry_mut(&mut self, name: &str) -> Option<&mut BuilderEntry<'data>> {
        match self.search_entry(name) {
            Ok(index) => Some(&mut self.entries[index]),
            Err(_) => None,
        }
    }

    ///
    /// Inserts the given entry, adding it one with the same name doesn't exist or overwriting the
    /// old one if there already is an entry with the same name. We also return a mutable reference
    /// to our newly inserted entry
    ///
    pub fn insert_entry(&mut self, entry: BuilderEntry<'data>) -> &mut BuilderEntry<'data> {
        match self.search_entry(&entry.name) {
            Ok(index) => {
                self.entries[index] = entry;
                &mut self.entries[index]
            }
            Err(index) => {
                self.entries.insert(index, entry);
                &mut self.entries[index]
            }
        }
    }

    ///
    /// Wraps `Self::insert_entry` for inserting a file
    ///
    pub fn insert_os_file(
        &mut self,
        name: String,
        compression: CompressionType,
        path: String,
    ) -> &mut BuilderEntry<'data> {
        let flags = match compression {
            CompressionType::Uncompressed => FileFlags::UNCOMPRESSED,
            CompressionType::Zlib => FileFlags::COMPRESSED_ZLIB,
        };
        let file = BuilderOSFile { flags, path };
        let item = BuilderItem::OSFile(file);
        let entry = BuilderEntry { name, item };
        self.insert_entry(entry)
    }

    ///
    /// Wraps `Self::insert_entry` for inserting a file
    ///
    pub fn insert_data_file(
        &mut self,
        name: String,
        compression: CompressionType,
        data: &'data [u8],
    ) -> &mut BuilderEntry<'data> {
        let flags = match compression {
            CompressionType::Uncompressed => FileFlags::UNCOMPRESSED,
            CompressionType::Zlib => FileFlags::COMPRESSED_ZLIB,
        };
        let file = BuilderDataFile { flags, data };
        let item = BuilderItem::DataFile(file);
        let entry = BuilderEntry { name, item };
        self.insert_entry(entry)
    }

    ///
    /// Wraps `Self::insert_folder` for inserting a folder
    ///
    pub fn insert_folder(&mut self, name: String) -> &mut BuilderEntry<'data> {
        let folder = BuilderFolder {
            entries: Vec::new(),
        };
        let item = BuilderItem::Folder(folder);
        let entry = BuilderEntry { name, item };
        self.insert_entry(entry)
    }
}

///
/// A file to be stored in the archive where the data will be loaded from another file on the OS
/// filesystem
///
pub struct BuilderOSFile {
    flags: FileFlags,
    path: String,
}

impl BuilderOSFile {
    ///
    /// File flags
    ///
    pub fn flags(&self) -> FileFlags {
        self.flags
    }

    ///
    /// Source file
    ///
    pub fn path(&self) -> &str {
        &self.path
    }
}

///
/// A file to be stored in the archive where the data will be loaded from a byte array already in
/// the process's address space (could be a memory mapped file or regular data)
///
pub struct BuilderDataFile<'data> {
    flags: FileFlags,
    data: &'data [u8],
}

impl<'data> BuilderDataFile<'data> {
    pub fn flags(&self) -> FileFlags {
        self.flags
    }

    pub fn data(&self) -> &'data [u8] {
        self.data
    }
}

///
/// Wraps a folder or file
///
pub enum BuilderItem<'data> {
    Folder(BuilderFolder<'data>),
    OSFile(BuilderOSFile),
    DataFile(BuilderDataFile<'data>),
}

impl<'data> BuilderItem<'data> {
    pub fn folder_ref(&self) -> Option<&BuilderFolder<'data>> {
        match self {
            BuilderItem::Folder(folder) => Some(folder),
            BuilderItem::OSFile(_) => None,
            BuilderItem::DataFile(_) => None,
        }
    }

    pub fn folder_mut(&mut self) -> Option<&mut BuilderFolder<'data>> {
        match self {
            BuilderItem::Folder(folder) => Some(folder),
            BuilderItem::OSFile(_) => None,
            BuilderItem::DataFile(_) => None,
        }
    }

    pub fn os_file_ref(&self) -> Option<&BuilderOSFile> {
        match self {
            BuilderItem::Folder(_) => None,
            BuilderItem::OSFile(file) => Some(file),
            BuilderItem::DataFile(_) => None,
        }
    }

    pub fn os_file_mut(&mut self) -> Option<&mut BuilderOSFile> {
        match self {
            BuilderItem::Folder(_) => None,
            BuilderItem::OSFile(file) => Some(file),
            BuilderItem::DataFile(_) => None,
        }
    }

    pub fn data_file_ref(&self) -> Option<&BuilderDataFile<'data>> {
        match self {
            BuilderItem::Folder(_) => None,
            BuilderItem::OSFile(_) => None,
            BuilderItem::DataFile(file) => Some(file),
        }
    }

    pub fn data_file_mut(&mut self) -> Option<&mut BuilderDataFile<'data>> {
        match self {
            BuilderItem::Folder(_) => None,
            BuilderItem::OSFile(_) => None,
            BuilderItem::DataFile(file) => Some(file),
        }
    }
}

///
/// An entry in the archive's directory structure
///
pub struct BuilderEntry<'data> {
    name: String,
    item: BuilderItem<'data>,
}

impl<'data> BuilderEntry<'data> {
    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn item_ref(&self) -> &BuilderItem<'data> {
        &self.item
    }

    pub fn item_mut(&mut self) -> &mut BuilderItem<'data> {
        &mut self.item
    }
}

///
/// Internal struct used
///
struct EntrySectionItem {
    ///
    /// Offset from stat of file to where te data is stored
    ///
    data_offset: u64,
    ///
    /// File flags
    ///
    file_flags: FileFlags,
    ///
    /// Hash of the name
    ///
    name_hash: u64,
    ///
    /// Offset from start of the file to where the name is stored
    ///
    name_offset: u64,
}

struct WalkerCtx {
    file: File,
    file_list: Vec<EntrySectionItem>,
    directory_section: Cursor<Vec<u8>>,
    hasher: DefaultHasher,
}

impl WalkerCtx {
    ///
    ///
    ///
    fn write_entries(&mut self) -> io::Result<()> {
        // Swap out because borrow checker
        let mut vec = Vec::new();
        std::mem::swap(&mut vec, &mut self.file_list);

        // Write out the entries
        self.write_bytes(vec.len().as_data_slice())?;
        for entry in vec.iter() {
            let raw_entry = raw::EntryRaw {
                string_hash: entry.name_hash,
                string_ptr: entry.name_offset,
                flags: entry.file_flags.into(),
                data_ptr: entry.data_offset,
            };
            self.write_bytes(raw_entry.as_data_slice())?;
        }
        self.write_align_8()?;

        // Swap file list back in
        std::mem::swap(&mut vec, &mut self.file_list);

        Ok(())
    }

    ///
    ///
    ///
    fn write_directory_section(&mut self) -> io::Result<()> {
        // Swap vec out because borrow checker
        let mut vec = Cursor::new(Vec::new());
        std::mem::swap(&mut vec, &mut self.directory_section);

        // Write out the directory section
        let bytes = vec.get_ref();
        self.write_bytes(bytes)?;

        // Swap vec back in
        std::mem::swap(&mut vec, &mut self.directory_section);

        Ok(())
    }

    ///
    /// Writes the given string to the file and returns a file offset to the start of the string's
    /// data
    ///
    fn write_string(&mut self, string: &str) -> io::Result<u64> {
        let out = self.write_bytes(string.len().as_data_slice())?;
        self.write_bytes(string.as_bytes())?;
        Ok(out)
    }

    ///
    /// Pads the file to align to the next 8 byte alignment
    ///
    /// Returns a file offset to where we started writing the padding and the number of padding
    /// bytes written
    ///
    fn write_align_8(&mut self) -> io::Result<(u64, u64)> {
        const NULL_BYTES: [u8; 8] = [0xFF; 8];

        let pos = self.file.seek(SeekFrom::Current(0))?;
        let align = 8 - (pos & 0b111);
        let align = if align == 8 { 0 } else { align };

        self.write_bytes(&NULL_BYTES[0..align as usize])?;

        Ok((pos, align))
    }

    ///
    /// Write the given bytes to the file and accumulate them in the current hasher
    ///
    fn write_bytes(&mut self, bytes: &[u8]) -> io::Result<u64> {
        let ptr = self.file.seek(SeekFrom::Current(0))?;
        self.file.write_all(bytes)?;
        self.hasher.write(bytes);
        Ok(ptr)
    }
}

///
///
///
pub struct ArchiveBuilder<'data> {
    root: BuilderFolder<'data>,
}

impl<'data> ArchiveBuilder<'data> {
    ///
    /// New empty builder
    ///
    pub fn new() -> Self {
        Self {
            root: BuilderFolder {
                entries: Vec::new(),
            },
        }
    }

    ///
    /// Builds an archive from the given directory
    ///
    pub fn from_directory(path: &Path) -> io::Result<Self> {
        let mut builder = ArchiveBuilder::new();

        let read_dir = resolve_symlink(path)?;

        let current = builder.root_mut();
        for entry in read_dir {
            let entry = entry?;
            handle_dir_entry(current, entry)?;
        }

        return Ok(builder);

        ///
        /// Recursive function for handling a DirEntry in the directory walk
        ///
        fn handle_dir_entry(current: &mut BuilderFolder, entry: DirEntry) -> io::Result<()> {
            let metadata = entry.metadata()?;

            if metadata.file_type().is_dir() {
                let name = entry.file_name().into_string().unwrap();
                let folder = current.insert_folder(name);
                let folder = folder.item_mut().folder_mut().unwrap();
                for entry in entry.path().read_dir()? {
                    let entry = entry?;
                    handle_dir_entry(folder, entry)?;
                }
                Ok(())
            } else if metadata.file_type().is_file() {
                let name = entry.file_name().into_string().unwrap();
                let path = entry.path().into_os_string().into_string().unwrap();
                current.insert_os_file(name, CompressionType::Uncompressed, path);
                Ok(())
            } else if metadata.file_type().is_symlink() {
                Err(io::Error::new(
                    io::ErrorKind::Other,
                    "We don't support walking symlinks",
                ))
            } else {
                Err(io::Error::new(
                    io::ErrorKind::Other,
                    "Found unknown entry type",
                ))
            }
        }

        ///
        /// Resolves the root path to a ReadDir by following all symlinks until we get to a
        /// directory
        ///
        fn resolve_symlink(path: &Path) -> io::Result<ReadDir> {
            let metadata = path.metadata()?;

            if metadata.file_type().is_dir() {
                Ok(path.read_dir()?)
            } else if metadata.file_type().is_symlink() {
                resolve_symlink(&path.read_link()?)
            } else if metadata.file_type().is_file() {
                Err(io::Error::new(
                    io::ErrorKind::Other,
                    "Found file while expecting a directory",
                ))
            } else {
                Err(io::Error::new(
                    io::ErrorKind::Other,
                    "Found unknown entry type while expecting a directory",
                ))
            }
        }
    }

    ///
    /// Get a reference to the root of the archive
    ///
    pub fn root_ref(&self) -> &BuilderFolder<'data> {
        &self.root
    }

    ///
    /// Get a mutable reference to the root of the archive
    ///
    pub fn root_mut(&mut self) -> &mut BuilderFolder<'data> {
        &mut self.root
    }

    ///
    /// Consume the builder and output the archive to the given file
    ///
    pub fn build(self, mut file: File) -> io::Result<()> {
        //
        // Write a blank header, we'll come back and write the real one later
        //
        file.write_all(&[0u8; 512])?;

        //
        // Context struct used for the directory walker
        //
        let mut ctx = WalkerCtx {
            file,
            file_list: Vec::new(),
            directory_section: Cursor::new(Vec::new()),
            hasher: Default::default(),
        };

        //
        // Walk the internal archive file tree.
        //
        // This will write the blob section and prepare the file_list for writing the entry section
        //
        self.walk(&mut ctx)?;
        ctx.write_align_8()?;

        //
        // Get the final hash of the blob section and reset the hasher
        //
        let blob_hash = ctx.hasher.finish();
        ctx.hasher = DefaultHasher::new();

        //
        // Get the entry section offset for use in the header later
        //
        let entry_section_offset = ctx.file.seek(SeekFrom::Current(0))?;

        //
        // Write out the entry section
        //
        ctx.write_entries()?;

        //
        // Get the final hash of the entry section and reset the hasher
        //
        let _entry_hash = ctx.hasher.finish();
        ctx.hasher = DefaultHasher::new();

        //
        // Get the directory section offset for use in the header later
        //
        let directory_section_offset = ctx.file.seek(SeekFrom::Current(0))?;

        //
        // Write the directory section
        //
        ctx.write_directory_section()?;

        //
        // Get the final hash of the directory section and reset the hasher
        //
        let _directory_hash = ctx.hasher.finish();
        ctx.hasher = DefaultHasher::new();

        //
        // Seek back to the start and write out the header
        //
        ctx.file.seek(SeekFrom::Start(0))?;
        let header = raw::HeaderRaw {
            magic: HEADER_MAGIC,
            version: VERSION_0_5_0,
            blob_hash,
            ent_ptr: entry_section_offset,
            dir_ptr: directory_section_offset,
            padding: [86; 472],
        };
        ctx.write_bytes(header.as_data_slice())?;

        Ok(())
    }

    ///
    /// Kick off function for walking the archive tree for outputting the file
    ///
    fn walk(&self, ctx: &mut WalkerCtx) -> io::Result<()> {
        let mut dir_offset = 0;
        self.walk_directory(ctx, &mut dir_offset, "", &self.root)
    }

    ///
    /// Recursive function for walking down the directory tree
    ///
    fn walk_directory(
        &self,
        ctx: &mut WalkerCtx,
        dir_offset: &mut u64,
        name: &str,
        folder: &BuilderFolder,
    ) -> io::Result<()> {
        //
        // Hash the directory name
        //
        let mut hasher = DefaultHasher::default();
        name.hash(&mut hasher);

        //
        // Write the directory section entry to the temporary buffer
        //
        let dir = raw::DirRaw {
            next_offset: u64::max_value(),
            index: ctx.file_list.len() as u64,
            child_count: folder.entries().len() as u64,
        };
        ctx.directory_section.write_all(dir.as_data_slice())?;

        //
        // Build and store the entry, which will be written to the file later
        //
        let item = EntrySectionItem {
            data_offset: 0,
            file_flags: FileFlags::from_bits(0).unwrap(),
            name_hash: hasher.finish(),
            name_offset: ctx.write_string(name)?,
        };
        ctx.write_align_8()?;

        ctx.file_list.push(item);

        //
        // Walk the child directories
        //
        for entry in folder.entries().iter() {
            let mut new_dir_offset = 0;
            match &entry.item {
                BuilderItem::Folder(folder) => {
                    self.walk_directory(ctx, &mut new_dir_offset, &entry.name, folder)?
                }
                BuilderItem::OSFile(file) => {
                    self.walk_os_file(ctx, &mut new_dir_offset, &entry.name, file)?
                }
                BuilderItem::DataFile(file) => {
                    self.walk_data_file(ctx, &mut new_dir_offset, &entry.name, file)?
                }
            }
            *dir_offset += new_dir_offset;
        }

        //
        // Patch the next_offset in out directory section entry
        //
        *dir_offset += 3 * 8;
        let offset_i64 = *dir_offset as i64;
        ctx.directory_section.seek(SeekFrom::Current(-offset_i64))?;
        ctx.directory_section
            .write_all((*dir_offset).as_data_slice())?;
        ctx.directory_section
            .seek(SeekFrom::Current(offset_i64 - 8))?;

        Ok(())
    }

    ///
    /// Output a file to the entry section
    ///
    fn walk_os_file(
        &self,
        ctx: &mut WalkerCtx,
        dir_offset: &mut u64,
        name: &str,
        file: &BuilderOSFile,
    ) -> io::Result<()> {
        //
        // Hash the file name
        //
        let mut hasher = DefaultHasher::default();
        name.hash(&mut hasher);

        //
        // Read the file
        //
        let file_handle = fs::OpenOptions::new()
            .read(true)
            .write(false)
            .create(false)
            .open(&file.path)?;
        let memory_map = unsafe { memmap::MmapOptions::new().map(&file_handle)? };

        let data_offset = ctx.write_bytes(memory_map.len().as_data_slice())?;
        ctx.write_bytes(&memory_map)?;
        ctx.write_align_8()?;

        //
        // Write the directory section entry to the temporary buffer
        //
        let dir = raw::DirRaw {
            next_offset: 3 * 8,
            index: ctx.file_list.len() as u64,
            child_count: 0,
        };
        *dir_offset += 3 * 8;
        ctx.directory_section.write_all(dir.as_data_slice())?;

        //
        // Build and store the entry, which will be written to the file later
        //
        let item = EntrySectionItem {
            data_offset,
            file_flags: FileFlags::UNCOMPRESSED,
            name_hash: hasher.finish(),
            name_offset: ctx.write_string(name)?,
        };
        ctx.write_align_8()?;

        ctx.file_list.push(item);

        Ok(())
    }

    ///
    /// Output a file to the entry section
    ///
    fn walk_data_file(
        &self,
        ctx: &mut WalkerCtx,
        dir_offset: &mut u64,
        name: &str,
        file: &BuilderDataFile,
    ) -> io::Result<()> {
        //
        // Hash the file name
        //
        let mut hasher = DefaultHasher::default();
        name.hash(&mut hasher);

        let data_offset = ctx.write_bytes(file.data().len().as_data_slice())?;
        ctx.write_bytes(file.data())?;
        ctx.write_align_8()?;

        //
        // Write the directory section entry to the temporary buffer
        //
        let dir = raw::DirRaw {
            next_offset: 3 * 8,
            index: ctx.file_list.len() as u64,
            child_count: 0,
        };
        *dir_offset += 3 * 8;
        ctx.directory_section.write_all(dir.as_data_slice())?;

        //
        // Build and store the entry, which will be written to the file later
        //
        let item = EntrySectionItem {
            data_offset,
            file_flags: FileFlags::UNCOMPRESSED,
            name_hash: hasher.finish(),
            name_offset: ctx.write_string(name)?,
        };
        ctx.write_align_8()?;

        ctx.file_list.push(item);

        Ok(())
    }
}
