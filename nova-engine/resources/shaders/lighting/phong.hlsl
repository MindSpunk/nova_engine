/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

float3 PhongDiffuse(float3 frag_to_light, float3 normal, float3 light_colour)
{
    return light_colour * clamp(dot(frag_to_light, normal), 0, 1);
}

float3 BlinnPhongSpecular(float3 frag_to_light, float3 frag_to_cam, float3 normal, float3 light_colour, float shininess)
{
    float3 half_dir = normalize(frag_to_light + frag_to_cam);
    float  result   = pow(max(dot(normal, half_dir), 0), shininess);
    return (light_colour * result) * max(dot(frag_to_light, normal), 0);
}

float3 PhongSpecular(float3 frag_to_light, float3 cam_to_frag, float3 normal, float3 light_colour, float shininess)
{
    float3 reflected = reflect(cam_to_frag, normal);
    float  result    = pow(max(dot(frag_to_light, reflected), 0), shininess);
    return (light_colour * result) * max(dot(frag_to_light, normal), 0);
}

float PhongAttenuation(float distance)
{
    return 1.0 / (distance * distance);
}