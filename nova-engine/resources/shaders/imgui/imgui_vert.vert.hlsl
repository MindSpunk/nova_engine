/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

struct VertexInput
{
    [[vk::location(0)]] float2 Pos   : SV_POSITION;
    [[vk::location(1)]] float2 UV    : TEXCOORD0;
    [[vk::location(2)]] float4 Color : COLOR0;
};

struct PixelInput
{
    float4 Color : COLOR0;
    float2 UV    : TEXCOORD0;
};

struct PushConstantLayout
{
    float2 Scale;
    float2 Translate;
};

[[vk::push_constant]]
PushConstantLayout PushConstants;

[[vk::location(0)]]
PixelInput main(in VertexInput input, out float4 Pos : SV_POSITION)
{
    
    PixelInput output;

    Pos = float4((input.Pos * PushConstants.Scale) + PushConstants.Translate,0,1.0);
    output.Color = input.Color;
    output.UV = input.UV;
    
    return output;
}