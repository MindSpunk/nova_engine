/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "lighting/phong.hlsl"
#include "core/base_layouts.hlsl"

[[vk::binding(0,0)]]
ConstantBuffer<CameraLayout> camera;

float4 main(in VertexOutput input) : SV_TARGET
{
    float3 light_pos        = float3(0.0, 0.0, 8);
    float3 light_colour     = float3(5.0, 5.0, 5.0);
    float3 ambient_light    = float3(0.1, 0.1, 0.1);
    float3 normal           = normalize(input.normal);
    float3 frag_to_cam      = normalize(camera.position - input.position);
    float3 frag_to_light    = normalize(light_pos - input.position);
    float3 out_colour       = float3(0,0,0);

    float attenuation       = PhongAttenuation(length(light_pos - input.position));

    out_colour += ambient_light;
    out_colour += PhongDiffuse(frag_to_light, normal, light_colour) * attenuation;
    out_colour += BlinnPhongSpecular(frag_to_light, frag_to_cam, normal, light_colour, 64) * attenuation;

    return float4(out_colour, 1.0);
}