/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "core/base_layouts.hlsl"

struct VertexInput
{
    [[vk::location(0)]] float3 position   : SV_POSITION;
    [[vk::location(1)]] float3 normal     : NORMAL;
    [[vk::location(2)]] float3 tangent    : TANGENT;
    [[vk::location(3)]] float2 tex_coord  : TEXCOORD;
};

[[vk::push_constant]]
ConstantBuffer<ModelLayout> model;

[[vk::binding(0,0)]]
ConstantBuffer<CameraLayout> camera;

VertexOutput main(in VertexInput input, out float4 pos : SV_POSITION)
{
    VertexOutput output;

    float4 in_pos = float4(input.position, 1.0);

    float3x3 normal_matrix = (float3x3)model.normal_matrix;

    output.position    = mul(in_pos, model.model_matrix).xyz;
    output.normal      = normalize(mul(input.normal, normal_matrix));
    output.tangent     = normalize(mul(input.tangent, normal_matrix));
    output.tex_coord   = input.tex_coord;

    float4 out_pos = mul(in_pos, model.model_matrix);
           out_pos = mul(out_pos, camera.view_matrix);
           out_pos = mul(out_pos, camera.proj_matrix);
    pos = out_pos; //mul(mul(mul(in_pos, model.model_matrix), camera.view_matrix), camera.proj_matrix);

    return output;
}
