/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use std::path::{Path, PathBuf};
use std::{env, fs};

extern crate nova_target as target;

fn find_and_copy_dll(name: &Path, out_dir: &Path) {
    let mut dllpath: Option<PathBuf> = None;

    let path = env::var_os("PATH").expect("Failed to get path env variable");

    for p in env::split_paths(&path) {
        let mut maybe_lib = p.clone();
        maybe_lib.push(name);

        if maybe_lib.exists() && maybe_lib.is_file() {
            dllpath = Some(maybe_lib);
            break;
        }
    }

    let dllpath = match dllpath {
        None => panic!("Failed to find {:?}", name),
        Some(dllpath) => dllpath,
    };

    let mut output_name = out_dir.to_path_buf();
    output_name.push(name);

    let result = fs::copy(&dllpath, &output_name);
    if result.is_err() {
        panic!("Failed to copy {:?} to {:?}", &dllpath, &output_name);
    }
}

///
/// Copy all the required dlls to the working directory used by the debugger
///
fn copy_dependencies_to_working_dir() {
    find_and_copy_dll(Path::new("libwinpthread-1.dll"), Path::new("../"));
    find_and_copy_dll(Path::new("libgcc_s_seh-1.dll"), Path::new("../"));
    find_and_copy_dll(Path::new("libstdc++-6.dll"), Path::new("../"));
}

fn main() {
    let platform = target::build::target_platform();

    // If we're going gnu windows
    if platform.is_gnu() {
        copy_dependencies_to_working_dir();
    }
}
