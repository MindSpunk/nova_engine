/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

mod builder;
mod entry;
mod internal;
mod storage;

pub use builder::FilesystemBuilder;
pub use entry::Entry;
pub use entry::FileWriter;
pub use storage::Files;
pub use storage::Packages;

use crate::path::Component;

///
/// An instance of a virtual filesystem
///
pub struct Filesystem<'packages> {
    root: internal::Entry<'packages>,
}

impl<'packages> Filesystem<'packages> {
    ///
    /// Constructs a builder for building a new Filesystem
    ///
    pub fn builder() -> FilesystemBuilder {
        FilesystemBuilder::new()
    }

    ///
    /// Gets fresh instances of the backing storage objects that need to outlive the Filesystem
    ///
    pub fn storage<'memory, 'files>(
        memory: &'memory mut [u8],
    ) -> (Files, Packages<'memory, 'files>) {
        FilesystemBuilder::storage(memory)
    }

    ///
    /// Get a reference to the root directory
    ///
    pub fn root(&self) -> &Entry {
        self.root.as_ref()
    }

    ///
    /// Look up an entry by path
    ///
    pub fn lookup(&self, path: &crate::path::Path) -> Option<&Entry<'packages>> {
        internal_lookup_ref(&self.root, path).map(|v| v.as_ref())
    }

    pub fn create_file(
        &mut self,
        path: &crate::path::Path,
    ) -> std::io::Result<&mut Entry<'packages>> {
        internal_create_file(&mut self.root, path).map(|v| v.as_mut())
    }

    pub fn create_folder(
        &mut self,
        path: &crate::path::Path,
    ) -> std::io::Result<&mut Entry<'packages>> {
        internal_create_folder(&mut self.root, path).map(|v| v.as_mut())
    }
}

///
/// Lookup a path and get the internal::Entry it points to
///
pub(crate) fn internal_lookup_ref<'a, 'packages>(
    entry: &'a internal::Entry<'packages>,
    path: &crate::path::Path,
) -> Option<&'a internal::Entry<'packages>> {
    let mut current = entry;
    for component in path.components() {
        match component {
            Component::RootDir => continue,
            Component::Parent => return None,
            Component::Normal(path) => {
                if let Some(folder) = current.folder_ref() {
                    if let Some(entry) = folder.get_item_ref(path) {
                        current = entry;
                    } else {
                        return None;
                    }
                } else {
                    return None;
                }
            }
        }
    }
    Some(current)
}

///
/// Lookup a path and get the internal::Entry it points to
///
pub(crate) fn internal_lookup_mut<'a, 'packages>(
    entry: &'a mut internal::Entry<'packages>,
    path: &crate::path::Path,
) -> Option<&'a mut internal::Entry<'packages>> {
    let mut current = entry;
    for component in path.components() {
        match component {
            Component::RootDir => continue,
            Component::Parent => return None,
            Component::Normal(path) => {
                if let Some(folder) = current.folder_mut() {
                    if let Some(entry) = folder.get_item_mut(path) {
                        current = entry;
                    } else {
                        return None;
                    }
                } else {
                    return None;
                }
            }
        }
    }
    Some(current)
}

pub(crate) fn no_file_name_err() -> std::io::Error {
    let kind = std::io::ErrorKind::Other;
    let error = "No file name in the given path";
    std::io::Error::new(kind, error)
}

pub(crate) fn file_over_folder_err() -> std::io::Error {
    let kind = std::io::ErrorKind::AlreadyExists;
    let error = "Trying to create a folder over a file";
    std::io::Error::new(kind, error)
}

pub(crate) fn folder_over_file_err() -> std::io::Error {
    let kind = std::io::ErrorKind::AlreadyExists;
    let error = "Trying to create a file over a folder";
    std::io::Error::new(kind, error)
}

pub(crate) fn not_an_os_folder() -> std::io::Error {
    let kind = std::io::ErrorKind::PermissionDenied;
    let error = "Tried to write to a folder in the vfs that does not map to an OS folder";
    std::io::Error::new(kind, error)
}

pub(crate) fn no_folder_exists_create() -> std::io::Error {
    let kind = std::io::ErrorKind::NotFound;
    let error = "Tried to create a file in a folder that doesn't exist";
    std::io::Error::new(kind, error)
}

pub(crate) fn file_already_exists_create() -> std::io::Error {
    let kind = std::io::ErrorKind::AlreadyExists;
    let error = "Tried to create on a file that already exists";
    std::io::Error::new(kind, error)
}

pub(crate) fn no_file_name() -> std::io::Error {
    let kind = std::io::ErrorKind::Other;
    let error = "No file name in the given path";
    std::io::Error::new(kind, error)
}

pub(crate) fn mount_folder_onto_file() -> std::io::Error {
    let kind = std::io::ErrorKind::AlreadyExists;
    let error = "Tried to mount folder onto file";
    std::io::Error::new(kind, error)
}

pub(crate) fn mount_point_invalid() -> std::io::Error {
    let kind = std::io::ErrorKind::AlreadyExists;
    let error = "Tried to mount folder onto file";
    std::io::Error::new(kind, error)
}

pub(crate) fn internal_get_create_target<'a, 'packages>(
    entry: &'a mut internal::Entry<'packages>,
    path: &crate::path::Path,
) -> std::io::Result<&'a mut internal::Folder<'packages>> {
    //
    // If the path doesn't have a parent then we can't make the file, writing to the root is
    // impossible implicitly as it can never be an OSFolder
    //
    if let Some(parent) = path.parent() {
        //
        // Check if the parent path of the file exists
        //
        if let Some(parent) = internal_lookup_mut(entry, parent) {
            //
            // Check if the parent is in fact a folder
            //
            if let internal::Entry::Folder(folder) = parent {
                //
                // Check if this folder is an OSFolder
                //
                if folder.path.is_some() {
                    Ok(folder)
                } else {
                    Err(not_an_os_folder())
                }
            } else {
                Err(file_already_exists_create())
            }
        } else {
            Err(no_folder_exists_create())
        }
    } else {
        Err(not_an_os_folder())
    }
}

pub(crate) fn internal_create_file<'a, 'packages>(
    entry: &'a mut internal::Entry<'packages>,
    path: &crate::path::Path,
) -> std::io::Result<&'a mut internal::Entry<'packages>> {
    let folder = internal_get_create_target(entry, path)?;
    let os_path = folder.path.as_ref().unwrap();

    //
    // Try and get the filename from the path
    //
    let file_name = match path.leaf_name() {
        None => return Err(no_file_name()),
        Some(file_name) => file_name,
    };

    //
    // Build the path of the final file on the filesystem
    //
    let mut file_os_path = os_path.clone();
    file_os_path.push(file_name.as_str());

    //
    // Create the file on disk
    //
    std::fs::OpenOptions::new()
        .write(true)
        .create_new(true)
        .open(&file_os_path)?;

    //
    // Insert the entry into the vfs
    //
    let item = internal::Item::OSFile(file_os_path);
    let entry = folder.insert_file(file_name.to_path_buf(), item);

    Ok(entry)
}

pub(crate) fn internal_create_folder<'a, 'packages>(
    entry: &'a mut internal::Entry<'packages>,
    path: &crate::path::Path,
) -> std::io::Result<&'a mut internal::Entry<'packages>> {
    let folder = internal_get_create_target(entry, path)?;
    let os_path = folder.path.as_ref().unwrap();

    //
    // Try and get the filename from the path
    //
    let file_name = match path.leaf_name() {
        None => return Err(no_file_name_err()),
        Some(file_name) => file_name,
    };

    //
    // Build the path of the final file on the filesystem
    //
    let mut file_os_path = os_path.clone();
    file_os_path.push(file_name.as_str());

    //
    // Create the folder on disk
    //
    std::fs::create_dir(&file_os_path)?;

    //
    // Insert the entry into the vfs
    //
    let entry = folder.insert_os_folder(file_name.to_path_buf(), file_os_path);

    Ok(entry)
}

///
/// Sub function for create_os_mount_point for handling a normal path component when iterating
/// through the mount point path components
///
pub(crate) fn handle_os_normal_component<'a, 'packages>(
    ctx: &'a mut internal::Entry<'packages>,
    name: &crate::path::Path,
) -> std::io::Result<&'a mut internal::Entry<'packages>> {
    match ctx {
        internal::Entry::Folder(_) => {
            ctx.os_folder_to_folder();
            handle_os_folder(ctx, name)
        }
        internal::Entry::File(_) => Err(file_over_folder_err()),
    }
}

pub(crate) fn handle_os_folder<'a, 'packages>(
    ctx: &'a mut internal::Entry<'packages>,
    name: &crate::path::Path,
) -> std::io::Result<&'a mut internal::Entry<'packages>> {
    let folder = ctx.folder_mut().unwrap();
    Ok(folder.get_item_or_insert_folder(name.to_path_buf()))
}

pub(crate) fn handle_os_normal_component_last<'a, 'packages>(
    ctx: &'a mut internal::Entry<'packages>,
    name: &crate::path::Path,
    path: &std::path::Path,
) -> std::io::Result<&'a mut internal::Entry<'packages>> {
    match ctx {
        internal::Entry::Folder(_) => {
            ctx.os_folder_to_folder();
            handle_os_folder_last(ctx, name, path)
        }
        internal::Entry::File(_) => Err(folder_over_file_err()),
    }
}

pub(crate) fn handle_os_folder_last<'a, 'packages>(
    ctx: &'a mut internal::Entry<'packages>,
    name: &crate::path::Path,
    path: &std::path::Path,
) -> std::io::Result<&'a mut internal::Entry<'packages>> {
    let folder = ctx.folder_mut().unwrap();
    let entry = folder.get_item_or_insert_os_folder(name.to_path_buf(), path.to_path_buf());
    if let internal::Entry::File(_) = entry {
        Err(mount_folder_onto_file())
    } else {
        Ok(entry)
    }
}

///
/// Sub function for create_os_mount_point for handling a normal path component when iterating
/// through the mount point path components
///
pub(crate) fn handle_bundle_normal_component<'a, 'packages>(
    ctx: &'a mut internal::Entry<'packages>,
    name: &crate::path::Path,
) -> std::io::Result<&'a mut internal::Entry<'packages>> {
    match ctx {
        internal::Entry::Folder(_) => {
            ctx.os_folder_to_folder();
            handle_bundle_folder(ctx, name)
        }
        internal::Entry::File(_) => Err(folder_over_file_err()),
    }
}

pub(crate) fn handle_bundle_folder<'a, 'packages>(
    ctx: &'a mut internal::Entry<'packages>,
    name: &crate::path::Path,
) -> std::io::Result<&'a mut internal::Entry<'packages>> {
    let folder = ctx.folder_mut().unwrap();
    let entry = folder.get_item_or_insert_folder(name.to_path_buf());
    if let internal::Entry::File(_) = entry {
        Err(mount_folder_onto_file())
    } else {
        entry.os_folder_to_folder();
        Ok(entry)
    }
}

///
/// Internal function for creating the folder for a mount point that will have an OS folder
/// mounted on to it
///
pub(crate) fn resolve_os_mount_point<'a, 'packages>(
    root: &'a mut internal::Entry<'packages>,
    mount_point: &str,
    path: &std::path::Path,
) -> std::io::Result<&'a mut internal::Entry<'packages>> {
    let mount_point = validate_mount_point(mount_point)?;
    let mount_point = create_os_mount_point(root, &mount_point, path)?;
    Ok(mount_point)
}

///
/// Creates all the directories needed for the given path to exist and attempt to make the
/// final mount point an OSFolder rather than a regular Folder
///
pub(crate) fn create_os_mount_point<'a, 'packages>(
    root: &'a mut internal::Entry<'packages>,
    mount_point: &crate::path::Path,
    path: &std::path::Path,
) -> std::io::Result<&'a mut internal::Entry<'packages>> {
    let mut ctx = root;
    let mut iter = mount_point.components().peekable();
    while let Some(component) = iter.next() {
        match component {
            Component::RootDir => {
                continue;
            }
            Component::Parent => {
                unreachable!();
            }
            Component::Normal(name) => {
                if iter.peek().is_none() {
                    ctx = handle_os_normal_component_last(ctx, name, path)?;
                } else {
                    ctx = handle_os_normal_component(ctx, name)?;
                }
            }
        }
    }
    Ok(ctx)
}

///
/// Internal function for creating the folder for a mount point that will have an archive
/// mounted on to it
///
pub(crate) fn resolve_bundle_mount_point<'a, 'packages>(
    root: &'a mut internal::Entry<'packages>,
    mount_point: &str,
) -> std::io::Result<&'a mut internal::Entry<'packages>> {
    let mount_point = validate_mount_point(mount_point)?;
    let mount_point = create_bundle_mount_point(root, &mount_point)?;
    Ok(mount_point)
}

///
/// Create the given directory
///
pub(crate) fn create_bundle_mount_point<'a, 'packages>(
    root: &'a mut internal::Entry<'packages>,
    mount_point: &crate::path::Path,
) -> std::io::Result<&'a mut internal::Entry<'packages>> {
    let mut ctx = root;
    let mut iter = mount_point.components();
    while let Some(component) = iter.next() {
        match component {
            Component::RootDir => continue,
            Component::Parent => unreachable!(),
            Component::Normal(name) => ctx = handle_bundle_normal_component(ctx, name)?,
        }
    }
    Ok(ctx)
}

pub(crate) fn validate_mount_point(mount_point: &str) -> std::io::Result<crate::path::PathBuf> {
    //
    // Validate the mount point path
    //
    let mount_point = crate::path::Path::new(mount_point);

    //
    // Resolve any relative paths in the mount point
    //
    let mount_point = mount_point.resolve();

    //
    // Ensure the path contains no parent parts
    //
    for component in mount_point.components() {
        if component == Component::Parent {
            return Err(mount_point_invalid());
        }
    }

    Ok(mount_point)
}
