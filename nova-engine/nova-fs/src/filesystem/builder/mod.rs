/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

mod archive_walker;
mod config_resolver;
mod directory_walker;

use crate::config::{Mount, MountConfig};
use crate::filesystem::builder::archive_walker::ArchiveWalker;
use crate::filesystem::builder::config_resolver::ConfigResolver;
use crate::filesystem::builder::directory_walker::DirectoryWalker;
use crate::filesystem::internal;
use crate::filesystem::{Files, Filesystem, Packages};
use std::collections::HashMap;
use std::convert::TryInto;

///
///
///
pub struct FilesystemBuilder {
    env_variables: HashMap<String, String>,
    configs: Vec<MountConfig>,
}

impl FilesystemBuilder {
    ///
    /// Constructs a new FilesystemBuilder
    ///
    pub fn new() -> Self {
        FilesystemBuilder {
            env_variables: Default::default(),
            configs: Default::default(),
        }
    }

    ///
    /// Split the given memory between the allocators the filesystem uses
    ///
    pub fn storage<'memory, 'files>(
        memory: &'memory mut [u8],
    ) -> (Files<'memory>, Packages<'memory, 'files>) {
        let (file_memory, package_memory) = memory.split_at_mut(memory.len() / 2);
        let files = Files::new(file_memory);
        let packages = Packages::new(package_memory);
        (files, packages)
    }

    ///
    /// Adds an environment variable to be used when resolving mount configs
    ///
    pub fn env_var(mut self, name: &str, var: &str) -> Self {
        self.env_variables.insert(name.to_string(), var.to_string());
        self
    }

    ///
    /// Adds a new config to use for resolving what to mount where
    ///
    pub fn config(mut self, config: MountConfig) -> Self {
        self.configs.push(config);
        self
    }

    ///
    /// Builds the filesystem structure, resolving all mount points specified by the provided
    /// configs and patching files over the top of others.
    ///
    pub fn build<'packages, 'memory, 'files>(
        self,
        files: &'files Files<'memory>,
        packages: &'packages Packages<'memory, 'files>,
    ) -> std::io::Result<Filesystem<'packages>> {
        //
        // Initialize an empty filesystem
        //
        let root = internal::Folder {
            name: "".try_into().unwrap(),
            path: None,
            entries: Vec::new(),
        };
        let root = internal::Entry::Folder(root);
        let mut fs = Filesystem { root };

        //
        // Unpack the builder because borrow checker
        //
        let configs = self.configs;
        let env = self.env_variables;

        //
        // Resolve all the provided configs down to a single list of mount entries
        //
        let config = ConfigResolver::new(&configs).resolve()?;

        //
        // Iterate through all the mount entries and mount w/e it points to
        //
        for mount in config.iter() {
            let config_path = std::path::Path::new("");
            match mount {
                //
                // If we want to mount a package we have to walk the entire directory of the
                // archive and insert the tree onto the mount point
                //
                Mount::Package(package) => {
                    ArchiveWalker::new(&packages, &files, &env).walk(
                        &mut fs,
                        config_path,
                        package,
                    )?;
                }
                //
                // If we want to mount a directory we have to walk the file tree we pointed at and
                // patch all the entries into the vfs tree
                //
                Mount::Directory(directory) => {
                    DirectoryWalker::new(&env).walk(&mut fs, config_path, directory)?;
                }
                //
                // This is unreachable as the config resolve phase ensures that there are no config
                // mount entries in the list
                //
                Mount::Config(_) => {
                    unreachable!();
                }
            }
        }

        Ok(fs)
    }
}
