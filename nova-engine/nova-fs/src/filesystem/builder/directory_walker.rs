/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::config::Directory;
use crate::filesystem::{internal, resolve_os_mount_point, Filesystem};
use std::collections::HashMap;
use std::fs::DirEntry;
use std::io::{Error, ErrorKind};

///
/// Context for walking a directory and mounting it into the virtual filesystem
///
pub struct DirectoryWalker<'env> {
    _env: &'env HashMap<String, String>,
}

impl<'env> DirectoryWalker<'env> {
    pub fn new(env: &'env HashMap<String, String>) -> Self {
        Self { _env: env }
    }

    pub fn walk<'packages>(
        mut self,
        fs: &mut Filesystem<'packages>,
        config_path: &std::path::Path,
        directory: &Directory,
    ) -> std::io::Result<()> {
        let path = std::path::Path::new(&directory.mount);
        let empty_path = std::path::Path::new("");
        let path = if path.is_absolute() || path.parent().unwrap() == empty_path {
            config_path.join(path)
        } else {
            path.to_path_buf()
        };

        let mount_point = resolve_os_mount_point(&mut fs.root, &directory.at, &path)?;

        match mount_point {
            internal::Entry::Folder(folder) => {
                if folder.os_path().is_some() {
                    self.walk_directory_os(folder, &path)
                } else {
                    self.walk_directory(folder, &path)
                }
            }
            internal::Entry::File(_) => unreachable!(),
        }
    }

    fn name_and_path_of(&self, entry: &DirEntry) -> (crate::path::PathBuf, std::path::PathBuf) {
        //
        // Allocate the string into the Strings pool
        //
        let name = entry.file_name().into_string().unwrap();
        let name = crate::path::PathBuf::from_string(name);

        //
        // Allocate the path into the Strings pool
        //
        let path = entry.path().into_os_string().into_string().unwrap();
        let path = std::path::PathBuf::from(path);

        (name, path)
    }

    ///
    ///
    ///
    fn walk_directory_os<'packages>(
        &mut self,
        current: &mut internal::Folder<'packages>,
        path: &std::path::Path,
    ) -> std::io::Result<()> {
        //
        // Iterate through all the entries in the given path
        //
        for entry in std::fs::read_dir(&path)? {
            //
            // Get the entry type
            //
            let entry = entry?;
            let ftype = entry.file_type()?;

            let (name, path) = self.name_and_path_of(&entry);

            if ftype.is_file() {
                //
                // Insert the file into the vfs
                //
                let file = internal::Item::OSFile(path);
                current.insert_file(name, file);
            } else if ftype.is_dir() {
                //
                // Recurse down the directory tree
                //
                match current.insert_os_folder(name.clone(), path.clone()) {
                    internal::Entry::Folder(current) => self.walk_directory_os(current, &path)?,
                    internal::Entry::File(_) => unreachable!(),
                }
            } else if ftype.is_symlink() {
                // TODO: Decide whether to support symlinks
                unimplemented!();
            } else {
                return Err(Error::new(ErrorKind::Other, "Unknown file type"));
            }
        }
        Ok(())
    }

    ///
    ///
    ///
    fn walk_directory<'packages>(
        &mut self,
        current: &mut internal::Folder<'packages>,
        path: &std::path::Path,
    ) -> std::io::Result<()> {
        //
        // Iterate through all the entries in the given path
        //
        for entry in std::fs::read_dir(&path)? {
            //
            // Get the entry type
            //
            let entry = entry?;
            let ftype = entry.file_type()?;

            let (name, path) = self.name_and_path_of(&entry);

            if ftype.is_file() {
                //
                // Insert the file into the vfs
                //
                let item = internal::Item::OSFile(path);
                current.insert_file(name, item);
            } else if ftype.is_dir() {
                let next = current.get_item_or_insert_os_folder(name.clone(), path.clone());
                if let internal::Entry::Folder(folder) = next {
                    if let Some(os_path) = folder.os_path() {
                        if os_path != &path {
                            next.os_folder_to_folder();
                        }
                    }
                }

                match next {
                    internal::Entry::Folder(folder) => {
                        if folder.os_path().is_some() {
                            self.walk_directory_os(folder, &path)?;
                        } else {
                            self.walk_directory(folder, &path)?;
                        }
                    }
                    internal::Entry::File(_) => {
                        return Err(Error::new(
                            ErrorKind::AlreadyExists,
                            "Tried to mount folder onto file",
                        ));
                    }
                }
            } else if ftype.is_symlink() {
                // TODO: Decide whether to support symlinks
                unimplemented!();
            } else {
                return Err(Error::new(ErrorKind::Other, "Unknown file type"));
            }
        }
        Ok(())
    }
}
