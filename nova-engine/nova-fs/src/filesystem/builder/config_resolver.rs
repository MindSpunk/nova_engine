/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::config::{Config, Directory, Mount, MountConfig, Package};
use std::io::{Error, ErrorKind, Read};

///
/// Struct that handles walking down the config tree and resolving all config references away so we
/// are left with a Vec that only contains non Config entries
///
pub struct ConfigResolver<'configs> {
    configs: &'configs Vec<MountConfig>,
    new_config: Vec<Mount>,
}

impl<'configs> ConfigResolver<'configs> {
    ///
    /// Constructs a new ConfigResolver
    ///
    pub fn new(configs: &'configs Vec<MountConfig>) -> Self {
        Self {
            configs,
            new_config: Vec::new(),
        }
    }

    ///
    /// Resolve the configs from the list passed at construction time, returning a flat list of
    /// Mount entries
    ///
    pub fn resolve(mut self) -> std::io::Result<Vec<Mount>> {
        //
        // Iterate through all the configs
        //
        for config in self.configs {
            //
            // Get the parent folder for the current config path
            //
            let config_path = config.path.parent().unwrap();
            let config_path = config_path.to_str().unwrap();

            //
            // For every config entry in the current config
            //
            for c in config.mount.iter() {
                //
                // Allocate an expandable string for appending to
                //
                let config_path = config_path.to_string();
                match c {
                    Mount::Package(package) => self.resolve_package(config_path, package),
                    Mount::Directory(directory) => self.resolve_directory(config_path, directory),
                    Mount::Config(config) => self.resolve_config(&config, config_path)?,
                }
            }
        }

        Ok(self.new_config)
    }

    fn resolve_package(&mut self, mut config_path: String, package: &Package) {
        let path = std::path::Path::new(&package.package);
        let empty_path = std::path::Path::new("");
        let path = if path.is_absolute() || path.parent().unwrap() == empty_path {
            package.package.clone()
        } else {
            config_path.push(std::path::MAIN_SEPARATOR);
            config_path.push_str(&package.package);
            config_path
        };

        self.new_config.push(Mount::Package(Package {
            package: path,
            at: package.at.clone(),
        }));
    }

    fn resolve_directory(&mut self, mut config_path: String, directory: &Directory) -> () {
        let path = std::path::Path::new(&directory.mount);
        let empty_path = std::path::Path::new("");
        let path = if path.is_absolute() || path.parent().unwrap() == empty_path {
            directory.mount.clone()
        } else {
            config_path.push(std::path::MAIN_SEPARATOR);
            config_path.push_str(&directory.mount);
            config_path
        };

        self.new_config.push(Mount::Directory(Directory {
            mount: path,
            at: directory.at.clone(),
        }));
    }

    ///
    /// Recursive function for reading a config from disk, parsing the config, validating the config
    /// and resolving the config.
    ///
    fn resolve_config(&mut self, config: &Config, mut config_path: String) -> std::io::Result<()> {
        //
        // Combine the two paths
        //
        config_path.push(std::path::MAIN_SEPARATOR);
        config_path.push_str(&config.config);

        //
        // Open the file for reading if it exists
        //
        let mut file = std::fs::OpenOptions::new()
            .write(false)
            .read(true)
            .create(false)
            .open(&config_path)?;

        //
        // Read the file string
        //
        let mut file_string = String::new();
        file.read_to_string(&mut file_string)?;

        //
        // Validate the config
        //
        let file_config = MountConfig::from_toml(&config_path, &file_string)
            .map_err(|err| Error::new(ErrorKind::InvalidData, err.to_string()))?;

        for mount in file_config.mount.iter() {
            let config_path = std::path::Path::new(&config_path)
                .parent()
                .unwrap()
                .to_path_buf()
                .into_os_string()
                .into_string()
                .unwrap();

            match mount {
                Mount::Package(package) => {
                    self.resolve_package(config_path, package);
                }
                Mount::Directory(directory) => {
                    self.resolve_directory(config_path, directory);
                }
                Mount::Config(sub_config) => {
                    self.resolve_config(&sub_config, config_path)?;
                }
            }
        }
        Ok(())
    }
}
