/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::config::Package;
use crate::filesystem::{internal, resolve_bundle_mount_point, Files, Filesystem, Packages};
use archive::Archive;
use error_message::err_msg;
use std::collections::HashMap;
use std::io::{Error, ErrorKind};

pub struct ArchiveWalker<'memory, 'builder, 'packages, 'files> {
    packages: &'packages Packages<'memory, 'files>,
    files: &'files Files<'memory>,
    _env: &'builder HashMap<String, String>,
}

impl<'memory, 'builder, 'packages, 'files> ArchiveWalker<'memory, 'builder, 'packages, 'files> {
    pub fn new(
        packages: &'packages Packages<'memory, 'files>,
        files: &'files Files<'memory>,
        env: &'builder HashMap<String, String>,
    ) -> Self {
        Self {
            packages,
            files,
            _env: env,
        }
    }

    pub fn walk<'strings>(
        mut self,
        fs: &mut Filesystem<'packages>,
        config_path: &std::path::Path,
        package: &Package,
    ) -> std::io::Result<()> {
        //
        // Combine the archive path with the config path if it is not absolute
        //
        let path = std::path::Path::new(&package.package);
        let empty_path = std::path::Path::new("");
        let path = if path.is_absolute() || path.parent().unwrap() == empty_path {
            config_path.join(path)
        } else {
            path.to_path_buf()
        };

        //
        // Open the file
        //
        let file = std::fs::OpenOptions::new()
            .read(true)
            .write(false)
            .create(false)
            .open(&path)?;

        //
        // Store the file into the arena
        //
        let file = self
            .files
            .store_object_ref(file)
            .expect(err_msg!("Ran out of memory for filesystem"));

        //
        // Memory map the file as an archive and store into the backing package arena
        //
        let archive = Archive::open(file)?;
        let archive = self
            .packages
            .store_object_ref(archive)
            .expect(err_msg!("Ran out of memory for filesystem"));

        let current = resolve_bundle_mount_point(&mut fs.root, &package.at)?;
        let current = current.folder_mut().unwrap();

        self.walk_archive(current, &archive.root())?;

        Ok(())
    }

    ///
    /// Function that recursively walks an archive and patches it into the virtual file system
    ///
    fn walk_archive(
        &mut self,
        current: &mut internal::Folder<'packages>,
        entry: &archive::Entry<'packages, 'files>,
    ) -> std::io::Result<()> {
        for child in entry.children().unwrap() {
            match current.get_item_mut(crate::path::Path::new(child.name())) {
                None => self.insert_new(current, &child)?,
                Some(existing) => self.already_exists(existing, &child)?,
            }
        }
        Ok(())
    }

    ///
    /// Function for handling when a file already exists in the vfs and we try to mount over the top
    /// of it
    ///
    fn already_exists(
        &mut self,
        existing: &mut internal::Entry<'packages>,
        child: &archive::Entry<'packages, 'files>,
    ) -> std::io::Result<()> {
        //
        // Check what type of object already exists and handle each case
        //
        match existing {
            folder @ internal::Entry::Folder(_) => {
                folder.os_folder_to_folder();
                let folder = folder.folder_mut().unwrap();
                self.folder_already_exists(child, folder)?
            }
            internal::Entry::File(item) => self.file_alread_exists(child, item)?,
        }

        Ok(())
    }

    ///
    /// Function called when we are trying to mount something on top of a folder that already exists
    /// in the vfs
    ///
    fn folder_already_exists(
        &mut self,
        child: &archive::Entry<'packages, 'files>,
        folder: &mut internal::Folder<'packages>,
    ) -> std::io::Result<()> {
        match child {
            //
            // We can't mount a file onto a folder as this would unmount other files previously
            // loaded. Although implementing mounting over a file would be trivial, it would
            // probably cause more headaches to users than it's worth with accidentally
            // replacing folders with files
            //
            archive::Entry::File(_) => {
                let kind = ErrorKind::AlreadyExists;
                let error = "Tried to mount a file onto a folder";
                return Err(Error::new(kind, error));
            }
            //
            // Recurse, depth first up the archive structure
            //
            child @ archive::Entry::Folder(_) => self.walk_archive(folder, child)?,
        }
        Ok(())
    }

    ///
    /// Function called when we are trying to mount something on top of a file that already exists
    /// within the vfs
    ///
    fn file_alread_exists(
        &mut self,
        child: &archive::Entry<'packages, 'files>,
        item: &mut internal::File<'packages>,
    ) -> std::io::Result<()> {
        match child {
            //
            // If we're trying to mount a file and there's already a file mounted at this path
            // we just update the entry it's pointing to
            //
            archive::Entry::File(file) => {
                item.item = internal::Item::PackageFile(*file);
            }
            //
            // For similar reasons as stated above with file => folder, mounting a folder onto
            // a file would cause similar headaches, so we treat it as an error
            //
            archive::Entry::Folder(_) => {
                let kind = ErrorKind::AlreadyExists;
                let error = "Tried to mount a folder onto a file";
                return Err(Error::new(kind, error));
            }
        };

        Ok(())
    }

    ///
    /// Function for handling when nothing has been mounted at a point in the vfs
    ///
    fn insert_new(
        &mut self,
        current: &mut internal::Folder<'packages>,
        entry: &archive::Entry<'packages, 'files>,
    ) -> std::io::Result<()> {
        let path = crate::path::PathBuf::from_str(entry.name());
        match entry {
            archive::Entry::File(file) => {
                let file = internal::Item::PackageFile(*file);
                current.insert_file(path, file);
            }
            entry @ archive::Entry::Folder(_) => {
                let folder = current.insert_folder(path);
                let folder = folder.folder_mut().unwrap();
                self.walk_archive(folder, entry)?;
            }
        }

        Ok(())
    }
}
