/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use alloc::PoolAllocator;
use archive::Archive;
use std::fs::File;
use std::ops::Deref;

///
/// Wrapped vector that holds all the archives that are currently open
///
pub struct Packages<'memory, 'files> {
    packages: PoolAllocator<'memory, Archive<'files>>,
}

impl<'memory, 'files> Packages<'memory, 'files> {
    pub fn new(memory: &'memory mut [u8]) -> Self {
        Self {
            packages: PoolAllocator::from_memory(memory),
        }
    }
}

impl<'memory, 'files> Deref for Packages<'memory, 'files> {
    type Target = PoolAllocator<'memory, Archive<'files>>;

    fn deref(&self) -> &Self::Target {
        &self.packages
    }
}

///
/// Wrapped vector that holds all the files for the archives to memory map
///
pub struct Files<'memory> {
    files: PoolAllocator<'memory, File>,
}

impl<'memory> Files<'memory> {
    pub fn new(memory: &'memory mut [u8]) -> Self {
        Self {
            files: PoolAllocator::from_memory(memory),
        }
    }
}

impl<'memory> Deref for Files<'memory> {
    type Target = PoolAllocator<'memory, File>;

    fn deref(&self) -> &Self::Target {
        &self.files
    }
}
