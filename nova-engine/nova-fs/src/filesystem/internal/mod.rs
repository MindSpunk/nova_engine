/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

///
/// A container for the child entities in a folder
///
#[derive(Clone)]
pub struct Folder<'packages> {
    pub name: crate::path::PathBuf,
    pub path: Option<std::path::PathBuf>,
    pub entries: Vec<Entry<'packages>>,
}

impl<'packages> Folder<'packages> {
    ///
    /// Get a reference to an item by name
    ///
    pub fn get_item_ref(&self, name: &crate::path::Path) -> Option<&Entry<'packages>> {
        match self
            .entries
            .binary_search_by(|probe| probe.get_name().cmp(name))
        {
            Ok(index) => Some(&self.entries[index]),
            Err(_) => None,
        }
    }

    ///
    /// Get a reference to an item by name
    ///
    pub fn get_item_mut(&mut self, name: &crate::path::Path) -> Option<&mut Entry<'packages>> {
        match self
            .entries
            .binary_search_by(|probe| probe.get_name().cmp(name))
        {
            Ok(index) => Some(&mut self.entries[index]),
            Err(_) => None,
        }
    }

    pub fn get_item_or_insert_item(&mut self, item: Entry<'packages>) -> &mut Entry<'packages> {
        match self
            .entries
            .binary_search_by(|probe| probe.get_name().cmp(item.get_name()))
        {
            Ok(index) => &mut self.entries[index],
            Err(index) => {
                self.entries.insert(index, item);
                &mut self.entries[index]
            }
        }
    }

    ///
    /// Get a reference to an existing item or insert a folder if there is not folder
    ///
    pub fn get_item_or_insert_os_folder(
        &mut self,
        name: crate::path::PathBuf,
        path: std::path::PathBuf,
    ) -> &mut Entry<'packages> {
        let entry = Folder {
            name,
            path: Some(path),
            entries: Vec::new(),
        };
        let entry = Entry::Folder(entry);
        self.get_item_or_insert_item(entry)
    }

    ///
    /// Get a reference to an existing item or insert a folder if there is not folder
    ///
    pub fn get_item_or_insert_folder(
        &mut self,
        name: crate::path::PathBuf,
    ) -> &mut Entry<'packages> {
        let entry = Folder {
            name,
            path: None,
            entries: Vec::new(),
        };
        let entry = Entry::Folder(entry);
        self.get_item_or_insert_item(entry)
    }

    ///
    /// Insert the given FolderItem into this folder
    ///
    pub fn insert_item(&mut self, item: Entry<'packages>) -> &mut Entry<'packages> {
        match self
            .entries
            .binary_search_by(|probe| probe.get_name().cmp(item.get_name()))
        {
            Ok(index) => {
                self.entries[index] = item;
                &mut self.entries[index]
            }
            Err(index) => {
                self.entries.insert(index, item);
                &mut self.entries[index]
            }
        }
    }

    ///
    /// Insert a new OSFolder into this folder
    ///
    pub fn insert_os_folder(
        &mut self,
        name: crate::path::PathBuf,
        path: std::path::PathBuf,
    ) -> &mut Entry<'packages> {
        let entry = Folder {
            name,
            path: Some(path),
            entries: Vec::new(),
        };
        let entry = Entry::Folder(entry);
        self.insert_item(entry)
    }

    ///
    /// Insert a new Folder into this folder
    ///
    pub fn insert_folder(&mut self, name: crate::path::PathBuf) -> &mut Entry<'packages> {
        let entry = Folder {
            name,
            path: None,
            entries: Vec::new(),
        };
        let entry = Entry::Folder(entry);
        self.insert_item(entry)
    }

    ///
    /// Insert a File into this folder
    ///
    pub fn insert_file(
        &mut self,
        name: crate::path::PathBuf,
        file: Item<'packages>,
    ) -> &mut Entry<'packages> {
        let entry = Entry::File(File { name, item: file });
        self.insert_item(entry)
    }

    ///
    /// Checks if this corresponds to a directory on disk
    ///
    pub fn os_path(&self) -> Option<&std::path::PathBuf> {
        self.path.as_ref()
    }
}

///
/// The different types of child entry in a folder
///
#[derive(Clone)]
pub enum Entry<'packages> {
    ///
    /// A folder
    ///
    Folder(Folder<'packages>),

    ///
    /// A file
    ///
    File(File<'packages>),
}

impl<'packages> AsRef<crate::filesystem::Entry<'packages>> for Entry<'packages> {
    fn as_ref(&self) -> &crate::filesystem::Entry<'packages> {
        unsafe { std::mem::transmute(self) }
    }
}

impl<'packages> AsMut<crate::filesystem::Entry<'packages>> for Entry<'packages> {
    fn as_mut(&mut self) -> &mut crate::filesystem::Entry<'packages> {
        unsafe { std::mem::transmute(self) }
    }
}

impl<'packages> Entry<'packages> {
    pub fn get_name(&self) -> &crate::path::Path {
        match self {
            Entry::Folder(folder) => &folder.name,
            Entry::File(file) => &file.name,
        }
    }

    pub fn folder_mut(&mut self) -> Option<&mut Folder<'packages>> {
        match self {
            Entry::Folder(v) => Some(v),
            Entry::File(_) => None,
        }
    }

    pub fn folder_ref(&self) -> Option<&Folder<'packages>> {
        match self {
            Entry::Folder(v) => Some(v),
            Entry::File(_) => None,
        }
    }

    ///
    /// Inplace conversion from OSFolder to Folder
    ///
    pub fn os_folder_to_folder(&mut self) {
        match self {
            Entry::Folder(folder) => folder.path = None,
            Entry::File(_) => {}
        }
    }
}

#[derive(Clone)]
pub struct File<'packages> {
    pub name: crate::path::PathBuf,
    pub item: Item<'packages>,
}

///
/// A file item in a folder
///
#[derive(Clone)]
pub enum Item<'packages> {
    ///
    ///
    ///
    PackageFile(archive::File<'packages>),

    ///
    ///
    ///
    OSFile(std::path::PathBuf),
}
