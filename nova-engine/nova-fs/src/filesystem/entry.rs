/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use super::internal;
use std::io::{Read, Seek, Write};

///
/// Trait for a file writer
///
pub trait FileWriter: Write + Seek {}

impl<T: Write + Seek> FileWriter for T {}

pub struct Entry<'packages> {
    pub(crate) entry: internal::Entry<'packages>,
}

impl<'packages> Entry<'packages> {
    ///
    /// The name of the entry
    ///
    pub fn name(&self) -> &crate::path::Path {
        self.entry.get_name()
    }

    ///
    /// Is this entry a file
    ///
    pub fn is_file(&self) -> bool {
        match &self.entry {
            internal::Entry::Folder(_) => false,
            internal::Entry::File(_) => true,
        }
    }

    ///
    /// Is this entry a folder
    ///
    pub fn is_folder(&self) -> bool {
        match &self.entry {
            internal::Entry::Folder(_) => true,
            internal::Entry::File(_) => false,
        }
    }

    ///
    /// Can this entry be written to in any way
    ///
    /// Only elements that aren't mounted from an archive are considered writable
    ///
    pub fn is_writable(&self) -> bool {
        match &self.entry {
            internal::Entry::Folder(folder) => folder.path.is_some(),
            internal::Entry::File(file) => match file.item {
                internal::Item::PackageFile(_) => false,
                internal::Item::OSFile(_) => true,
            },
        }
    }

    ///
    /// If the data for this file is mapped into the process address space and a slice to this
    /// memory can be retrieved without allocating
    ///
    pub fn is_resident(&self) -> bool {
        match &self.entry {
            internal::Entry::Folder(_) => false,
            internal::Entry::File(file) => match &file.item {
                internal::Item::PackageFile(file) => file.data().is_some(),
                internal::Item::OSFile(_) => false,
            },
        }
    }

    ///
    /// Gets a slice to the file data if it is store uncompressed and this entry is a file
    ///
    pub fn file_data(&self) -> Option<&'packages [u8]> {
        match &self.entry {
            internal::Entry::Folder(_) => None,
            internal::Entry::File(file) => match &file.item {
                internal::Item::PackageFile(file) => file.data(),
                internal::Item::OSFile(_) => None,
            },
        }
    }

    ///
    /// Gets a reader to the file data if this entry is a file
    ///
    pub fn file_reader(&self) -> Option<Box<dyn Read + 'packages>> {
        match &self.entry {
            internal::Entry::Folder(_) => None,
            internal::Entry::File(file) => match &file.item {
                internal::Item::PackageFile(file) => Some(file.reader()),
                internal::Item::OSFile(file) => {
                    let file = std::fs::OpenOptions::new()
                        .create(false)
                        .write(false)
                        .read(true)
                        .open(file)
                        .ok()?;
                    Some(Box::new(file))
                }
            },
        }
    }

    ///
    ///
    ///
    pub fn file_writer(&self) -> Option<Box<dyn FileWriter + 'packages>> {
        match &self.entry {
            internal::Entry::Folder(_) => None,
            internal::Entry::File(file) => match &file.item {
                internal::Item::PackageFile(_) => None,
                internal::Item::OSFile(file) => {
                    let file = std::fs::OpenOptions::new()
                        .create(false)
                        .write(true)
                        .read(true)
                        .open(file)
                        .ok()?;
                    Some(Box::new(file))
                }
            },
        }
    }

    pub fn children(&self) -> Option<impl Iterator<Item = &Entry<'packages>>> {
        match &self.entry {
            internal::Entry::Folder(folder) => Some(folder.entries.iter().map(|v| v.as_ref())),
            internal::Entry::File(_) => None,
        }
    }
}
