/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#[cfg(test)]
mod test;

use crate::path::PathBuf;
use std::cmp::Ordering;
use std::fmt::{Display, Error, Formatter};

///
/// A slice of a path. Mirrors std::path::Path as much as possible
///
/// Holds a path into the virtual filesystem
///
///
#[derive(PartialEq, PartialOrd, Eq, Ord, Hash, Debug)]
pub struct Path {
    path: str,
}

impl<'a> Path {
    ///
    /// Wraps the given string into a path if it is a valid path
    ///
    /// ```
    /// use nova_fs::path::Path;
    ///
    ///
    /// let path = Path::new("/");
    /// assert_eq!(path.as_str(), "/");
    ///
    /// ```
    pub fn new(path: &'a str) -> &'a Path {
        unsafe { core::mem::transmute(path) }
    }

    ///
    /// Converts this Path to an owned PathBuf
    ///
    pub fn to_path_buf(&self) -> PathBuf {
        PathBuf::from_str(&self.path)
    }

    ///
    /// Gets the underlying string slice of the path
    ///
    pub fn as_str(&self) -> &str {
        &self.path
    }

    ///
    /// Returns a path to the parent directory of whatever entity was passed, if one exists.
    ///
    /// ```
    /// use nova_fs::path::Path;
    ///
    /// let path = Path::new("/");
    /// assert_eq!(path.parent(), None);
    ///
    /// let path = Path::new("/hello_world");
    /// assert_eq!(path.parent(), Some("/".into()));
    ///
    /// let path = Path::new("/foo/bar/baz");
    /// assert_eq!(path.parent(), Some("/foo/bar".into()));
    ///
    /// let path = Path::new("baz");
    /// assert_eq!(path.parent(), None);
    /// ```
    ///
    pub fn parent(&self) -> Option<&Path> {
        if self.is_root() {
            None
        } else {
            let idx = self.path.rfind('/')?;
            let idx = if idx == 0 { 1 } else { idx };
            let sub = &self.path[0..idx];
            let parent = Path::new(sub);
            Some(parent)
        }
    }

    ///
    /// Gets the name of the leaf object in the path, either a file name or folder.
    ///
    /// ```
    /// use nova_fs::path::Path;
    ///
    /// let path = Path::new("/");
    /// assert_eq!(path.leaf_name(), None);
    ///
    /// let path = Path::new("/foo");
    /// assert_eq!(path.leaf_name().map(|v| v.as_str()), Some("foo"));
    ///
    /// let path = Path::new("/foo/bar/baz.spv");
    /// assert_eq!(path.leaf_name().map(|v| v.as_str()), Some("baz.spv"));
    /// ```
    ///
    pub fn leaf_name(&self) -> Option<&Path> {
        if !self.is_root() {
            let mut val = self.path.rsplitn(2, '/');
            Some(Path::new(val.next().unwrap()))
        } else {
            None
        }
    }

    ///
    /// Gets the name of the leaf object in the path, either a file name or folder, without the file
    /// extension.
    ///
    /// ```
    /// use nova_fs::path::Path;
    ///
    /// let path = Path::new("/");
    /// assert_eq!(path.leaf_stem(), None);
    ///
    /// let path = Path::new("/foo");
    /// assert_eq!(path.leaf_stem().map(|v| v.as_str()), Some("foo"));
    ///
    /// let path = Path::new("/foo/bar/baz.spv");
    /// assert_eq!(path.leaf_stem().map(|v| v.as_str()), Some("baz"));
    ///
    /// let path = Path::new("/foo/bar/.spv");
    /// assert_eq!(path.leaf_stem().map(|v| v.as_str()), Some(""));
    ///
    /// let path = Path::new("/foo/bar/test.frag.spv");
    /// assert_eq!(path.leaf_stem().map(|v| v.as_str()), Some("test.frag"));
    ///
    /// let path = Path::new("/foo/bar/test.");
    /// assert_eq!(path.leaf_stem().map(|v| v.as_str()), Some("test"));
    ///
    /// let path = Path::new("foo");
    /// assert_eq!(path.leaf_stem().map(|v| v.as_str()), Some("foo"));
    ///
    /// let path = Path::new("foo/bar/baz.spv");
    /// assert_eq!(path.leaf_stem().map(|v| v.as_str()), Some("baz"));
    ///
    /// let path = Path::new("foo/bar/.spv");
    /// assert_eq!(path.leaf_stem().map(|v| v.as_str()), Some(""));
    ///
    /// let path = Path::new("foo/bar/test.frag.spv");
    /// assert_eq!(path.leaf_stem().map(|v| v.as_str()), Some("test.frag"));
    ///
    /// let path = Path::new("foo/bar/test.");
    /// assert_eq!(path.leaf_stem().map(|v| v.as_str()), Some("test"));
    /// ```
    pub fn leaf_stem(&self) -> Option<&Path> {
        if !self.is_root() {
            let mut val = self.path.rsplitn(2, '/');
            let val = val.next().unwrap();
            if let Some(idx) = val.rfind('.') {
                Some(Path::new(&val[0..idx]))
            } else {
                Some(Path::new(val))
            }
        } else {
            None
        }
    }

    ///
    /// Gets the extension from the leaf object if it exists
    ///
    /// ```
    /// use nova_fs::path::Path;
    ///
    /// let path = Path::new("/");
    /// assert_eq!(path.extension(), None);
    ///
    /// let path = Path::new("/foo");
    /// assert_eq!(path.extension(), None);
    ///
    /// let path = Path::new("foo");
    /// assert_eq!(path.extension(), None);
    ///
    /// let path = Path::new("foo.spv");
    /// assert_eq!(path.extension(), Some("spv"));
    ///
    /// let path = Path::new("/foo/bar/baz.spv");
    /// assert_eq!(path.extension(), Some("spv"));
    ///
    /// let path = Path::new("/foo/bar/.spv");
    /// assert_eq!(path.extension(), Some("spv"));
    ///
    /// let path = Path::new("/foo/bar/test.frag.spv");
    /// assert_eq!(path.extension(), Some("spv"));
    ///
    /// let path = Path::new("/foo/bar/test.");
    /// assert_eq!(path.extension(), Some(""));
    ///
    /// let path = Path::new("foo/bar/baz.spv");
    /// assert_eq!(path.extension(), Some("spv"));
    ///
    /// let path = Path::new("foo/bar/.spv");
    /// assert_eq!(path.extension(), Some("spv"));
    ///
    /// let path = Path::new("foo/bar/test.frag.spv");
    /// assert_eq!(path.extension(), Some("spv"));
    ///
    /// let path = Path::new("foo/bar/test.");
    /// assert_eq!(path.extension(), Some(""));
    /// ```
    pub fn extension(&self) -> Option<&str> {
        if !self.is_root() {
            let mut val = self.path.rsplitn(2, '/');
            let val = val.next().unwrap();
            if let Some(idx) = val.rfind('.') {
                let val = &val[idx + 1..val.len()];
                Some(val)
            } else {
                None
            }
        } else {
            None
        }
    }

    ///
    /// Returns the concatenation of the two paths
    ///
    pub fn join<P: AsRef<Path>>(&self, path: P) -> PathBuf {
        let mut buf = PathBuf::with_capacity(self.path.len() + path.as_ref().as_str().len());
        buf.push(self);
        buf.push(path.as_ref());
        buf
    }

    ///
    /// Returns the path resolved down to containing as little '..' parent specifiers as possible
    ///
    /// ```
    /// use nova_fs::path::Path;
    ///
    /// let path = Path::new("/hello/there/../world");
    /// assert_eq!(path.resolve(), "/hello/world");
    ///
    /// let path = Path::new("../hello/there/../world");
    /// assert_eq!(path.resolve(), "../hello/world");
    ///
    /// let path = Path::new("/hello/there/../../world");
    /// assert_eq!(path.resolve(), "/world");
    ///
    /// let path = Path::new("../../hello/there/../world");
    /// assert_eq!(path.resolve(), "../../hello/world");
    /// ```
    pub fn resolve(&self) -> PathBuf {
        let mut new = PathBuf::with_capacity(self.path.len());

        for component in self.components() {
            match component {
                Component::RootDir => {
                    let root: &Path = Component::RootDir.into();
                    new.push(root);
                }
                Component::Parent => {
                    let parent: &Path = Component::Parent.into();
                    if new.is_root() || new.is_empty() {
                        new.push(parent);
                    } else {
                        let leaf = new.leaf_name().unwrap();
                        if leaf == ".." {
                            new.push(parent)
                        } else {
                            new.pop();
                        }
                    }
                }
                Component::Normal(v) => {
                    new.push(v);
                }
            }
        }

        new
    }

    ///
    /// Makes an owned PathBuf from the path and replaces the leaf name with the new provided name
    ///
    pub fn with_leaf_name<P: AsRef<Path>>(&self, name: P) -> PathBuf {
        let mut buf = self.to_path_buf();
        buf.set_leaf_name(name);
        buf
    }

    ///
    /// Makes an owned PathBuf from the path and replaces the leaf name with the new provided name
    ///
    pub fn with_extension<P: AsRef<Path>>(&self, extension: P) -> PathBuf {
        let mut buf = self.to_path_buf();
        buf.set_extension(extension);
        buf
    }

    ///
    /// Checks if the path starts with the given prefix
    ///
    pub fn starts_with<P: AsRef<Path>>(&self, base: P) -> bool {
        self.path.starts_with(base.as_ref().as_str())
    }

    ///
    /// Checks if the path ends with the given suffix
    ///
    pub fn ends_with<P: AsRef<Path>>(&self, child: P) -> bool {
        self.path.starts_with(child.as_ref().as_str())
    }

    ///
    /// Returns an iterator that yields the parent paths of the path from leaf to root.
    ///
    /// For example, "/hello/world/foo.baz" would yield in order:
    /// - "/hello/world/foo.baz"
    /// - "/hello/world"
    /// - "/hello"
    /// - "/"
    ///
    /// ```
    /// use nova_fs::path::Path;
    ///
    /// let path = Path::new("/foo/bar/baz.spv");
    /// let mut ancestors = path.ancestors();
    ///
    /// assert_eq!(ancestors.next().unwrap(), "/foo/bar/baz.spv");
    /// assert_eq!(ancestors.next().unwrap(), "/foo/bar");
    /// assert_eq!(ancestors.next().unwrap(), "/foo");
    /// assert_eq!(ancestors.next().unwrap(), "/");
    /// assert_eq!(ancestors.next(), None);
    ///
    /// let path = Path::new("foo/bar/baz.spv");
    /// let mut ancestors = path.ancestors();
    ///
    /// assert_eq!(ancestors.next().unwrap(), "foo/bar/baz.spv");
    /// assert_eq!(ancestors.next().unwrap(), "foo/bar");
    /// assert_eq!(ancestors.next().unwrap(), "foo");
    /// assert_eq!(ancestors.next(), None);
    ///
    /// ```
    pub fn ancestors(&self) -> Ancestors<'_> {
        Ancestors { path: self }
    }

    ///
    /// Returns an iterator that yields the components of the path from root to leaf
    ///
    /// For example, "/hello/world/foo.baz" would yield in order:
    /// - RootDir
    /// - "hello"
    /// - "world"
    /// - "foo.baz"
    /// ```
    /// use nova_fs::path::Path;
    /// use nova_fs::path::Component;
    ///
    /// let path = Path::new("/foo/bar/baz.spv");
    /// let mut components = path.components();
    ///
    /// assert_eq!(components.next().unwrap(), Component::RootDir);
    /// assert_eq!(components.next().unwrap(), Component::Normal(Path::new("foo")));
    /// assert_eq!(components.next().unwrap(), Component::Normal(Path::new("bar")));
    /// assert_eq!(components.next().unwrap(), Component::Normal(Path::new("baz.spv")));
    /// assert_eq!(components.next(), None);
    ///
    /// let mut components = path.components();
    ///
    /// assert_eq!(components.next().unwrap(), "/");
    /// assert_eq!(components.next().unwrap(), "foo");
    /// assert_eq!(components.next().unwrap(), "bar");
    /// assert_eq!(components.next().unwrap(), "baz.spv");
    /// assert_eq!(components.next(), None);
    ///
    ///
    ///
    /// let path = Path::new("foo/bar/baz.spv");
    /// let mut components = path.components();
    ///
    /// assert_eq!(components.next().unwrap(), Component::Normal(Path::new("foo")));
    /// assert_eq!(components.next().unwrap(), Component::Normal(Path::new("bar")));
    /// assert_eq!(components.next().unwrap(), Component::Normal(Path::new("baz.spv")));
    /// assert_eq!(components.next(), None);
    ///
    /// let mut components = path.components();
    ///
    /// assert_eq!(components.next().unwrap(), "foo");
    /// assert_eq!(components.next().unwrap(), "bar");
    /// assert_eq!(components.next().unwrap(), "baz.spv");
    /// assert_eq!(components.next(), None);
    ///
    /// ```
    pub fn components(&self) -> impl Iterator<Item = Component<'_>> {
        self.path.split('/').map(|v| {
            if v.is_empty() {
                Component::RootDir
            } else if v == ".." {
                Component::Parent
            } else {
                Component::Normal(Path::new(v))
            }
        })
    }

    ///
    /// Does this path point to the root directory
    ///
    /// ```
    /// use nova_fs::path::Path;
    ///
    /// let root = Path::new("/");
    /// let path = Path::new("/hello");
    ///
    /// assert!(root.is_root());
    /// assert!(!path.is_root());
    /// ```
    ///
    pub fn is_root(&self) -> bool {
        self.path.len() == 1 && self.path.ends_with('/')
    }

    ///
    /// Checks if the path is absolute
    ///
    /// ```
    /// use nova_fs::path::Path;
    ///
    /// let path = Path::new("/");
    /// assert!(path.is_absolute());
    ///
    /// let path = Path::new("hello");
    /// assert!(!path.is_absolute());
    ///
    /// let path = Path::new("/hello");
    /// assert!(path.is_absolute());
    /// ```
    pub fn is_absolute(&self) -> bool {
        self.path.starts_with('/')
    }

    ///
    /// Checks if the path is relative
    ///
    /// ```
    /// use nova_fs::path::Path;
    ///
    /// let path = Path::new("/");
    /// assert!(!path.is_relative());
    ///
    /// let path = Path::new("hello");
    /// assert!(path.is_relative());
    ///
    /// let path = Path::new("/hello");
    /// assert!(!path.is_relative());
    /// ```
    pub fn is_relative(&self) -> bool {
        self.path.is_empty() || !self.path.starts_with('/')
    }

    ///
    /// Checks if the path is empty
    ///
    pub fn is_empty(&self) -> bool {
        self.path.is_empty()
    }
}

impl Display for Path {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        f.write_str(&self.path)
    }
}

impl AsRef<Path> for Path {
    fn as_ref(&self) -> &Path {
        self
    }
}

impl PartialEq<str> for Path {
    fn eq(&self, other: &str) -> bool {
        self.path.eq(other)
    }
}

impl PartialOrd<str> for Path {
    fn partial_cmp(&self, other: &str) -> Option<Ordering> {
        self.path.partial_cmp(other)
    }
}

impl PartialEq<&str> for Path {
    fn eq(&self, other: &&str) -> bool {
        self.path.eq(*other)
    }
}

impl PartialOrd<&str> for Path {
    fn partial_cmp(&self, other: &&str) -> Option<Ordering> {
        self.path.partial_cmp(*other)
    }
}

impl<'a> Into<&'a Path> for &'a str {
    fn into(self) -> &'a Path {
        Path::new(self)
    }
}

///
/// An iterator that yields the parent paths of the path from leaf to root.
///
/// For example, "/hello/world/foo.baz" would yield in order:
/// - "/hello/world/foo.baz"
/// - "/hello/world"
/// - "/hello"
/// - "/"
///
pub struct Ancestors<'a> {
    path: &'a Path,
}

impl<'a> Iterator for Ancestors<'a> {
    type Item = &'a Path;

    fn next(&mut self) -> Option<Self::Item> {
        if self.path.as_str().is_empty() {
            None
        } else {
            let old = self.path;
            if let Some(new) = self.path.parent() {
                self.path = new;
            } else {
                self.path = Path::new("");
            }
            Some(old)
        }
    }
}

///
/// A component part of a path
///
#[derive(Copy, Clone, Debug, PartialOrd, PartialEq, Ord, Eq)]
pub enum Component<'a> {
    RootDir,
    Parent,
    Normal(&'a Path),
}

impl<'a> Into<&'a Path> for Component<'a> {
    fn into(self) -> &'a Path {
        match self {
            Component::RootDir => Path::new("/"),
            Component::Parent => Path::new(".."),
            Component::Normal(v) => v,
        }
    }
}

impl<'a> PartialEq<str> for Component<'a> {
    fn eq(&self, other: &str) -> bool {
        match self {
            Component::RootDir => other == "/",
            Component::Parent => other == "..",
            Component::Normal(v) => other == &v.path,
        }
    }
}

impl<'a> PartialEq<&str> for Component<'a> {
    fn eq(&self, other: &&str) -> bool {
        match self {
            Component::RootDir => *other == "/",
            Component::Parent => *other == "..",
            Component::Normal(v) => *other == &v.path,
        }
    }
}

impl<'a> PartialOrd<str> for Component<'a> {
    fn partial_cmp(&self, other: &str) -> Option<Ordering> {
        match self {
            Component::RootDir => "/".partial_cmp(other),
            Component::Parent => "..".partial_cmp(other),
            Component::Normal(v) => v.path.partial_cmp(other),
        }
    }
}

impl<'a> PartialOrd<&str> for Component<'a> {
    fn partial_cmp(&self, other: &&str) -> Option<Ordering> {
        match self {
            Component::RootDir => "/".partial_cmp(*other),
            Component::Parent => "..".partial_cmp(*other),
            Component::Normal(v) => v.path.partial_cmp(*other),
        }
    }
}
