/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::path::Component;
use crate::path::Path;

#[test]
pub fn new_test_0() {
    let path = Path::new("/hello_world");
    assert_eq!(path.as_str(), "/hello_world");
}

#[test]
pub fn parent_test_0() {
    let path = Path::new("/");
    assert_eq!(path.parent(), None);

    let path = Path::new("/hello_world");
    assert_eq!(path.parent(), Some("/".into()));

    let path = Path::new("/foo/bar/baz");
    assert_eq!(path.parent(), Some("/foo/bar".into()));

    let path = Path::new("baz");
    assert_eq!(path.parent(), None);
}

#[test]
pub fn leaf_name_test_0() {
    let path = Path::new("/");
    assert_eq!(path.leaf_name(), None);

    let path = Path::new("/foo");
    assert_eq!(path.leaf_name().map(|v| v.as_str()), Some("foo"));

    let path = Path::new("/foo/bar/baz.spv");
    assert_eq!(path.leaf_name().map(|v| v.as_str()), Some("baz.spv"));
}

#[test]
pub fn leaf_stem_test_0() {
    let path = Path::new("/");
    assert_eq!(path.leaf_stem(), None);

    let path = Path::new("/foo");
    assert_eq!(path.leaf_stem().map(|v| v.as_str()), Some("foo"));

    let path = Path::new("/foo/bar/baz.spv");
    assert_eq!(path.leaf_stem().map(|v| v.as_str()), Some("baz"));

    let path = Path::new("/foo/bar/.spv");
    assert_eq!(path.leaf_stem().map(|v| v.as_str()), Some(""));

    let path = Path::new("/foo/bar/test.frag.spv");
    assert_eq!(path.leaf_stem().map(|v| v.as_str()), Some("test.frag"));

    let path = Path::new("/foo/bar/test.");
    assert_eq!(path.leaf_stem().map(|v| v.as_str()), Some("test"));

    let path = Path::new("foo");
    assert_eq!(path.leaf_stem().map(|v| v.as_str()), Some("foo"));

    let path = Path::new("foo/bar/baz.spv");
    assert_eq!(path.leaf_stem().map(|v| v.as_str()), Some("baz"));

    let path = Path::new("foo/bar/.spv");
    assert_eq!(path.leaf_stem().map(|v| v.as_str()), Some(""));

    let path = Path::new("foo/bar/test.frag.spv");
    assert_eq!(path.leaf_stem().map(|v| v.as_str()), Some("test.frag"));

    let path = Path::new("foo/bar/test.");
    assert_eq!(path.leaf_stem().map(|v| v.as_str()), Some("test"));
}

#[test]
pub fn extension_test_0() {
    let path = Path::new("/");
    assert_eq!(path.extension(), None);

    let path = Path::new("/foo");
    assert_eq!(path.extension(), None);

    let path = Path::new("foo");
    assert_eq!(path.extension(), None);

    let path = Path::new("foo.spv");
    assert_eq!(path.extension(), Some("spv"));

    let path = Path::new("/foo/bar/baz.spv");
    assert_eq!(path.extension(), Some("spv"));

    let path = Path::new("/foo/bar/.spv");
    assert_eq!(path.extension(), Some("spv"));

    let path = Path::new("/foo/bar/test.frag.spv");
    assert_eq!(path.extension(), Some("spv"));

    let path = Path::new("/foo/bar/test.");
    assert_eq!(path.extension(), Some(""));

    let path = Path::new("foo/bar/baz.spv");
    assert_eq!(path.extension(), Some("spv"));

    let path = Path::new("foo/bar/.spv");
    assert_eq!(path.extension(), Some("spv"));

    let path = Path::new("foo/bar/test.frag.spv");
    assert_eq!(path.extension(), Some("spv"));

    let path = Path::new("foo/bar/test.");
    assert_eq!(path.extension(), Some(""));
}

#[test]
pub fn resolve_test_0() {
    let path = Path::new("/hello/there/../world");
    assert_eq!(path.resolve(), "/hello/world");

    let path = Path::new("../hello/there/../world");
    assert_eq!(path.resolve(), "../hello/world");

    let path = Path::new("/hello/there/../../world");
    assert_eq!(path.resolve(), "/world");

    let path = Path::new("../../hello/there/../world");
    assert_eq!(path.resolve(), "../../hello/world");
}

#[test]
pub fn ancestors_test_0() {
    let path = Path::new("/foo/bar/baz.spv");
    let mut ancestors = path.ancestors();

    assert_eq!(ancestors.next().unwrap(), "/foo/bar/baz.spv");
    assert_eq!(ancestors.next().unwrap(), "/foo/bar");
    assert_eq!(ancestors.next().unwrap(), "/foo");
    assert_eq!(ancestors.next().unwrap(), "/");
    assert_eq!(ancestors.next(), None);

    let path = Path::new("foo/bar/baz.spv");
    let mut ancestors = path.ancestors();

    assert_eq!(ancestors.next().unwrap(), "foo/bar/baz.spv");
    assert_eq!(ancestors.next().unwrap(), "foo/bar");
    assert_eq!(ancestors.next().unwrap(), "foo");
    assert_eq!(ancestors.next(), None);
}

#[test]
pub fn components_test_0() {
    let path = Path::new("/foo/bar/baz.spv");
    let mut components = path.components();

    assert_eq!(components.next().unwrap(), Component::RootDir);
    assert_eq!(
        components.next().unwrap(),
        Component::Normal(Path::new("foo"))
    );
    assert_eq!(
        components.next().unwrap(),
        Component::Normal(Path::new("bar"))
    );
    assert_eq!(
        components.next().unwrap(),
        Component::Normal(Path::new("baz.spv"))
    );
    assert_eq!(components.next(), None);

    let mut components = path.components();

    assert_eq!(components.next().unwrap(), "/");
    assert_eq!(components.next().unwrap(), "foo");
    assert_eq!(components.next().unwrap(), "bar");
    assert_eq!(components.next().unwrap(), "baz.spv");
    assert_eq!(components.next(), None);

    let path = Path::new("foo/bar/baz.spv");
    let mut components = path.components();

    assert_eq!(
        components.next().unwrap(),
        Component::Normal(Path::new("foo"))
    );
    assert_eq!(
        components.next().unwrap(),
        Component::Normal(Path::new("bar"))
    );
    assert_eq!(
        components.next().unwrap(),
        Component::Normal(Path::new("baz.spv"))
    );
    assert_eq!(components.next(), None);

    let mut components = path.components();

    assert_eq!(components.next().unwrap(), "foo");
    assert_eq!(components.next().unwrap(), "bar");
    assert_eq!(components.next().unwrap(), "baz.spv");
    assert_eq!(components.next(), None);
}

#[test]
pub fn is_root_test_0() {
    let root = Path::new("/");
    let path = Path::new("/hello");

    assert!(root.is_root());
    assert!(!path.is_root());
}

#[test]
pub fn is_absolute_test_0() {
    let path = Path::new("/");
    assert!(path.is_absolute());

    let path = Path::new("hello");
    assert!(!path.is_absolute());

    let path = Path::new("/hello");
    assert!(path.is_absolute());
}

#[test]
pub fn is_relative_test_0() {
    let path = Path::new("/");
    assert!(!path.is_relative());

    let path = Path::new("hello");
    assert!(path.is_relative());

    let path = Path::new("/hello");
    assert!(!path.is_relative());
}
