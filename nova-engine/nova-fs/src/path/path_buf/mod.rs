/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#[cfg(test)]
mod test;

use crate::path::Path;
use std::cmp::Ordering;
use std::fmt::{Debug, Display, Error, Formatter};
use std::ops::Deref;

///
/// An owned, modifiable Path
///
/// See the Path struct documentation for more information on paths
///
#[derive(Clone, PartialEq, PartialOrd, Eq, Ord, Hash)]
#[repr(transparent)]
pub struct PathBuf {
    path: String,
}

impl PathBuf {
    ///
    /// Constructs a new PathBuf pointing to the root directory
    ///
    pub fn new() -> Self {
        Self {
            path: "".to_string(),
        }
    }

    ///
    /// Constructs a new PathBuf pointing to the root directory
    ///
    pub fn with_capacity(capacity: usize) -> Self {
        let path = String::with_capacity(capacity);
        Self { path }
    }

    ///
    /// Construct a new PathBuf from the given path string
    ///
    /// ```
    /// use nova_fs::path::PathBuf;
    ///
    /// let path = PathBuf::from_string(String::from("/hello/world"));
    /// assert_eq!(path.as_str(), "/hello/world");
    ///
    /// ```
    ///
    ///
    pub fn from_string(path: String) -> PathBuf {
        Self { path }
    }

    ///
    /// Construct a new PathBuf from the given path string
    ///
    /// ```
    /// use nova_fs::path::PathBuf;
    ///
    /// let path = PathBuf::from_str("/hello/world");
    /// assert_eq!(path.as_str(), "/hello/world");
    ///
    /// ```
    ///
    pub fn from_str(path: &str) -> PathBuf {
        Self {
            path: path.to_string(),
        }
    }

    ///
    /// Coerces to a Path slice
    ///
    pub fn as_path(&self) -> &Path {
        Path::new(&self.path)
    }

    ///
    /// Extends self by appending path on as a child
    ///
    /// ```
    /// use nova_fs::path::PathBuf;
    /// use nova_fs::path::Path;
    ///
    /// let mut path = PathBuf::from_str("/");
    /// assert!(path.is_root());
    ///
    /// path.push(Path::new("foo"));
    /// assert_eq!(path, "/foo");
    ///
    /// path.push(Path::new("bar"));
    /// assert_eq!(path, "/foo/bar");
    ///
    /// path.push(Path::new("baz.spv"));
    /// assert_eq!(path, "/foo/bar/baz.spv");
    ///
    /// path.push(Path::new("/bar"));
    /// assert_eq!(path, "/bar");
    ///
    /// let mut path = PathBuf::from_str("");
    /// assert!(path.is_empty());
    ///
    /// path.push(Path::new("bar"));
    /// assert_eq!(path, "bar");
    ///
    /// path.push(Path::new("baz"));
    /// assert_eq!(path, "bar/baz");
    /// ```
    ///
    pub fn push<P: AsRef<Path>>(&mut self, path: P) {
        let path = path.as_ref();

        if path.as_str().starts_with('/') {
            self.path = path.as_str().to_string();
        } else {
            if !self.path.ends_with('/') && !self.path.is_empty() {
                self.path.push('/');
            }
            self.path.push_str(path.as_str());
        };
    }

    ///
    /// Truncates `self` to `self.parent`.
    ///
    /// Returns `false` and does nothing if `self.parent` is `None`. Otherwise, returns `true`.
    ///
    /// ```
    /// use nova_fs::path::PathBuf;
    ///
    /// let mut path = PathBuf::from_str("/foo/bar/baz.spv");
    /// assert!(!path.is_root());
    /// assert_eq!(path, "/foo/bar/baz.spv");
    ///
    /// assert!(path.pop());
    /// assert_eq!(path, "/foo/bar");
    ///
    /// assert!(path.pop());
    /// assert_eq!(path, "/foo");
    ///
    /// assert!(path.pop());
    /// assert_eq!(path, "/");
    ///
    /// assert!(!path.pop());
    /// assert_eq!(path, "/");
    ///
    ///
    /// let mut path = PathBuf::from_str("foo/bar/baz.spv");
    /// assert!(!path.is_root());
    /// assert_eq!(path, "foo/bar/baz.spv");
    ///
    /// assert!(path.pop());
    /// assert_eq!(path, "foo/bar");
    ///
    /// assert!(path.pop());
    /// assert_eq!(path, "foo");
    ///
    /// assert!(!path.pop());
    /// assert_eq!(path, "foo");
    /// ```
    ///
    pub fn pop(&mut self) -> bool {
        if let Some(idx) = self.path.rfind('/') {
            if self.path.len() == 1 {
                false
            } else {
                let idx = if idx == 0 { 1 } else { idx };
                self.path.truncate(idx);
                true
            }
        } else {
            false
        }
    }

    ///
    /// Replaces the leaf name with the new given leaf name.
    ///
    /// ```
    /// use nova_fs::path::PathBuf;
    /// use nova_fs::path::Path;
    ///
    /// let mut path = PathBuf::from_str("/foo/bar/baz.spv");
    /// path.set_leaf_name(Path::new("bop.spirv"));
    /// assert_eq!(path, "/foo/bar/bop.spirv");
    ///
    /// let mut path = PathBuf::from_str("baz.spv");
    /// path.set_leaf_name(Path::new("bop.spirv"));
    /// assert_eq!(path, "bop.spirv");
    ///
    /// let mut path = PathBuf::from_str("/baz.spv");
    /// path.set_leaf_name(Path::new("bop.spirv"));
    /// assert_eq!(path, "/bop.spirv");
    ///
    /// let mut path = PathBuf::from_str("/");
    /// path.set_leaf_name(Path::new("bop.spirv"));
    /// assert_eq!(path, "/bop.spirv");
    /// ```
    ///
    pub fn set_leaf_name<P: AsRef<Path>>(&mut self, name: P) {
        if self.is_absolute() || self.parent().is_some() {
            self.pop();
            self.push(name);
        } else {
            self.path.clear();
            self.path.push_str(name.as_ref().as_str());
        }
    }

    ///
    /// Replaces the leaf name with the new given leaf name.
    ///
    /// ```
    /// use nova_fs::path::PathBuf;
    /// use nova_fs::path::Path;
    ///
    /// let mut path = PathBuf::from_str("/foo/bar/baz.spv");
    /// path.set_leaf_name(Path::new("bop.spirv"));
    /// assert_eq!(path, "/foo/bar/bop.spirv");
    ///
    /// let mut path = PathBuf::from_str("foo/bar/baz.spv");
    /// path.set_leaf_name(Path::new("bop.spirv"));
    /// assert_eq!(path, "foo/bar/bop.spirv");
    ///
    /// let mut path = PathBuf::from_str("baz.spv");
    /// path.set_leaf_name(Path::new("bop.spirv"));
    /// assert_eq!(path, "bop.spirv");
    ///
    /// let mut path = PathBuf::from_str("/baz.spv");
    /// path.set_leaf_name(Path::new("bop.spirv"));
    /// assert_eq!(path, "/bop.spirv");
    ///
    /// let mut path = PathBuf::from_str("/");
    /// path.set_leaf_name(Path::new("bop.spirv"));
    /// assert_eq!(path, "/bop.spirv");
    /// ```
    ///
    pub fn set_extension<P: AsRef<Path>>(&mut self, extension: P) -> bool {
        if !self.is_root() {
            let mut val = self.path.rsplitn(2, '/');
            let val = val.next().unwrap();
            if let Some(idx) = val.rfind('.') {
                self.path.truncate(idx);
            }
            self.path.push('.');
            self.path.push_str(extension.as_ref().as_str());
            true
        } else {
            false
        }
    }
}

impl Display for PathBuf {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        <String as Display>::fmt(&self.path, f)
    }
}

impl Debug for PathBuf {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        <String as Debug>::fmt(&self.path, f)
    }
}

impl Into<String> for PathBuf {
    fn into(self) -> String {
        self.path
    }
}

impl Deref for PathBuf {
    type Target = Path;

    fn deref(&self) -> &Self::Target {
        Path::new(&self.path)
    }
}

impl AsRef<Path> for PathBuf {
    fn as_ref(&self) -> &Path {
        self.as_path()
    }
}

impl PartialEq<str> for PathBuf {
    fn eq(&self, other: &str) -> bool {
        self.path.eq(other)
    }
}

impl PartialOrd<str> for PathBuf {
    fn partial_cmp(&self, other: &str) -> Option<Ordering> {
        PartialOrd::partial_cmp(other, &self.path)
    }
}

impl PartialEq<&str> for PathBuf {
    fn eq(&self, other: &&str) -> bool {
        self.path.eq(*other)
    }
}

impl PartialOrd<&str> for PathBuf {
    fn partial_cmp(&self, other: &&str) -> Option<Ordering> {
        PartialOrd::partial_cmp(*other, &self.path)
    }
}

impl From<&str> for PathBuf {
    fn from(value: &str) -> Self {
        PathBuf::from_str(value)
    }
}

impl From<String> for PathBuf {
    fn from(value: String) -> Self {
        PathBuf::from_string(value)
    }
}
