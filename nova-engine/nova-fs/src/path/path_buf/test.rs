/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::path::{Path, PathBuf};

#[test]
pub fn from_string_test_0() {
    let path = PathBuf::from_string(String::from("/hello/world"));
    assert_eq!(path.as_str(), "/hello/world");
}

#[test]
pub fn from_str_test_0() {
    let path = PathBuf::from_str("/hello/world");
    assert_eq!(path.as_str(), "/hello/world");
}

#[test]
pub fn push_test_0() {
    let mut path = PathBuf::from_str("/");
    assert!(path.is_root());

    path.push(Path::new("foo"));
    assert_eq!(path, "/foo");

    path.push(Path::new("bar"));
    assert_eq!(path, "/foo/bar");

    path.push(Path::new("baz.spv"));
    assert_eq!(path, "/foo/bar/baz.spv");

    path.push(Path::new("/bar"));
    assert_eq!(path, "/bar");

    let mut path = PathBuf::from_str("");
    assert!(path.is_empty());

    path.push(Path::new("bar"));
    assert_eq!(path, "bar");

    path.push(Path::new("baz"));
    assert_eq!(path, "bar/baz");
}

#[test]
pub fn pop_test_0() {
    let mut path = PathBuf::from_str("/foo/bar/baz.spv");

    assert!(!path.is_root());
    assert_eq!(path, "/foo/bar/baz.spv");

    assert!(path.pop());
    assert_eq!(path, "/foo/bar");

    assert!(path.pop());
    assert_eq!(path, "/foo");

    assert!(path.pop());
    assert_eq!(path, "/");

    assert!(!path.pop());
    assert_eq!(path, "/");

    let mut path = PathBuf::from_str("foo/bar/baz.spv");

    assert!(!path.is_root());
    assert_eq!(path, "foo/bar/baz.spv");

    assert!(path.pop());
    assert_eq!(path, "foo/bar");

    assert!(path.pop());
    assert_eq!(path, "foo");

    assert!(!path.pop());
    assert_eq!(path, "foo");
}

#[test]
pub fn set_leaf_name_test_0() {
    let mut path = PathBuf::from_str("/foo/bar/baz.spv");
    path.set_leaf_name(Path::new("bop.spirv"));
    assert_eq!(path, "/foo/bar/bop.spirv");

    let mut path = PathBuf::from_str("baz.spv");
    path.set_leaf_name(Path::new("bop.spirv"));
    assert_eq!(path, "bop.spirv");

    let mut path = PathBuf::from_str("/baz.spv");
    path.set_leaf_name(Path::new("bop.spirv"));
    assert_eq!(path, "/bop.spirv");

    let mut path = PathBuf::from_str("/");
    path.set_leaf_name(Path::new("bop.spirv"));
    assert_eq!(path, "/bop.spirv");
}

#[test]
pub fn set_extension_test_0() {
    let mut path = PathBuf::from_str("/foo/bar/baz.spv");
    path.set_leaf_name(Path::new("bop.spirv"));
    assert_eq!(path, "/foo/bar/bop.spirv");

    let mut path = PathBuf::from_str("foo/bar/baz.spv");
    path.set_leaf_name(Path::new("bop.spirv"));
    assert_eq!(path, "foo/bar/bop.spirv");

    let mut path = PathBuf::from_str("baz.spv");
    path.set_leaf_name(Path::new("bop.spirv"));
    assert_eq!(path, "bop.spirv");

    let mut path = PathBuf::from_str("/baz.spv");
    path.set_leaf_name(Path::new("bop.spirv"));
    assert_eq!(path, "/bop.spirv");

    let mut path = PathBuf::from_str("/");
    path.set_leaf_name(Path::new("bop.spirv"));
    assert_eq!(path, "/bop.spirv");
}
