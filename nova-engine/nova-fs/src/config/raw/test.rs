/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::config::raw::RawMountConfig;
use crate::config::test::TEST_STR;

#[test]
pub fn parse_test_0() {
    let value: RawMountConfig = toml::from_str(TEST_STR).unwrap();

    let v = &value.mount[0];
    assert_eq!(v.package, Some("assets/core.package".to_string()));
    assert_eq!(v.mount, None);
    assert_eq!(v.config, None);
    assert_eq!(v.at, Some("/core".to_string()));

    let v = &value.mount[1];
    assert_eq!(v.package, Some("assets/extra.package".to_string()));
    assert_eq!(v.mount, None);
    assert_eq!(v.config, None);
    assert_eq!(v.at, Some("/extra".to_string()));

    let v = &value.mount[2];
    assert_eq!(v.package, Some("assets/game.package".to_string()));
    assert_eq!(v.mount, None);
    assert_eq!(v.config, None);
    assert_eq!(v.at, Some("/assets".to_string()));

    let v = &value.mount[3];
    assert_eq!(v.package, None);
    assert_eq!(v.mount, Some("saves".to_string()));
    assert_eq!(v.config, None);
    assert_eq!(v.at, Some("/save".to_string()));
}
