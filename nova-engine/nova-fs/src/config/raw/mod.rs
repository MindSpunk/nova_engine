/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#[cfg(test)]
mod test;

use crate::config::{Config, ConfigValidationError, Directory, Mount, MountConfig, Package};
use serde::Deserialize;
use serde::Serialize;

///
/// The raw struct used to define the schema for the nova-fs Mount.toml config format
///
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct RawMountConfig {
    pub mount: Vec<RawMount>,
}

impl RawMountConfig {
    ///
    /// Parse the given toml string into a RawMountConfig
    ///
    pub fn from_toml(string: &str) -> Result<RawMountConfig, toml::de::Error> {
        toml::from_str(string)
    }

    ///
    /// Validate the RawMountConfig and transition it to a valid MountConfig or produce an error
    /// if the config is invalid
    ///
    pub fn validate(self, path: std::path::PathBuf) -> Result<MountConfig, ConfigValidationError> {
        let mut mount = Vec::with_capacity(self.mount.len());
        for m in self.mount {
            if m.package.is_some() {
                if m.at.is_none() {
                    return Err(ConfigValidationError::MissingMountPoint(m));
                } else {
                    let package = Package {
                        package: m.package.unwrap(),
                        at: m.at.unwrap(),
                    };
                    mount.push(Mount::Package(package));
                }
            } else if m.mount.is_some() {
                if m.at.is_none() {
                    return Err(ConfigValidationError::MissingMountPoint(m));
                } else {
                    let directory = Directory {
                        mount: m.mount.unwrap(),
                        at: m.at.unwrap(),
                    };
                    mount.push(Mount::Directory(directory));
                }
            } else if m.config.is_some() {
                let config = Config {
                    config: m.config.unwrap(),
                };
                mount.push(Mount::Config(config));
            } else {
                return Err(ConfigValidationError::UnknownMountType(m));
            }
        }

        for i in 1..mount.len() {
            let check = &mount[i - 1];
            for j in i..mount.len() {
                let other = &mount[j];
                if check == other {
                    let err = ConfigValidationError::DuplicateEntries(check.clone());
                    return Err(err);
                }
            }
        }

        Ok(MountConfig { path, mount })
    }
}

///
/// A raw element in the table of a Mount.toml config
///
#[derive(Clone, Debug, Serialize, Deserialize, PartialOrd, PartialEq, Ord, Eq)]
pub struct RawMount {
    pub at: Option<String>,
    pub package: Option<String>,
    pub mount: Option<String>,
    pub config: Option<String>,
}
