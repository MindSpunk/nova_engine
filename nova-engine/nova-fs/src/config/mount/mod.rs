/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#[cfg(test)]
mod test;

use crate::config::raw::{RawMount, RawMountConfig};
use serde::export::fmt::Error;
use serde::export::Formatter;
use std::fmt::{Debug, Display};

///
/// A derivation from `RawMountConfig`
///
/// The result of validating a RawMountConfig
///
#[derive(Clone, Debug)]
pub struct MountConfig {
    pub path: std::path::PathBuf,
    pub mount: Vec<Mount>,
}

impl MountConfig {
    pub fn load<P: AsRef<std::path::Path>>(path: P) -> Result<MountConfig, ConfigLoadError> {
        let string =
            std::fs::read_to_string(path.as_ref()).map_err(|err| ConfigLoadError::IOError(err))?;
        Self::from_toml(path, &string)
    }

    pub fn from_toml<P: AsRef<std::path::Path>>(
        path: P,
        string: &str,
    ) -> Result<MountConfig, ConfigLoadError> {
        let config =
            RawMountConfig::from_toml(string).map_err(|err| ConfigLoadError::ParseError(err))?;
        config
            .validate(path.as_ref().to_path_buf())
            .map_err(|err| ConfigLoadError::ValidationError(err))
    }
}

///
/// An object to be mounted in the virtual filesystem
///
#[derive(Clone, Debug, PartialOrd, PartialEq, Ord, Eq)]
pub enum Mount {
    ///
    /// Mount the given package file into the filesystem
    ///
    Package(Package),

    ///
    /// Mount the given directory into the filesystem
    ///
    Directory(Directory),

    ///
    /// Parse and mount the given sub config as if it's contents were placed inline inplace of this
    /// entry
    ///
    Config(Config),
}

///
/// The information required to mount a package
///
#[derive(Clone, Debug, PartialOrd, PartialEq, Ord, Eq)]
pub struct Package {
    pub package: String,
    pub at: String,
}

///
/// The information required to mount a directory
///
#[derive(Clone, Debug, PartialOrd, PartialEq, Ord, Eq)]
pub struct Directory {
    pub mount: String,
    pub at: String,
}

///
/// The information required to mount a sub config
///
#[derive(Clone, Debug, PartialOrd, PartialEq, Ord, Eq)]
pub struct Config {
    pub config: String,
}

///
///
///
#[derive(Debug)]
pub enum ConfigLoadError {
    ///
    /// If the config failed to load because it fails validation
    ///
    ValidationError(ConfigValidationError),

    ///
    /// If the config failed to load because it failed to parse a valid toml document
    ///
    ParseError(toml::de::Error),

    ///
    /// If the config failed to load because of an IO error
    ///
    IOError(std::io::Error),
}

impl Display for ConfigLoadError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        Debug::fmt(self, f)
    }
}

impl std::error::Error for ConfigLoadError {}

///
///
///
#[derive(Clone, Debug, PartialOrd, PartialEq, Ord, Eq)]
pub enum ConfigValidationError {
    ///
    /// If a package or directory mount was defined without a mount point
    ///
    MissingMountPoint(RawMount),

    ///
    /// If an entry does not conform to the requirements of any of the expected mount types
    ///
    UnknownMountType(RawMount),

    ///
    /// There are multiple instances of the same entry in the list
    ///
    DuplicateEntries(Mount),
}

impl Display for ConfigValidationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        Debug::fmt(self, f)
    }
}

impl std::error::Error for ConfigValidationError {}
