/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::config::raw::{RawMount, RawMountConfig};
use crate::config::test::*;
use crate::config::{ConfigValidationError, Mount, Package};
use std::path::PathBuf;

#[test]
pub fn validation_test_0() {
    let value: RawMountConfig = toml::from_str(TEST_STR).unwrap();
    let value = value.validate(PathBuf::new()).unwrap();

    match &value.mount[0] {
        Mount::Package(v) => {
            assert_eq!(v.package, "assets/core.package");
            assert_eq!(v.at, "/core");
        }
        Mount::Directory(_) | Mount::Config(_) => panic!("Incorrect type"),
    }

    match &value.mount[1] {
        Mount::Package(v) => {
            assert_eq!(v.package, "assets/extra.package");
            assert_eq!(v.at, "/extra");
        }
        Mount::Directory(_) | Mount::Config(_) => panic!("Incorrect type"),
    }

    match &value.mount[2] {
        Mount::Package(v) => {
            assert_eq!(v.package, "assets/game.package");
            assert_eq!(v.at, "/assets");
        }
        Mount::Directory(_) | Mount::Config(_) => panic!("Incorrect type"),
    }

    match &value.mount[3] {
        Mount::Package(_) => panic!("Incorrect type"),
        Mount::Directory(v) => {
            assert_eq!(v.mount, "saves");
            assert_eq!(v.at, "/save");
        }
        Mount::Config(_) => panic!("Incorrect type"),
    }

    match &value.mount[4] {
        Mount::Package(_) | Mount::Directory(_) => panic!("Incorrect type"),
        Mount::Config(v) => {
            assert_eq!(v.config, "mods/Mount.toml");
        }
    }
}

#[test]
pub fn validation_test_1() {
    let value: RawMountConfig = toml::from_str(ERR_MISSING_PACKAGE_TEST_STR).unwrap();
    let value = value.validate(PathBuf::new());

    assert!(value.is_err());
    assert_eq!(
        value.err().unwrap(),
        ConfigValidationError::MissingMountPoint(RawMount {
            at: None,
            package: Some("assets/extra.package".to_string()),
            mount: None,
            config: None
        })
    );
}

#[test]
pub fn validation_test_2() {
    let value: RawMountConfig = toml::from_str(ERR_MISSING_DIR_TEST_STR).unwrap();
    let value = value.validate(PathBuf::new());

    assert!(value.is_err());
    assert_eq!(
        value.err().unwrap(),
        ConfigValidationError::MissingMountPoint(RawMount {
            at: None,
            package: None,
            mount: Some("save".to_string()),
            config: None
        })
    );
}

#[test]
pub fn validation_test_3() {
    let value: RawMountConfig = toml::from_str(ERR_UNKNOWN_TEST_STR).unwrap();
    let value = value.validate(PathBuf::new());

    assert!(value.is_err());
    assert_eq!(
        value.err().unwrap(),
        ConfigValidationError::UnknownMountType(RawMount {
            at: None,
            package: None,
            mount: None,
            config: None
        })
    );
}

#[test]
pub fn validation_test_4() {
    let value: RawMountConfig = toml::from_str(ERR_DUPLICATE_TEST_STR).unwrap();
    let value = value.validate(PathBuf::new());

    assert!(value.is_err());
    assert_eq!(
        value.err().unwrap(),
        ConfigValidationError::DuplicateEntries(Mount::Package(Package {
            package: "assets/core.package".to_string(),
            at: "/core".to_string()
        }))
    );
}
