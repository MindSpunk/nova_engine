/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

pub const TEST_STR: &'static str = r#"
[[mount]]
package = "assets/core.package"
at = "/core"

[[mount]]
package = "assets/extra.package"
at = "/extra"

[[mount]]
package = "assets/game.package"
at = "/assets"

[[mount]]
mount = "saves"
at = "/save"

[[mount]]
config = "mods/Mount.toml"
"#;

pub const ERR_MISSING_PACKAGE_TEST_STR: &'static str = r#"
[[mount]]
package = "assets/core.package"
at = "/core"

[[mount]]
package = "assets/extra.package"
"#;

pub const ERR_MISSING_DIR_TEST_STR: &'static str = r#"
[[mount]]
package = "assets/core.package"
at = "/core"

[[mount]]
mount = "save"
"#;

pub const ERR_UNKNOWN_TEST_STR: &'static str = r#"
[[mount]]
package = "assets/core.package"
at = "/core"

[[mount]]
"#;

pub const ERR_DUPLICATE_TEST_STR: &'static str = r#"
[[mount]]
package = "assets/core.package"
at = "/core"

[[mount]]
package = "assets/core.package"
at = "/core"
"#;
