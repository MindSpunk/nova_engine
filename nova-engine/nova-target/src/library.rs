/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

///
/// This macro expands into code for detecting the target platform (what the current executable has
/// been built for) inside a library crate.
///
/// Expand this macro somewhere in the crate using this library like the example provided below.
/// This will provide a set of functions will be defined for getting the current `target_platform`,
/// `target_architecture` and `target_build_type`. These functions will be defined in the module
/// that the macro is called in.
///
/// ```
/// nova_target::target_functions!();
/// ```
///
/// The macro must be used to provide these functions as the code must be compiled inside the crate
/// using these functions as the library wont have access to the cfg values used to detect the
/// platform. These can't be compiled in this library crate as the library would have to depend on
/// itself as a build dependency.
///
#[macro_export]
macro_rules! target_functions {
    () => {
        ///
        /// Returns the target platform
        ///
        #[inline]
        pub fn target_platform() -> $crate::Platform {
            if cfg!(NOVA_BUILD_PLATFORM_TARGET_is_windows) {
                if cfg!(NOVA_BUILD_PLATFORM_TARGET_is_msvc) {
                    $crate::Platform::WindowsMSVC
                } else if cfg!(NOVA_BUILD_PLATFORM_TARGET_is_gnu) {
                    $crate::Platform::WindowsGNU
                } else {
                    panic!("Unsupported Platform")
                }
            } else if cfg!(NOVA_BUILD_PLATFORM_TARGET_is_android) {
                $crate::Platform::Android
            } else if cfg!(NOVA_BUILD_PLATFORM_TARGET_is_linux) {
                $crate::Platform::Linux
            } else {
                panic!("Unsupported Platform")
            }
        }

        ///
        /// Returns the target architecture
        ///
        #[inline]
        pub fn target_architecture() -> $crate::Architecture {
            if cfg!(NOVA_BUILD_ARCH_TARGET_is_x86_64) {
                $crate::Architecture::X8664
            } else if cfg!(NOVA_BUILD_ARCH_TARGET_is_aarch64) {
                $crate::Architecture::AARCH64
            } else {
                panic!("Unsupported Architecture")
            }
        }

        ///
        /// Returns the target build type
        ///
        #[inline]
        pub fn target_build_type() -> $crate::BuildType {
            if cfg!(NOVA_BUILD_PROFILE_TARGET_is_release) {
                $crate::BuildType::Release
            } else if cfg!(NOVA_BUILD_PROFILE_TARGET_is_debug) {
                $crate::BuildType::Debug
            } else {
                panic!("Unsupported Build Profile")
            }
        }

        ///
        /// Returns whether we are compiling for an x86 platform. Useful for guarding use of x86 intrinsics
        ///
        #[cfg(any(TARGET_is_x86_64, TARGET_is_i686))]
        const fn internal_target_is_x86() -> bool {
            true
        }

        ///
        /// Returns whether we are compiling for an x86 platform. Useful for guarding use of x86 intrinsics
        ///
        #[cfg(not(any(TARGET_is_x86_64, TARGET_is_i686)))]
        const fn internal_target_is_x86() -> bool {
            false
        }

        ///
        /// Returns whether we are compiling for an x86 platform. Useful for guarding use of x86 intrinsics
        ///
        pub const fn target_is_x86() -> bool {
            internal_target_is_x86()
        }
    };
}

///
/// This macro expands into code for detecting the host platform (the host is the system that
/// compiled the library which can differ from the target when cross compiling) inside a library
/// crate.
///
/// Expand this macro somewhere in the crate using this library like the example provided below.
///
/// This will provide a set of functions will be defined for getting the current `host_platform`,
/// `host_architecture` and `host_build_type`. These functions will be defined in the module
/// that the macro is called in.
///
/// ```
/// nova_target::host_functions!();
/// ```
///
/// The macro must be used to provide these functions as the code must be compiled inside the crate
/// using these functions as the library wont have access to the cfg values used to detect the
/// platform. These can't be compiled in this library crate as the library would have to depend on
/// itself as a build dependency.
///
#[macro_export]
macro_rules! host_functions {
    () => {
        ///
        /// Returns the host platform
        ///
        #[inline]
        pub fn host_platform() -> $crate::Platform {
            if cfg!(NOVA_BUILD_PLATFORM_HOST_is_windows) {
                if cfg!(NOVA_BUILD_PLATFORM_HOST_is_msvc) {
                    $crate::Platform::WindowsMSVC
                } else if cfg!(NOVA_BUILD_PLATFORM_HOST_is_gnu) {
                    $crate::Platform::WindowsGNU
                } else {
                    panic!("Unsupported Platform")
                }
            } else if cfg!(NOVA_BUILD_PLATFORM_HOST_is_android) {
                $crate::Platform::Android
            } else if cfg!(NOVA_BUILD_PLATFORM_HOST_is_linux) {
                $crate::Platform::Linux
            } else {
                panic!("Unsupported Platform")
            }
        }

        ///
        /// Returns the host architecture
        ///
        #[inline]
        pub fn host_architecture() -> $crate::Architecture {
            if cfg!(NOVA_BUILD_ARCH_HOST_is_x86_64) {
                $crate::Architecture::X8664
            } else if cfg!(NOVA_BUILD_ARCH_HOST_is_aarch64) {
                $crate::Architecture::AARCH64
            } else {
                panic!("Unsupported Architecture")
            }
        }

        ///
        /// Returns the host build type
        ///
        #[inline]
        pub fn host_build_type() -> $crate::BuildType {
            if cfg!(NOVA_BUILD_PROFILE_HOST_is_release) {
                $crate::BuildType::Release
            } else if cfg!(NOVA_BUILD_PROFILE_HOST_is_debug) {
                $crate::BuildType::Debug
            } else {
                panic!("Unsupported Build Profile")
            }
        }

        ///
        /// Returns whether we are compiling for an x86 platform. Useful for guarding use of x86 intrinsics
        ///
        #[cfg(any(HOST_is_x86_64, HOST_is_i686))]
        const fn internal_host_is_x86() -> bool {
            true
        }

        ///
        /// Returns whether we are compiling for an x86 platform. Useful for guarding use of x86 intrinsics
        ///
        #[cfg(not(any(HOST_is_x86_64, HOST_is_i686)))]
        const fn internal_host_is_x86() -> bool {
            false
        }

        ///
        /// Returns whether we are compiling for an x86 platform. Useful for guarding use of x86 intrinsics
        ///
        pub const fn host_is_x86() -> bool {
            internal_host_is_x86()
        }
    };
}
