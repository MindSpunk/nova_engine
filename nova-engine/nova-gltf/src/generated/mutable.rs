/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use serde::{Deserialize, Serialize};
use std::collections::HashMap;

/// The root object for a glTF asset.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Gltf {
    /// An array of accessors.
    #[serde(rename = "accessors")]
    accessors: Option<Vec<Accessor>>,

    /// An array of keyframe animations.
    #[serde(rename = "animations")]
    animations: Option<Vec<Animation>>,

    /// Metadata about the glTF asset.
    #[serde(rename = "asset")]
    asset: Asset,

    /// An array of buffers.
    #[serde(rename = "buffers")]
    buffers: Option<Vec<Buffer>>,

    /// An array of bufferViews.
    #[serde(rename = "bufferViews")]
    buffer_views: Option<Vec<BufferView>>,

    /// An array of cameras.
    #[serde(rename = "cameras")]
    cameras: Option<Vec<Camera>>,

    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    /// Names of glTF extensions required to properly load this asset.
    #[serde(rename = "extensionsRequired")]
    extensions_required: Option<Vec<String>>,

    /// Names of glTF extensions used somewhere in this asset.
    #[serde(rename = "extensionsUsed")]
    extensions_used: Option<Vec<String>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// An array of images.
    #[serde(rename = "images")]
    images: Option<Vec<Image>>,

    /// An array of materials.
    #[serde(rename = "materials")]
    materials: Option<Vec<Material>>,

    /// An array of meshes.
    #[serde(rename = "meshes")]
    meshes: Option<Vec<Mesh>>,

    /// An array of nodes.
    #[serde(rename = "nodes")]
    nodes: Option<Vec<Node>>,

    /// An array of samplers.
    #[serde(rename = "samplers")]
    samplers: Option<Vec<Sampler>>,

    /// The index of the default scene.
    #[serde(rename = "scene")]
    scene: Option<i32>,

    /// An array of scenes.
    #[serde(rename = "scenes")]
    scenes: Option<Vec<Scene>>,

    /// An array of skins.
    #[serde(rename = "skins")]
    skins: Option<Vec<Skin>>,

    /// An array of textures.
    #[serde(rename = "textures")]
    textures: Option<Vec<Texture>>,
}

/// A typed view into a bufferView.  A bufferView contains raw binary data.  An accessor
/// provides a typed view into a bufferView or a subset of a bufferView similar to how
/// WebGL's `vertexAttribPointer()` defines an attribute in a buffer.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Accessor {
    /// The index of the bufferView.
    #[serde(rename = "bufferView")]
    buffer_view: Option<i32>,

    /// The offset relative to the start of the bufferView in bytes.
    #[serde(rename = "byteOffset")]
    byte_offset: Option<i32>,

    /// The datatype of components in the attribute.
    #[serde(rename = "componentType")]
    component_type: i32,

    /// The number of attributes referenced by this accessor.
    #[serde(rename = "count")]
    count: i32,

    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// Maximum value of each component in this attribute.
    #[serde(rename = "max")]
    max: Option<Vec<f32>>,

    /// Minimum value of each component in this attribute.
    #[serde(rename = "min")]
    min: Option<Vec<f32>>,

    /// The user-defined name of this object.
    #[serde(rename = "name")]
    name: Option<String>,

    /// Specifies whether integer data values should be normalized.
    #[serde(rename = "normalized")]
    normalized: Option<bool>,

    /// Sparse storage of attributes that deviate from their initialization value.
    #[serde(rename = "sparse")]
    sparse: Option<AccessorSparse>,

    /// Specifies if the attribute is a scalar, vector, or matrix.
    #[serde(rename = "type")]
    accessor_type: String,
}

/// Sparse storage of attributes that deviate from their initialization value.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct AccessorSparse {
    /// Number of entries stored in the sparse array.
    #[serde(rename = "count")]
    count: i32,

    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// Index array of size `count` that points to those accessor attributes that deviate from
    /// their initialization value. Indices must strictly increase.
    #[serde(rename = "indices")]
    indices: AccessorSparseIndices,

    /// Array of size `count` times number of components, storing the displaced accessor
    /// attributes pointed by `indices`. Substituted values must have the same `componentType`
    /// and number of components as the base accessor.
    #[serde(rename = "values")]
    values: AccessorSparseValues,
}

/// Index array of size `count` that points to those accessor attributes that deviate from
/// their initialization value. Indices must strictly increase.
///
/// Indices of those attributes that deviate from their initialization value.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct AccessorSparseIndices {
    /// The index of the bufferView with sparse indices. Referenced bufferView can't have
    /// ARRAY_BUFFER or ELEMENT_ARRAY_BUFFER target.
    #[serde(rename = "bufferView")]
    buffer_view: i32,

    /// The offset relative to the start of the bufferView in bytes. Must be aligned.
    #[serde(rename = "byteOffset")]
    byte_offset: Option<i32>,

    /// The indices data type.
    #[serde(rename = "componentType")]
    component_type: i32,

    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,
}

/// Array of size `count` times number of components, storing the displaced accessor
/// attributes pointed by `indices`. Substituted values must have the same `componentType`
/// and number of components as the base accessor.
///
/// Array of size `accessor.sparse.count` times number of components storing the displaced
/// accessor attributes pointed by `accessor.sparse.indices`.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct AccessorSparseValues {
    /// The index of the bufferView with sparse values. Referenced bufferView can't have
    /// ARRAY_BUFFER or ELEMENT_ARRAY_BUFFER target.
    #[serde(rename = "bufferView")]
    buffer_view: i32,

    /// The offset relative to the start of the bufferView in bytes. Must be aligned.
    #[serde(rename = "byteOffset")]
    byte_offset: Option<i32>,

    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,
}

/// A keyframe animation.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Animation {
    /// An array of channels, each of which targets an animation's sampler at a node's property.
    /// Different channels of the same animation can't have equal targets.
    #[serde(rename = "channels")]
    channels: Vec<AnimationChannel>,

    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// The user-defined name of this object.
    #[serde(rename = "name")]
    name: Option<String>,

    /// An array of samplers that combines input and output accessors with an interpolation
    /// algorithm to define a keyframe graph (but not its target).
    #[serde(rename = "samplers")]
    samplers: Vec<AnimationSampler>,
}

/// Targets an animation's sampler at a node's property.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct AnimationChannel {
    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// The index of a sampler in this animation used to compute the value for the target.
    #[serde(rename = "sampler")]
    sampler: i32,

    /// The index of the node and TRS property to target.
    #[serde(rename = "target")]
    target: AnimationChannelTarget,
}

/// The index of the node and TRS property to target.
///
/// The index of the node and TRS property that an animation channel targets.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct AnimationChannelTarget {
    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// The index of the node to target.
    #[serde(rename = "node")]
    node: Option<i32>,

    /// The name of the node's TRS property to modify, or the "weights" of the Morph Targets it
    /// instantiates. For the "translation" property, the values that are provided by the sampler
    /// are the translation along the x, y, and z axes. For the "rotation" property, the values
    /// are a quaternion in the order (x, y, z, w), where w is the scalar. For the "scale"
    /// property, the values are the scaling factors along the x, y, and z axes.
    #[serde(rename = "path")]
    path: String,
}

/// Combines input and output accessors with an interpolation algorithm to define a keyframe
/// graph (but not its target).
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct AnimationSampler {
    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// The index of an accessor containing keyframe input values, e.g., time.
    #[serde(rename = "input")]
    input: i32,

    /// Interpolation algorithm.
    #[serde(rename = "interpolation")]
    interpolation: Option<String>,

    /// The index of an accessor, containing keyframe output values.
    #[serde(rename = "output")]
    output: i32,
}

/// Metadata about the glTF asset.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Asset {
    /// A copyright message suitable for display to credit the content creator.
    #[serde(rename = "copyright")]
    copyright: Option<String>,

    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// Tool that generated this glTF model.  Useful for debugging.
    #[serde(rename = "generator")]
    generator: Option<String>,

    /// The minimum glTF version that this asset targets.
    #[serde(rename = "minVersion")]
    min_version: Option<String>,

    /// The glTF version that this asset targets.
    #[serde(rename = "version")]
    version: String,
}

/// A view into a buffer generally representing a subset of the buffer.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct BufferView {
    /// The index of the buffer.
    #[serde(rename = "buffer")]
    buffer: i32,

    /// The total byte length of the buffer view.
    #[serde(rename = "byteLength")]
    byte_length: i32,

    /// The offset into the buffer in bytes.
    #[serde(rename = "byteOffset")]
    byte_offset: Option<i32>,

    /// The stride, in bytes.
    #[serde(rename = "byteStride")]
    byte_stride: Option<i32>,

    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// The user-defined name of this object.
    #[serde(rename = "name")]
    name: Option<String>,

    /// The target that the GPU buffer should be bound to.
    #[serde(rename = "target")]
    target: Option<i32>,
}

/// A buffer points to binary geometry, animation, or skins.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Buffer {
    /// The length of the buffer in bytes.
    #[serde(rename = "byteLength")]
    byte_length: i32,

    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// The user-defined name of this object.
    #[serde(rename = "name")]
    name: Option<String>,

    /// The uri of the buffer.
    #[serde(rename = "uri")]
    uri: Option<String>,
}

/// A camera's projection.  A node can reference a camera to apply a transform to place the
/// camera in the scene.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Camera {
    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// The user-defined name of this object.
    #[serde(rename = "name")]
    name: Option<String>,

    /// An orthographic camera containing properties to create an orthographic projection matrix.
    #[serde(rename = "orthographic")]
    orthographic: Option<CameraOrthographic>,

    /// A perspective camera containing properties to create a perspective projection matrix.
    #[serde(rename = "perspective")]
    perspective: Option<CameraPerspective>,

    /// Specifies if the camera uses a perspective or orthographic projection.
    #[serde(rename = "type")]
    camera_type: String,
}

/// An orthographic camera containing properties to create an orthographic projection matrix.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct CameraOrthographic {
    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// The floating-point horizontal magnification of the view. Must not be zero.
    #[serde(rename = "xmag")]
    xmag: f32,

    /// The floating-point vertical magnification of the view. Must not be zero.
    #[serde(rename = "ymag")]
    ymag: f32,

    /// The floating-point distance to the far clipping plane. `zfar` must be greater than
    /// `znear`.
    #[serde(rename = "zfar")]
    zfar: f32,

    /// The floating-point distance to the near clipping plane.
    #[serde(rename = "znear")]
    znear: f32,
}

/// A perspective camera containing properties to create a perspective projection matrix.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct CameraPerspective {
    /// The floating-point aspect ratio of the field of view.
    #[serde(rename = "aspectRatio")]
    aspect_ratio: Option<f32>,

    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// The floating-point vertical field of view in radians.
    #[serde(rename = "yfov")]
    yfov: f32,

    /// The floating-point distance to the far clipping plane.
    #[serde(rename = "zfar")]
    zfar: Option<f32>,

    /// The floating-point distance to the near clipping plane.
    #[serde(rename = "znear")]
    znear: f32,
}

/// Image data used to create a texture. Image can be referenced by URI or `bufferView`
/// index. `mimeType` is required in the latter case.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Image {
    /// The index of the bufferView that contains the image. Use this instead of the image's uri
    /// property.
    #[serde(rename = "bufferView")]
    buffer_view: Option<i32>,

    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// The image's MIME type. Required if `bufferView` is defined.
    #[serde(rename = "mimeType")]
    mime_type: Option<String>,

    /// The user-defined name of this object.
    #[serde(rename = "name")]
    name: Option<String>,

    /// The uri of the image.
    #[serde(rename = "uri")]
    uri: Option<String>,
}

/// The material appearance of a primitive.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Material {
    /// The alpha cutoff value of the material.
    #[serde(rename = "alphaCutoff")]
    alpha_cutoff: Option<f32>,

    /// The alpha rendering mode of the material.
    #[serde(rename = "alphaMode")]
    alpha_mode: Option<String>,

    /// Specifies whether the material is double sided.
    #[serde(rename = "doubleSided")]
    double_sided: Option<bool>,

    /// The emissive color of the material.
    #[serde(rename = "emissiveFactor")]
    emissive_factor: Option<Vec<f32>>,

    /// The emissive map texture.
    #[serde(rename = "emissiveTexture")]
    emissive_texture: Option<TextureInfo>,

    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// The user-defined name of this object.
    #[serde(rename = "name")]
    name: Option<String>,

    /// The normal map texture.
    #[serde(rename = "normalTexture")]
    normal_texture: Option<MaterialNormalTextureInfoClass>,

    /// The occlusion map texture.
    #[serde(rename = "occlusionTexture")]
    occlusion_texture: Option<MaterialOcclusionTextureInfoClass>,

    /// A set of parameter values that are used to define the metallic-roughness material model
    /// from Physically-Based Rendering (PBR) methodology. When not specified, all the default
    /// values of `pbrMetallicRoughness` apply.
    #[serde(rename = "pbrMetallicRoughness")]
    pbr_metallic_roughness: Option<MaterialPbrMetallicRoughness>,
}

/// The emissive map texture.
///
/// The base color texture.
///
/// The metallic-roughness texture.
///
/// Reference to a texture.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct TextureInfo {
    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// The index of the texture.
    #[serde(rename = "index")]
    index: i32,

    /// The set index of texture's TEXCOORD attribute used for texture coordinate mapping.
    #[serde(rename = "texCoord")]
    tex_coord: Option<i32>,
}

/// The normal map texture.
///
/// Reference to a texture.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct MaterialNormalTextureInfoClass {
    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// The index of the texture.
    #[serde(rename = "index")]
    index: i32,

    /// The scalar multiplier applied to each normal vector of the normal texture.
    #[serde(rename = "scale")]
    scale: Option<f32>,

    /// The set index of texture's TEXCOORD attribute used for texture coordinate mapping.
    #[serde(rename = "texCoord")]
    tex_coord: Option<i32>,
}

/// The occlusion map texture.
///
/// Reference to a texture.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct MaterialOcclusionTextureInfoClass {
    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// The index of the texture.
    #[serde(rename = "index")]
    index: i32,

    /// A scalar multiplier controlling the amount of occlusion applied.
    #[serde(rename = "strength")]
    strength: Option<f32>,

    /// The set index of texture's TEXCOORD attribute used for texture coordinate mapping.
    #[serde(rename = "texCoord")]
    tex_coord: Option<i32>,
}

/// A set of parameter values that are used to define the metallic-roughness material model
/// from Physically-Based Rendering (PBR) methodology. When not specified, all the default
/// values of `pbrMetallicRoughness` apply.
///
/// A set of parameter values that are used to define the metallic-roughness material model
/// from Physically-Based Rendering (PBR) methodology.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct MaterialPbrMetallicRoughness {
    /// The material's base color factor.
    #[serde(rename = "baseColorFactor")]
    base_color_factor: Option<Vec<f32>>,

    /// The base color texture.
    #[serde(rename = "baseColorTexture")]
    base_color_texture: Option<TextureInfo>,

    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// The metalness of the material.
    #[serde(rename = "metallicFactor")]
    metallic_factor: Option<f32>,

    /// The metallic-roughness texture.
    #[serde(rename = "metallicRoughnessTexture")]
    metallic_roughness_texture: Option<TextureInfo>,

    /// The roughness of the material.
    #[serde(rename = "roughnessFactor")]
    roughness_factor: Option<f32>,
}

/// A set of primitives to be rendered.  A node can contain one mesh.  A node's transform
/// places the mesh in the scene.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Mesh {
    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// The user-defined name of this object.
    #[serde(rename = "name")]
    name: Option<String>,

    /// An array of primitives, each defining geometry to be rendered with a material.
    #[serde(rename = "primitives")]
    primitives: Vec<MeshPrimitive>,

    /// Array of weights to be applied to the Morph Targets.
    #[serde(rename = "weights")]
    weights: Option<Vec<f32>>,
}

/// Geometry to be rendered with the given material.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct MeshPrimitive {
    /// A dictionary object, where each key corresponds to mesh attribute semantic and each value
    /// is the index of the accessor containing attribute's data.
    #[serde(rename = "attributes")]
    attributes: HashMap<String, i32>,

    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// The index of the accessor that contains the indices.
    #[serde(rename = "indices")]
    indices: Option<i32>,

    /// The index of the material to apply to this primitive when rendering.
    #[serde(rename = "material")]
    material: Option<i32>,

    /// The type of primitives to render.
    #[serde(rename = "mode")]
    mode: Option<i32>,

    /// An array of Morph Targets, each  Morph Target is a dictionary mapping attributes (only
    /// `POSITION`, `NORMAL`, and `TANGENT` supported) to their deviations in the Morph Target.
    #[serde(rename = "targets")]
    targets: Option<Vec<HashMap<String, i32>>>,
}

/// A node in the node hierarchy.  When the node contains `skin`, all `mesh.primitives` must
/// contain `JOINTS_0` and `WEIGHTS_0` attributes.  A node can have either a `matrix` or any
/// combination of `translation`/`rotation`/`scale` (TRS) properties. TRS properties are
/// converted to matrices and postmultiplied in the `T * R * S` order to compose the
/// transformation matrix; first the scale is applied to the vertices, then the rotation, and
/// then the translation. If none are provided, the transform is the identity. When a node is
/// targeted for animation (referenced by an animation.channel.target), only TRS properties
/// may be present; `matrix` will not be present.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Node {
    /// The index of the camera referenced by this node.
    #[serde(rename = "camera")]
    camera: Option<i32>,

    /// The indices of this node's children.
    #[serde(rename = "children")]
    children: Option<Vec<i32>>,

    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// A floating-point 4x4 transformation matrix stored in column-major order.
    #[serde(rename = "matrix")]
    matrix: Option<Vec<f32>>,

    /// The index of the mesh in this node.
    #[serde(rename = "mesh")]
    mesh: Option<i32>,

    /// The user-defined name of this object.
    #[serde(rename = "name")]
    name: Option<String>,

    /// The node's unit quaternion rotation in the order (x, y, z, w), where w is the scalar.
    #[serde(rename = "rotation")]
    rotation: Option<Vec<f32>>,

    /// The node's non-uniform scale, given as the scaling factors along the x, y, and z axes.
    #[serde(rename = "scale")]
    scale: Option<Vec<f32>>,

    /// The index of the skin referenced by this node.
    #[serde(rename = "skin")]
    skin: Option<i32>,

    /// The node's translation along the x, y, and z axes.
    #[serde(rename = "translation")]
    translation: Option<Vec<f32>>,

    /// The weights of the instantiated Morph Target. Number of elements must match number of
    /// Morph Targets of used mesh.
    #[serde(rename = "weights")]
    weights: Option<Vec<f32>>,
}

/// Texture sampler properties for filtering and wrapping modes.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Sampler {
    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// Magnification filter.
    #[serde(rename = "magFilter")]
    mag_filter: Option<i32>,

    /// Minification filter.
    #[serde(rename = "minFilter")]
    min_filter: Option<i32>,

    /// The user-defined name of this object.
    #[serde(rename = "name")]
    name: Option<String>,

    /// s wrapping mode.
    #[serde(rename = "wrapS")]
    wrap_s: Option<i32>,

    /// t wrapping mode.
    #[serde(rename = "wrapT")]
    wrap_t: Option<i32>,
}

/// The root nodes of a scene.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Scene {
    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// The user-defined name of this object.
    #[serde(rename = "name")]
    name: Option<String>,

    /// The indices of each root node.
    #[serde(rename = "nodes")]
    nodes: Option<Vec<i32>>,
}

/// Joints and matrices defining a skin.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Skin {
    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// The index of the accessor containing the floating-point 4x4 inverse-bind matrices.  The
    /// default is that each matrix is a 4x4 identity matrix, which implies that inverse-bind
    /// matrices were pre-applied.
    #[serde(rename = "inverseBindMatrices")]
    inverse_bind_matrices: Option<i32>,

    /// Indices of skeleton nodes, used as joints in this skin.
    #[serde(rename = "joints")]
    joints: Vec<i32>,

    /// The user-defined name of this object.
    #[serde(rename = "name")]
    name: Option<String>,

    /// The index of the node used as a skeleton root.
    #[serde(rename = "skeleton")]
    skeleton: Option<i32>,
}

/// A texture and its sampler.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Texture {
    #[serde(rename = "extensions")]
    extensions: Option<HashMap<String, serde_json::Value>>,

    #[serde(rename = "extras")]
    extras: Option<serde_json::Value>,

    /// The user-defined name of this object.
    #[serde(rename = "name")]
    name: Option<String>,

    /// The index of the sampler used by this texture. When undefined, a sampler with repeat
    /// wrapping and auto filtering should be used.
    #[serde(rename = "sampler")]
    sampler: Option<i32>,

    /// The index of the image used by this texture. When undefined, it is expected that an
    /// extension or other mechanism will supply an alternate texture source, otherwise behavior
    /// is undefined.
    #[serde(rename = "source")]
    source: Option<i32>,
}
