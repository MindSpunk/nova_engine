/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use serde_json;

const FILE_TEST_STR: &'static str = r#"
{
  "accessors": [
    {
      "bufferView": 0,
      "componentType": 5126,
      "count": 11505,
      "type": "VEC2"
    },
    {
      "bufferView": 1,
      "componentType": 5126,
      "count": 11505,
      "type": "VEC3"
    },
    {
      "bufferView": 2,
      "componentType": 5126,
      "count": 11505,
      "type": "VEC4"
    },
    {
      "bufferView": 3,
      "componentType": 5126,
      "count": 11505,
      "type": "VEC3",
      "max": [
        0.019474864,
        0.05784153,
        0.019474864
      ],
      "min": [
        -0.0194748752,
        0.0,
        -0.0194748677
      ]
    },
    {
      "bufferView": 4,
      "componentType": 5123,
      "count": 54972,
      "type": "SCALAR"
    }
  ],
  "asset": {
    "generator": "glTF Tools for Unity",
    "version": "2.0"
  },
  "bufferViews": [
    {
      "buffer": 0,
      "byteLength": 92040
    },
    {
      "buffer": 0,
      "byteOffset": 92040,
      "byteLength": 138060
    },
    {
      "buffer": 0,
      "byteOffset": 230100,
      "byteLength": 184080
    },
    {
      "buffer": 0,
      "byteOffset": 414180,
      "byteLength": 138060
    },
    {
      "buffer": 0,
      "byteOffset": 552240,
      "byteLength": 109944
    }
  ],
  "buffers": [
    {
      "uri": "Corset.bin",
      "byteLength": 662184
    }
  ],
  "images": [
    {
      "uri": "Corset_baseColor.png"
    },
    {
      "uri": "Corset_occlusionRoughnessMetallic.png"
    },
    {
      "uri": "Corset_normal.png"
    }
  ],
  "meshes": [
    {
      "primitives": [
        {
          "attributes": {
            "TEXCOORD_0": 0,
            "NORMAL": 1,
            "TANGENT": 2,
            "POSITION": 3
          },
          "indices": 4,
          "material": 0
        }
      ],
      "name": "pCube49"
    }
  ],
  "materials": [
    {
      "pbrMetallicRoughness": {
        "baseColorTexture": {
          "index": 0
        },
        "metallicRoughnessTexture": {
          "index": 1
        }
      },
      "normalTexture": {
        "index": 2
      },
      "occlusionTexture": {
        "index": 1
      },
      "name": "Corset_O"
    }
  ],
  "nodes": [
    {
      "mesh": 0,
      "rotation": [
        0.0,
        1.0,
        0.0,
        0.0
      ],
      "name": "Corset"
    }
  ],
  "scene": 0,
  "scenes": [
    {
      "nodes": [
        0
      ]
    }
  ],
  "textures": [
    {
      "source": 0
    },
    {
      "source": 1
    },
    {
      "source": 2
    }
  ]
}
"#;

#[test]
pub fn immutable() {
    use crate::generated::immutable::Gltf;

    let value: Gltf = serde_json::from_str(FILE_TEST_STR).unwrap();
    println!("Size {}", core::mem::size_of::<Gltf>());
    println!("{:#?}", value);
}

#[test]
pub fn mutable() {
    use crate::generated::mutable::Gltf;

    let value: Gltf = serde_json::from_str(FILE_TEST_STR).unwrap();
    println!("Size {}", core::mem::size_of::<Gltf>());
    println!("{:#?}", value);
}
