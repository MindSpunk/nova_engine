/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use raw::FMOD_2D;
use raw::FMOD_3D;
use raw::FMOD_3D_CUSTOMROLLOFF;
use raw::FMOD_3D_HEADRELATIVE;
use raw::FMOD_3D_IGNOREGEOMETRY;
use raw::FMOD_3D_INVERSEROLLOFF;
use raw::FMOD_3D_INVERSETAPEREDROLLOFF;
use raw::FMOD_3D_LINEARROLLOFF;
use raw::FMOD_3D_LINEARSQUAREROLLOFF;
use raw::FMOD_3D_WORLDRELATIVE;
use raw::FMOD_ACCURATETIME;
use raw::FMOD_CREATECOMPRESSEDSAMPLE;
use raw::FMOD_CREATESAMPLE;
use raw::FMOD_CREATESTREAM;
use raw::FMOD_IGNORETAGS;
use raw::FMOD_LOOP_BIDI;
use raw::FMOD_LOOP_NORMAL;
use raw::FMOD_LOOP_OFF;
use raw::FMOD_LOWMEM;
use raw::FMOD_MODE;
use raw::FMOD_MPEGSEARCH;
use raw::FMOD_NONBLOCKING;
use raw::FMOD_OPENMEMORY;
use raw::FMOD_OPENMEMORY_POINT;
use raw::FMOD_OPENONLY;
use raw::FMOD_OPENRAW;
use raw::FMOD_OPENUSER;
use raw::FMOD_UNIQUE;
use raw::FMOD_VIRTUAL_PLAYFROMSTART;

bitflags! {
    ///
    /// A wrapper for the FMOD_MODE bit flags type
    ///
    pub struct Mode: FMOD_MODE {
        const LOOP_OFF = FMOD_LOOP_OFF;
        const LOOP_NORMAL = FMOD_LOOP_NORMAL;
        const LOOP_BIDI = FMOD_LOOP_BIDI;
        const TWO_D = FMOD_2D;
        const THREE_D = FMOD_3D;
        const CREATESTREAM = FMOD_CREATESTREAM;
        const CREATESAMPLE = FMOD_CREATESAMPLE;
        const CREATECOMPRESSEDSAMPLE = FMOD_CREATECOMPRESSEDSAMPLE;
        const OPENUSER = FMOD_OPENUSER;
        const OPENMEMORY = FMOD_OPENMEMORY;
        const OPENMEMORY_POINT = FMOD_OPENMEMORY_POINT;
        const OPENRAW = FMOD_OPENRAW;
        const OPENONLY = FMOD_OPENONLY;
        const ACCURATETIME = FMOD_ACCURATETIME;
        const MPEGSEARCH = FMOD_MPEGSEARCH;
        const NONBLOCKING = FMOD_NONBLOCKING;
        const UNIQUE = FMOD_UNIQUE;
        const THREE_D_HEADRELATIVE = FMOD_3D_HEADRELATIVE;
        const THREE_D_WORLDRELATIVE = FMOD_3D_WORLDRELATIVE;
        const THREE_D_INVERSEROLLOFF = FMOD_3D_INVERSEROLLOFF;
        const THREE_D_LINEARROLLOFF = FMOD_3D_LINEARROLLOFF;
        const THREE_D_LINEARSQUAREROLLOFF = FMOD_3D_LINEARSQUAREROLLOFF;
        const THREE_D_INVERSETAPEREDROLLOFF = FMOD_3D_INVERSETAPEREDROLLOFF;
        const THREE_D_CUSTOMROLLOFF = FMOD_3D_CUSTOMROLLOFF;
        const THREE_D_IGNOREGEOMETRY = FMOD_3D_IGNOREGEOMETRY;
        const IGNORETAGS = FMOD_IGNORETAGS;
        const LOWMEM = FMOD_LOWMEM;
        const VIRTUAL_PLAYFROMSTART = FMOD_VIRTUAL_PLAYFROMSTART;
    }
}
