/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::core::Vector3;

///
/// FMOD_3D_ATTRIBUTES
///
#[repr(C)]
#[derive(Debug, Clone)]
pub struct Attributes3D {
    pub position: Vector3,
    pub velocity: Vector3,
    pub forward: Vector3,
    pub up: Vector3,
}

impl Attributes3D {
    pub fn from_ffi(ffi: raw::FMOD_3D_ATTRIBUTES) -> Self {
        Self {
            position: Vector3::from_ffi(ffi.position),
            velocity: Vector3::from_ffi(ffi.velocity),
            forward: Vector3::from_ffi(ffi.forward),
            up: Vector3::from_ffi(ffi.up),
        }
    }

    pub fn into_ffi(self) -> raw::FMOD_3D_ATTRIBUTES {
        raw::FMOD_3D_ATTRIBUTES {
            position: self.position.into_ffi(),
            velocity: self.velocity.into_ffi(),
            forward: self.forward.into_ffi(),
            up: self.up.into_ffi(),
        }
    }
}
