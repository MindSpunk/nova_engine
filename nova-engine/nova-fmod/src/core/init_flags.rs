/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use raw::FMOD_INITFLAGS;
use raw::FMOD_INIT_3D_RIGHTHANDED;
use raw::FMOD_INIT_CHANNEL_DISTANCEFILTER;
use raw::FMOD_INIT_CHANNEL_LOWPASS;
use raw::FMOD_INIT_GEOMETRY_USECLOSEST;
use raw::FMOD_INIT_MEMORY_TRACKING;
use raw::FMOD_INIT_MIX_FROM_UPDATE;
use raw::FMOD_INIT_NORMAL;
use raw::FMOD_INIT_PREFER_DOLBY_DOWNMIX;
use raw::FMOD_INIT_PROFILE_ENABLE;
use raw::FMOD_INIT_PROFILE_METER_ALL;
use raw::FMOD_INIT_STREAM_FROM_UPDATE;
use raw::FMOD_INIT_THREAD_UNSAFE;
use raw::FMOD_INIT_VOL0_BECOMES_VIRTUAL;

bitflags! {
    ///
    /// A wrapper for the FMOD_INITFLAGS bit flags type
    ///
    pub struct InitFlags: FMOD_INITFLAGS {
        const NORMAL = FMOD_INIT_NORMAL;
        const STREAM_FROM_UPDATE = FMOD_INIT_STREAM_FROM_UPDATE;
        const MIX_FROM_UPDATE = FMOD_INIT_MIX_FROM_UPDATE;
        const THREE_D_RIGHTHANDED = FMOD_INIT_3D_RIGHTHANDED;
        const CHANNEL_LOWPASS = FMOD_INIT_CHANNEL_LOWPASS;
        const CHANNEL_DISTANCEFILTER = FMOD_INIT_CHANNEL_DISTANCEFILTER;
        const PROFILE_ENABLE = FMOD_INIT_PROFILE_ENABLE;
        const VOL0_BECOMES_VIRTUAL = FMOD_INIT_VOL0_BECOMES_VIRTUAL;
        const GEOMETRY_USECLOSEST = FMOD_INIT_GEOMETRY_USECLOSEST;
        const PREFER_DOLBY_DOWNMIX = FMOD_INIT_PREFER_DOLBY_DOWNMIX;
        const THREAD_UNSAFE = FMOD_INIT_THREAD_UNSAFE;
        const PROFILE_METER_ALL = FMOD_INIT_PROFILE_METER_ALL;
        const MEMORY_TRACKING = FMOD_INIT_MEMORY_TRACKING;
    }
}
