/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::core::make_result;
use crate::core::Result;

///
/// FMOD_SYSTEM
///
#[derive(Copy, Clone)]
pub struct System {
    pub(crate) system: *mut raw::FMOD_SYSTEM,
}

impl System {
    ///
    /// Gets a null handle
    ///
    pub fn null() -> Self {
        Self {
            system: std::ptr::null_mut(),
        }
    }

    ///
    /// Creates a handle from the ffi type
    ///
    pub fn from_ffi(ffi: *mut raw::FMOD_SYSTEM) -> Self {
        Self { system: ffi }
    }

    ///
    /// FMOD_System_Release
    ///
    pub fn release(&self) -> Result<()> {
        use raw::FMOD_System_Release;
        unsafe {
            let err = FMOD_System_Release(self.system);
            make_result((), err)
        }
    }
}
