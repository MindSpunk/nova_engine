/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::core::make_result;
use crate::core::Guid;
use crate::core::Result;
use std::ffi::CString;
use std::mem::MaybeUninit;

///
/// FMOD_STUDIO_VCA
///
#[derive(Copy, Clone)]
pub struct VCA {
    pub(crate) vca: *mut raw::FMOD_STUDIO_VCA,
}

impl VCA {
    ///
    /// Gets a null handle
    ///
    pub fn null() -> Self {
        Self {
            vca: std::ptr::null_mut(),
        }
    }

    ///
    /// Creates a handle from the ffi type
    ///
    pub fn from_ffi(ffi: *mut raw::FMOD_STUDIO_VCA) -> Self {
        Self { vca: ffi }
    }

    ///
    /// FMOD_Studio_VCA_IsValid
    ///
    pub fn is_valid(&self) -> bool {
        use raw::FMOD_Studio_VCA_IsValid;
        unsafe {
            let val = FMOD_Studio_VCA_IsValid(self.vca);
            val != 0
        }
    }

    ///
    /// FMOD_Studio_VCA_GetID
    ///
    pub fn get_id(&self) -> Result<Guid> {
        use raw::FMOD_Studio_VCA_GetID;
        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_VCA_GetID(self.vca, val.as_mut_ptr());
            make_result(val, err).map(|val| Guid::from_ffi(val.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_VCA_GetPath
    ///
    pub fn get_path(&self) -> Result<CString> {
        use raw::FMOD_Studio_VCA_GetPath;

        unsafe {
            let mut retrieved = 0;
            let err = FMOD_Studio_VCA_GetPath(self.vca, std::ptr::null_mut(), 0, &mut retrieved);
            make_result((), err)?;
            let mut out = Vec::<u8>::with_capacity(retrieved as usize);
            out.resize(retrieved as usize, 0);
            let err = FMOD_Studio_VCA_GetPath(
                self.vca,
                out.as_mut_ptr() as *mut _,
                out.len() as _,
                std::ptr::null_mut(),
            );
            make_result(CString::from_vec_unchecked(out), err)
        }
    }

    ///
    /// FMOD_Studio_VCA_GetVolume
    ///
    pub fn get_volume(&self) -> Result<(f32, f32)> {
        use raw::FMOD_Studio_VCA_GetVolume;

        unsafe {
            let mut val1 = MaybeUninit::uninit();
            let mut val2 = MaybeUninit::uninit();
            let err = FMOD_Studio_VCA_GetVolume(self.vca, val1.as_mut_ptr(), val2.as_mut_ptr());
            make_result((val1, val2), err)
                .map(|(val1, val2)| (val1.assume_init(), val2.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_VCA_SetVolume
    ///
    pub fn set_volume(&self, volume: f32) -> Result<()> {
        use raw::FMOD_Studio_VCA_SetVolume;

        unsafe {
            let err = FMOD_Studio_VCA_SetVolume(self.vca, volume);
            make_result((), err)
        }
    }
}
