/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

mod advanced_settings;
mod bank;
mod bank_info;
mod buffer_info;
mod buffer_usage;
mod bus;
mod command_capture_flags;
mod command_info;
mod command_replay;
mod command_replay_flags;
mod cpu_usage;
mod event_callback_type;
mod event_description;
mod event_instance;
mod event_property;
mod init_flags;
mod instance_type;
mod load_bank_flags;
mod load_memory_mode;
mod loading_state;
mod memory_usage;
mod parameter_description;
mod parameter_flags;
mod parameter_id;
mod parameter_type;
mod playback_state;
mod sound_info;
mod stop_mode;
mod system;
mod system_callback_type;
mod user_property;
mod user_property_type;
mod vca;

pub use advanced_settings::AdvancedSettings;
pub use bank::Bank;
pub use bank_info::BankInfo;
pub use buffer_info::BufferInfo;
pub use buffer_usage::BufferUsage;
pub use bus::Bus;
pub use command_capture_flags::CommandCaptureFlags;
pub use command_info::CommandInfo;
pub use command_replay::CommandReplay;
pub use command_replay_flags::CommandReplayFlags;
pub use cpu_usage::CPUUsage;
pub use event_callback_type::EventCallbackType;
pub use event_description::EventDescription;
pub use event_instance::EventInstance;
pub use event_property::EventProperty;
pub use init_flags::InitFlags;
pub use instance_type::InstanceType;
pub use load_bank_flags::LoadBankFlags;
pub use load_memory_mode::LoadMemoryMode;
pub use loading_state::LoadingState;
pub use memory_usage::MemoryUsage;
pub use parameter_description::ParameterDescription;
pub use parameter_flags::ParameterFlags;
pub use parameter_id::ParameterID;
pub use parameter_type::ParameterType;
pub use playback_state::PlaybackState;
pub use sound_info::SoundInfo;
pub use stop_mode::StopMode;
pub use system::System;
pub use system_callback_type::SystemCallbackType;
pub use user_property::UserProperty;
pub use user_property_type::UserPropertyType;
pub use vca::VCA;
