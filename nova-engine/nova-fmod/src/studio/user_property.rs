/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::studio::UserPropertyType;
use raw::FMOD_STUDIO_USER_PROPERTY;
use std::ffi::CStr;

///
/// FMOD_STUDIO_USER_PROPERTY
///
#[repr(C)]
#[derive(Clone)]
pub struct UserProperty<'a> {
    pub name: &'a CStr,
    pub p_type: UserPropertyType<'a>,
}

impl<'a> UserProperty<'a> {
    #[inline]
    pub unsafe fn from_ffi(ffi: FMOD_STUDIO_USER_PROPERTY) -> Self {
        Self {
            name: CStr::from_ptr(ffi.name),
            p_type: UserPropertyType::from_ffi(ffi.type_, ffi.__bindgen_anon_1),
        }
    }

    pub fn info_ffi(self) -> FMOD_STUDIO_USER_PROPERTY {
        let (type_, __bindgen_anon_1) = self.p_type.into_ffi();
        FMOD_STUDIO_USER_PROPERTY {
            name: self.name.as_ptr(),
            type_,
            __bindgen_anon_1,
        }
    }
}
