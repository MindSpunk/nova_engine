/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::core::{make_result, Guid};
use crate::core::{Attributes3D, Result};
use crate::studio::{
    AdvancedSettings, Bank, BankInfo, BufferUsage, Bus, CPUUsage, CommandCaptureFlags,
    CommandReplay, CommandReplayFlags, EventDescription, LoadBankFlags, LoadMemoryMode,
    MemoryUsage, ParameterDescription, ParameterID, SoundInfo, SystemCallbackType, VCA,
};
use raw::FMOD_STUDIO_SYSTEM;
use raw::FMOD_VERSION;
use std::ffi::{CStr, CString};
use std::mem::MaybeUninit;
use std::os::raw::c_int;

pub struct SystemInitializer<'a> {
    system: &'a mut System,
    max_channels: i32,
    studio_flags: crate::studio::InitFlags,
    flags: crate::core::InitFlags,
    extra_driver_data: Option<*mut ()>,
}

impl<'a> SystemInitializer<'a> {
    pub fn live_update(mut self) -> Self {
        self.studio_flags
            .set(crate::studio::InitFlags::LIVEUPDATE, true);
        self
    }

    pub fn allow_missing_plugins(mut self) -> Self {
        self.studio_flags
            .set(crate::studio::InitFlags::ALLOW_MISSING_PLUGINS, true);
        self
    }

    pub fn deferred_callbacks(mut self) -> Self {
        self.studio_flags
            .set(crate::studio::InitFlags::DEFERRED_CALLBACKS, true);
        self
    }

    pub fn load_from_update(mut self) -> Self {
        self.studio_flags
            .set(crate::studio::InitFlags::LOAD_FROM_UPDATE, true);
        self
    }

    pub fn max_channels(mut self, max_channels: i32) -> Self {
        self.max_channels = max_channels;
        self
    }

    pub fn initialize(self) -> Result<()> {
        unsafe {
            self.system.initialize(
                self.max_channels,
                self.studio_flags,
                self.flags,
                self.extra_driver_data,
            )
        }
    }
}

#[derive(Copy, Clone)]
pub struct System {
    pub(crate) system: *mut FMOD_STUDIO_SYSTEM,
}

impl System {
    ///
    /// Creates a null handle
    ///
    pub fn null() -> Self {
        Self {
            system: std::ptr::null_mut(),
        }
    }

    ///
    /// Creates a handle from the ffi type
    ///
    pub fn from_ffi(ffi: *mut FMOD_STUDIO_SYSTEM) -> Self {
        Self { system: ffi }
    }

    ///
    /// FMOD_Studio_System_Create
    ///
    pub fn create() -> Result<Self> {
        use raw::FMOD_Studio_System_Create;
        unsafe {
            let mut system: MaybeUninit<*mut FMOD_STUDIO_SYSTEM> = MaybeUninit::uninit();
            let err = FMOD_Studio_System_Create(system.as_mut_ptr(), FMOD_VERSION);
            let system = make_result(system, err);
            system.map(|system| {
                let system = system.assume_init();
                System { system }
            })
        }
    }

    ///
    /// FMOD_Studio_System_IsValid
    ///
    pub fn is_valid(&self) -> bool {
        use raw::FMOD_Studio_System_IsValid;
        unsafe {
            let val = FMOD_Studio_System_IsValid(self.system);
            val != 0
        }
    }

    ///
    /// FMOD_Studio_System_SetAdvancedSettings
    ///
    pub fn set_advanced_settings(&self, settings: &AdvancedSettings) -> Result<()> {
        use raw::FMOD_Studio_System_SetAdvancedSettings;
        use raw::FMOD_STUDIO_ADVANCEDSETTINGS;

        unsafe {
            let mut val = settings.clone().into_ffi();
            let val_ptr = &mut val as *mut FMOD_STUDIO_ADVANCEDSETTINGS;
            let err = FMOD_Studio_System_SetAdvancedSettings(self.system, val_ptr);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_System_GetAdvancedSettings
    ///
    pub fn get_advanced_settings(&self) -> Result<AdvancedSettings> {
        use raw::FMOD_Studio_System_GetAdvancedSettings;

        unsafe {
            let mut out = MaybeUninit::uninit();
            let err = FMOD_Studio_System_GetAdvancedSettings(self.system, out.as_mut_ptr());
            make_result(out, err).map(|out| AdvancedSettings::from_ffi(out.assume_init()))
        }
    }

    ///
    /// Safe wrapper for FMOD_Studio_System_Initialize that prevents unsafe flags from being set
    /// such as FMOD
    ///
    pub fn initializer(&mut self) -> SystemInitializer {
        SystemInitializer {
            system: self,
            max_channels: 512,
            studio_flags: crate::studio::InitFlags::NORMAL,
            flags: crate::core::InitFlags::NORMAL,
            extra_driver_data: None,
        }
    }

    ///
    /// FMOD_Studio_System_Initialize
    ///
    /// This function is extremely unsafe. It is only exposed for API completeness sake. The
    /// `SYNCHRONOUS_UPDATE` flag completely breaks the safety of the wrapper as everything assumes
    /// this flag is NOT set. This allows all the methods to take a shared reference as the API is
    /// thread safe if this flag is not set.
    ///
    /// Setting `SYNCHRONOUS_UPDATE` will remove the thread safety meaning race conditions WILL
    /// happen in your code as the contract with rust for shared mutability is broken.
    ///
    /// If you're going to use the `SYNCHRONOUS_UPDATE` flag, just use the raw bindings, it will
    /// cause you less pain.
    ///
    pub unsafe fn initialize(
        &mut self,
        max_channels: i32,
        studio_flags: crate::studio::InitFlags,
        flags: crate::core::InitFlags,
        extra_driver_data: Option<*mut ()>,
    ) -> Result<()> {
        use raw::FMOD_Studio_System_Initialize;
        let extra_driver_data = extra_driver_data.unwrap_or(std::ptr::null_mut());
        let err = FMOD_Studio_System_Initialize(
            self.system,
            max_channels,
            studio_flags.bits(),
            flags.bits(),
            extra_driver_data as *mut _,
        );
        make_result((), err)
    }

    ///
    /// FMOD_Studio_System_Release
    ///
    pub fn release(self) -> Result<()> {
        use raw::FMOD_Studio_System_Release;
        unsafe {
            let err = FMOD_Studio_System_Release(self.system);
            std::mem::forget(self);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_System_Update
    ///
    pub fn update(&self) -> Result<()> {
        use raw::FMOD_Studio_System_Update;
        unsafe {
            let err = FMOD_Studio_System_Update(self.system);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_System_GetCoreSystem
    ///
    pub fn get_core_system(&self) -> Result<crate::core::System> {
        use raw::FMOD_Studio_System_GetCoreSystem;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_System_GetCoreSystem(self.system, val.as_mut_ptr());
            make_result(val, err).map(|out| {
                let out = out.assume_init();
                crate::core::System { system: out }
            })
        }
    }

    ///
    /// FMOD_Studio_System_GetEvent
    ///
    pub fn get_event(&self, path_or_id: &CStr) -> Result<EventDescription> {
        use raw::FMOD_Studio_System_GetEvent;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let str_ptr = path_or_id.as_ptr();
            let err = FMOD_Studio_System_GetEvent(self.system, str_ptr, val.as_mut_ptr());
            make_result(val, err).map(|out| {
                let out = out.assume_init();
                EventDescription {
                    event_description: out,
                }
            })
        }
    }

    ///
    /// FMOD_Studio_System_GetBus
    ///
    pub fn get_bus(&self, path_or_id: &CStr) -> Result<Bus> {
        use raw::FMOD_Studio_System_GetBus;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let str_ptr = path_or_id.as_ptr();
            let err = FMOD_Studio_System_GetBus(self.system, str_ptr, val.as_mut_ptr());
            make_result(val, err).map(|out| {
                let out = out.assume_init();
                Bus { bus: out }
            })
        }
    }

    ///
    /// FMOD_Studio_System_GetVCA
    ///
    pub fn get_vca(&self, path_or_id: &CStr) -> Result<VCA> {
        use raw::FMOD_Studio_System_GetVCA;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let str_ptr = path_or_id.as_ptr();
            let err = FMOD_Studio_System_GetVCA(self.system, str_ptr, val.as_mut_ptr());
            make_result(val, err).map(|out| {
                let out = out.assume_init();
                VCA { vca: out }
            })
        }
    }

    ///
    /// FMOD_Studio_System_GetBank
    ///
    pub fn get_bank(&self, path_or_id: &CStr) -> Result<Bank> {
        use raw::FMOD_Studio_System_GetBank;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let str_ptr = path_or_id.as_ptr();
            let err = FMOD_Studio_System_GetBank(self.system, str_ptr, val.as_mut_ptr());
            make_result(val, err).map(|out| {
                let out = out.assume_init();
                Bank { bank: out }
            })
        }
    }

    ///
    /// FMOD_Studio_System_GetEventByID
    ///
    pub fn get_event_by_id(&self, id: &Guid) -> Result<EventDescription> {
        use raw::FMOD_Studio_System_GetEventByID;
        use raw::FMOD_GUID;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let ptr = id as *const Guid as *const FMOD_GUID;
            let err = FMOD_Studio_System_GetEventByID(self.system, ptr, val.as_mut_ptr());
            make_result(val, err).map(|out| {
                let out = out.assume_init();
                EventDescription {
                    event_description: out,
                }
            })
        }
    }

    ///
    /// FMOD_Studio_System_GetBusByID
    ///
    pub fn get_bus_by_id(&self, id: &Guid) -> Result<Bus> {
        use raw::FMOD_Studio_System_GetBusByID;
        use raw::FMOD_GUID;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let ptr = id as *const Guid as *const FMOD_GUID;
            let err = FMOD_Studio_System_GetBusByID(self.system, ptr, val.as_mut_ptr());
            make_result(val, err).map(|out| {
                let out = out.assume_init();
                Bus { bus: out }
            })
        }
    }

    ///
    /// FMOD_Studio_System_GetVCAByID
    ///
    pub fn get_vca_by_id(&self, id: &Guid) -> Result<VCA> {
        use raw::FMOD_Studio_System_GetVCAByID;
        use raw::FMOD_GUID;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let ptr = id as *const Guid as *const FMOD_GUID;
            let err = FMOD_Studio_System_GetVCAByID(self.system, ptr, val.as_mut_ptr());
            make_result(val, err).map(|out| {
                let out = out.assume_init();
                VCA { vca: out }
            })
        }
    }

    ///
    /// FMOD_Studio_System_GetBankByID
    ///
    pub fn get_bank_by_id(&self, id: &Guid) -> Result<Bank> {
        use raw::FMOD_Studio_System_GetBankByID;
        use raw::FMOD_GUID;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let ptr = id as *const Guid as *const FMOD_GUID;
            let err = FMOD_Studio_System_GetBankByID(self.system, ptr, val.as_mut_ptr());
            make_result(val, err).map(|out| {
                let out = out.assume_init();
                Bank { bank: out }
            })
        }
    }

    ///
    /// FMOD_Studio_System_GetSoundInfo
    ///
    pub fn get_sound_info(&self, key: &CStr) -> Result<SoundInfo> {
        use raw::FMOD_Studio_System_GetSoundInfo;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_System_GetSoundInfo(self.system, key.as_ptr(), val.as_mut_ptr());
            make_result(val, err).map(|out| SoundInfo::from_ffi(out.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_System_GetParameterDescriptionByID
    ///
    pub fn get_parameter_description_by_id(&self, id: ParameterID) -> Result<ParameterDescription> {
        use raw::FMOD_Studio_System_GetParameterDescriptionByID;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_System_GetParameterDescriptionByID(
                self.system,
                id.into_ffi(),
                val.as_mut_ptr(),
            );
            make_result(val, err).map(|val| {
                let val = val.assume_init();
                let val = ParameterDescription::from_ffi(val);
                val
            })
        }
    }

    ///
    /// FMOD_Studio_System_GetParameterDescriptionByName
    ///
    pub fn get_parameter_description_by_name(&self, name: &CStr) -> Result<ParameterDescription> {
        use raw::FMOD_Studio_System_GetParameterDescriptionByName;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_System_GetParameterDescriptionByName(
                self.system,
                name.as_ptr(),
                val.as_mut_ptr(),
            );
            make_result(val, err).map(|val| {
                let val = val.assume_init();
                let val = ParameterDescription::from_ffi(val);
                val
            })
        }
    }

    ///
    /// FMOD_Studio_System_GetParameterByID
    ///
    pub fn get_parameter_by_id(&self, id: ParameterID) -> Result<(f32, f32)> {
        use raw::FMOD_Studio_System_GetParameterByID;

        unsafe {
            let mut val1 = MaybeUninit::uninit();
            let mut val2 = MaybeUninit::uninit();
            let err = FMOD_Studio_System_GetParameterByID(
                self.system,
                id.into_ffi(),
                val1.as_mut_ptr(),
                val2.as_mut_ptr(),
            );
            make_result((val1, val2), err)
                .map(|(val1, val2)| (val1.assume_init(), val2.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_System_SetParameterByID
    ///
    pub fn set_parameter_by_id(
        &self,
        id: ParameterID,
        value: f32,
        ignore_seek_speed: bool,
    ) -> Result<()> {
        use raw::FMOD_Studio_System_SetParameterByID;

        unsafe {
            let ignoreseekspeed = if ignore_seek_speed { 1 } else { 0 };
            let err = FMOD_Studio_System_SetParameterByID(
                self.system,
                id.into_ffi(),
                value,
                ignoreseekspeed,
            );
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_System_SetParametersByIDs
    ///
    /// Will use the length of the shortest array
    ///
    pub fn set_parameters_by_ids(
        &self,
        ids: &[ParameterID],
        values: &[f32],
        ignore_seek_speed: bool,
    ) -> Result<()> {
        use raw::FMOD_Studio_System_SetParametersByIDs;
        unsafe {
            let ignoreseekspeed = if ignore_seek_speed { 1 } else { 0 };
            let err = FMOD_Studio_System_SetParametersByIDs(
                self.system,
                ids.as_ptr() as *const raw::FMOD_STUDIO_PARAMETER_ID,
                values.as_ptr() as *mut f32,
                usize::min(ids.len(), values.len()) as _,
                ignoreseekspeed,
            );
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_System_GetParameterByName
    ///
    pub fn get_parameter_by_name(&self, name: &CStr) -> Result<(f32, f32)> {
        use raw::FMOD_Studio_System_GetParameterByName;

        unsafe {
            let mut val1 = MaybeUninit::uninit();
            let mut val2 = MaybeUninit::uninit();
            let err = FMOD_Studio_System_GetParameterByName(
                self.system,
                name.as_ptr(),
                val1.as_mut_ptr(),
                val2.as_mut_ptr(),
            );
            make_result((val1, val2), err)
                .map(|(val1, val2)| (val1.assume_init(), val2.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_System_SetParameterByName
    ///
    pub fn set_parameter_by_name(
        &self,
        name: &CStr,
        value: f32,
        ignore_seek_speed: bool,
    ) -> Result<()> {
        use raw::FMOD_Studio_System_SetParameterByName;

        unsafe {
            let ignoreseekspeed = if ignore_seek_speed { 1 } else { 0 };
            let err = FMOD_Studio_System_SetParameterByName(
                self.system,
                name.as_ptr(),
                value,
                ignoreseekspeed,
            );
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_System_LookupID
    ///
    pub fn lookup_id(&self, path: &CStr) -> Result<Guid> {
        use raw::FMOD_Studio_System_LookupID;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_System_LookupID(self.system, path.as_ptr(), val.as_mut_ptr());
            make_result(val, err).map(|val| Guid::from_ffi(val.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_System_LookupPath
    ///
    pub fn lookup_path(&self, id: &Guid) -> Result<CString> {
        use raw::FMOD_Studio_System_LookupPath;

        unsafe {
            let mut retrieved = 0;
            let err = FMOD_Studio_System_LookupPath(
                self.system,
                id as *const Guid as *const _,
                std::ptr::null_mut(),
                0,
                &mut retrieved,
            );
            make_result((), err)?;
            let mut out = Vec::<u8>::with_capacity(retrieved as usize);
            out.resize(retrieved as usize, 0);
            let err = FMOD_Studio_System_LookupPath(
                self.system,
                id as *const Guid as *const _,
                out.as_mut_ptr() as *mut _,
                out.len() as _,
                std::ptr::null_mut(),
            );
            make_result(CString::from_vec_unchecked(out), err)
        }
    }

    ///
    /// FMOD_Studio_System_GetNumListeners
    ///
    pub fn get_num_listeners(&self) -> Result<usize> {
        use raw::FMOD_Studio_System_GetNumListeners;

        unsafe {
            let mut out = MaybeUninit::uninit();
            let err = FMOD_Studio_System_GetNumListeners(self.system, out.as_mut_ptr());
            make_result(out, err).map(|out| out.assume_init() as usize)
        }
    }

    ///
    /// FMOD_Studio_System_SetNumListeners
    ///
    pub fn set_num_listeners(&self, num_listeners: usize) -> Result<()> {
        use raw::FMOD_Studio_System_SetNumListeners;

        unsafe {
            let err = FMOD_Studio_System_SetNumListeners(self.system, num_listeners as c_int);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_System_GetListenerAttributes
    ///
    pub fn get_listener_attributes(&self, index: usize) -> Result<Attributes3D> {
        use raw::FMOD_Studio_System_GetListenerAttributes;

        unsafe {
            let mut out = MaybeUninit::uninit();
            let err =
                FMOD_Studio_System_GetListenerAttributes(self.system, index as _, out.as_mut_ptr());
            make_result(out, err).map(|out| Attributes3D::from_ffi(out.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_System_SetListenerAttributes
    ///
    pub fn set_listener_attributes(&self, index: usize, attributes: Attributes3D) -> Result<()> {
        use raw::FMOD_Studio_System_SetListenerAttributes;

        unsafe {
            let mut attributes = attributes.into_ffi();
            let err =
                FMOD_Studio_System_SetListenerAttributes(self.system, index as _, &mut attributes);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_System_GetListenerWeight
    ///
    pub fn get_listener_weight(&self, index: usize) -> Result<f32> {
        use raw::FMOD_Studio_System_GetListenerWeight;

        unsafe {
            let mut out = MaybeUninit::uninit();
            let err =
                FMOD_Studio_System_GetListenerWeight(self.system, index as _, out.as_mut_ptr());
            make_result(out, err).map(|out| out.assume_init())
        }
    }

    ///
    /// FMOD_Studio_System_SetListenerWeight
    ///
    pub fn set_listener_weight(&self, index: usize, weight: f32) -> Result<()> {
        use raw::FMOD_Studio_System_SetListenerWeight;

        unsafe {
            let err = FMOD_Studio_System_SetListenerWeight(self.system, index as _, weight);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_System_LoadBankFile
    ///
    pub fn load_bank_file(&self, filename: &CStr, flags: LoadBankFlags) -> Result<Bank> {
        use raw::FMOD_Studio_System_LoadBankFile;

        unsafe {
            let mut out = MaybeUninit::uninit();
            let err = FMOD_Studio_System_LoadBankFile(
                self.system,
                filename.as_ptr(),
                flags.bits(),
                out.as_mut_ptr(),
            );
            make_result(out, err).map(|val| Bank {
                bank: val.assume_init(),
            })
        }
    }

    ///
    /// FMOD_Studio_System_LoadBankMemory
    ///
    /// Unsafe because the LoadMemoryMode enum allows this function to take a pointer to a slice
    /// that could be used after free by the internal FMOD implementation
    ///
    pub unsafe fn load_bank_memory(
        &self,
        buffer: &[u8],
        mode: LoadMemoryMode,
        flags: LoadBankFlags,
    ) -> Result<Bank> {
        use raw::FMOD_Studio_System_LoadBankMemory;

        let mut out = MaybeUninit::uninit();
        let err = FMOD_Studio_System_LoadBankMemory(
            self.system,
            buffer.as_ptr() as *const _,
            buffer.len() as _,
            mode.into_ffi(),
            flags.bits(),
            out.as_mut_ptr(),
        );
        make_result(out, err).map(|val| Bank {
            bank: val.assume_init(),
        })
    }

    ///
    /// FMOD_Studio_System_LoadBankCustom
    ///
    /// There's really no way to make this safe as it exposes a raw userdata handle and unsafe
    /// callback functions
    ///
    pub unsafe fn load_bank_custom(&self, info: BankInfo, flags: LoadBankFlags) -> Result<Bank> {
        use raw::FMOD_Studio_System_LoadBankCustom;

        let mut out = MaybeUninit::uninit();
        let info = info.into_ffi();
        let err =
            FMOD_Studio_System_LoadBankCustom(self.system, &info, flags.bits(), out.as_mut_ptr());
        make_result(out, err).map(|val| Bank {
            bank: val.assume_init(),
        })
    }

    ///
    /// FMOD_Studio_System_RegisterPlugin
    ///
    pub unsafe fn register_plugin(&self, description: &raw::FMOD_DSP_DESCRIPTION) -> Result<()> {
        use raw::FMOD_Studio_System_RegisterPlugin;
        let err = FMOD_Studio_System_RegisterPlugin(self.system, description);
        make_result((), err)
    }

    ///
    /// FMOD_Studio_System_UnregisterPlugin
    ///
    pub fn unregister_plugin(&self, name: &CStr) -> Result<()> {
        use raw::FMOD_Studio_System_UnregisterPlugin;
        unsafe {
            let err = FMOD_Studio_System_UnregisterPlugin(self.system, name.as_ptr());
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_System_UnloadAll
    ///
    pub fn unload_all(&self) -> Result<()> {
        use raw::FMOD_Studio_System_UnloadAll;
        unsafe {
            let err = FMOD_Studio_System_UnloadAll(self.system);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_System_FlushCommands
    ///
    pub fn flush_commands(&self) -> Result<()> {
        use raw::FMOD_Studio_System_FlushCommands;
        unsafe {
            let err = FMOD_Studio_System_FlushCommands(self.system);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_System_FlushSampleLoading
    ///
    pub fn flush_sample_loading(&self) -> Result<()> {
        use raw::FMOD_Studio_System_FlushSampleLoading;
        unsafe {
            let err = FMOD_Studio_System_FlushSampleLoading(self.system);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_System_StartCommandCapture
    ///
    pub fn start_command_capture(&self, filename: &CStr, flags: CommandCaptureFlags) -> Result<()> {
        use raw::FMOD_Studio_System_StartCommandCapture;
        unsafe {
            let err = FMOD_Studio_System_StartCommandCapture(
                self.system,
                filename.as_ptr(),
                flags.bits(),
            );
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_System_StopCommandCapture
    ///
    pub fn stop_command_capture(&self) -> Result<()> {
        use raw::FMOD_Studio_System_StopCommandCapture;
        unsafe {
            let err = FMOD_Studio_System_StopCommandCapture(self.system);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_System_LoadCommandReplay
    ///
    pub fn load_command_capture(
        &self,
        filename: &CStr,
        flags: CommandReplayFlags,
    ) -> Result<CommandReplay> {
        use raw::FMOD_Studio_System_LoadCommandReplay;
        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_System_LoadCommandReplay(
                self.system,
                filename.as_ptr(),
                flags.bits(),
                val.as_mut_ptr(),
            );
            make_result(val, err).map(|val| CommandReplay {
                command_replay: val.assume_init(),
            })
        }
    }

    ///
    /// FMOD_Studio_System_GetBankCount
    ///
    pub fn get_bank_count(&self) -> Result<usize> {
        use raw::FMOD_Studio_System_GetBankCount;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_System_GetBankCount(self.system, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init() as usize)
        }
    }

    ///
    /// FMOD_Studio_System_GetBankList
    ///
    pub fn get_bank_list(&self, buffer: &mut [Bank]) -> Result<usize> {
        use raw::FMOD_Studio_System_GetBankList;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_System_GetBankList(
                self.system,
                buffer.as_mut_ptr() as *mut _,
                buffer.len() as _,
                val.as_mut_ptr(),
            );
            make_result(val, err).map(|val| val.assume_init() as usize)
        }
    }

    ///
    /// FMOD_Studio_System_GetParameterDescriptionCount
    ///
    pub fn get_parameter_description_count(&self) -> Result<usize> {
        use raw::FMOD_Studio_System_GetParameterDescriptionCount;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err =
                FMOD_Studio_System_GetParameterDescriptionCount(self.system, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init() as usize)
        }
    }

    ///
    /// FMOD_Studio_System_GetParameterDescriptionList
    ///
    pub fn get_parameter_description_list(
        &self,
        buffer: &mut [ParameterDescription],
    ) -> Result<usize> {
        use raw::FMOD_Studio_System_GetParameterDescriptionList;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_System_GetParameterDescriptionList(
                self.system,
                buffer.as_mut_ptr() as *mut _,
                buffer.len() as _,
                val.as_mut_ptr(),
            );
            make_result(val, err).map(|val| val.assume_init() as usize)
        }
    }

    ///
    /// FMOD_Studio_System_GetCPUUsage
    ///
    pub fn get_cpu_usage(&self) -> Result<CPUUsage> {
        use raw::FMOD_Studio_System_GetCPUUsage;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_System_GetCPUUsage(self.system, val.as_mut_ptr());
            make_result(val, err).map(|val| CPUUsage::from_ffi(val.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_System_GetBufferUsage
    ///
    pub fn get_buffer_usage(&self) -> Result<BufferUsage> {
        use raw::FMOD_Studio_System_GetBufferUsage;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_System_GetBufferUsage(self.system, val.as_mut_ptr());
            make_result(val, err).map(|val| BufferUsage::from_ffi(val.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_System_ResetBufferUsage
    ///
    pub fn reset_buffer_usage(&self) -> Result<()> {
        use raw::FMOD_Studio_System_ResetBufferUsage;

        unsafe {
            let err = FMOD_Studio_System_ResetBufferUsage(self.system);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_System_SetCallback
    ///
    pub fn set_callback(
        &self,
        callback: raw::FMOD_STUDIO_SYSTEM_CALLBACK,
        callback_mask: SystemCallbackType,
    ) -> Result<()> {
        use raw::FMOD_Studio_System_SetCallback;

        unsafe {
            let err = FMOD_Studio_System_SetCallback(self.system, callback, callback_mask.bits());
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_System_GetUserData
    ///
    pub fn get_userdata(&self) -> Result<*mut ()> {
        use raw::FMOD_Studio_System_GetUserData;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_System_GetUserData(self.system, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init() as *mut ())
        }
    }

    ///
    /// FMOD_Studio_System_SetUserData
    ///
    pub fn set_userdata(&self, userdata: *mut ()) -> Result<()> {
        use raw::FMOD_Studio_System_SetUserData;

        unsafe {
            let err = FMOD_Studio_System_SetUserData(self.system, userdata as *mut _);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_System_GetMemoryUsage
    ///
    pub fn get_memory_usage(&self) -> Result<MemoryUsage> {
        use raw::FMOD_Studio_System_GetMemoryUsage;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_System_GetMemoryUsage(self.system, val.as_mut_ptr());
            make_result(val, err).map(|val| MemoryUsage::from_ffi(val.assume_init()))
        }
    }
}

unsafe impl Send for System {}
unsafe impl Sync for System {}
