/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::raw::{FMOD_STUDIO_USER_PROPERTY__bindgen_ty_1, FMOD_STUDIO_USER_PROPERTY_TYPE};
use std::ffi::CStr;
use std::os::raw::c_int;

///
/// FMOD_STUDIO_USER_PROPERTY_TYPE and FMOD_STUDIO_USER_PROPERTY union mapped into a rust enum
/// (tagged union)
///
#[repr(u32)]
#[derive(Debug, Clone, PartialEq)]
pub enum UserPropertyType<'a> {
    Integer(c_int),
    Boolean(bool),
    Float(f32),
    String(&'a CStr),
}

impl<'a> UserPropertyType<'a> {
    #[inline]
    pub unsafe fn from_ffi(
        p_type: FMOD_STUDIO_USER_PROPERTY_TYPE,
        vals: FMOD_STUDIO_USER_PROPERTY__bindgen_ty_1,
    ) -> Self {
        match p_type {
            FMOD_STUDIO_USER_PROPERTY_TYPE::FMOD_STUDIO_USER_PROPERTY_TYPE_INTEGER => {
                UserPropertyType::Integer(vals.intvalue)
            }
            FMOD_STUDIO_USER_PROPERTY_TYPE::FMOD_STUDIO_USER_PROPERTY_TYPE_BOOLEAN => {
                UserPropertyType::Boolean(vals.boolvalue != 0)
            }
            FMOD_STUDIO_USER_PROPERTY_TYPE::FMOD_STUDIO_USER_PROPERTY_TYPE_FLOAT => {
                UserPropertyType::Float(vals.floatvalue)
            }
            FMOD_STUDIO_USER_PROPERTY_TYPE::FMOD_STUDIO_USER_PROPERTY_TYPE_STRING => {
                UserPropertyType::String(CStr::from_ptr(vals.stringvalue))
            }
            FMOD_STUDIO_USER_PROPERTY_TYPE::FMOD_STUDIO_USER_PROPERTY_TYPE_FORCEINT => {
                unreachable!()
            }
        }
    }

    #[inline]
    pub fn into_ffi(
        self,
    ) -> (
        FMOD_STUDIO_USER_PROPERTY_TYPE,
        FMOD_STUDIO_USER_PROPERTY__bindgen_ty_1,
    ) {
        match self {
            UserPropertyType::Integer(integer) => {
                let t = FMOD_STUDIO_USER_PROPERTY_TYPE::FMOD_STUDIO_USER_PROPERTY_TYPE_INTEGER;
                let v = FMOD_STUDIO_USER_PROPERTY__bindgen_ty_1 { intvalue: integer };
                (t, v)
            }
            UserPropertyType::Boolean(boolean) => {
                let boolean = if boolean { 1 } else { 0 };
                let t = FMOD_STUDIO_USER_PROPERTY_TYPE::FMOD_STUDIO_USER_PROPERTY_TYPE_BOOLEAN;
                let v = FMOD_STUDIO_USER_PROPERTY__bindgen_ty_1 { boolvalue: boolean };
                (t, v)
            }
            UserPropertyType::Float(float) => {
                let t = FMOD_STUDIO_USER_PROPERTY_TYPE::FMOD_STUDIO_USER_PROPERTY_TYPE_FLOAT;
                let v = FMOD_STUDIO_USER_PROPERTY__bindgen_ty_1 { floatvalue: float };
                (t, v)
            }
            UserPropertyType::String(string) => {
                let t = FMOD_STUDIO_USER_PROPERTY_TYPE::FMOD_STUDIO_USER_PROPERTY_TYPE_STRING;
                let v = FMOD_STUDIO_USER_PROPERTY__bindgen_ty_1 {
                    stringvalue: string.as_ptr(),
                };
                (t, v)
            }
        }
    }
}
