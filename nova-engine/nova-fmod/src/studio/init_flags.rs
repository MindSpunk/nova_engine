/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use raw::FMOD_STUDIO_INITFLAGS;
use raw::FMOD_STUDIO_INIT_ALLOW_MISSING_PLUGINS;
use raw::FMOD_STUDIO_INIT_DEFERRED_CALLBACKS;
use raw::FMOD_STUDIO_INIT_LIVEUPDATE;
use raw::FMOD_STUDIO_INIT_LOAD_FROM_UPDATE;
use raw::FMOD_STUDIO_INIT_MEMORY_TRACKING;
use raw::FMOD_STUDIO_INIT_NORMAL;
use raw::FMOD_STUDIO_INIT_SYNCHRONOUS_UPDATE;

bitflags! {
    ///
    /// A wrapper around the FMOD_STUDIO_INIT bit flags type
    ///
    pub struct InitFlags: FMOD_STUDIO_INITFLAGS {
        const NORMAL = FMOD_STUDIO_INIT_NORMAL;
        const LIVEUPDATE = FMOD_STUDIO_INIT_LIVEUPDATE;
        const ALLOW_MISSING_PLUGINS = FMOD_STUDIO_INIT_ALLOW_MISSING_PLUGINS;
        const SYNCHRONOUS_UPDATE = FMOD_STUDIO_INIT_SYNCHRONOUS_UPDATE;
        const DEFERRED_CALLBACKS = FMOD_STUDIO_INIT_DEFERRED_CALLBACKS;
        const LOAD_FROM_UPDATE = FMOD_STUDIO_INIT_LOAD_FROM_UPDATE;
        const MEMORY_TRACKING = FMOD_STUDIO_INIT_MEMORY_TRACKING;
    }
}
