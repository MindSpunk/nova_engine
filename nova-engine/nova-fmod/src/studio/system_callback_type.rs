/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use raw::FMOD_STUDIO_SYSTEM_CALLBACK_ALL;
use raw::FMOD_STUDIO_SYSTEM_CALLBACK_BANK_UNLOAD;
use raw::FMOD_STUDIO_SYSTEM_CALLBACK_POSTUPDATE;
use raw::FMOD_STUDIO_SYSTEM_CALLBACK_PREUPDATE;
use raw::FMOD_STUDIO_SYSTEM_CALLBACK_TYPE;

bitflags! {
    ///
    /// A wrapper around the FMOD_STUDIO_SYSTEM_CALLBACK_TYPE bit flags type
    ///
    pub struct SystemCallbackType: FMOD_STUDIO_SYSTEM_CALLBACK_TYPE {
        const PREUPDATE = FMOD_STUDIO_SYSTEM_CALLBACK_PREUPDATE;
        const POSTUPDATE = FMOD_STUDIO_SYSTEM_CALLBACK_POSTUPDATE;
        const BANK_UNLOAD = FMOD_STUDIO_SYSTEM_CALLBACK_BANK_UNLOAD;
        const ALL = FMOD_STUDIO_SYSTEM_CALLBACK_ALL;
    }
}
