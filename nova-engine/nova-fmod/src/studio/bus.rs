/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::core::ChannelGroup;
use crate::core::{make_result, Guid, Result};
use crate::studio::{MemoryUsage, StopMode};
use std::ffi::CString;
use std::mem::MaybeUninit;
use std::os::raw::c_uint;

///
/// FMOD_STUDIO_BUS
///
#[derive(Copy, Clone)]
pub struct Bus {
    pub(crate) bus: *mut raw::FMOD_STUDIO_BUS,
}

impl Bus {
    ///
    /// Gets a null handle
    ///
    pub fn null() -> Self {
        Self {
            bus: std::ptr::null_mut(),
        }
    }

    ///
    /// Creates a handle from the ffi type
    ///
    pub fn from_ffi(ffi: *mut raw::FMOD_STUDIO_BUS) -> Self {
        Self { bus: ffi }
    }

    ///
    /// FMOD_Studio_Bus_IsValid
    ///
    pub fn is_valid(&self) -> bool {
        use raw::FMOD_Studio_Bus_IsValid;
        unsafe {
            let val = FMOD_Studio_Bus_IsValid(self.bus);
            val != 0
        }
    }

    ///
    /// FMOD_Studio_Bus_GetID
    ///
    pub fn get_id(&self) -> Result<Guid> {
        use raw::FMOD_Studio_Bus_GetID;
        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_Bus_GetID(self.bus, val.as_mut_ptr());
            make_result(val, err).map(|val| Guid::from_ffi(val.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_Bus_GetPath
    ///
    pub fn get_path(&self) -> Result<CString> {
        use raw::FMOD_Studio_Bus_GetPath;

        unsafe {
            let mut retrieved = 0;
            let err = FMOD_Studio_Bus_GetPath(self.bus, std::ptr::null_mut(), 0, &mut retrieved);
            make_result((), err)?;
            let mut out = Vec::<u8>::with_capacity(retrieved as usize);
            out.resize(retrieved as usize, 0);
            let err = FMOD_Studio_Bus_GetPath(
                self.bus,
                out.as_mut_ptr() as *mut _,
                out.len() as _,
                std::ptr::null_mut(),
            );
            make_result(CString::from_vec_unchecked(out), err)
        }
    }

    ///
    /// FMOD_Studio_Bus_GetVolume
    ///
    pub fn get_volume(&self) -> Result<(f32, f32)> {
        use raw::FMOD_Studio_Bus_GetVolume;

        unsafe {
            let mut val1 = MaybeUninit::uninit();
            let mut val2 = MaybeUninit::uninit();
            let err = FMOD_Studio_Bus_GetVolume(self.bus, val1.as_mut_ptr(), val2.as_mut_ptr());
            make_result((val1, val2), err)
                .map(|(val1, val2)| (val1.assume_init(), val2.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_Bus_SetVolume
    ///
    pub fn set_volume(&self, volume: f32) -> Result<()> {
        use raw::FMOD_Studio_Bus_SetVolume;

        unsafe {
            let err = FMOD_Studio_Bus_SetVolume(self.bus, volume);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_Bus_GetPaused
    ///
    pub fn get_paused(&self) -> Result<bool> {
        use raw::FMOD_Studio_Bus_GetPaused;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_Bus_GetPaused(self.bus, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init() != 0)
        }
    }

    ///
    /// FMOD_Studio_Bus_SetPaused
    ///
    pub fn set_paused(&self, paused: bool) -> Result<()> {
        use raw::FMOD_Studio_Bus_SetPaused;

        unsafe {
            let paused = if paused { 1 } else { 0 };
            let err = FMOD_Studio_Bus_SetPaused(self.bus, paused);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_Bus_GetMute
    ///
    pub fn get_mute(&self) -> Result<bool> {
        use raw::FMOD_Studio_Bus_GetMute;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_Bus_GetMute(self.bus, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init() != 0)
        }
    }

    ///
    /// FMOD_Studio_Bus_SetMute
    ///
    pub fn set_mute(&self, paused: bool) -> Result<()> {
        use raw::FMOD_Studio_Bus_SetMute;

        unsafe {
            let paused = if paused { 1 } else { 0 };
            let err = FMOD_Studio_Bus_SetMute(self.bus, paused);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_Bus_StopAllEvents
    ///
    pub fn stop_all_events(&self, mode: StopMode) -> Result<()> {
        use raw::FMOD_Studio_Bus_StopAllEvents;

        unsafe {
            let err = FMOD_Studio_Bus_StopAllEvents(self.bus, mode.into_ffi());
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_Bus_LockChannelGroup
    ///
    pub fn lock_channel_group(&self) -> Result<()> {
        use raw::FMOD_Studio_Bus_LockChannelGroup;
        unsafe {
            let err = FMOD_Studio_Bus_LockChannelGroup(self.bus);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_Bus_UnlockChannelGroup
    ///
    pub fn unlock_channel_group(&self) -> Result<()> {
        use raw::FMOD_Studio_Bus_UnlockChannelGroup;
        unsafe {
            let err = FMOD_Studio_Bus_UnlockChannelGroup(self.bus);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_Bus_GetChannelGroup
    ///
    pub fn get_channel_group(&self) -> Result<ChannelGroup> {
        use raw::FMOD_Studio_Bus_GetChannelGroup;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_Bus_GetChannelGroup(self.bus, val.as_mut_ptr());
            make_result(val, err).map(|val| ChannelGroup {
                channel_group: val.assume_init(),
            })
        }
    }

    ///
    /// FMOD_Studio_Bus_GetCPUUsage
    ///
    pub fn get_cpu_usage(&self) -> Result<(c_uint, c_uint)> {
        use raw::FMOD_Studio_Bus_GetCPUUsage;

        unsafe {
            let mut val1 = MaybeUninit::uninit();
            let mut val2 = MaybeUninit::uninit();
            let err = FMOD_Studio_Bus_GetCPUUsage(self.bus, val1.as_mut_ptr(), val2.as_mut_ptr());
            make_result((val1, val2), err)
                .map(|(val1, val2)| (val1.assume_init(), val2.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_Bus_GetMemoryUsage
    ///
    pub fn get_memory_usage(&self) -> Result<MemoryUsage> {
        use raw::FMOD_Studio_Bus_GetMemoryUsage;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_Bus_GetMemoryUsage(self.bus, val.as_mut_ptr());
            make_result(val, err).map(|val| MemoryUsage::from_ffi(val.assume_init()))
        }
    }
}
