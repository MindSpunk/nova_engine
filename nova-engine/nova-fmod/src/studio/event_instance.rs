/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::core::make_result;
use crate::core::ChannelGroup;
use crate::core::{Attributes3D, Result};
use crate::studio::{
    EventCallbackType, EventProperty, MemoryUsage, ParameterID, PlaybackState, StopMode,
};
use raw::FMOD_STUDIO_EVENTINSTANCE;
use std::ffi::CStr;
use std::mem::MaybeUninit;
use std::os::raw::{c_int, c_uint};

#[derive(Copy, Clone)]
pub struct EventInstance {
    pub(crate) event_instance: *mut FMOD_STUDIO_EVENTINSTANCE,
}

impl EventInstance {
    ///
    /// Gets a null handle
    ///
    pub fn null() -> Self {
        Self {
            event_instance: std::ptr::null_mut(),
        }
    }

    ///
    /// Creates a handle from the ffi type
    ///
    pub fn from_ffi(ffi: *mut raw::FMOD_STUDIO_EVENTINSTANCE) -> Self {
        Self {
            event_instance: ffi,
        }
    }

    ///
    /// FMOD_Studio_EventInstance_IsValid
    ///
    pub fn is_valid(&self) -> bool {
        use raw::FMOD_Studio_EventInstance_IsValid;

        unsafe {
            let val = FMOD_Studio_EventInstance_IsValid(self.event_instance);
            val != 0
        }
    }

    ///
    /// FMOD_Studio_EventInstance_GetVolume
    ///
    pub fn get_volume(&self) -> Result<(f32, f32)> {
        use raw::FMOD_Studio_EventInstance_GetVolume;

        unsafe {
            let mut val1 = MaybeUninit::uninit();
            let mut val2 = MaybeUninit::uninit();
            let err = FMOD_Studio_EventInstance_GetVolume(
                self.event_instance,
                val1.as_mut_ptr(),
                val2.as_mut_ptr(),
            );
            make_result((val1, val2), err)
                .map(|(val1, val2)| (val1.assume_init(), val2.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_EventInstance_SetVolume
    ///
    pub fn set_volume(&self, volume: f32) -> Result<()> {
        use raw::FMOD_Studio_EventInstance_SetVolume;

        unsafe {
            let err = FMOD_Studio_EventInstance_SetVolume(self.event_instance, volume);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_EventInstance_GetPitch
    ///
    pub fn get_pitch(&self) -> Result<(f32, f32)> {
        use raw::FMOD_Studio_EventInstance_GetPitch;

        unsafe {
            let mut val1 = MaybeUninit::uninit();
            let mut val2 = MaybeUninit::uninit();
            let err = FMOD_Studio_EventInstance_GetPitch(
                self.event_instance,
                val1.as_mut_ptr(),
                val2.as_mut_ptr(),
            );
            make_result((val1, val2), err)
                .map(|(val1, val2)| (val1.assume_init(), val2.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_EventInstance_SetPitch
    ///
    pub fn set_pitch(&self, volume: f32) -> Result<()> {
        use raw::FMOD_Studio_EventInstance_SetPitch;

        unsafe {
            let err = FMOD_Studio_EventInstance_SetPitch(self.event_instance, volume);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_EventInstance_Get3DAttributes
    ///
    pub fn get_3d_attributes(&self) -> Result<Attributes3D> {
        use raw::FMOD_Studio_EventInstance_Get3DAttributes;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err =
                FMOD_Studio_EventInstance_Get3DAttributes(self.event_instance, val.as_mut_ptr());
            make_result(val, err).map(|val| Attributes3D::from_ffi(val.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_EventInstance_Set3DAttributes
    ///
    pub fn set_3d_attributes(&self, attributes: &Attributes3D) -> Result<()> {
        use raw::FMOD_Studio_EventInstance_Set3DAttributes;

        unsafe {
            let attributes = attributes as *const Attributes3D;
            let attributes = attributes as *mut Attributes3D;
            let attributes = attributes as *mut raw::FMOD_3D_ATTRIBUTES;
            let err = FMOD_Studio_EventInstance_Set3DAttributes(self.event_instance, attributes);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_EventInstance_GetListenerMask
    ///
    pub fn get_listener_mask(&self) -> Result<c_uint> {
        use raw::FMOD_Studio_EventInstance_GetListenerMask;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err =
                FMOD_Studio_EventInstance_GetListenerMask(self.event_instance, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init())
        }
    }

    ///
    /// FMOD_Studio_EventInstance_SetListenerMask
    ///
    pub fn set_listener_mask(&self, mask: c_uint) -> Result<()> {
        use raw::FMOD_Studio_EventInstance_SetListenerMask;

        unsafe {
            let err = FMOD_Studio_EventInstance_SetListenerMask(self.event_instance, mask);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_EventInstance_GetProperty
    ///
    pub fn get_event_property(&self, index: EventProperty) -> Result<f32> {
        use raw::FMOD_Studio_EventInstance_GetProperty;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_EventInstance_GetProperty(
                self.event_instance,
                index.into_ffi(),
                val.as_mut_ptr(),
            );
            make_result(val, err).map(|val| val.assume_init())
        }
    }

    ///
    /// FMOD_Studio_EventInstance_SetProperty
    ///
    pub fn set_event_property(&self, index: EventProperty, value: f32) -> Result<()> {
        use raw::FMOD_Studio_EventInstance_SetProperty;

        unsafe {
            let err =
                FMOD_Studio_EventInstance_SetProperty(self.event_instance, index.into_ffi(), value);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_EventInstance_GetReverbLevel
    ///
    pub fn get_reverb_level(&self, index: usize) -> Result<f32> {
        use raw::FMOD_Studio_EventInstance_GetReverbLevel;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_EventInstance_GetReverbLevel(
                self.event_instance,
                index as c_int,
                val.as_mut_ptr(),
            );
            make_result(val, err).map(|val| val.assume_init())
        }
    }

    ///
    /// FMOD_Studio_EventInstance_SetReverbLevel
    ///
    pub fn set_reverb_level(&self, index: usize, value: f32) -> Result<()> {
        use raw::FMOD_Studio_EventInstance_SetReverbLevel;

        unsafe {
            let err = FMOD_Studio_EventInstance_SetReverbLevel(
                self.event_instance,
                index as c_int,
                value,
            );
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_EventInstance_GetPaused
    ///
    pub fn get_paused(&self) -> Result<bool> {
        use raw::FMOD_Studio_EventInstance_GetPaused;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_EventInstance_GetPaused(self.event_instance, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init() != 0)
        }
    }

    ///
    /// FMOD_Studio_EventInstance_SetPaused
    ///
    pub fn set_paused(&self, paused: bool) -> Result<()> {
        use raw::FMOD_Studio_EventInstance_SetPaused;

        unsafe {
            let paused = if paused { 1 } else { 0 };
            let err = FMOD_Studio_EventInstance_SetPaused(self.event_instance, paused);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_EventInstance_Start
    ///
    pub fn start(&self) -> Result<()> {
        use raw::FMOD_Studio_EventInstance_Start;

        unsafe {
            let err = FMOD_Studio_EventInstance_Start(self.event_instance);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_EventInstance_Stop
    ///
    pub fn stop(&self, mode: StopMode) -> Result<()> {
        use raw::FMOD_Studio_EventInstance_Stop;

        unsafe {
            let err = FMOD_Studio_EventInstance_Stop(self.event_instance, mode.into_ffi());
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_EventInstance_GetTimelinePosition
    ///
    pub fn get_timeline_position(&self) -> Result<c_int> {
        use raw::FMOD_Studio_EventInstance_GetTimelinePosition;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_EventInstance_GetTimelinePosition(
                self.event_instance,
                val.as_mut_ptr(),
            );
            make_result(val, err).map(|val| val.assume_init())
        }
    }

    ///
    /// FMOD_Studio_EventInstance_SetTimelinePosition
    ///
    pub fn set_timeline_position(&self, position: c_int) -> Result<()> {
        use raw::FMOD_Studio_EventInstance_SetTimelinePosition;

        unsafe {
            let err = FMOD_Studio_EventInstance_SetTimelinePosition(self.event_instance, position);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_EventInstance_GetPlaybackState
    ///
    pub fn get_playback_state(&self) -> Result<PlaybackState> {
        use raw::FMOD_Studio_EventInstance_GetPlaybackState;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err =
                FMOD_Studio_EventInstance_GetPlaybackState(self.event_instance, val.as_mut_ptr());
            make_result(val, err).map(|val| PlaybackState::from_ffi(val.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_EventInstance_GetChannelGroup
    ///
    pub fn get_channel_group(&self) -> Result<ChannelGroup> {
        use raw::FMOD_Studio_EventInstance_GetChannelGroup;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err =
                FMOD_Studio_EventInstance_GetChannelGroup(self.event_instance, val.as_mut_ptr());
            make_result(val, err).map(|val| ChannelGroup {
                channel_group: val.assume_init(),
            })
        }
    }

    ///
    /// FMOD_Studio_EventInstance_Release
    ///
    pub fn release(&self) -> Result<()> {
        use raw::FMOD_Studio_EventInstance_Release;

        unsafe {
            let err = FMOD_Studio_EventInstance_Release(self.event_instance);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_EventInstance_IsVirtual
    ///
    pub fn is_virtual(&self) -> Result<bool> {
        use raw::FMOD_Studio_EventInstance_IsVirtual;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_EventInstance_IsVirtual(self.event_instance, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init() != 0)
        }
    }

    ///
    /// FMOD_Studio_EventInstance_GetParameterByName
    ///
    pub fn get_parameter_by_name(&self, name: &CStr) -> Result<(f32, f32)> {
        use raw::FMOD_Studio_EventInstance_GetParameterByName;

        unsafe {
            let mut val1 = MaybeUninit::uninit();
            let mut val2 = MaybeUninit::uninit();
            let err = FMOD_Studio_EventInstance_GetParameterByName(
                self.event_instance,
                name.as_ptr(),
                val1.as_mut_ptr(),
                val2.as_mut_ptr(),
            );
            make_result((val1, val2), err)
                .map(|(val1, val2)| (val1.assume_init(), val2.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_EventInstance_SetParameterByName
    ///
    pub fn set_parameter_by_name(
        &self,
        name: &CStr,
        value: f32,
        ignore_seek_speed: bool,
    ) -> Result<()> {
        use raw::FMOD_Studio_EventInstance_SetParameterByName;

        unsafe {
            let ignoreseekspeed = if ignore_seek_speed { 1 } else { 0 };
            let err = FMOD_Studio_EventInstance_SetParameterByName(
                self.event_instance,
                name.as_ptr(),
                value,
                ignoreseekspeed,
            );
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_EventInstance_GetParameterByID
    ///
    pub fn get_parameter_by_id(&self, id: ParameterID) -> Result<(f32, f32)> {
        use raw::FMOD_Studio_EventInstance_GetParameterByID;

        unsafe {
            let mut val1 = MaybeUninit::uninit();
            let mut val2 = MaybeUninit::uninit();
            let err = FMOD_Studio_EventInstance_GetParameterByID(
                self.event_instance,
                id.into_ffi(),
                val1.as_mut_ptr(),
                val2.as_mut_ptr(),
            );
            make_result((val1, val2), err)
                .map(|(val1, val2)| (val1.assume_init(), val2.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_EventInstance_SetParameterByID
    ///
    pub fn set_parameter_by_id(
        &self,
        id: ParameterID,
        value: f32,
        ignore_seek_speed: bool,
    ) -> Result<()> {
        use raw::FMOD_Studio_EventInstance_SetParameterByID;

        unsafe {
            let ignoreseekspeed = if ignore_seek_speed { 1 } else { 0 };
            let err = FMOD_Studio_EventInstance_SetParameterByID(
                self.event_instance,
                id.into_ffi(),
                value,
                ignoreseekspeed,
            );
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_EventInstance_SetParametersByIDs
    ///
    /// Will use the length of the shortest array
    ///
    pub fn set_parameters_by_ids(
        &self,
        ids: &[ParameterID],
        values: &[f32],
        ignore_seek_speed: bool,
    ) -> Result<()> {
        use raw::FMOD_Studio_EventInstance_SetParametersByIDs;
        unsafe {
            let ignoreseekspeed = if ignore_seek_speed { 1 } else { 0 };
            let err = FMOD_Studio_EventInstance_SetParametersByIDs(
                self.event_instance,
                ids.as_ptr() as *const raw::FMOD_STUDIO_PARAMETER_ID,
                values.as_ptr() as *mut f32,
                usize::min(ids.len(), values.len()) as _,
                ignoreseekspeed,
            );
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_EventInstance_TriggerCue
    ///
    pub fn trigger_cue(&self) -> Result<()> {
        use raw::FMOD_Studio_EventInstance_TriggerCue;

        unsafe {
            let err = FMOD_Studio_EventInstance_TriggerCue(self.event_instance);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_EventInstance_SetCallback
    ///
    pub fn set_callback(
        &self,
        callback: raw::FMOD_STUDIO_EVENT_CALLBACK,
        callback_mask: EventCallbackType,
    ) -> Result<()> {
        use raw::FMOD_Studio_EventInstance_SetCallback;

        unsafe {
            let err = FMOD_Studio_EventInstance_SetCallback(
                self.event_instance,
                callback,
                callback_mask.bits(),
            );
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_EventInstance_GetUserData
    ///
    pub unsafe fn get_userdata(&self) -> Result<*mut ()> {
        use raw::FMOD_Studio_EventInstance_GetUserData;

        let mut val = MaybeUninit::uninit();
        let err = FMOD_Studio_EventInstance_GetUserData(self.event_instance, val.as_mut_ptr());
        make_result(val, err).map(|val| val.assume_init() as *mut ())
    }

    ///
    /// FMOD_Studio_EventInstance_SetUserData
    ///
    pub unsafe fn set_userdata(&self, userdata: *mut ()) -> Result<()> {
        use raw::FMOD_Studio_EventInstance_SetUserData;

        let err = FMOD_Studio_EventInstance_SetUserData(self.event_instance, userdata as *mut _);
        make_result((), err)
    }

    ///
    /// FMOD_Studio_EventInstance_GetCPUUsage
    ///
    pub fn get_cpu_usage(&self) -> Result<(c_uint, c_uint)> {
        use raw::FMOD_Studio_EventInstance_GetCPUUsage;

        unsafe {
            let mut val1 = MaybeUninit::uninit();
            let mut val2 = MaybeUninit::uninit();
            let err = FMOD_Studio_EventInstance_GetCPUUsage(
                self.event_instance,
                val1.as_mut_ptr(),
                val2.as_mut_ptr(),
            );
            make_result((val1, val2), err)
                .map(|(val1, val2)| (val1.assume_init(), val2.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_EventInstance_GetMemoryUsage
    ///
    pub fn get_memory_usage(&self) -> Result<MemoryUsage> {
        use raw::FMOD_Studio_EventInstance_GetMemoryUsage;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err =
                FMOD_Studio_EventInstance_GetMemoryUsage(self.event_instance, val.as_mut_ptr());
            make_result(val, err).map(|val| MemoryUsage::from_ffi(val.assume_init()))
        }
    }
}
