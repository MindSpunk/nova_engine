/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::core::{make_result, Result};
use crate::studio::{CommandInfo, PlaybackState, System};
use std::ffi::CStr;
use std::mem::MaybeUninit;
use std::os::raw::c_char;

#[derive(Copy, Clone)]
pub struct CommandReplay {
    pub(crate) command_replay: *mut raw::FMOD_STUDIO_COMMANDREPLAY,
}

impl CommandReplay {
    ///
    /// Gets a null handle
    ///
    pub fn null() -> Self {
        Self {
            command_replay: std::ptr::null_mut(),
        }
    }

    ///
    /// Creates a handle from the ffi type
    ///
    pub fn from_ffi(ffi: *mut raw::FMOD_STUDIO_COMMANDREPLAY) -> Self {
        Self {
            command_replay: ffi,
        }
    }

    ///
    /// FMOD_Studio_CommandReplay_IsValid
    ///
    pub fn is_valid(&self) -> bool {
        use raw::FMOD_Studio_CommandReplay_IsValid;
        unsafe {
            let val = FMOD_Studio_CommandReplay_IsValid(self.command_replay);
            val != 0
        }
    }

    ///
    /// FMOD_Studio_CommandReplay_GetSystem
    ///
    pub fn get_system(&self) -> Result<System> {
        use raw::FMOD_Studio_CommandReplay_GetSystem;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_CommandReplay_GetSystem(self.command_replay, val.as_mut_ptr());
            make_result(val, err).map(|val| System {
                system: val.assume_init(),
            })
        }
    }

    ///
    /// FMOD_Studio_CommandReplay_GetLength
    ///
    pub fn get_length(&self) -> Result<f32> {
        use raw::FMOD_Studio_CommandReplay_GetLength;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_CommandReplay_GetLength(self.command_replay, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init())
        }
    }

    ///
    /// FMOD_Studio_CommandReplay_GetCommandCount
    ///
    pub fn get_command_count(&self) -> Result<usize> {
        use raw::FMOD_Studio_CommandReplay_GetCommandCount;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err =
                FMOD_Studio_CommandReplay_GetCommandCount(self.command_replay, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init() as usize)
        }
    }

    ///
    /// FMOD_Studio_CommandReplay_GetCommandInfo
    ///
    pub fn get_command_info(&self, commandindex: usize) -> Result<CommandInfo> {
        use raw::FMOD_Studio_CommandReplay_GetCommandInfo;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_CommandReplay_GetCommandInfo(
                self.command_replay,
                commandindex as _,
                val.as_mut_ptr(),
            );
            make_result(val, err).map(|val| CommandInfo::from_ffi(val.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_CommandReplay_GetCommandString
    ///
    pub fn get_command_string(&self, commandindex: usize, buffer: &mut [c_char]) -> Result<()> {
        use raw::FMOD_Studio_CommandReplay_GetCommandString;

        unsafe {
            let err = FMOD_Studio_CommandReplay_GetCommandString(
                self.command_replay,
                commandindex as _,
                buffer.as_mut_ptr(),
                buffer.len() as _,
            );
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_CommandReplay_GetCommandAtTime
    ///
    pub fn get_command_at_time(&self, time: f32) -> Result<usize> {
        use raw::FMOD_Studio_CommandReplay_GetCommandAtTime;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_CommandReplay_GetCommandAtTime(
                self.command_replay,
                time,
                val.as_mut_ptr(),
            );
            make_result(val, err).map(|val| val.assume_init() as usize)
        }
    }

    ///
    /// FMOD_Studio_CommandReplay_SetBankPath
    ///
    pub fn set_bank_path(&self, bank_path: &CStr) -> Result<()> {
        use raw::FMOD_Studio_CommandReplay_SetBankPath;

        unsafe {
            let err =
                FMOD_Studio_CommandReplay_SetBankPath(self.command_replay, bank_path.as_ptr());
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_CommandReplay_Start
    ///
    pub fn start(&self) -> Result<()> {
        use raw::FMOD_Studio_CommandReplay_Start;
        unsafe {
            let err = FMOD_Studio_CommandReplay_Start(self.command_replay);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_CommandReplay_Stop
    ///
    pub fn stop(&self) -> Result<()> {
        use raw::FMOD_Studio_CommandReplay_Stop;
        unsafe {
            let err = FMOD_Studio_CommandReplay_Stop(self.command_replay);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_CommandReplay_SeekToTime
    ///
    pub fn seek_to_time(&self, time: f32) -> Result<()> {
        use raw::FMOD_Studio_CommandReplay_SeekToTime;
        unsafe {
            let err = FMOD_Studio_CommandReplay_SeekToTime(self.command_replay, time);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_CommandReplay_SeekToCommand
    ///
    pub fn seek_to_command(&self, commandindex: usize) -> Result<()> {
        use raw::FMOD_Studio_CommandReplay_SeekToCommand;
        unsafe {
            let err =
                FMOD_Studio_CommandReplay_SeekToCommand(self.command_replay, commandindex as _);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_CommandReplay_GetPaused
    ///
    pub fn get_paused(&self) -> Result<bool> {
        use raw::FMOD_Studio_CommandReplay_GetPaused;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_CommandReplay_GetPaused(self.command_replay, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init() != 0)
        }
    }

    ///
    /// FMOD_Studio_CommandReplay_SetPaused
    ///
    pub fn set_paused(&self, paused: bool) -> Result<()> {
        use raw::FMOD_Studio_CommandReplay_SetPaused;

        unsafe {
            let paused = if paused { 1 } else { 0 };
            let err = FMOD_Studio_CommandReplay_SetPaused(self.command_replay, paused);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_CommandReplay_GetPlaybackState
    ///
    pub fn get_playback_state(&self) -> Result<PlaybackState> {
        use raw::FMOD_Studio_CommandReplay_GetPlaybackState;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err =
                FMOD_Studio_CommandReplay_GetPlaybackState(self.command_replay, val.as_mut_ptr());
            make_result(val, err).map(|val| PlaybackState::from_ffi(val.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_CommandReplay_GetCurrentCommand
    ///
    pub fn get_current_command(&self) -> Result<(usize, f32)> {
        use raw::FMOD_Studio_CommandReplay_GetCurrentCommand;

        unsafe {
            let mut val1 = MaybeUninit::uninit();
            let mut val2 = MaybeUninit::uninit();
            let err = FMOD_Studio_CommandReplay_GetCurrentCommand(
                self.command_replay,
                val1.as_mut_ptr(),
                val2.as_mut_ptr(),
            );
            make_result((val1, val2), err)
                .map(|(val1, val2)| (val1.assume_init() as usize, val2.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_CommandReplay_Release
    ///
    pub fn release(&self) -> Result<()> {
        use raw::FMOD_Studio_CommandReplay_Release;
        unsafe {
            let err = FMOD_Studio_CommandReplay_Release(self.command_replay);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_CommandReplay_SetFrameCallback
    ///
    pub fn set_frame_callback(
        &self,
        callback: raw::FMOD_STUDIO_COMMANDREPLAY_FRAME_CALLBACK,
    ) -> Result<()> {
        use raw::FMOD_Studio_CommandReplay_SetFrameCallback;

        unsafe {
            let err = FMOD_Studio_CommandReplay_SetFrameCallback(self.command_replay, callback);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_CommandReplay_SetLoadBankCallback
    ///
    pub fn set_load_bank_callback(
        &self,
        callback: raw::FMOD_STUDIO_COMMANDREPLAY_LOAD_BANK_CALLBACK,
    ) -> Result<()> {
        use raw::FMOD_Studio_CommandReplay_SetLoadBankCallback;

        unsafe {
            let err = FMOD_Studio_CommandReplay_SetLoadBankCallback(self.command_replay, callback);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_CommandReplay_SetCreateInstanceCallback
    ///
    pub fn set_create_instance_callback(
        &self,
        callback: raw::FMOD_STUDIO_COMMANDREPLAY_CREATE_INSTANCE_CALLBACK,
    ) -> Result<()> {
        use raw::FMOD_Studio_CommandReplay_SetCreateInstanceCallback;

        unsafe {
            let err =
                FMOD_Studio_CommandReplay_SetCreateInstanceCallback(self.command_replay, callback);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_CommandReplay_GetUserData
    ///
    pub fn get_userdata(&self) -> Result<*mut ()> {
        use raw::FMOD_Studio_CommandReplay_GetUserData;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_CommandReplay_GetUserData(self.command_replay, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init() as *mut ())
        }
    }

    ///
    /// FMOD_Studio_CommandReplay_SetUserData
    ///
    pub fn set_userdata(&self, userdata: *mut ()) -> Result<()> {
        use raw::FMOD_Studio_CommandReplay_SetUserData;

        unsafe {
            let err =
                FMOD_Studio_CommandReplay_SetUserData(self.command_replay, userdata as *mut _);
            make_result((), err)
        }
    }
}
