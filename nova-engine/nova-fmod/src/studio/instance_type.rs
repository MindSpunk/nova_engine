/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::raw::FMOD_STUDIO_INSTANCETYPE;

///
/// FMOD_STUDIO_INSTANCETYPE
///
#[repr(u32)]
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum InstanceType {
    None = 0,
    System = 1,
    EventDescription = 2,
    EventInstance = 3,
    ParameterInstance = 4,
    Bus = 5,
    VCA = 6,
    Bank = 7,
    CommandReplay = 8,
}

impl InstanceType {
    pub fn from_ffi(ffi: raw::FMOD_STUDIO_INSTANCETYPE) -> Self {
        match ffi {
            FMOD_STUDIO_INSTANCETYPE::FMOD_STUDIO_INSTANCETYPE_NONE => InstanceType::None,
            FMOD_STUDIO_INSTANCETYPE::FMOD_STUDIO_INSTANCETYPE_SYSTEM => InstanceType::System,
            FMOD_STUDIO_INSTANCETYPE::FMOD_STUDIO_INSTANCETYPE_EVENTDESCRIPTION => {
                InstanceType::EventDescription
            }
            FMOD_STUDIO_INSTANCETYPE::FMOD_STUDIO_INSTANCETYPE_EVENTINSTANCE => {
                InstanceType::EventInstance
            }
            FMOD_STUDIO_INSTANCETYPE::FMOD_STUDIO_INSTANCETYPE_PARAMETERINSTANCE => {
                InstanceType::ParameterInstance
            }
            FMOD_STUDIO_INSTANCETYPE::FMOD_STUDIO_INSTANCETYPE_BUS => InstanceType::Bus,
            FMOD_STUDIO_INSTANCETYPE::FMOD_STUDIO_INSTANCETYPE_VCA => InstanceType::VCA,
            FMOD_STUDIO_INSTANCETYPE::FMOD_STUDIO_INSTANCETYPE_BANK => InstanceType::Bank,
            FMOD_STUDIO_INSTANCETYPE::FMOD_STUDIO_INSTANCETYPE_COMMANDREPLAY => {
                InstanceType::CommandReplay
            }
            FMOD_STUDIO_INSTANCETYPE::FMOD_STUDIO_INSTANCETYPE_FORCEINT => unreachable!(),
        }
    }

    pub fn into_ffi(self) -> raw::FMOD_STUDIO_INSTANCETYPE {
        match self {
            InstanceType::None => raw::FMOD_STUDIO_INSTANCETYPE::FMOD_STUDIO_INSTANCETYPE_NONE,
            InstanceType::System => raw::FMOD_STUDIO_INSTANCETYPE::FMOD_STUDIO_INSTANCETYPE_SYSTEM,
            InstanceType::EventDescription => {
                raw::FMOD_STUDIO_INSTANCETYPE::FMOD_STUDIO_INSTANCETYPE_EVENTDESCRIPTION
            }
            InstanceType::EventInstance => {
                raw::FMOD_STUDIO_INSTANCETYPE::FMOD_STUDIO_INSTANCETYPE_EVENTINSTANCE
            }
            InstanceType::ParameterInstance => {
                raw::FMOD_STUDIO_INSTANCETYPE::FMOD_STUDIO_INSTANCETYPE_PARAMETERINSTANCE
            }
            InstanceType::Bus => raw::FMOD_STUDIO_INSTANCETYPE::FMOD_STUDIO_INSTANCETYPE_BUS,
            InstanceType::VCA => raw::FMOD_STUDIO_INSTANCETYPE::FMOD_STUDIO_INSTANCETYPE_VCA,
            InstanceType::Bank => raw::FMOD_STUDIO_INSTANCETYPE::FMOD_STUDIO_INSTANCETYPE_BANK,
            InstanceType::CommandReplay => {
                raw::FMOD_STUDIO_INSTANCETYPE::FMOD_STUDIO_INSTANCETYPE_COMMANDREPLAY
            }
        }
    }
}
