/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

///
/// FMOD_STUDIO_EVENT_PROPERTY
///
#[repr(u32)]
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum EventProperty {
    ChannelPriority = 0,
    ScheduleDelay = 1,
    ScheduleLookahead = 2,
    MinimumDistance = 3,
    MaximumDistance = 4,
    Cooldown = 5,
    Max = 6,
}

impl EventProperty {
    pub fn from_ffi(ffi: raw::FMOD_STUDIO_EVENT_PROPERTY) -> Self {
        match ffi {
            raw::FMOD_STUDIO_EVENT_PROPERTY::FMOD_STUDIO_EVENT_PROPERTY_CHANNELPRIORITY => {
                EventProperty::ChannelPriority
            }
            raw::FMOD_STUDIO_EVENT_PROPERTY::FMOD_STUDIO_EVENT_PROPERTY_SCHEDULE_DELAY => {
                EventProperty::ScheduleDelay
            }
            raw::FMOD_STUDIO_EVENT_PROPERTY::FMOD_STUDIO_EVENT_PROPERTY_SCHEDULE_LOOKAHEAD => {
                EventProperty::ScheduleLookahead
            }
            raw::FMOD_STUDIO_EVENT_PROPERTY::FMOD_STUDIO_EVENT_PROPERTY_MINIMUM_DISTANCE => {
                EventProperty::MinimumDistance
            }
            raw::FMOD_STUDIO_EVENT_PROPERTY::FMOD_STUDIO_EVENT_PROPERTY_MAXIMUM_DISTANCE => {
                EventProperty::MaximumDistance
            }
            raw::FMOD_STUDIO_EVENT_PROPERTY::FMOD_STUDIO_EVENT_PROPERTY_COOLDOWN => {
                EventProperty::Cooldown
            }
            raw::FMOD_STUDIO_EVENT_PROPERTY::FMOD_STUDIO_EVENT_PROPERTY_MAX => EventProperty::Max,
            raw::FMOD_STUDIO_EVENT_PROPERTY::FMOD_STUDIO_EVENT_PROPERTY_FORCEINT => unreachable!(),
        }
    }

    pub fn into_ffi(self) -> raw::FMOD_STUDIO_EVENT_PROPERTY {
        match self {
            EventProperty::ChannelPriority => {
                raw::FMOD_STUDIO_EVENT_PROPERTY::FMOD_STUDIO_EVENT_PROPERTY_CHANNELPRIORITY
            }
            EventProperty::ScheduleDelay => {
                raw::FMOD_STUDIO_EVENT_PROPERTY::FMOD_STUDIO_EVENT_PROPERTY_SCHEDULE_DELAY
            }
            EventProperty::ScheduleLookahead => {
                raw::FMOD_STUDIO_EVENT_PROPERTY::FMOD_STUDIO_EVENT_PROPERTY_SCHEDULE_LOOKAHEAD
            }
            EventProperty::MinimumDistance => {
                raw::FMOD_STUDIO_EVENT_PROPERTY::FMOD_STUDIO_EVENT_PROPERTY_MINIMUM_DISTANCE
            }
            EventProperty::MaximumDistance => {
                raw::FMOD_STUDIO_EVENT_PROPERTY::FMOD_STUDIO_EVENT_PROPERTY_MAXIMUM_DISTANCE
            }
            EventProperty::Cooldown => {
                raw::FMOD_STUDIO_EVENT_PROPERTY::FMOD_STUDIO_EVENT_PROPERTY_COOLDOWN
            }
            EventProperty::Max => raw::FMOD_STUDIO_EVENT_PROPERTY::FMOD_STUDIO_EVENT_PROPERTY_MAX,
        }
    }
}
