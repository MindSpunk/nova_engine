/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

///
/// FMOD_STUDIO_CPU_USAGE
///
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct CPUUsage {
    pub dsp_usage: f32,
    pub stream_usage: f32,
    pub geometry_usage: f32,
    pub update_usage: f32,
    pub studio_usage: f32,
}

impl CPUUsage {
    pub fn from_ffi(ffi: raw::FMOD_STUDIO_CPU_USAGE) -> Self {
        Self {
            dsp_usage: ffi.dspusage,
            stream_usage: ffi.streamusage,
            geometry_usage: ffi.geometryusage,
            update_usage: ffi.updateusage,
            studio_usage: ffi.studiousage,
        }
    }

    pub fn into_ffi(self) -> raw::FMOD_STUDIO_CPU_USAGE {
        raw::FMOD_STUDIO_CPU_USAGE {
            dspusage: self.dsp_usage,
            streamusage: self.stream_usage,
            geometryusage: self.geometry_usage,
            updateusage: self.update_usage,
            studiousage: self.studio_usage,
        }
    }
}
