/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use std::os::raw::c_int;

///
/// FMOD_STUDIO_BUFFER_INFO
///
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct BufferInfo {
    pub current_usage: c_int,
    pub peak_usage: c_int,
    pub capacity: c_int,
    pub stall_count: c_int,
    pub stall_time: f32,
}

impl BufferInfo {
    pub fn from_ffi(ffi: raw::FMOD_STUDIO_BUFFER_INFO) -> Self {
        Self {
            current_usage: ffi.currentusage,
            peak_usage: ffi.peakusage,
            capacity: ffi.capacity,
            stall_count: ffi.stallcount,
            stall_time: ffi.stalltime,
        }
    }

    pub fn into_ffi(self) -> raw::FMOD_STUDIO_BUFFER_INFO {
        raw::FMOD_STUDIO_BUFFER_INFO {
            currentusage: self.current_usage,
            peakusage: self.peak_usage,
            capacity: self.capacity,
            stallcount: self.stall_count,
            stalltime: self.stall_time,
        }
    }
}
