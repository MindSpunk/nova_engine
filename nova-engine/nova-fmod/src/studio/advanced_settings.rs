/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use raw::FMOD_STUDIO_ADVANCEDSETTINGS;
use std::ffi::CStr;
use std::os::raw::c_int;
use std::os::raw::c_uint;

///
/// FMOD_STUDIO_ADVANCEDSETTINGS
///
#[repr(C)]
#[derive(Debug, Clone)]
pub struct AdvancedSettings<'a> {
    pub cb_size: c_int,
    pub command_queue_size: c_uint,
    pub handle_initial_size: c_uint,
    pub studio_update_period: c_int,
    pub idle_sample_data_pool_size: c_int,
    pub streaming_schedule_delay: c_uint,
    pub encryption_key: &'a CStr,
}

impl<'a> AdvancedSettings<'a> {
    pub unsafe fn from_ffi(ffi: FMOD_STUDIO_ADVANCEDSETTINGS) -> Self {
        Self {
            cb_size: ffi.cbsize,
            command_queue_size: ffi.commandqueuesize,
            handle_initial_size: ffi.handleinitialsize,
            studio_update_period: ffi.studioupdateperiod,
            idle_sample_data_pool_size: ffi.idlesampledatapoolsize,
            streaming_schedule_delay: ffi.streamingscheduledelay,
            encryption_key: CStr::from_ptr(ffi.encryptionkey),
        }
    }

    pub fn into_ffi(self) -> FMOD_STUDIO_ADVANCEDSETTINGS {
        FMOD_STUDIO_ADVANCEDSETTINGS {
            cbsize: self.cb_size,
            commandqueuesize: self.command_queue_size,
            handleinitialsize: self.handle_initial_size,
            studioupdateperiod: self.studio_update_period,
            idlesampledatapoolsize: self.idle_sample_data_pool_size,
            streamingscheduledelay: self.streaming_schedule_delay,
            encryptionkey: self.encryption_key.as_ptr(),
        }
    }
}
