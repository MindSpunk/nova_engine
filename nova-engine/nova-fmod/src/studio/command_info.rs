/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::studio::instance_type::InstanceType;
use std::ffi::CStr;
use std::os::raw::{c_int, c_uint};

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct CommandInfo<'a> {
    pub command_name: &'a CStr,
    pub parent_command_index: c_int,
    pub frame_number: c_int,
    pub frame_time: f32,
    pub instance_type: InstanceType,
    pub output_type: InstanceType,
    pub instance_handle: c_uint,
    pub output_handle: c_uint,
}

impl<'a> CommandInfo<'a> {
    pub unsafe fn from_ffi(ffi: raw::FMOD_STUDIO_COMMAND_INFO) -> Self {
        Self {
            command_name: CStr::from_ptr(ffi.commandname),
            parent_command_index: ffi.parentcommandindex,
            frame_number: ffi.framenumber,
            frame_time: ffi.frametime,
            instance_type: InstanceType::from_ffi(ffi.instancetype),
            output_type: InstanceType::from_ffi(ffi.outputtype),
            instance_handle: ffi.instancehandle,
            output_handle: ffi.outputhandle,
        }
    }

    pub fn into_ffi(self) -> raw::FMOD_STUDIO_COMMAND_INFO {
        raw::FMOD_STUDIO_COMMAND_INFO {
            commandname: self.command_name.as_ptr(),
            parentcommandindex: self.parent_command_index,
            framenumber: self.frame_number,
            frametime: self.frame_time,
            instancetype: self.instance_type.into_ffi(),
            outputtype: self.output_type.into_ffi(),
            instancehandle: self.instance_handle,
            outputhandle: self.output_handle,
        }
    }
}
