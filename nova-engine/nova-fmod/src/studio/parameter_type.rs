/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

///
/// FMOD_STUDIO_PARAMETER_TYPE
///
#[repr(u32)]
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum ParameterType {
    GameControlled = 0,
    AutomaticDistance = 1,
    AutomaticEventConeAngle = 2,
    AutomaticEventOrientation = 3,
    AutomaticDirection = 4,
    AutomaticElevation = 5,
    AutomaticListenerOrientation = 6,
    AutomaticSpeed = 7,
    Max = 8,
}

impl ParameterType {
    pub fn from_ffi(ffi: raw::FMOD_STUDIO_PARAMETER_TYPE) -> Self {
        match ffi {
            raw::FMOD_STUDIO_PARAMETER_TYPE::FMOD_STUDIO_PARAMETER_GAME_CONTROLLED => {
                ParameterType::GameControlled
            }
            raw::FMOD_STUDIO_PARAMETER_TYPE::FMOD_STUDIO_PARAMETER_AUTOMATIC_DISTANCE => {
                ParameterType::AutomaticDistance
            }
            raw::FMOD_STUDIO_PARAMETER_TYPE::FMOD_STUDIO_PARAMETER_AUTOMATIC_EVENT_CONE_ANGLE => {
                ParameterType::AutomaticEventConeAngle
            }
            raw::FMOD_STUDIO_PARAMETER_TYPE::FMOD_STUDIO_PARAMETER_AUTOMATIC_EVENT_ORIENTATION => {
                ParameterType::AutomaticEventOrientation
            }
            raw::FMOD_STUDIO_PARAMETER_TYPE::FMOD_STUDIO_PARAMETER_AUTOMATIC_DIRECTION => {
                ParameterType::AutomaticDirection
            }
            raw::FMOD_STUDIO_PARAMETER_TYPE::FMOD_STUDIO_PARAMETER_AUTOMATIC_ELEVATION => {
                ParameterType::AutomaticElevation
            }
            raw::FMOD_STUDIO_PARAMETER_TYPE::FMOD_STUDIO_PARAMETER_AUTOMATIC_LISTENER_ORIENTATION => {
                ParameterType::AutomaticListenerOrientation
            }
            raw::FMOD_STUDIO_PARAMETER_TYPE::FMOD_STUDIO_PARAMETER_AUTOMATIC_SPEED => {
                ParameterType::AutomaticSpeed
            }
            raw::FMOD_STUDIO_PARAMETER_TYPE::FMOD_STUDIO_PARAMETER_MAX => ParameterType::Max,
            raw::FMOD_STUDIO_PARAMETER_TYPE::FMOD_STUDIO_PARAMETER_FORCEINT => unreachable!(),
        }
    }

    pub fn into_ffi(self) -> raw::FMOD_STUDIO_PARAMETER_TYPE {
        match self {
            ParameterType::GameControlled => {
                raw::FMOD_STUDIO_PARAMETER_TYPE::FMOD_STUDIO_PARAMETER_GAME_CONTROLLED
            }
            ParameterType::AutomaticDistance => {
                raw::FMOD_STUDIO_PARAMETER_TYPE::FMOD_STUDIO_PARAMETER_AUTOMATIC_DISTANCE
            }
            ParameterType::AutomaticEventConeAngle => {
                raw::FMOD_STUDIO_PARAMETER_TYPE::FMOD_STUDIO_PARAMETER_AUTOMATIC_EVENT_CONE_ANGLE
            }
            ParameterType::AutomaticEventOrientation => {
                raw::FMOD_STUDIO_PARAMETER_TYPE::FMOD_STUDIO_PARAMETER_AUTOMATIC_EVENT_ORIENTATION
            }
            ParameterType::AutomaticDirection => {
                raw::FMOD_STUDIO_PARAMETER_TYPE::FMOD_STUDIO_PARAMETER_AUTOMATIC_DIRECTION
            }
            ParameterType::AutomaticElevation => {
                raw::FMOD_STUDIO_PARAMETER_TYPE::FMOD_STUDIO_PARAMETER_AUTOMATIC_ELEVATION
            }
            ParameterType::AutomaticListenerOrientation => {
                raw::FMOD_STUDIO_PARAMETER_TYPE::FMOD_STUDIO_PARAMETER_AUTOMATIC_LISTENER_ORIENTATION
            }
            ParameterType::AutomaticSpeed => {
                raw::FMOD_STUDIO_PARAMETER_TYPE::FMOD_STUDIO_PARAMETER_AUTOMATIC_SPEED
            }
            ParameterType::Max => raw::FMOD_STUDIO_PARAMETER_TYPE::FMOD_STUDIO_PARAMETER_MAX,
        }
    }
}
