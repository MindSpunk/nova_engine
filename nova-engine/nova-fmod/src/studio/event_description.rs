/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::core::make_result;
use crate::core::Guid;
use crate::core::Result;
use crate::studio::{
    EventCallbackType, EventInstance, LoadingState, ParameterDescription, ParameterID, UserProperty,
};
use bitflags::_core::mem::MaybeUninit;
use raw::FMOD_STUDIO_EVENTDESCRIPTION;
use std::ffi::{CStr, CString};
use std::os::raw::c_int;

#[repr(C)]
pub struct EventDescription {
    pub(crate) event_description: *mut FMOD_STUDIO_EVENTDESCRIPTION,
}

impl EventDescription {
    ///
    /// Gets a null handle
    ///
    pub fn null() -> Self {
        Self {
            event_description: std::ptr::null_mut(),
        }
    }

    ///
    /// Creates a handle from the ffi type
    ///
    pub fn from_ffi(ffi: *mut raw::FMOD_STUDIO_EVENTDESCRIPTION) -> Self {
        Self {
            event_description: ffi,
        }
    }

    ///
    /// FMOD_Studio_EventDescription_IsValid
    ///
    pub fn is_valid(&self) -> bool {
        use raw::FMOD_Studio_EventDescription_IsValid;

        unsafe {
            let val = FMOD_Studio_EventDescription_IsValid(self.event_description);
            val != 0
        }
    }

    ///
    /// FMOD_Studio_EventDescription_GetID
    ///
    pub fn get_id(&self) -> Result<Guid> {
        use raw::FMOD_Studio_EventDescription_GetID;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_EventDescription_GetID(self.event_description, val.as_mut_ptr());
            make_result(val, err).map(|val| Guid::from_ffi(val.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_EventDescription_GetPath
    ///
    pub fn get_path(&self) -> Result<CString> {
        use raw::FMOD_Studio_EventDescription_GetPath;

        unsafe {
            let mut retrieved = 0;
            let err = FMOD_Studio_EventDescription_GetPath(
                self.event_description,
                std::ptr::null_mut(),
                0,
                &mut retrieved,
            );
            make_result((), err)?;
            let mut out = Vec::<u8>::with_capacity(retrieved as usize);
            out.resize(retrieved as usize, 0);
            let err = FMOD_Studio_EventDescription_GetPath(
                self.event_description,
                out.as_mut_ptr() as *mut _,
                out.len() as _,
                std::ptr::null_mut(),
            );
            make_result(CString::from_vec_unchecked(out), err)
        }
    }

    ///
    /// FMOD_Studio_EventDescription_GetParameterDescriptionCount
    ///
    pub fn get_parameter_description_count(&self) -> Result<usize> {
        use raw::FMOD_Studio_EventDescription_GetParameterDescriptionCount;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_EventDescription_GetParameterDescriptionCount(
                self.event_description,
                val.as_mut_ptr(),
            );
            make_result(val, err).map(|val| val.assume_init() as usize)
        }
    }

    ///
    /// FMOD_Studio_EventDescription_GetParameterDescriptionByIndex
    ///
    pub fn get_parameter_description_by_index(&self, index: usize) -> Result<ParameterDescription> {
        use raw::FMOD_Studio_EventDescription_GetParameterDescriptionByIndex;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_EventDescription_GetParameterDescriptionByIndex(
                self.event_description,
                index as _,
                val.as_mut_ptr(),
            );
            make_result(val, err).map(|val| {
                let val = val.assume_init();
                let val = ParameterDescription::from_ffi(val);
                val
            })
        }
    }

    ///
    /// FMOD_Studio_EventDescription_GetParameterDescriptionByName
    ///
    pub fn get_parameter_description_by_name(&self, name: &CStr) -> Result<ParameterDescription> {
        use raw::FMOD_Studio_EventDescription_GetParameterDescriptionByName;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_EventDescription_GetParameterDescriptionByName(
                self.event_description,
                name.as_ptr(),
                val.as_mut_ptr(),
            );
            make_result(val, err).map(|val| {
                let val = val.assume_init();
                let val = ParameterDescription::from_ffi(val);
                val
            })
        }
    }

    ///
    /// FMOD_Studio_EventDescription_GetParameterDescriptionByID
    ///
    pub fn get_parameter_description_by_id(&self, id: ParameterID) -> Result<ParameterDescription> {
        use raw::FMOD_Studio_EventDescription_GetParameterDescriptionByID;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_EventDescription_GetParameterDescriptionByID(
                self.event_description,
                id.into_ffi(),
                val.as_mut_ptr(),
            );
            make_result(val, err).map(|val| {
                let val = val.assume_init();
                let val = ParameterDescription::from_ffi(val);
                val
            })
        }
    }

    ///
    /// FMOD_Studio_EventDescription_GetUserPropertyCount
    ///
    pub fn get_length_user_property_count(&self) -> Result<usize> {
        use raw::FMOD_Studio_EventDescription_GetUserPropertyCount;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_EventDescription_GetUserPropertyCount(
                self.event_description,
                val.as_mut_ptr(),
            );
            make_result(val, err).map(|val| val.assume_init() as usize)
        }
    }

    ///
    /// FMOD_Studio_EventDescription_GetUserPropertyByIndex
    ///
    pub fn get_user_property_by_index(&self, index: usize) -> Result<UserProperty> {
        use raw::FMOD_Studio_EventDescription_GetUserPropertyByIndex;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_EventDescription_GetUserPropertyByIndex(
                self.event_description,
                index as _,
                val.as_mut_ptr(),
            );
            make_result(val, err).map(|val| {
                let val = val.assume_init();
                let val = UserProperty::from_ffi(val);
                val
            })
        }
    }

    ///
    /// FMOD_Studio_EventDescription_GetUserProperty
    ///
    pub fn get_user_property(&self, name: &CStr) -> Result<UserProperty> {
        use raw::FMOD_Studio_EventDescription_GetUserProperty;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_EventDescription_GetUserProperty(
                self.event_description,
                name.as_ptr(),
                val.as_mut_ptr(),
            );
            make_result(val, err).map(|val| {
                let val = val.assume_init();
                let val = UserProperty::from_ffi(val);
                val
            })
        }
    }

    ///
    /// FMOD_Studio_EventDescription_GetLength
    ///
    pub fn get_length(&self) -> Result<usize> {
        use raw::FMOD_Studio_EventDescription_GetLength;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err =
                FMOD_Studio_EventDescription_GetLength(self.event_description, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init() as usize)
        }
    }

    ///
    /// FMOD_Studio_EventDescription_GetMaximumDistance
    ///
    pub fn get_maximum_distance(&self) -> Result<f32> {
        use raw::FMOD_Studio_EventDescription_GetMaximumDistance;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_EventDescription_GetMaximumDistance(
                self.event_description,
                val.as_mut_ptr(),
            );
            make_result(val, err).map(|val| val.assume_init())
        }
    }

    ///
    /// FMOD_Studio_EventDescription_GetMinimumDistance
    ///
    pub fn get_minimum_distance(&self) -> Result<f32> {
        use raw::FMOD_Studio_EventDescription_GetMinimumDistance;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_EventDescription_GetMinimumDistance(
                self.event_description,
                val.as_mut_ptr(),
            );
            make_result(val, err).map(|val| val.assume_init())
        }
    }

    ///
    /// FMOD_Studio_EventDescription_GetSoundSize
    ///
    pub fn get_sound_size(&self) -> Result<f32> {
        use raw::FMOD_Studio_EventDescription_GetSoundSize;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err =
                FMOD_Studio_EventDescription_GetSoundSize(self.event_description, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init())
        }
    }

    ///
    /// FMOD_Studio_EventDescription_IsSnapshot
    ///
    pub fn is_snapshot(&self) -> Result<bool> {
        use raw::FMOD_Studio_EventDescription_IsSnapshot;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err =
                FMOD_Studio_EventDescription_IsSnapshot(self.event_description, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init() != 0)
        }
    }

    ///
    /// FMOD_Studio_EventDescription_IsOneshot
    ///
    pub fn is_oneshot(&self) -> Result<bool> {
        use raw::FMOD_Studio_EventDescription_IsOneshot;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err =
                FMOD_Studio_EventDescription_IsOneshot(self.event_description, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init() != 0)
        }
    }

    ///
    /// FMOD_Studio_EventDescription_IsStream
    ///
    pub fn is_stream(&self) -> Result<bool> {
        use raw::FMOD_Studio_EventDescription_IsStream;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err =
                FMOD_Studio_EventDescription_IsStream(self.event_description, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init() != 0)
        }
    }

    ///
    /// FMOD_Studio_EventDescription_Is3D
    ///
    pub fn is_3d(&self) -> Result<bool> {
        use raw::FMOD_Studio_EventDescription_Is3D;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_EventDescription_Is3D(self.event_description, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init() != 0)
        }
    }

    ///
    /// FMOD_Studio_EventDescription_HasCue
    ///
    pub fn has_cue(&self) -> Result<bool> {
        use raw::FMOD_Studio_EventDescription_HasCue;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_EventDescription_HasCue(self.event_description, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init() != 0)
        }
    }

    ///
    /// FMOD_Studio_EventDescription_CreateInstance
    ///
    pub fn create_instance(&self) -> Result<EventInstance> {
        use raw::FMOD_Studio_EventDescription_CreateInstance;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_EventDescription_CreateInstance(
                self.event_description,
                val.as_mut_ptr(),
            );
            make_result(val, err).map(|val| EventInstance {
                event_instance: val.assume_init(),
            })
        }
    }

    ///
    /// FMOD_Studio_EventDescription_GetInstanceCount
    ///
    pub fn get_instance_count(&self) -> Result<usize> {
        use raw::FMOD_Studio_EventDescription_GetInstanceCount;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_EventDescription_GetInstanceCount(
                self.event_description,
                val.as_mut_ptr(),
            );
            make_result(val, err).map(|val| val.assume_init() as usize)
        }
    }

    ///
    /// FMOD_Studio_EventDescription_GetInstanceList
    ///
    pub fn get_instance_list(&self, list: &mut [EventInstance]) -> Result<usize> {
        use raw::FMOD_Studio_EventDescription_GetInstanceList;

        unsafe {
            let mut count = MaybeUninit::uninit();
            let err = FMOD_Studio_EventDescription_GetInstanceList(
                self.event_description,
                list.as_mut_ptr() as *mut _,
                list.len() as c_int,
                count.as_mut_ptr(),
            );
            make_result(count, err).map(|v| v.assume_init() as usize)
        }
    }

    ///
    /// FMOD_Studio_EventDescription_LoadSampleData
    ///
    pub fn load_sample_data(&self) -> Result<()> {
        use raw::FMOD_Studio_EventDescription_LoadSampleData;

        unsafe {
            let err = FMOD_Studio_EventDescription_LoadSampleData(self.event_description);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_EventDescription_UnloadSampleData
    ///
    pub fn unload_sample_data(&self) -> Result<()> {
        use raw::FMOD_Studio_EventDescription_UnloadSampleData;

        unsafe {
            let err = FMOD_Studio_EventDescription_UnloadSampleData(self.event_description);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_EventDescription_GetSampleLoadingState
    ///
    pub fn get_sample_loading_state(&self) -> Result<LoadingState> {
        use raw::FMOD_Studio_EventDescription_GetSampleLoadingState;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_EventDescription_GetSampleLoadingState(
                self.event_description,
                val.as_mut_ptr(),
            );
            make_result(val, err).map(|val| LoadingState::from_ffi(val.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_EventDescription_GetSampleLoadingState
    ///
    pub fn release_all_instances(&self) -> Result<()> {
        use raw::FMOD_Studio_EventDescription_ReleaseAllInstances;

        unsafe {
            let err = FMOD_Studio_EventDescription_ReleaseAllInstances(self.event_description);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_EventDescription_SetCallback
    ///
    pub fn set_callback(
        &self,
        callback: raw::FMOD_STUDIO_EVENT_CALLBACK,
        callback_mask: EventCallbackType,
    ) -> Result<()> {
        use raw::FMOD_Studio_EventDescription_SetCallback;

        unsafe {
            let err = FMOD_Studio_EventDescription_SetCallback(
                self.event_description,
                callback,
                callback_mask.bits(),
            );
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_EventDescription_GetUserData
    ///
    pub unsafe fn get_userdata(&self) -> Result<*mut ()> {
        use raw::FMOD_Studio_EventDescription_GetUserData;

        let mut val = MaybeUninit::uninit();
        let err =
            FMOD_Studio_EventDescription_GetUserData(self.event_description, val.as_mut_ptr());
        make_result(val, err).map(|val| val.assume_init() as *mut ())
    }

    ///
    /// FMOD_Studio_EventDescription_SetUserData
    ///
    pub unsafe fn set_userdata(&self, userdata: *mut ()) -> Result<()> {
        use raw::FMOD_Studio_EventDescription_SetUserData;

        let err =
            FMOD_Studio_EventDescription_SetUserData(self.event_description, userdata as *mut _);
        make_result((), err)
    }
}

unsafe impl Send for EventDescription {}
unsafe impl Sync for EventDescription {}
