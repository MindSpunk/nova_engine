/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::core::Mode;
use crate::raw::FMOD_CREATESOUNDEXINFO;
use std::os::raw::{c_char, c_int};

///
/// FMOD_STUDIO_SOUND_INFO
///
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct SoundInfo {
    pub name_or_data: *const c_char,
    pub mode: Mode,
    pub ex_info: FMOD_CREATESOUNDEXINFO,
    pub subsound_index: c_int,
}

impl SoundInfo {
    pub fn from_ffi(ffi: raw::FMOD_STUDIO_SOUND_INFO) -> Self {
        Self {
            name_or_data: ffi.name_or_data,
            mode: Mode::from_bits(ffi.mode).unwrap(),
            ex_info: ffi.exinfo,
            subsound_index: ffi.subsoundindex,
        }
    }

    pub fn into_ffi(self) -> raw::FMOD_STUDIO_SOUND_INFO {
        raw::FMOD_STUDIO_SOUND_INFO {
            name_or_data: self.name_or_data,
            mode: self.mode.bits(),
            exinfo: self.ex_info,
            subsoundindex: self.subsound_index,
        }
    }
}
