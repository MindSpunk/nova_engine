/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::studio::{ParameterFlags, ParameterID, ParameterType};
use raw::FMOD_STUDIO_PARAMETER_DESCRIPTION;
use std::ffi::CStr;

///
/// FMOD_STUDIO_PARAMETER_DESCRIPTION
///
#[repr(C)]
#[derive(Debug, Clone)]
pub struct ParameterDescription<'a> {
    pub name: &'a CStr,
    pub id: ParameterID,
    pub minimum: f32,
    pub maximum: f32,
    pub default_value: f32,
    pub p_type: ParameterType,
    pub flags: ParameterFlags,
}

impl<'a> ParameterDescription<'a> {
    pub unsafe fn from_ffi(ffi: FMOD_STUDIO_PARAMETER_DESCRIPTION) -> Self {
        Self {
            name: CStr::from_ptr(ffi.name),
            id: ParameterID::from_ffi(ffi.id),
            minimum: ffi.minimum,
            maximum: ffi.maximum,
            default_value: ffi.defaultvalue,
            p_type: ParameterType::from_ffi(ffi.type_),
            flags: ParameterFlags::from_bits(ffi.flags).unwrap(),
        }
    }

    pub fn into_ffi(self) -> FMOD_STUDIO_PARAMETER_DESCRIPTION {
        FMOD_STUDIO_PARAMETER_DESCRIPTION {
            name: self.name.as_ptr(),
            id: self.id.into_ffi(),
            minimum: self.minimum,
            maximum: self.maximum,
            defaultvalue: self.default_value,
            type_: self.p_type.into_ffi(),
            flags: self.flags.bits(),
        }
    }
}
