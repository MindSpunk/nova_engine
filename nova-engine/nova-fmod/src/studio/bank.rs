/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::core::Result;
use crate::core::{make_result, Guid};
use crate::studio::{EventDescription, LoadingState, VCA};
use std::ffi::CString;
use std::mem::MaybeUninit;

///
/// FMOD_STUDIO_BANK
///
#[derive(Copy, Clone)]
pub struct Bank {
    pub(crate) bank: *mut raw::FMOD_STUDIO_BANK,
}

impl Bank {
    ///
    /// Gets a null handle
    ///
    pub fn null() -> Self {
        Self {
            bank: std::ptr::null_mut(),
        }
    }

    ///
    /// Creates a handle from the ffi type
    ///
    pub fn from_ffi(ffi: *mut raw::FMOD_STUDIO_BANK) -> Self {
        Self { bank: ffi }
    }

    ///
    /// FMOD_Studio_Bank_IsValid
    ///
    pub fn is_valid(&self) -> bool {
        use raw::FMOD_Studio_Bank_IsValid;
        unsafe {
            let val = FMOD_Studio_Bank_IsValid(self.bank);
            val != 0
        }
    }

    ///
    /// FMOD_Studio_Bank_GetID
    ///
    pub fn get_id(&self) -> Result<Guid> {
        use raw::FMOD_Studio_Bank_GetID;
        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_Bank_GetID(self.bank, val.as_mut_ptr());
            make_result(val, err).map(|val| Guid::from_ffi(val.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_Bank_GetPath
    ///
    pub fn get_path(&self) -> Result<CString> {
        use raw::FMOD_Studio_Bank_GetPath;

        unsafe {
            let mut retrieved = 0;
            let err = FMOD_Studio_Bank_GetPath(self.bank, std::ptr::null_mut(), 0, &mut retrieved);
            make_result((), err)?;
            let mut out = Vec::<u8>::with_capacity(retrieved as usize);
            out.resize(retrieved as usize, 0);
            let err = FMOD_Studio_Bank_GetPath(
                self.bank,
                out.as_mut_ptr() as *mut _,
                out.len() as _,
                std::ptr::null_mut(),
            );
            make_result(CString::from_vec_unchecked(out), err)
        }
    }

    ///
    /// FMOD_Studio_Bank_Unload
    ///
    pub fn unload(&self) -> Result<()> {
        use raw::FMOD_Studio_Bank_Unload;
        unsafe {
            let err = FMOD_Studio_Bank_Unload(self.bank);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_Bank_LoadSampleData
    ///
    pub fn load_sample_data(&self) -> Result<()> {
        use raw::FMOD_Studio_Bank_LoadSampleData;
        unsafe {
            let err = FMOD_Studio_Bank_LoadSampleData(self.bank);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_Bank_UnloadSampleData
    ///
    pub fn unload_sample_data(&self) -> Result<()> {
        use raw::FMOD_Studio_Bank_UnloadSampleData;
        unsafe {
            let err = FMOD_Studio_Bank_UnloadSampleData(self.bank);
            make_result((), err)
        }
    }

    ///
    /// FMOD_Studio_Bank_GetLoadingState
    ///
    pub fn get_loading_state(&self) -> Result<LoadingState> {
        use raw::FMOD_Studio_Bank_GetLoadingState;
        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_Bank_GetLoadingState(self.bank, val.as_mut_ptr());
            make_result(val, err).map(|val| LoadingState::from_ffi(val.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_Bank_GetSampleLoadingState
    ///
    pub fn get_sample_loading_state(&self) -> Result<LoadingState> {
        use raw::FMOD_Studio_Bank_GetSampleLoadingState;
        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_Bank_GetSampleLoadingState(self.bank, val.as_mut_ptr());
            make_result(val, err).map(|val| LoadingState::from_ffi(val.assume_init()))
        }
    }

    ///
    /// FMOD_Studio_Bank_GetStringCount
    ///
    pub fn get_string_count(&self) -> Result<usize> {
        use raw::FMOD_Studio_Bank_GetStringCount;
        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_Bank_GetStringCount(self.bank, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init() as usize)
        }
    }

    ///
    /// FMOD_Studio_Bank_GetPath
    ///
    pub fn get_string_info(&self, index: usize) -> Result<(CString, Guid)> {
        use raw::FMOD_Studio_Bank_GetStringInfo;

        unsafe {
            let mut retrieved = 0;
            let err = FMOD_Studio_Bank_GetStringInfo(
                self.bank,
                index as _,
                std::ptr::null_mut(),
                std::ptr::null_mut(),
                0,
                &mut retrieved,
            );
            make_result((), err)?;
            let mut out1 = Vec::<u8>::with_capacity(retrieved as usize);
            out1.resize(retrieved as usize, 0);
            let mut out2 = MaybeUninit::uninit();
            let err = FMOD_Studio_Bank_GetStringInfo(
                self.bank,
                index as _,
                out2.as_mut_ptr(),
                out1.as_mut_ptr() as *mut _,
                out1.len() as _,
                std::ptr::null_mut(),
            );
            make_result((CString::from_vec_unchecked(out1), out2), err)
                .map(|(string, guid)| (string, Guid::from_ffi(guid.assume_init())))
        }
    }

    ///
    /// FMOD_Studio_Bank_GetEventCount
    ///
    pub fn get_event_count(&self) -> Result<usize> {
        use raw::FMOD_Studio_Bank_GetEventCount;
        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_Bank_GetEventCount(self.bank, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init() as usize)
        }
    }

    ///
    /// FMOD_Studio_Bank_GetEventList
    ///
    pub fn get_event_list(&self, list: &mut [EventDescription]) -> Result<usize> {
        use raw::FMOD_Studio_Bank_GetEventList;

        unsafe {
            let mut count = MaybeUninit::uninit();
            let err = FMOD_Studio_Bank_GetEventList(
                self.bank,
                list.as_mut_ptr() as *mut _,
                list.len() as _,
                count.as_mut_ptr(),
            );
            make_result(count, err).map(|v| v.assume_init() as usize)
        }
    }

    ///
    /// FMOD_Studio_Bank_GetBusCount
    ///
    pub fn get_bus_count(&self) -> Result<usize> {
        use raw::FMOD_Studio_Bank_GetBusCount;
        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_Bank_GetBusCount(self.bank, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init() as usize)
        }
    }

    ///
    /// FMOD_Studio_Bank_GetBusList
    ///
    pub fn get_bank_list(&self, list: &mut [Bank]) -> Result<usize> {
        use raw::FMOD_Studio_Bank_GetBusList;

        unsafe {
            let mut count = MaybeUninit::uninit();
            let err = FMOD_Studio_Bank_GetBusList(
                self.bank,
                list.as_mut_ptr() as *mut _,
                list.len() as _,
                count.as_mut_ptr(),
            );
            make_result(count, err).map(|v| v.assume_init() as usize)
        }
    }

    ///
    /// FMOD_Studio_Bank_GetVCACount
    ///
    pub fn get_vca_count(&self) -> Result<usize> {
        use raw::FMOD_Studio_Bank_GetVCACount;
        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_Bank_GetVCACount(self.bank, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init() as usize)
        }
    }

    ///
    /// FMOD_Studio_Bank_GetVCAList
    ///
    pub fn get_vca_list(&self, list: &mut [VCA]) -> Result<usize> {
        use raw::FMOD_Studio_Bank_GetVCAList;

        unsafe {
            let mut count = MaybeUninit::uninit();
            let err = FMOD_Studio_Bank_GetVCAList(
                self.bank,
                list.as_mut_ptr() as *mut _,
                list.len() as _,
                count.as_mut_ptr(),
            );
            make_result(count, err).map(|v| v.assume_init() as usize)
        }
    }

    ///
    /// FMOD_Studio_Bank_GetUserData
    ///
    pub fn get_userdata(&self) -> Result<*mut ()> {
        use raw::FMOD_Studio_Bank_GetUserData;

        unsafe {
            let mut val = MaybeUninit::uninit();
            let err = FMOD_Studio_Bank_GetUserData(self.bank, val.as_mut_ptr());
            make_result(val, err).map(|val| val.assume_init() as *mut ())
        }
    }

    ///
    /// FMOD_Studio_Bank_SetUserData
    ///
    pub fn set_userdata(&self, userdata: *mut ()) -> Result<()> {
        use raw::FMOD_Studio_Bank_SetUserData;

        unsafe {
            let err = FMOD_Studio_Bank_SetUserData(self.bank, userdata as *mut _);
            make_result((), err)
        }
    }
}
