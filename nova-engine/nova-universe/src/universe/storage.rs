/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::arena::{NodeArena, NodeIndex};
use crate::node::NodeItem;
use crate::resource::{ResourceBox, ResourceID};
use crate::{Node, NodeReference, Resource, TNodeReference, Transform3D};
use nova_math::types::Mat4x4;
use std::any::TypeId;
use std::cell::UnsafeCell;
use std::collections::HashMap;
use std::mem::MaybeUninit;

// =============================================================================================
///
///
///
pub type NodeHierarchyStorage = HashMap<NodeReference, NodeItem>;

// =============================================================================================
///
///
///
pub struct NodeArenaStorage {
    pub(crate) nodes: HashMap<TypeId, UnsafeCell<NodeArena>>,
    pub(crate) transforms: HashMap<TypeId, UnsafeCell<Vec<Transform3D>>>,
    pub(crate) matrices: HashMap<TypeId, UnsafeCell<Vec<Mat4x4>>>,
}

impl NodeArenaStorage {
    ///
    ///
    ///
    pub fn new() -> Self {
        Self {
            nodes: HashMap::with_capacity(128),
            transforms: HashMap::with_capacity(128),
            matrices: HashMap::with_capacity(128),
        }
    }

    ///
    ///
    ///
    #[inline]
    pub fn create_storage<T: Node>(&mut self, capacity: usize) {
        if !capacity.is_power_of_two() {
            panic!(
                "Attempted to create non power of two capacity: {}",
                capacity
            );
        }

        self.nodes
            .entry(TypeId::of::<T>())
            .or_insert(UnsafeCell::new(NodeArena::new::<T>(capacity).unwrap()));

        let mut transforms = Vec::with_capacity(capacity);
        transforms.resize(capacity, Transform3D::default());
        self.transforms
            .entry(TypeId::of::<T>())
            .or_insert(UnsafeCell::new(transforms));

        let mut matrices = Vec::with_capacity(capacity);
        matrices.resize(capacity, Mat4x4::identity());
        self.matrices
            .entry(TypeId::of::<T>())
            .or_insert(UnsafeCell::new(matrices));
    }

    ///
    ///
    ///
    #[inline]
    pub(crate) fn nodes_ref(&self, typeid: TypeId) -> &NodeArena {
        self.nodes
            .get(&typeid)
            .map(|v| unsafe { &*v.get() })
            .unwrap()
    }

    ///
    ///
    ///
    #[inline]
    pub(crate) fn nodes_mut(&mut self, typeid: TypeId) -> &mut NodeArena {
        self.nodes
            .get_mut(&typeid)
            .map(|v| unsafe { &mut *v.get() })
            .unwrap()
    }

    ///
    ///
    ///
    #[inline]
    pub(crate) fn transforms_ref(&self, typeid: TypeId) -> &Vec<Transform3D> {
        self.transforms
            .get(&typeid)
            .map(|v| unsafe { &*v.get() })
            .unwrap()
    }

    ///
    ///
    ///
    #[inline]
    pub(crate) fn transforms_mut(&mut self, typeid: TypeId) -> &mut Vec<Transform3D> {
        self.transforms
            .get_mut(&typeid)
            .map(|v| unsafe { &mut *v.get() })
            .unwrap()
    }

    ///
    ///
    ///
    #[inline]
    pub(crate) fn matrices_ref(&self, typeid: TypeId) -> &Vec<Mat4x4> {
        self.matrices
            .get(&typeid)
            .map(|v| unsafe { &*v.get() })
            .unwrap()
    }

    ///
    ///
    ///
    #[inline]
    pub(crate) fn matrices_mut(&mut self, typeid: TypeId) -> &mut Vec<Mat4x4> {
        self.matrices
            .get_mut(&typeid)
            .map(|v| unsafe { &mut *v.get() })
            .unwrap()
    }
}

///
/// Store a new node and return its index
///
pub(crate) fn node_storage_store<T: Node>(
    nodes: &mut NodeArena,
    transforms: &mut Vec<Transform3D>,
    matrices: &mut Vec<Mat4x4>,
    v: T,
) -> TNodeReference<T> {
    unsafe {
        let node_index = nodes.allocate(&v as *const T as *const u8);
        std::mem::forget(v);

        if node_index >= transforms.len() {
            transforms.resize(transforms.len() * 2, Transform3D::default());
            matrices.resize(matrices.len() * 2, Mat4x4::identity());
        }

        transforms[node_index] = Transform3D::default();
        matrices[node_index] = Mat4x4::identity();

        TNodeReference::<T> {
            index: NodeIndex {
                index: node_index as u32,
                generation: nodes.generation_ref(node_index).read(),
            },
            phantom: Default::default(),
        }
    }
}

///
/// Remove the node at index, if one exists
///
pub(crate) fn node_storage_remove<T: Node>(
    nodes: &mut NodeArena,
    node: TNodeReference<T>,
) -> Option<T> {
    if node_storage_is_valid(nodes, node) {
        let mut out_node = MaybeUninit::<T>::uninit();
        unsafe {
            nodes.remove(node.index.index as usize, out_node.as_mut_ptr() as *mut u8);
        }

        Some(unsafe { out_node.assume_init() })
    } else {
        None
    }
}

///
/// Remove the node at index, if one exists
///
pub(crate) fn node_storage_erase<R: Into<NodeReference>>(
    nodes: &mut NodeArena,
    node: R,
) -> Option<()> {
    let node: NodeReference = node.into();
    if node_storage_is_valid(nodes, node) {
        unsafe {
            nodes.erase(node.index.index as usize);
        }

        Some(())
    } else {
        None
    }
}

///
/// Get a reference to the node data
///
pub(crate) fn node_storage_get_node_ref<T: Node>(
    nodes: &NodeArena,
    node: TNodeReference<T>,
) -> Option<&T> {
    if node_storage_is_valid(nodes, node) {
        let node_ref;
        unsafe {
            let node_ptr = nodes.node_ref(node.index.index as usize);
            let node_ptr = node_ptr as *const T;
            node_ref = node_ptr.as_ref().unwrap();
        }
        Some(node_ref)
    } else {
        None
    }
}

///
/// Get a mutable reference to the node data
///
pub(crate) fn node_storage_get_node_mut<T: Node>(
    nodes: &mut NodeArena,
    node: TNodeReference<T>,
) -> Option<&mut T> {
    if node_storage_is_valid(nodes, node) {
        let node_ref;
        unsafe {
            let node_ptr = nodes.node_mut(node.index.index as usize);
            let node_ptr = node_ptr as *mut T;
            node_ref = node_ptr.as_mut().unwrap();
        }
        Some(node_ref)
    } else {
        None
    }
}

///
/// Is the reference valid and points to the object it was created from
///
#[inline]
pub(crate) fn node_storage_is_valid<R: Into<NodeReference>>(arena: &NodeArena, node: R) -> bool {
    let node: NodeReference = node.into();
    if node.index.generation.is_alive() {
        let generation = unsafe { *arena.generation_ref(node.index.index as usize) };
        if node.index.generation == generation {
            true
        } else {
            false
        }
    } else {
        false
    }
}

// =================================================================================================
///
///
///
pub struct ResourceStorage<'resources> {
    pub(crate) boxes: HashMap<ResourceID, UnsafeCell<ResourceBox<'resources>>>,
}

impl<'resources> ResourceStorage<'resources> {
    ///
    ///
    ///
    pub fn new() -> Self {
        Self {
            boxes: HashMap::with_capacity(64),
        }
    }

    ///
    ///
    ///
    pub fn add_resource<T: Resource + 'resources>(&mut self, v: T) {
        let cell = ResourceBox::new(v);
        let cell = UnsafeCell::new(cell);

        self.boxes.insert(ResourceID::of::<T>(), cell);
    }

    ///
    ///
    ///
    pub fn delete_resource<T: Resource + 'resources>(&mut self) -> Option<()> {
        let item = self.boxes.remove(&ResourceID::of::<T>());
        item.map(|v| {
            drop(v.into_inner());
        })
    }
}
