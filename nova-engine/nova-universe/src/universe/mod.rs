/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

mod storage;

use crate::arena::NodeArena;
use crate::node::NodeItem;
use crate::resource::ResourceID;
use crate::{Dependencies, NodeReference, TaskBox, TaskGraph};
use crate::{Node, Resource};
use crate::{TNodeReference, Transform3D};
use nova_math::types::{Mat4x4, Quat, Vec3};
use std::any::TypeId;
use std::cell::UnsafeCell;
use std::collections::HashMap;

pub use self::storage::NodeArenaStorage;
pub use self::storage::NodeHierarchyStorage;
pub use self::storage::ResourceStorage;
pub(crate) use crate::universe::storage::{
    node_storage_erase, node_storage_get_node_mut, node_storage_get_node_ref,
    node_storage_is_valid, node_storage_remove, node_storage_store,
};
use std::ops::Deref;

// =================================================================================================
///
///
///
pub struct Universe<'resources> {
    pub(crate) node_storage: NodeArenaStorage,
    pub(crate) node_hierarchy: UnsafeCell<NodeHierarchyStorage>,
    pub(crate) resource_storage: ResourceStorage<'resources>,
}

impl<'resources> Universe<'resources> {
    ///
    ///
    ///
    pub fn new() -> Self {
        Self {
            node_storage: NodeArenaStorage::new(),
            node_hierarchy: UnsafeCell::new(NodeHierarchyStorage::with_capacity(2048)),
            resource_storage: ResourceStorage::new(),
        }
    }

    ///
    /// Register a node type to the universe with the given default capacity
    ///
    /// # Info
    ///
    /// Registering a node is required as it is what creates a backing storage buffer for the nodes
    /// in the universe. This way we only need to allocate memory for the required node types
    ///
    pub fn register_node_type<T: Node>(&mut self, capacity: usize) {
        self.node_storage.create_storage::<T>(capacity);
    }

    ///
    ///
    ///
    pub fn add_resource<T: Resource + 'resources>(&mut self, v: T) {
        self.resource_storage.add_resource(v);
    }

    ///
    ///
    ///
    pub fn delete_resource<T: Resource + 'resources>(&mut self) -> Option<()> {
        self.resource_storage.delete_resource::<T>()
    }

    ///
    ///
    ///
    pub fn get_node_ref<T: Node>(&self, node: TNodeReference<T>) -> Option<&T> {
        let node_arena = unsafe {
            let nodes = self.node_storage.nodes.get(&TypeId::of::<T>()).unwrap();
            &*nodes.get()
        };
        node_storage_get_node_ref(node_arena, node)
    }

    ///
    ///
    ///
    pub fn get_node_mut<T: Node>(&mut self, node: TNodeReference<T>) -> Option<&mut T> {
        let node_arena = unsafe {
            let nodes = self.node_storage.nodes.get_mut(&TypeId::of::<T>()).unwrap();
            &mut *nodes.get()
        };
        node_storage_get_node_mut(node_arena, node)
    }

    ///
    ///
    ///
    pub fn get_node_transform_ref<R: Into<NodeReference>>(&self, node: R) -> Option<&Transform3D> {
        let node: NodeReference = node.into();
        if self.is_valid(node) {
            let x = &self.node_storage.transforms_ref(node.node_type())[node.index.index as usize];
            Some(x)
        } else {
            None
        }
    }

    ///
    ///
    ///
    pub fn get_node_transform_mut<R: Into<NodeReference>>(
        &mut self,
        node: R,
    ) -> Option<&mut Transform3D> {
        let node: NodeReference = node.into();
        if self.is_valid(node) {
            let x =
                &mut self.node_storage.transforms_mut(node.node_type())[node.index.index as usize];
            Some(x)
        } else {
            None
        }
    }

    ///
    ///
    ///
    pub fn get_node_matrix_ref<R: Into<NodeReference>>(&self, node: R) -> Option<&Mat4x4> {
        let node: NodeReference = node.into();
        if self.is_valid(node) {
            let x = &self.node_storage.matrices_ref(node.node_type())[node.index.index as usize];
            Some(x)
        } else {
            None
        }
    }

    ///
    ///
    ///
    pub fn get_node_matrix_mut<R: Into<NodeReference>>(&mut self, node: R) -> Option<&mut Mat4x4> {
        let node: NodeReference = node.into();
        if self.is_valid(node) {
            let x =
                &mut self.node_storage.matrices_mut(node.node_type())[node.index.index as usize];
            Some(x)
        } else {
            None
        }
    }

    ///
    ///
    ///
    pub fn create_node<T: Node>(&mut self, v: T, transform: Transform3D) -> TNodeReference<T> {
        let node_arena = unsafe {
            let nodes = self.node_storage.nodes.get_mut(&TypeId::of::<T>()).unwrap();
            &mut *nodes.get()
        };
        let node_transforms = unsafe {
            let nodes = self
                .node_storage
                .transforms
                .get_mut(&TypeId::of::<T>())
                .unwrap();
            &mut *nodes.get()
        };
        let node_matrices = unsafe {
            let nodes = self
                .node_storage
                .matrices
                .get_mut(&TypeId::of::<T>())
                .unwrap();
            &mut *nodes.get()
        };
        let node_hierarchy = unsafe { &mut *self.node_hierarchy.get() };
        universe_create_node(
            node_arena,
            node_transforms,
            node_matrices,
            node_hierarchy,
            v,
            transform,
        )
    }

    ///
    ///
    ///
    pub fn create_child_node<T: Node, R: Into<NodeReference>>(
        &mut self,
        v: T,
        transform: Transform3D,
        parent: R,
    ) -> Option<TNodeReference<T>> {
        let node_arenas = &mut self.node_storage.nodes;
        let node_transforms = unsafe {
            let nodes = self
                .node_storage
                .transforms
                .get_mut(&TypeId::of::<T>())
                .unwrap();
            &mut *nodes.get()
        };
        let node_matrices = unsafe {
            let nodes = self
                .node_storage
                .matrices
                .get_mut(&TypeId::of::<T>())
                .unwrap();
            &mut *nodes.get()
        };
        let node_hierarchy = unsafe { &mut *self.node_hierarchy.get() };

        universe_create_child_node(
            node_arenas,
            node_transforms,
            node_matrices,
            node_hierarchy,
            v,
            transform,
            parent,
        )
    }

    pub fn node_builder<T: Node>(&mut self, node: T) -> NodeBuilder<'_, 'resources, T> {
        NodeBuilder {
            universe: self,
            node,
            transform: Default::default(),
            parent: NodeReference::null(),
        }
    }

    ///
    ///
    ///
    pub fn remove_node<T: Node>(&mut self, node: TNodeReference<T>) -> T {
        let node_arenas = &mut self.node_storage.nodes;
        let node_hierarchy = unsafe { &mut *self.node_hierarchy.get() };
        universe_remove_node(node_arenas, node_hierarchy, node)
    }

    ///
    ///
    ///
    pub fn erase_node<R: Into<NodeReference>>(&mut self, node: R) {
        let node_arenas = &mut self.node_storage.nodes;
        let node_hierarchy = unsafe { &mut *self.node_hierarchy.get() };
        universe_erase_node(node_arenas, node_hierarchy, node)
    }

    ///
    ///
    ///
    pub fn remove_node_reparent<R: Into<NodeReference>>(&mut self, node: R, new_parent: R) {
        let node = node.into();
        let new_parent = new_parent.into();

        let node_arenas = &mut self.node_storage.nodes;
        let node_hierarchy = unsafe { &mut *self.node_hierarchy.get() };

        universe_remove_node_reparent(node_arenas, node_hierarchy, node, new_parent)
    }

    ///
    ///
    ///
    pub fn get_resource_ref<T: Resource + 'resources>(&self) -> Option<&T> {
        self.resource_storage
            .boxes
            .get(&ResourceID::of::<T>())
            .map(|v| unsafe {
                let resource = &*v.get();
                let resource = resource.get_self_ptr();
                let resource = resource as *const T;
                &*resource
            })
    }

    ///
    ///
    ///
    pub fn get_resource_mut<T: Resource + 'resources>(&mut self) -> Option<&mut T> {
        self.resource_storage
            .boxes
            .get_mut(&ResourceID::of::<T>())
            .map(|v| unsafe {
                let resource = &mut *v.get();
                let resource = resource.get_self_ptr_mut();
                let resource = resource as *mut T;
                &mut *resource
            })
    }

    ///
    /// A simple way to execute a single task on the Universe without having to create an entire
    /// TaskGraph just to execute a single task. This is useful when you have a one time fire task
    /// that runs completely synchronously (such as an initial setup task) or when you need to
    /// guarantee that a task is called on a certain thread.
    ///
    pub fn execute_task(&mut self, task: &TaskBox, func: &impl Fn(&mut Dependencies)) {
        let mut dependencies = Dependencies {
            reads: Vec::new(),
            writes: Vec::new(),
        };

        func(&mut dependencies);

        let access = TaskGraph::fill_access_state(&dependencies, self);

        (task.get_inner())(access);
    }

    ///
    ///
    ///
    pub fn is_valid<R: Into<NodeReference>>(&self, node: R) -> bool {
        let node = node.into();
        let node_type = node.node_type();
        let node_arena = unsafe {
            let nodes = self.node_storage.nodes.get(&node_type).unwrap();
            &*nodes.get()
        };
        node_storage_is_valid(node_arena.deref(), node)
    }

    ///
    ///
    ///
    #[inline]
    pub(crate) fn get_node_hierarchy(&self) -> &NodeHierarchyStorage {
        unsafe { &*self.node_hierarchy.get() }
    }

    ///
    ///
    ///
    #[inline]
    pub(crate) unsafe fn node_hierarchy_cell(&self) -> &UnsafeCell<NodeHierarchyStorage> {
        &self.node_hierarchy
    }
}

///
///
///
pub struct NodeBuilder<'a, 'resources, T: Node> {
    universe: &'a mut Universe<'resources>,
    node: T,
    transform: Transform3D,
    parent: NodeReference,
}

impl<'a, 'resources, T: Node> NodeBuilder<'a, 'resources, T> {
    ///
    ///
    ///
    pub fn position(mut self, pos: Vec3) -> Self {
        self.transform.pos = pos;
        self
    }

    ///
    ///
    ///
    pub fn rotation(mut self, rot: Quat) -> Self {
        self.transform.rot = rot;
        self
    }

    ///
    ///
    ///
    pub fn scale(mut self, scl: Vec3) -> Self {
        self.transform.scl = scl;
        self
    }

    ///
    ///
    ///
    pub fn transform(mut self, transform: Transform3D) -> Self {
        self.transform = transform;
        self
    }

    ///
    ///
    ///
    pub fn parent<I: Into<NodeReference>>(mut self, parent: I) -> Self {
        self.parent = parent.into();
        self
    }

    ///
    ///
    ///
    pub fn build(self) -> Option<TNodeReference<T>> {
        if self.parent.is_null() {
            Some(self.universe.create_node(self.node, self.transform))
        } else {
            self.universe
                .create_child_node(self.node, self.transform, self.parent)
        }
    }
}

pub(crate) unsafe trait NodeArenasWrapper {
    fn get_arena_ref(&self, typeid: &TypeId) -> &NodeArena;
    fn get_arena_mut(&self, typeid: &TypeId) -> &mut NodeArena;
}

unsafe impl NodeArenasWrapper for HashMap<TypeId, UnsafeCell<NodeArena>> {
    fn get_arena_ref(&self, typeid: &TypeId) -> &NodeArena {
        unsafe { &*self.get(typeid).unwrap().get() }
    }

    fn get_arena_mut(&self, typeid: &TypeId) -> &mut NodeArena {
        unsafe { &mut *self.get(typeid).unwrap().get() }
    }
}

unsafe impl<'a> NodeArenasWrapper for HashMap<TypeId, UnsafeCell<&'a mut NodeArena>> {
    fn get_arena_ref(&self, typeid: &TypeId) -> &NodeArena {
        let arena = unsafe { &*self.get(typeid).unwrap().get() };
        *arena
    }

    fn get_arena_mut(&self, typeid: &TypeId) -> &mut NodeArena {
        let arena = unsafe { &mut *self.get(typeid).unwrap().get() };
        *arena
    }
}

///
///
///
pub(crate) fn universe_create_node<T: Node>(
    node_arena: &mut NodeArena,
    node_transforms: &mut Vec<Transform3D>,
    node_matrices: &mut Vec<Mat4x4>,
    node_hierarchy: &mut NodeHierarchyStorage,
    v: T,
    transform: Transform3D,
) -> TNodeReference<T> {
    let node = node_storage_store(node_arena, node_transforms, node_matrices, v);

    node_hierarchy.insert(
        node.into(),
        NodeItem {
            parent: NodeReference::null(),
            children: Vec::new(),
        },
    );
    node_transforms[node.index.index as usize] = transform;

    node
}

///
///
///
pub(crate) fn universe_create_child_node<T: Node, R: Into<NodeReference>>(
    node_arenas: &mut impl NodeArenasWrapper,
    node_transforms: &mut Vec<Transform3D>,
    node_matrices: &mut Vec<Mat4x4>,
    node_hierarchy: &mut NodeHierarchyStorage,
    v: T,
    transform: Transform3D,
    parent: R,
) -> Option<TNodeReference<T>> {
    let parent: NodeReference = parent.into();

    let node_arena = node_arenas.get_arena_mut(&TypeId::of::<T>());
    let parent_arena = node_arenas.get_arena_ref(&parent.node_type());

    if node_storage_is_valid(parent_arena, parent) {
        let node = node_storage_store(node_arena, node_transforms, node_matrices, v);

        let item = NodeItem {
            parent,
            children: Vec::new(),
        };

        node_hierarchy.insert(node.into(), item);
        node_hierarchy
            .get_mut(&parent)
            .unwrap()
            .children
            .push(node.into());

        node_transforms[node.index.index as usize] = transform;

        Some(node)
    } else {
        None
    }
}

///
///
///
pub(crate) fn universe_remove_node_reparent<R: Into<NodeReference>>(
    node_arenas: &mut impl NodeArenasWrapper,
    node_hierarchy: &mut NodeHierarchyStorage,
    node: R,
    new_parent: R,
) {
    let node: NodeReference = node.into();
    let new_parent: NodeReference = new_parent.into();

    let node_type = node.node_type();
    let new_parent_type = new_parent.node_type();

    let node_arena = node_arenas.get_arena_mut(&node_type);
    let new_parent_arena = node_arenas.get_arena_mut(&new_parent_type);

    if node_storage_is_valid(node_arena, node)
        && node_storage_is_valid(new_parent_arena, new_parent)
    {
        node_storage_erase(node_arena, node).unwrap();

        let mut parent = node_hierarchy.remove(&new_parent).unwrap();

        let item = node_hierarchy.remove(&node.into()).unwrap();
        for child in item.children.iter() {
            node_hierarchy.get_mut(child).unwrap().parent = new_parent;
            parent.children.push(*child);
        }

        node_hierarchy.insert(new_parent, parent);

        universe_pop_child_from_parent(node_hierarchy, node, item);
    }
}

///
///
///
pub(crate) fn universe_remove_node<T: Node>(
    node_arenas: &mut impl NodeArenasWrapper,
    node_hierarchy: &mut NodeHierarchyStorage,
    node: TNodeReference<T>,
) -> T {
    let node_arena = node_arenas.get_arena_mut(&TypeId::of::<T>());
    let v = node_storage_remove(node_arena, node).unwrap();

    let item = node_hierarchy.remove(&node.into()).unwrap();

    for node in item.children.iter() {
        universe_remove_child(node_arenas, node_hierarchy, *node);
    }

    universe_pop_child_from_parent(node_hierarchy, node, item);

    v
}

///
///
///
pub(crate) fn universe_erase_node<R: Into<NodeReference>>(
    node_arenas: &mut impl NodeArenasWrapper,
    node_hierarchy: &mut NodeHierarchyStorage,
    node: R,
) {
    let node = node.into();
    let node_arena = node_arenas.get_arena_mut(&node.node_type());
    node_storage_erase(node_arena, node).unwrap();

    let item = node_hierarchy.remove(&node).unwrap();

    for node in item.children.iter() {
        universe_remove_child(node_arenas, node_hierarchy, *node);
    }

    universe_pop_child_from_parent(node_hierarchy, node, item);
}

///
///
///
pub(crate) fn universe_remove_child<R: Into<NodeReference>>(
    node_arenas: &mut impl NodeArenasWrapper,
    node_hierarchy: &mut NodeHierarchyStorage,
    node: R,
) {
    let node: NodeReference = node.into();
    let node_arena = node_arenas.get_arena_mut(&node.node_type());
    node_storage_erase(node_arena, node);

    let item = node_hierarchy.remove(&node).unwrap();
    for child in item.children.iter() {
        universe_remove_child(node_arenas, node_hierarchy, *child);
    }
}

///
///
///
pub(crate) fn universe_pop_child_from_parent<R: Into<NodeReference>>(
    node_hierarchy: &mut NodeHierarchyStorage,
    node: R,
    item: NodeItem,
) {
    let node: NodeReference = node.into();
    if !item.parent.is_null() {
        let parent = node_hierarchy.get_mut(&item.parent).unwrap();

        let pos = parent.children.iter().position(|x| *x == node).unwrap();
        parent.children.remove(pos);
    }
}
