/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

mod hierarchy;
mod matrices;
mod nodes;
mod transforms;
mod universe;

pub use self::hierarchy::NodeHierarchy;
pub use self::hierarchy::NodeHierarchyMut;
pub use self::matrices::Matrices;
pub use self::matrices::MatricesMut;
pub use self::matrices::MatrixGroup;
pub use self::matrices::MatrixGroupMut;
pub use self::nodes::Nodes;
pub use self::nodes::NodesMut;
pub use self::transforms::TransformGroup;
pub use self::transforms::TransformGroupMut;
pub use self::transforms::Transforms;
pub use self::transforms::TransformsMut;
pub use self::universe::TaskUniverse;
pub use self::universe::TaskUniverseBuilder;
use crate::arena::NodeArena;
use crate::universe::NodeHierarchyStorage;
use crate::{Dependency, Node, Resource, Transform3D};
use nova_math::types::Mat4x4;
use std::any::TypeId;
use std::cell::{RefCell, UnsafeCell};
use std::collections::HashMap;
use std::marker::PhantomData;

// =================================================================================================
///
///
///
pub struct DataAccessState<'resources> {
    pub(crate) read_data: HashMap<Dependency, *const ()>,
    pub(crate) write_data: HashMap<Dependency, *mut ()>,
    pub(crate) read_transforms: Option<*const HashMap<TypeId, UnsafeCell<Vec<Transform3D>>>>,
    pub(crate) write_transforms: Option<*mut HashMap<TypeId, UnsafeCell<Vec<Transform3D>>>>,
    pub(crate) read_matrices: Option<*const HashMap<TypeId, UnsafeCell<Vec<Mat4x4>>>>,
    pub(crate) write_matrices: Option<*mut HashMap<TypeId, UnsafeCell<Vec<Mat4x4>>>>,
    pub(crate) read_hierarchy: Option<*const NodeHierarchyStorage>,
    pub(crate) write_hierarchy: Option<*mut NodeHierarchyStorage>,
    pub(crate) phantom: PhantomData<&'resources ()>,
}

// =================================================================================================
///
///
///
pub struct DataAccess<'resources> {
    state: RefCell<DataAccessState<'resources>>,
}

impl<'resources> DataAccess<'resources> {
    ///
    ///
    ///
    pub(crate) fn new(state: DataAccessState<'resources>) -> Self {
        Self {
            state: RefCell::new(state),
        }
    }

    ///
    ///
    ///
    pub fn get_nodes_ref<T: Node>(&self) -> Option<Nodes<'_, T>> {
        let mut state = self.state.borrow_mut();

        let dependable = Dependency::node::<T>();
        let reference = state.read_data.remove(&dependable);

        reference.map(|v| unsafe {
            let arena = core::mem::transmute::<*const (), &NodeArena>(v);
            Nodes::new(arena)
        })
    }

    ///
    ///
    ///
    pub fn get_nodes_mut<T: Node>(&self) -> Option<NodesMut<'_, T>> {
        let mut state = self.state.borrow_mut();

        let dependable = Dependency::node::<T>();
        let reference = state.write_data.remove(&dependable);

        reference.map(|v| unsafe {
            let arena = core::mem::transmute::<*mut (), &mut NodeArena>(v);
            NodesMut::new(arena)
        })
    }

    ///
    ///
    ///
    pub fn get_transforms_ref(&self) -> Option<TransformGroup<'_>> {
        let mut state = self.state.borrow_mut();
        let ptr = state.read_transforms.take();
        ptr.map(|v| TransformGroup::new(v))
    }

    ///
    ///
    ///
    pub fn get_transforms_mut(&self) -> Option<TransformGroupMut<'_>> {
        let mut state = self.state.borrow_mut();
        let ptr = state.write_transforms.take();
        ptr.map(|v| TransformGroupMut::new(v))
    }

    ///
    ///
    ///
    pub fn get_matrices_ref(&self) -> Option<MatrixGroup<'_>> {
        let mut state = self.state.borrow_mut();
        let ptr = state.read_matrices.take();
        ptr.map(|v| MatrixGroup::new(v))
    }

    ///
    ///
    ///
    pub fn get_matrices_mut(&self) -> Option<MatrixGroupMut<'_>> {
        let mut state = self.state.borrow_mut();
        let ptr = state.write_matrices.take();
        ptr.map(|v| MatrixGroupMut::new(v))
    }

    ///
    ///
    ///
    pub fn get_resource_ref<T: Resource + 'resources>(&self) -> Option<&T> {
        let mut state = self.state.borrow_mut();

        let dependable = Dependency::resource::<T>();
        let reference = state.read_data.remove(&dependable);

        reference.map(|v| unsafe { core::mem::transmute::<*const (), &T>(v) })
    }

    ///
    ///
    ///
    pub fn get_resource_mut<T: Resource + 'resources>(&self) -> Option<&mut T> {
        let mut state = self.state.borrow_mut();

        let dependable = Dependency::resource::<T>();
        let reference = state.write_data.remove(&dependable);

        reference.map(|v| unsafe { core::mem::transmute::<*mut (), &mut T>(v) })
    }

    ///
    ///
    ///
    pub fn get_node_hierarchy_ref(&self) -> Option<NodeHierarchy<'_>> {
        let mut state = self.state.borrow_mut();
        let ptr = state.read_hierarchy.take();
        ptr.map(|v| NodeHierarchy {
            node_hierarchy: v,
            phantom: Default::default(),
        })
    }

    ///
    ///
    ///
    pub fn get_node_hierarchy_mut(&self) -> Option<NodeHierarchyMut<'_>> {
        let mut state = self.state.borrow_mut();
        let ptr = state.write_hierarchy.take();
        ptr.map(|v| NodeHierarchyMut {
            node_hierarchy: v,
            phantom: Default::default(),
        })
    }
}

unsafe impl<'resources> Send for DataAccess<'resources> {}
