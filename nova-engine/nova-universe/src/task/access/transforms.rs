/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::{Node, NodeReference, TNodeReference, Transform3D};
use std::any::TypeId;
use std::cell::UnsafeCell;
use std::collections::HashMap;
use std::marker::PhantomData;
use std::ops::{Index, IndexMut};

// =================================================================================================
///
/// Wrapper for immutable access to node transforms
///
pub struct TransformGroup<'a> {
    transforms: *const HashMap<TypeId, UnsafeCell<Vec<Transform3D>>>,
    phantom: PhantomData<&'a ()>,
}

impl<'a> TransformGroup<'a> {
    ///
    ///
    ///
    pub(crate) fn new(transforms: *const HashMap<TypeId, UnsafeCell<Vec<Transform3D>>>) -> Self {
        Self {
            transforms,
            phantom: Default::default(),
        }
    }

    ///
    ///
    ///
    pub fn for_type<T: Node>(&self) -> Transforms<'_, T> {
        let transforms = unsafe {
            let transforms = &*self.transforms;
            &*transforms.get(&TypeId::of::<T>()).unwrap().get()
        };
        Transforms::new(transforms)
    }
}

impl<'a> Index<NodeReference> for TransformGroup<'a> {
    type Output = Transform3D;

    fn index(&self, node: NodeReference) -> &Self::Output {
        unsafe {
            let vec = &*self.transforms;
            let vec = &*vec.get(&node.node_type()).unwrap().get();
            &vec[node.index.index as usize]
        }
    }
}

// =================================================================================================
///
/// Wrapper for immutable access to node transforms
///
pub struct TransformGroupMut<'a> {
    pub(crate) transforms: *mut HashMap<TypeId, UnsafeCell<Vec<Transform3D>>>,
    pub(crate) phantom: PhantomData<&'a mut ()>,
}

impl<'a> TransformGroupMut<'a> {
    ///
    ///
    ///
    pub(crate) fn new(transforms: *mut HashMap<TypeId, UnsafeCell<Vec<Transform3D>>>) -> Self {
        Self {
            transforms,
            phantom: Default::default(),
        }
    }

    ///
    ///
    ///
    pub fn for_type<T: Node>(&self) -> Transforms<'_, T> {
        let transforms = unsafe {
            let transforms = &*self.transforms;
            &*transforms.get(&TypeId::of::<T>()).unwrap().get()
        };
        Transforms::new(transforms)
    }

    ///
    ///
    ///
    pub fn for_type_mut<T: Node>(&mut self) -> TransformsMut<'_, T> {
        let transforms = unsafe {
            let transforms = &mut *self.transforms;
            &mut *transforms.get_mut(&TypeId::of::<T>()).unwrap().get()
        };
        TransformsMut::new(transforms)
    }
}

impl<'a, R: Into<NodeReference>> Index<R> for TransformGroupMut<'a> {
    type Output = Transform3D;

    fn index(&self, node: R) -> &Self::Output {
        unsafe {
            let node = node.into();
            let vec = &*self.transforms;
            let vec = &*vec.get(&node.node_type()).unwrap().get();
            &vec[node.index.index as usize]
        }
    }
}

impl<'a, R: Into<NodeReference>> IndexMut<R> for TransformGroupMut<'a> {
    fn index_mut(&mut self, node: R) -> &mut Self::Output {
        unsafe {
            let node = node.into();
            let vec = &mut *self.transforms;
            let vec = &mut *vec.get(&node.node_type()).unwrap().get();
            &mut vec[node.index.index as usize]
        }
    }
}

// =================================================================================================
///
/// Wrapper for immutable access to node transforms
///
pub struct Transforms<'a, T: Node> {
    transforms: &'a Vec<Transform3D>,
    phantom: PhantomData<T>,
}

impl<'a, T: Node> Transforms<'a, T> {
    ///
    ///
    ///
    pub(crate) fn new(transforms: &'a Vec<Transform3D>) -> Self {
        Transforms {
            transforms,
            phantom: Default::default(),
        }
    }
}

impl<'a, T: Node> Index<TNodeReference<T>> for Transforms<'a, T> {
    type Output = Transform3D;

    fn index(&self, node: TNodeReference<T>) -> &Self::Output {
        &self.transforms[node.index.index as usize]
    }
}

// =================================================================================================
///
/// Wrapper for mutable access to the transform of a node
///
pub struct TransformsMut<'a, T: Node> {
    transforms: &'a mut Vec<Transform3D>,
    phantom: PhantomData<T>,
}

impl<'a, T: Node> TransformsMut<'a, T> {
    ///
    ///
    ///
    pub(crate) fn new(transforms: &'a mut Vec<Transform3D>) -> Self {
        TransformsMut {
            transforms,
            phantom: Default::default(),
        }
    }
}

impl<'a, T: Node> Index<TNodeReference<T>> for TransformsMut<'a, T> {
    type Output = Transform3D;

    fn index(&self, node: TNodeReference<T>) -> &Self::Output {
        &self.transforms[node.index.index as usize]
    }
}

impl<'a, T: Node> IndexMut<TNodeReference<T>> for TransformsMut<'a, T> {
    fn index_mut(&mut self, node: TNodeReference<T>) -> &mut Self::Output {
        &mut self.transforms[node.index.index as usize]
    }
}
