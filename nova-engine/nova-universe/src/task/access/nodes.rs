/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::arena::{NodeArena, NodeIndex};
use crate::{Node, TNodeReference};
use std::marker::PhantomData;
use std::ops::{Index, IndexMut};

// =================================================================================================
///
///
///
fn is_valid<T: Node>(nodes: &NodeArena, node: TNodeReference<T>) -> bool {
    let generation = unsafe {
        nodes
            .generation_ref(node.index.index as usize)
            .as_ref()
            .unwrap()
    };
    if generation.is_alive() {
        if generation == &node.index.generation {
            true
        } else {
            false
        }
    } else {
        false
    }
}

// =================================================================================================
///
/// Wrapper for immutable access to nodes
///
pub struct Nodes<'a, T: Node> {
    arena: &'a NodeArena,
    phantom: PhantomData<&'a [T]>,
}

impl<'a, T: Node> Nodes<'a, T> {
    ///
    ///
    ///
    pub(crate) fn new(arena: &'a NodeArena) -> Self {
        Self {
            arena,
            phantom: Default::default(),
        }
    }

    ///
    /// Check if the given node is still valid and points to the object it was created from
    ///
    #[inline]
    pub fn is_valid(&self, node: TNodeReference<T>) -> bool {
        is_valid(&self.arena, node)
    }

    ///
    /// Iterate over all of the nodes in this arena, calling the given functor for each valid node
    /// yielded
    ///
    pub fn for_each(&self, mut f: impl FnMut(TNodeReference<T>, &Nodes<'_, T>)) {
        let wrap_mask = self.arena.capacity() - 1;
        let mut visited = 0;

        for i in 0..self.arena.capacity() {
            let i = i & wrap_mask;
            if visited >= self.arena.node_count() {
                return;
            }
            let generation = unsafe { self.arena.generation_ref(i).as_ref().unwrap() };
            if generation.is_alive() {
                let node = TNodeReference::<T> {
                    index: NodeIndex {
                        index: i as u32,
                        generation: *generation,
                    },
                    phantom: Default::default(),
                };
                f(node, self);
                visited += 1;
            }
        }
    }
}

impl<'a, T: Node> Index<TNodeReference<T>> for Nodes<'a, T> {
    type Output = T;

    fn index(&self, node: TNodeReference<T>) -> &Self::Output {
        if self.is_valid(node) {
            let node_ref;
            unsafe {
                let node_ptr = self.arena.node_ref(node.index.index as usize);
                let node_ptr = node_ptr as *const T;
                node_ref = node_ptr.as_ref().unwrap();
            }
            node_ref
        } else {
            panic!("Tried to index an invalid node");
        }
    }
}

// =================================================================================================
///
/// Wrapper for mutable access to nodes
///
pub struct NodesMut<'a, T: Node> {
    pub(crate) arena: &'a mut NodeArena,
    phantom: PhantomData<&'a mut [T]>,
}

impl<'a, T: Node> NodesMut<'a, T> {
    ///
    ///
    ///
    pub(crate) fn new(arena: &'a mut NodeArena) -> Self {
        Self {
            arena,
            phantom: PhantomData::default(),
        }
    }

    ///
    /// Check if the given node is still valid and points to the object it was created from
    ///
    pub fn is_valid(&self, node: TNodeReference<T>) -> bool {
        is_valid(&self.arena, node)
    }

    ///
    /// Iterate over all of the nodes in this arena, calling the given functor for each valid node
    /// yielded
    ///
    pub fn for_each(&self, mut f: impl FnMut(TNodeReference<T>, &Nodes<'_, T>)) {
        let nodes = Nodes::new(self.arena);
        let wrap_mask = self.arena.capacity() - 1;
        let mut visited = 0;

        for i in 0..self.arena.capacity() {
            let i = i & wrap_mask;
            if visited >= self.arena.node_count() {
                return;
            }
            let generation = unsafe { self.arena.generation_ref(i).as_ref().unwrap() };
            if generation.is_alive() {
                let node = TNodeReference::<T> {
                    index: NodeIndex {
                        index: i as u32,
                        generation: *generation,
                    },
                    phantom: Default::default(),
                };
                f(node, &nodes);
                visited += 1;
            }
        }
    }

    ///
    /// Iterate mutable over all of the nodes in this arena, calling the given functor for each
    /// valid node yielded
    ///
    pub fn for_each_mut(&mut self, mut f: impl FnMut(TNodeReference<T>, &mut NodesMut<'_, T>)) {
        let wrap_mask = self.arena.capacity() - 1;
        let mut visited = 0;

        for i in 0..self.arena.capacity() {
            let i = i & wrap_mask;
            if visited >= self.arena.node_count() {
                return;
            }
            let generation = unsafe { self.arena.generation_ref(i).as_ref().unwrap() };
            if generation.is_alive() {
                let node = TNodeReference::<T> {
                    index: NodeIndex {
                        index: i as u32,
                        generation: *generation,
                    },
                    phantom: Default::default(),
                };
                f(node, self);
                visited += 1;
            }
        }
    }
}

impl<'a, T: Node> Index<TNodeReference<T>> for NodesMut<'a, T> {
    type Output = T;

    fn index(&self, node: TNodeReference<T>) -> &Self::Output {
        if self.is_valid(node) {
            let node_ref;
            unsafe {
                let node_ptr = self.arena.node_ref(node.index.index as usize);
                let node_ptr = node_ptr as *const T;
                node_ref = node_ptr.as_ref().unwrap();
            }
            node_ref
        } else {
            panic!("Tried to index an invalid node");
        }
    }
}

impl<'a, T: Node> IndexMut<TNodeReference<T>> for NodesMut<'a, T> {
    fn index_mut(&mut self, node: TNodeReference<T>) -> &mut Self::Output {
        if self.is_valid(node) {
            let node_mut;
            unsafe {
                let node_ptr = self.arena.node_mut(node.index.index as usize);
                let node_ptr = node_ptr as *mut T;
                node_mut = node_ptr.as_mut().unwrap();
            }
            node_mut
        } else {
            panic!("Tried to index an invalid node");
        }
    }
}
