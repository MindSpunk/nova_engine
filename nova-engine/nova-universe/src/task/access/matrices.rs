/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::{Node, NodeReference, TNodeReference};
use nova_math::types::Mat4x4;
use std::any::TypeId;
use std::cell::UnsafeCell;
use std::collections::HashMap;
use std::marker::PhantomData;
use std::ops::{Index, IndexMut};

// =================================================================================================
///
/// Wrapper for immutable access to node transform matrices
///
pub struct MatrixGroup<'a> {
    matrices: *const HashMap<TypeId, UnsafeCell<Vec<Mat4x4>>>,
    phantom: PhantomData<&'a ()>,
}

impl<'a> MatrixGroup<'a> {
    ///
    ///
    ///
    pub(crate) fn new(matrices: *const HashMap<TypeId, UnsafeCell<Vec<Mat4x4>>>) -> Self {
        Self {
            matrices,
            phantom: Default::default(),
        }
    }

    ///
    ///
    ///
    pub fn for_type<T: Node>(&self) -> Matrices<'_, T> {
        let matrices = unsafe {
            let matrices = &*self.matrices;
            &*matrices.get(&TypeId::of::<T>()).unwrap().get()
        };
        Matrices::new(matrices)
    }
}

impl<'a> Index<NodeReference> for MatrixGroup<'a> {
    type Output = Mat4x4;

    fn index(&self, node: NodeReference) -> &Self::Output {
        unsafe {
            let vec = &*self.matrices;
            let vec = &*vec.get(&node.node_type()).unwrap().get();
            &vec[node.index.index as usize]
        }
    }
}

// =================================================================================================
///
/// Wrapper for immutable access to node transform matrices
///
pub struct MatrixGroupMut<'a> {
    pub(crate) matrices: *mut HashMap<TypeId, UnsafeCell<Vec<Mat4x4>>>,
    pub(crate) phantom: PhantomData<&'a mut ()>,
}

impl<'a> MatrixGroupMut<'a> {
    ///
    ///
    ///
    pub(crate) fn new(matrices: *mut HashMap<TypeId, UnsafeCell<Vec<Mat4x4>>>) -> Self {
        Self {
            matrices,
            phantom: Default::default(),
        }
    }

    ///
    ///
    ///
    pub fn for_type<T: Node>(&self) -> Matrices<'_, T> {
        let matrices = unsafe {
            let matrices = &*self.matrices;
            &*matrices.get(&TypeId::of::<T>()).unwrap().get()
        };
        Matrices::new(matrices)
    }

    ///
    ///
    ///
    pub fn for_type_mut<T: Node>(&mut self) -> MatricesMut<'_, T> {
        let matrices = unsafe {
            let matrices = &mut *self.matrices;
            &mut *matrices.get_mut(&TypeId::of::<T>()).unwrap().get()
        };
        MatricesMut::new(matrices)
    }
}

impl<'a, R: Into<NodeReference>> Index<R> for MatrixGroupMut<'a> {
    type Output = Mat4x4;

    fn index(&self, node: R) -> &Self::Output {
        unsafe {
            let node = node.into();
            let vec = &*self.matrices;
            let vec = &*vec.get(&node.node_type()).unwrap().get();
            &vec[node.index.index as usize]
        }
    }
}

impl<'a, R: Into<NodeReference>> IndexMut<R> for MatrixGroupMut<'a> {
    fn index_mut(&mut self, node: R) -> &mut Self::Output {
        unsafe {
            let node = node.into();
            let vec = &mut *self.matrices;
            let vec = &mut *vec.get(&node.node_type()).unwrap().get();
            &mut vec[node.index.index as usize]
        }
    }
}

// =================================================================================================
///
/// Wrapper for immutable access to the node transform matrices
///
pub struct Matrices<'a, T: Node> {
    matrices: &'a Vec<Mat4x4>,
    phantom: PhantomData<T>,
}

impl<'a, T: Node> Matrices<'a, T> {
    ///
    ///
    ///
    pub(crate) fn new(matrices: &'a Vec<Mat4x4>) -> Self {
        Matrices {
            matrices,
            phantom: Default::default(),
        }
    }
}

impl<'a, T: Node> Index<TNodeReference<T>> for Matrices<'a, T> {
    type Output = Mat4x4;

    fn index(&self, node: TNodeReference<T>) -> &Self::Output {
        &self.matrices[node.index.index as usize]
    }
}

// =================================================================================================
///
///
///
pub struct MatricesMut<'a, T: Node> {
    matrices: &'a mut Vec<Mat4x4>,
    phantom: PhantomData<T>,
}

impl<'a, T: Node> MatricesMut<'a, T> {
    ///
    ///
    ///
    pub(crate) fn new(matrices: &'a mut Vec<Mat4x4>) -> Self {
        MatricesMut {
            matrices,
            phantom: Default::default(),
        }
    }
}

impl<'a, T: Node> Index<TNodeReference<T>> for MatricesMut<'a, T> {
    type Output = Mat4x4;

    fn index(&self, node: TNodeReference<T>) -> &Self::Output {
        &self.matrices[node.index.index as usize]
    }
}

impl<'a, T: Node> IndexMut<TNodeReference<T>> for MatricesMut<'a, T> {
    fn index_mut(&mut self, node: TNodeReference<T>) -> &mut Self::Output {
        &mut self.matrices[node.index.index as usize]
    }
}
