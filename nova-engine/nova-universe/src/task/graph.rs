/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::arena::NodeArena;
use crate::task::{DataAccessState, DependencyInner};
use crate::{DataAccess, Dependencies, Dependency, Universe};
use crossbeam::atomic::AtomicCell;
use crossbeam::sync::WaitGroup;
use rayon::prelude::*;
use std::collections::hash_map::RandomState;
use std::collections::HashMap;
use std::sync::atomic::{AtomicBool, Ordering};

// =================================================================================================
///
///
///
pub struct TaskBox {
    inner: Box<dyn Fn(DataAccess) + Sync + Send>,
}

impl TaskBox {
    pub fn new(f: impl Fn(DataAccess) + 'static + Sync + Send) -> Self {
        Self { inner: Box::new(f) }
    }

    pub(crate) fn get_inner(&self) -> &Box<dyn Fn(DataAccess) + Sync + Send> {
        &self.inner
    }
}

// =================================================================================================
///
///
///
struct TaskNode {
    task: TaskBox,
    depends_on: Vec<usize>,
    depended_by: Vec<usize>,
}

// =================================================================================================
///
///
///
pub struct TaskGraph {
    tasks: Vec<TaskNode>,
    dependencies: Vec<Dependencies>,
    leaves: Vec<usize>,
}

impl TaskGraph {
    ///
    ///
    ///
    pub fn builder() -> TaskGraphBuilder {
        TaskGraphBuilder::new()
    }

    ///
    ///
    ///
    pub fn execute(&self, universe: &mut Universe) {
        struct WorkerPayload<'resources> {
            access: DataAccess<'resources>,
            _wg: WaitGroup,
        }

        let wg = WaitGroup::new();

        let done = {
            let mut done = Vec::with_capacity(self.tasks.len());
            for _ in 0..self.tasks.len() {
                done.push(AtomicBool::new(false));
            }
            done
        };

        let payloads: Vec<AtomicCell<Option<WorkerPayload>>> = {
            let mut payloads = Vec::with_capacity(self.tasks.len());
            for deps in self.dependencies.iter() {
                let access = Self::fill_access_state(deps, universe);
                let wg = wg.clone();
                let payload = WorkerPayload { access, _wg: wg };
                let payload = Some(payload);
                let payload = AtomicCell::new(payload);
                payloads.push(payload);
            }
            payloads
        };

        fn exec_task<'env>(
            task: usize,
            graph: &'env TaskGraph,
            done: &'env Vec<AtomicBool>,
            payloads: &'env Vec<AtomicCell<Option<WorkerPayload>>>,
        ) {
            let task_node = &graph.tasks[task];
            let payload = &payloads[task];
            if let Some(payload) = payload.swap(None) {
                let access = payload.access;
                let _wg = payload._wg;

                (*task_node.task.get_inner())(access);

                done[task].store(true, Ordering::SeqCst);

                task_node.depended_by.par_iter().for_each(|next| {
                    let mut can_run = true;
                    for parent in graph.tasks[*next].depends_on.iter() {
                        if !done[*parent].load(Ordering::SeqCst) {
                            can_run = false;
                            break;
                        }
                    }
                    if can_run {
                        let next = *next;
                        exec_task(next, graph, done, payloads);
                    }
                });
            }
        }

        self.leaves.par_iter().for_each(|task| {
            let task = *task;
            let done = &done;
            let payloads = &payloads;
            let graph = self;
            exec_task(task, graph, done, payloads);
        });

        wg.wait();
    }

    ///
    ///
    ///
    pub(crate) fn fill_access_state<'resources>(
        deps: &Dependencies,
        universe: &mut Universe<'resources>,
    ) -> DataAccess<'resources> {
        let mut state = DataAccessState {
            read_data: Default::default(),
            write_data: Default::default(),
            read_transforms: None,
            write_transforms: None,
            read_matrices: None,
            write_matrices: None,
            read_hierarchy: None,
            write_hierarchy: None,
            phantom: Default::default(),
        };

        let resources = &universe.resource_storage.boxes;

        for read in deps.reads.iter() {
            match &read.inner {
                DependencyInner::NodeType(t) => {
                    let storage = universe.node_storage.nodes_ref(*t);
                    state
                        .read_data
                        .insert(*read, storage as *const NodeArena as *const ());
                }
                DependencyInner::Resource(t) => {
                    let resource = resources.get(t).unwrap();

                    let resource = unsafe { resource.get().as_ref().unwrap() };

                    let resource = unsafe { resource.get_self_ptr() };

                    state.read_data.insert(*read, resource);
                }
                DependencyInner::Matrices => {
                    state.read_matrices = Some(&universe.node_storage.matrices as *const _);
                }
                DependencyInner::NodeHierarchy => {
                    state.read_hierarchy = Some(universe.get_node_hierarchy() as *const _);
                }
                DependencyInner::Transforms => {
                    state.read_transforms = Some(&universe.node_storage.transforms as *const _);
                }
                DependencyInner::Task(_) => {}
            }
        }

        for write in deps.writes.iter() {
            match &write.inner {
                DependencyInner::NodeType(t) => {
                    let storage = universe.node_storage.nodes_mut(*t);
                    state
                        .write_data
                        .insert(*write, storage as *mut NodeArena as *mut ());
                }
                DependencyInner::Resource(t) => {
                    let resource = resources.get(t).unwrap();
                    let resource = unsafe { resource.get().as_mut().unwrap() };
                    let resource = unsafe { resource.get_self_ptr_mut() };
                    state.write_data.insert(*write, resource);
                }
                DependencyInner::Matrices => {
                    state.write_matrices = Some(&mut universe.node_storage.matrices as *mut _);
                }
                DependencyInner::NodeHierarchy => {
                    state.write_hierarchy = Some(unsafe { universe.node_hierarchy_cell().get() });
                }
                DependencyInner::Transforms => {
                    state.write_transforms = Some(&mut universe.node_storage.transforms as *mut _);
                }
                DependencyInner::Task(_) => {}
            }
        }
        DataAccess::new(state)
    }
}

// =================================================================================================
///
///
///
pub struct TaskGraphBuilder {
    tasks: Vec<TaskBox>,
    dependencies: Vec<Dependencies>,
}

impl TaskGraphBuilder {
    ///
    ///
    ///
    pub fn new() -> Self {
        TaskGraphBuilder {
            tasks: Vec::with_capacity(64),
            dependencies: Vec::with_capacity(64),
        }
    }

    ///
    ///
    ///
    pub fn add_task(&mut self, task: TaskBox, func: impl Fn(&mut Dependencies)) -> Dependency {
        let mut dependencies = Dependencies {
            reads: Vec::new(),
            writes: Vec::new(),
        };

        (func)(&mut dependencies);

        let task_id = self.tasks.len();

        let dependable = unsafe { Dependency::new_task(task_id) };
        dependencies.write(dependable);

        self.tasks.push(task);
        self.dependencies.push(dependencies);

        dependable
    }

    ///
    ///
    ///
    pub fn build(self) -> TaskGraph {
        let mut tasks: Vec<TaskNode> = self
            .tasks
            .into_iter()
            .map(|v| TaskNode {
                task: v,
                depends_on: Vec::with_capacity(4),
                depended_by: Vec::with_capacity(4),
            })
            .collect();
        let dependencies = self.dependencies;

        let mut last_write: HashMap<Dependency, usize> = HashMap::new();
        let mut last_reads: HashMap<Dependency, Vec<usize>> = HashMap::new();

        for task_num in 0..tasks.len() {
            let task_deps = &dependencies[task_num];
            Self::handle_writes(
                &mut tasks,
                &mut last_write,
                &mut last_reads,
                task_num,
                task_deps,
            );

            Self::handle_reads(
                &mut tasks,
                &mut last_write,
                &mut last_reads,
                task_num,
                task_deps,
            );
        }

        let mut leaves = Vec::with_capacity(8);

        for (i, task_node) in tasks.iter().enumerate() {
            if task_node.depends_on.is_empty() {
                leaves.push(i);
            }
        }

        TaskGraph {
            tasks,
            dependencies,
            leaves,
        }
    }

    ///
    ///
    ///
    fn handle_writes(
        tasks: &mut Vec<TaskNode>,
        last_write: &mut HashMap<Dependency, usize, RandomState>,
        last_reads: &mut HashMap<Dependency, Vec<usize>, RandomState>,
        task_num: usize,
        task_deps: &Dependencies,
    ) {
        for write in task_deps.writes.iter() {
            last_write.insert(write.clone(), task_num);

            match last_reads.get_mut(write) {
                None => {}
                Some(reads) => {
                    for read in reads.iter() {
                        if *read != task_num {
                            if !tasks[task_num].depends_on.contains(read) {
                                tasks[task_num].depends_on.push(*read);
                            }
                            if !tasks[*read].depended_by.contains(&task_num) {
                                tasks[*read].depended_by.push(task_num);
                            }
                        }
                    }

                    reads.clear();
                }
            }
        }
    }

    ///
    ///
    ///
    fn handle_reads(
        tasks: &mut Vec<TaskNode>,
        last_write: &mut HashMap<Dependency, usize, RandomState>,
        last_reads: &mut HashMap<Dependency, Vec<usize>, RandomState>,
        task_num: usize,
        task_deps: &Dependencies,
    ) {
        for read in task_deps.reads.iter() {
            match last_reads.get_mut(read) {
                None => {
                    let mut vec = Vec::with_capacity(4);
                    vec.push(task_num);
                    last_reads.insert(read.clone(), vec);
                }
                Some(vec) => {
                    vec.push(task_num);
                }
            }

            match last_write.get(&read) {
                None => {}
                Some(write) => {
                    if *write != task_num {
                        if !tasks[task_num].depends_on.contains(write) {
                            tasks[task_num].depends_on.push(*write);
                        }
                        if !tasks[*write].depended_by.contains(&task_num) {
                            tasks[*write].depended_by.push(task_num);
                        }
                    }
                }
            }
        }
    }
}
