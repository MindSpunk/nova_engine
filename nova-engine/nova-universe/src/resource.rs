/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use std::alloc::Layout;
use std::marker::PhantomData;

///
/// We need a unique ID for each resource
///
pub trait Resource: Send + Sync {
    fn resource_name_id() -> &'static str;
}

///
/// Macro for generating a unique ID for a Resource struct
///
#[macro_export]
macro_rules! generate_resource_name_id {
    () => {
        concat!(
            env!("CARGO_PKG_AUTHORS"),
            ";;",
            env!("CARGO_PKG_VERSION"),
            ";;",
            module_path!(),
            "::",
            stringify!($node_type)
        )
    };
}

///
/// Macro for implementing the Resource trait.
///
/// # Parameters
///
/// 1. The type to implement Node for
///
#[macro_export]
macro_rules! impl_resource {
    ($resource_type: ident) => {
        impl $crate::Resource for $resource_type {
            fn resource_name_id() -> &'static str {
                concat!(
                    env!("CARGO_PKG_AUTHORS"),
                    ";;",
                    env!("CARGO_PKG_VERSION"),
                    ";;",
                    module_path!(),
                    "::",
                    stringify!($node_type)
                )
            }
        }
    };
}

pub struct ResourceBox<'resources> {
    ptr: *mut u8,
    drop_fn: unsafe fn(*mut u8),
    phantom: PhantomData<&'resources mut ()>,
}

impl<'resources> Drop for ResourceBox<'resources> {
    fn drop(&mut self) {
        unsafe {
            (self.drop_fn)(self.ptr);
        }
    }
}

impl<'resources> ResourceBox<'resources> {
    ///
    ///
    ///
    pub fn new<T: Resource + 'resources>(v: T) -> Self {
        let ptr = unsafe { std::alloc::alloc(Layout::for_value(&v)) };
        unsafe { (ptr as *mut T).write(v) };
        Self {
            ptr,
            drop_fn: Self::drop_func::<T>,
            phantom: Default::default(),
        }
    }

    ///
    /// Get a pointer to the data
    ///
    pub unsafe fn get_self_ptr(&self) -> *const () {
        self.ptr as *const ()
    }

    ///
    /// Get a mutable pointer to data
    ///
    pub unsafe fn get_self_ptr_mut(&mut self) -> *mut () {
        self.ptr as *mut ()
    }

    ///
    /// Wrapper for dropping under type erasure
    ///
    #[inline(never)]
    unsafe fn drop_func<T>(v: *mut u8) {
        let v = v as *mut T;
        let layout = Layout::for_value(&*v);
        core::ptr::drop_in_place(v);
        std::alloc::dealloc(v as *mut u8, layout);
    }
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug, Hash)]
pub struct ResourceID {
    id: &'static str,
}

impl ResourceID {
    pub fn of<T: Resource>() -> Self {
        Self {
            id: T::resource_name_id(),
        }
    }
}
