/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use std::any::TypeId;

use crate::arena::NodeIndex;
use crate::impl_node;
use crate::Node;
use std::hash::{Hash, Hasher};
use std::marker::PhantomData;

// =================================================================================================
///
/// A dummy node used for being able to resolve the type of a NodeArenaBox, never try and use this
///
#[derive(Copy, Clone)]
pub struct NullNode();

impl_node!(NullNode);

// =================================================================================================
///
/// Node type and index pair
///
#[derive(Copy, Clone, Hash, PartialEq, Eq)]
pub struct NodeReference {
    type_id: TypeId,
    pub(crate) index: NodeIndex,
}

impl NodeReference {
    ///
    /// A null reference
    ///
    #[inline]
    pub fn null() -> NodeReference {
        Self {
            type_id: TypeId::of::<NullNode>(),
            index: Default::default(),
        }
    }

    ///
    /// Is the reference a null reference
    ///
    #[inline]
    pub fn is_null(&self) -> bool {
        self.type_id == TypeId::of::<NullNode>() && self.index.generation.is_dead()
    }

    ///
    /// Get the TypeId of the node this reference refers to
    ///
    #[inline]
    pub fn node_type(&self) -> TypeId {
        self.type_id
    }

    ///
    /// Convert this to a typed node reference, if we provide the correct type
    ///
    #[inline]
    pub fn to_typed<T: Node>(self) -> Option<TNodeReference<T>> {
        if TypeId::of::<T>() == self.type_id {
            Some(TNodeReference::<T> {
                index: self.index,
                phantom: Default::default(),
            })
        } else {
            None
        }
    }
}

// =================================================================================================
///
/// A typed node reference
///
#[derive(Clone, PartialEq, Eq)]
pub struct TNodeReference<T: Node> {
    pub(crate) index: NodeIndex,
    pub(crate) phantom: PhantomData<T>,
}

impl<T: Node> Copy for TNodeReference<T> {}

impl<T: Node> TNodeReference<T> {
    ///
    ///
    ///
    #[inline]
    pub fn to_untyped(self) -> NodeReference {
        NodeReference {
            type_id: TypeId::of::<T>(),
            index: self.index,
        }
    }
}

impl<T: Node> Hash for TNodeReference<T> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.to_untyped().hash(state)
    }
}

impl<T: Node> Into<NodeReference> for TNodeReference<T> {
    fn into(self) -> NodeReference {
        self.to_untyped()
    }
}
