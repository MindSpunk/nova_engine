/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::arena::Generation;
use crate::Node;
use std::alloc::Layout;
use std::mem::{align_of, size_of};

// =================================================================================================
///
///
///
#[derive(Clone)]
pub struct NodeArena {
    node_layout: Layout,
    nodes: *mut u8,
    generations: *mut Generation,
    capacity: usize,
    drop_fn: unsafe fn(*mut u8),
    node_head: usize,
    node_count: usize,
}

impl NodeArena {
    ///
    /// Create a new NodeArena
    ///
    pub fn new<T: Node>(capacity: usize) -> Option<Self> {
        if !capacity.is_power_of_two() {
            return None;
        }

        unsafe {
            // Calculate the size of the nodes buffer
            let size = size_of::<T>();
            let align = align_of::<T>();
            let node_layout = Layout::from_size_align_unchecked(size, align);
            let nodes_layout = Self::nodes_layout(node_layout, capacity);

            // Allocate the nodes buffer
            let nodes = std::alloc::alloc(nodes_layout);

            // Calculate the size of the generations buffer
            let size = size_of::<Generation>() * capacity;
            let align = align_of::<Generation>();
            let generations_layout = Layout::from_size_align_unchecked(size, align);

            // Allocate the generations buffer
            let generations = std::alloc::alloc(generations_layout) as *mut Generation;

            // Default initialize the generations buffer
            for i in 0..capacity {
                generations.add(i).write(Generation::default());
            }

            Some(Self {
                node_layout,
                nodes,
                generations,
                capacity,
                drop_fn: Self::drop_fn::<T>,
                node_head: 0,
                node_count: 0,
            })
        }
    }

    unsafe fn drop_fn<T: Node>(ptr: *mut u8) {
        let ptr = ptr as *mut T;
        let node = ptr.read();
        drop(node);
    }

    #[inline]
    unsafe fn nodes_layout(node_layout: Layout, capacity: usize) -> Layout {
        Layout::from_size_align_unchecked(node_layout.size() * capacity, node_layout.align())
    }

    #[inline]
    unsafe fn generations_layout(capacity: usize) -> Layout {
        Layout::from_size_align_unchecked(
            size_of::<Generation>() * capacity,
            align_of::<Generation>(),
        )
    }

    pub unsafe fn allocate(&mut self, src: *const u8) -> usize {
        // Reallocate the buffer if there's no space for more nodes
        if self.node_count == self.capacity {
            self.expand();
        }

        // Search the buffer for a free space
        let wrap_mask = self.capacity - 1;
        for i in self.node_head..self.node_head + self.capacity {
            let i = i & wrap_mask;

            let generation = self.generations.add(i).as_mut().unwrap();
            if generation.is_dead() {
                generation.resurrect();

                self.node_count += 1;
                self.node_head = (i + 1) & wrap_mask;

                self.nodes
                    .add(i)
                    .copy_from_nonoverlapping(src, self.node_layout.size());

                return i;
            }
        }
        panic!(
            "Reached bad code: {}, {}, {:?}",
            self.capacity, self.node_count, self.node_layout
        )
    }

    pub unsafe fn erase(&mut self, index: usize) {
        // Kill generation
        let generation = self.generations.add(index).as_mut().unwrap();
        generation.kill();

        // Drop node
        let node = self.nodes.add(index * self.node_layout.size());
        (self.drop_fn)(node);

        self.node_count -= 1;
    }

    pub unsafe fn remove(&mut self, index: usize, out: *mut u8) {
        // Kill generation
        let generation = self.generations.add(index).as_mut().unwrap();
        generation.kill();

        // Copy out node
        let node = self.nodes.add(index * self.node_layout.size());
        out.copy_from_nonoverlapping(node, self.node_layout.size());

        self.node_count -= 1;
    }

    pub unsafe fn generation_ref(&self, index: usize) -> *const Generation {
        self.generations.add(index) as *const Generation
    }

    //pub unsafe fn generation_mut(&mut self, index: usize) -> *mut Generation {
    //    self.generations.add(index)
    //}

    pub unsafe fn node_ref(&self, index: usize) -> *const u8 {
        self.nodes.add(index * self.node_layout.size()) as *const u8
    }

    pub unsafe fn node_mut(&mut self, index: usize) -> *mut u8 {
        self.nodes.add(index * self.node_layout.size())
    }

    pub fn capacity(&self) -> usize {
        self.capacity
    }

    pub fn node_count(&self) -> usize {
        self.node_count
    }

    // Doubles the size of the backing buffers
    fn expand(&mut self) {
        unsafe {
            // Get double the current size
            let new_len = self.capacity * 2;

            // Allocate a new buffer for the nodes
            let new_nodes_layout = Self::nodes_layout(self.node_layout, new_len);
            let new_nodes = std::alloc::alloc(new_nodes_layout);

            // Allocate a new buffer for the generations
            let new_generations_layout = Self::generations_layout(new_len);
            let new_generations = std::alloc::alloc(new_generations_layout) as *mut Generation;

            // Copy data to new buffers
            self.nodes
                .copy_to_nonoverlapping(new_nodes, self.node_layout.size() * self.capacity);
            self.generations
                .copy_to_nonoverlapping(new_generations, self.capacity);

            // Initialize generations as the second half of the buffer will be uninitialized memory
            for i in self.capacity..self.capacity * 2 {
                new_generations.add(i).write(Generation::default());
            }

            // Deallocate old node buffer
            let old_nodes_layout = Self::nodes_layout(self.node_layout, self.capacity);
            std::alloc::dealloc(self.nodes, old_nodes_layout);

            // Deallocate old generation buffer
            let old_generations_layout = Self::generations_layout(self.capacity);
            std::alloc::dealloc(self.generations as *mut u8, old_generations_layout);

            self.nodes = new_nodes;
            self.generations = new_generations;
            self.node_head = self.capacity;
            self.capacity = new_len;
        }
    }
}

impl Drop for NodeArena {
    fn drop(&mut self) {
        unsafe {
            for i in 0..self.capacity {
                let generation = self.generations.add(i).as_ref().unwrap();
                if generation.is_alive() {
                    let node = self.nodes.add(i * self.node_layout.size());
                    (self.drop_fn)(node);
                }
            }
        }
    }
}
