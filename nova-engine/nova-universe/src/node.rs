/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::NodeReference;

// =================================================================================================
///
/// A trait that must be implemented by all node types
///
pub trait Node: Clone + Send + Sync + 'static {
    ///
    /// Gets a string name for this node type, primary use being serialization. Should not be UI
    /// facing. Must be unique so you should add a personal prefix to the name to make sure the
    /// name doesn't collide
    ///
    fn node_name() -> &'static str;

    ///
    /// Gets a string name for this node type, primary for pretty printing and UI usage
    ///
    fn pretty_node_name() -> &'static str;
}

///
/// Macro for implementing the Node trait.
///
/// # Parameters
///
/// 1. The type to implement Node for
///
#[macro_export]
macro_rules! impl_node {
    ($node_type: ident) => {
        impl $crate::Node for $node_type {
            fn node_name() -> &'static str {
                concat!(module_path!(), "::", stringify!($node_type))
            }

            fn pretty_node_name() -> &'static str {
                concat!(module_path!(), "::", stringify!($node_type))
            }
        }
    };
}

///
/// Macro for implementing the Node trait.
///
/// # Parameters
///
/// 1. The type to implement Node for
/// 2. A prefix to be used in the node_name
/// 3. A pretty name to be used for the node for things like UI
///
#[macro_export]
macro_rules! impl_node_pretty {
    ($node_type: ident, $pretty_name: expr) => {
        impl $crate::Node for $node_type {
            fn node_name() -> &'static str {
                concat!(module_path!(), "::", stringify!($node_type))
            }

            fn pretty_node_name() -> &'static str {
                $pretty_name
            }
        }
    };
}

// =================================================================================================
///
/// An element in the node graph
///
#[derive(Clone)]
pub struct NodeItem {
    pub parent: NodeReference,
    pub children: Vec<NodeReference>,
}
