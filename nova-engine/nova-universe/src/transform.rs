/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use nova_math::types::{Mat4x4, Quat, Vec3};

///
/// The position, rotation and scale of a node
///
#[derive(Clone, Debug)]
pub struct Transform3D {
    pub pos: Vec3,
    pub rot: Quat,
    pub scl: Vec3,
}

impl Transform3D {
    ///
    /// Create a new Transform3D from the given parts
    ///
    pub fn new(pos: Vec3, rot: Quat, scl: Vec3) -> Self {
        Self { pos, rot, scl }
    }
    ///
    /// Calculate the model matrix from the transform
    ///
    pub fn model_matrix(&self) -> Mat4x4 {
        let rotation: Mat4x4 = self.rot.into();
        let mut matrix = Mat4x4::identity();
        matrix.translate(self.pos);
        matrix.scale(self.scl);
        matrix *= rotation;
        matrix
    }

    ///
    /// Get the forward vector
    ///
    pub fn get_forward(&self) -> Vec3 {
        self.rot * Vec3::forward()
    }

    ///
    /// Get the up vector
    ///
    pub fn get_up(&self) -> Vec3 {
        self.rot * Vec3::up()
    }

    ///
    /// Get the right vector
    ///
    pub fn get_right(&self) -> Vec3 {
        self.rot * Vec3::right()
    }
}

impl Default for Transform3D {
    fn default() -> Self {
        Self {
            pos: Vec3::zero(),
            rot: Quat::identity(),
            scl: Vec3::one(),
        }
    }
}
