/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::impl_node;
use crate::{Dependency, TaskBox, TaskGraphBuilder, Universe};

#[derive(Clone)]
struct TestNode1 {
    val: String,
}

impl TestNode1 {
    fn new(val: String) -> Self {
        Self { val }
    }
}

impl_node!(TestNode1);

#[test]
pub fn graph_test_01() {
    let mut universe = Universe::new();
    let mut graph = TaskGraphBuilder::new();

    let task = TaskBox::new(|_| {
        println!("Task0");
    });
    let task_0 = graph.add_task(task, |_| {});

    // ==

    let task = TaskBox::new(|_| {
        println!("Task2");
    });
    let task_2 = graph.add_task(task, |v| {
        v.read(task_0);
    });

    let task = TaskBox::new(|_| {
        println!("Task1");
    });
    let task_1 = graph.add_task(task, |v| {
        v.read(task_0);
    });

    // ==

    let task = TaskBox::new(|_| {
        println!("Task3");
    });
    let task_3 = graph.add_task(task, |v| {
        v.read(task_1);
        v.read(task_2);
    });

    // ==

    let task = TaskBox::new(|_| {
        println!("Task5");
    });
    let _task_5 = graph.add_task(task, |v| {
        v.read(task_3);
    });

    let task = TaskBox::new(|_| {
        println!("Task4");
    });
    let _task_4 = graph.add_task(task, |v| {
        v.read(task_3);
    });

    // ==

    let graph = graph.build();
    graph.execute(&mut universe);
}

#[test]
pub fn graph_test_02() {
    let mut universe = Universe::new();
    universe.register_node_type::<TestNode1>(4);

    universe
        .node_builder(TestNode1::new("Node1".into()))
        .build()
        .unwrap();
    universe
        .node_builder(TestNode1::new("Node2".into()))
        .build()
        .unwrap();
    universe
        .node_builder(TestNode1::new("Node3".into()))
        .build()
        .unwrap();
    universe
        .node_builder(TestNode1::new("Node4".into()))
        .build()
        .unwrap();

    let mut graph = TaskGraphBuilder::new();

    let task = TaskBox::new(|access| {
        let mut nodes = access.get_nodes_mut::<TestNode1>().unwrap();

        nodes.for_each_mut(|node, nodes| {
            let n = &mut nodes[node];
            println!("Task0: {}", &n.val);
            n.val.push_str("A");
        });
    });
    graph.add_task(task, |deps| {
        deps.write(Dependency::node::<TestNode1>());
    });

    // ==

    let task = TaskBox::new(|access| {
        let nodes = access.get_nodes_ref::<TestNode1>().unwrap();

        nodes.for_each(|node, nodes| {
            let n = &nodes[node];
            println!("Task2: {}", &n.val);
        });
    });
    graph.add_task(task, |deps| {
        deps.read(Dependency::node::<TestNode1>());
    });

    let task = TaskBox::new(|access| {
        let nodes = access.get_nodes_ref::<TestNode1>().unwrap();

        nodes.for_each(|node, nodes| {
            let n = &nodes[node];
            println!("Task1: {}", &n.val);
        });
    });
    graph.add_task(task, |deps| {
        deps.read(Dependency::node::<TestNode1>());
    });

    // ==

    let task = TaskBox::new(|access| {
        let mut nodes = access.get_nodes_mut::<TestNode1>().unwrap();

        nodes.for_each_mut(|node, nodes| {
            let n = &mut nodes[node];
            println!("Task3: {}", &n.val);
            n.val.push_str("B");
        });
    });
    graph.add_task(task, |deps| {
        deps.write(Dependency::node::<TestNode1>());
    });

    // ==

    let task = TaskBox::new(|access| {
        let nodes = access.get_nodes_ref::<TestNode1>().unwrap();

        nodes.for_each(|node, nodes| {
            let n = &nodes[node];
            println!("Task5: {}", &n.val);
        });
    });
    graph.add_task(task, |deps| {
        deps.read(Dependency::node::<TestNode1>());
    });

    let task = TaskBox::new(|access| {
        let nodes = access.get_nodes_ref::<TestNode1>().unwrap();

        nodes.for_each(|node, nodes| {
            let n = &nodes[node];
            println!("Task4: {}", &n.val);
        });
    });
    graph.add_task(task, |deps| {
        deps.read(Dependency::node::<TestNode1>());
    });

    // ==

    let graph = graph.build();
    graph.execute(&mut universe);
}
