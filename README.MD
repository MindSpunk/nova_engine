# NovaEngine

NovaEngine is a cross-platform PC Rust/WebAssembly based 3D game engine.

[![Build status](https://dev.azure.com/nathanvoglsam420/NovaEngine/_apis/build/status/NovaEngine-CI)](https://dev.azure.com/nathanvoglsam420/NovaEngine/_build/latest?definitionId=1)

1. [Information](#information)
    - [Supported Platforms](#supported-platforms)
    - [Dependencies](#dependencies)
        - [Windows](#windows)
        - [Linux](#linux)
2. [Samples](#samples-HOME)
3. [Downloads](#downloads)
4. [Compilation Guide](#compilation-guide)
    - [Windows MSVC](#windows-msvc)
    - [Windows GNU](#windows-gnu)
    - [Linux](#linux)
5. [Design Goals](#design-goals)
6. [License](#license)

## Information

> Currently NovaEngine in very early stages of development, most of the features described are goals and not currently implemented

NovaEngine is a cross-platform 3D game engine with a Rust based runtime and a WebAssembly based scripting interface.

NovaEngine aims to provide a nice Rust API for interfacing with the engine at a low level where performance is key, while for simple scripting a general purpose WebAssembly engine will be provided to allow for scripts to be run without engine recompilation. A WebAssembly and simple scripting interface will be provided by default to allow for easy integration with nova_engine's ECS. Using WebAssembly for scripting allows for many languages to target nova_engine, as long as they can compile to WASM.

NovaEngine is designed to make routine tasks simple, so building is just a `cargo build` and an export is just `cargo run -p nova-install`.

### Supported Platforms

NovaEngine exports to, or plans to target:

- ✅ Windows x64 (MSVC, MinGW)
- ✅ Linux x64

There are currently no plans to support MacOS. If Apple ever decides to take game developers seriously and provide industry standard tools such as Vulkan or not terrible (and now depreceated) OpenGL drivers then I will consider supporting MacOS. Otherwise I will waste no time supporting a company that does not support it's own developers.

### Dependencies

#### Windows

- ✅ [Git for Windows](https://git-scm.com/)
- ✅ [Git LFS for Windows](https://git-lfs.github.com/)
- ✅ [CMake](https://cmake.org/)
- ✅ [Ninja](https://github.com/ninja-build/ninja/releases)
- ✅ [Clang/LLVM](http://releases.llvm.org/7.0.0/LLVM-7.0.0-win64.exe)
- ✅ [MinGW-W64](https://sourceforge.net/projects/mingw-w64/)
- ✅ [Visual Studio 2017](https://visualstudio.microsoft.com/) with C++ and Game Development Workloads
- ✅ [Steam Audio](https://valvesoftware.github.io/steam-audio/)
- ✅ [VulkanSDK](https://vulkan.lunarg.com/)

#### Linux

- ✅ Git
- ✅ Git LFS
- ✅ CMake
- ✅ Ninja
- ✅ libsdl2
- ✅ libclang/llvm
- ✅ libgtk-3-dev
- ✅ [Steam Audio](https://valvesoftware.github.io/steam-audio/)
- ✅ [VulkanSDK](https://vulkan.lunarg.com/)

### Samples

> TODO: When API Stable Write Samples

### Downloads

> NovaEngine is in very early stages of development so the only thing you're really downloading is some compiled binaries of the tools + the current Test "game".

There are two methods of installing NovaEngine. The preferred method, building from source, will be described in the [Compilation Guide](#compilation-guide) section.

For installing a binary build, download the artifacts for the latest build from the CI setup and extract the archive.

- [Linux x64](https://gitlab.com/MindSpunk/nova_engine/-/jobs/artifacts/nova-engine-0.3/download?job=nova_engine)
- [Windows x64](https://ci.appveyor.com/project/Nathan49136/novaengine/build/artifacts)

## Compilation Guide

> NovaEngine is in very early stages of development so this may change. Ideally this shouldn't change much but if it does it will most likely be a breaking change.

NovaEngine builds entirely within cargo, and only uses cmake internally to compile C and C++ dependencies. You should be able to just install the required software and call `cargo build` and be off to the races.

### Windows MSVC

First we need to install all the dependencies:

- Install Clang/LLVM from [here](http://releases.llvm.org/7.0.0/LLVM-7.0.0-win64.exe)
- Install VulkanSDK from [here](https://vulkan.lunarg.com/)
- Download the Ninja build system and make sure ninja.exe is in the PATH or similarly accessible by the build scripts
- Install Visual Studio 2017 and C++ Workload

Now all the dependencies are installed.

- Execute:

```bash
git lfs clone https://gitlab.com/MindSpunk/nova_engine.git --recurse-submodules
cd nova_engine/nova-engine
cargo build
```

## Windows GNU

First we need to install all the dependencies:

- Install Clang/LLVM from [here](http://releases.llvm.org/7.0.0/LLVM-7.0.0-win64.exe)
- Install VulkanSDK from [here](https://vulkan.lunarg.com/)
- Install MinGW-w64 from [here](https://sourceforge.net/projects/mingw-w64/)
- Download the Ninja build system and make sure ninja.exe is in the PATH or similarly accessible by the build scripts
- Open an MinGW-w64 prompt (can use open_cmd.bat in nova-engine folder if MinGW installed to default location)

Now all the dependencies are installed.

- Execute:

```bash
git lfs clone https://gitlab.com/MindSpunk/nova_engine.git --recurse-submodules
cd nova_engine/nova-engine
cargo build
```

### Linux

Instructions for installing dependencies will contain the Ubuntu package name in brackets i.e (libx11-dev)

First we need to install all the dependencies:

- Install VulkanSDK from [here](https://vulkan.lunarg.com/)
  - Make sure to add `source <vulkan_sdk_dir>/setup-env.sh` to your ~/.bash_rc and other similar configuration files (especially your desktop shell if you want IDEs to work)
- Install LLVM and libclang from your package manager (llvm libclang1 libclang-dev)
- Install the Ninja build system (ninja-build)
- Install GTK3 libraries (libgtk-3-dev)
- Install SDL2 dev libraries from your package manager (libsdl2-dev)

Now all the dependencies are installed.

```bash
git lfs clone https://gitlab.com/MindSpunk/NovaEngine.git --recurse-submodules
cd nova_engine/nova-engine
cargo build
```

## Design Goals

> TODO: Writeup of Engine Design Goals

## License

nova_engine is licensed under the MIT license.